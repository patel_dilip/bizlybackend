const mongoose = require('mongoose')
const schema = mongoose.Schema

const chainCategorySchema = new schema(
    {
        rootCategoryName: { type: String },
        categories: [{
            categoryName: { type: String },
        }],
        createdAt: { type: Date, default: Date.now },
        updatedAt: { type: Date, default: Date.now },
        addedBy: { type: schema.Types.ObjectId, ref: 'admins' },
        updatedBy: { type: schema.Types.ObjectId, ref: 'admins' }
    }
)
 
module.exports = {
    category: mongoose.model('chaincategory', chainCategorySchema)
}
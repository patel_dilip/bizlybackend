const mongoose = require('mongoose')
const schema = mongoose.Schema

const chainSchema = new schema(
    {
        chain_info: {
            chain_code: String,
            display_name: String,
            legal_name: String
        },
        media_info: {
            chain_logo: String,
            chain_pictures: [],   // array of files
            chain_videos: []     // array of files
        },
        outlets: {
            outlets_tree: []
        },
        establishment_info: {
            establish_moreinfo: [],
            establish_services: [],
            establish_rules: [],
            establish_ambience: [],
            establish_cuisines: [],
        },
        addedBy: { type: schema.Types.ObjectId, ref: 'admins' },
        createdAt: { type: Date, default: Date.now },
        updatedAt: { type: Date, default: Date.now },
        updatedBy: { type: schema.Types.ObjectId, ref: 'admins' }
    }
)

module.exports = {
    chainModel: mongoose.model('chain', chainSchema)
}
const mongoose = require('mongoose')
const schema = mongoose.Schema

const charges_product_schema = new schema(
    {
        productName: { type: String },
        description: { type: String },
        images: [],
        videos: [],
        completeData: [],
        status: { type: Boolean, default: true },
        addedBy: { type: schema.Types.ObjectId, ref: 'admins' },
        createdAt: { type: Date, default: Date.now },
        updatedAt: { type: Date, default: Date.now },
        updatedBy: { type: schema.Types.ObjectId, ref: 'admins' }
    }
)

module.exports = {
    charges_product_model: mongoose.model('chargesproduct', charges_product_schema)
}
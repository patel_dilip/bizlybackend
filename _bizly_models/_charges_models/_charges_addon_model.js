const mongoose = require('mongoose')
const schema = mongoose.Schema

const charges_addon_schema = new schema(
    {
        AddonsName: { type: String },
        // productAssociated: { type: String },
        description: { type: String },
        images: [],
        videos: [],
        completeData: [],
        status: { type: Boolean, default: true },
        addedBy: { type: schema.Types.ObjectId, ref: 'admins' },
        createdAt: { type: Date, default: Date.now },
        updatedAt: { type: Date, default: Date.now },
        updatedBy: { type: schema.Types.ObjectId, ref: 'admins' }
    }
)

module.exports = {
    charges_addon_model: mongoose.model('chargesaddon', charges_addon_schema)
}
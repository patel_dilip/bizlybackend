const mongoose = require("mongoose");
const schema = mongoose.Schema;

const charges_integration_schema = new schema({
  integrationName: { type: String },
  // productAssociated: { type: String },
  addOnsAssociated: { },
  TypeOfIntegration: { type: String },
  description: { type: String },
  images: [],
  videos: [],
  completeData: [],
  status: { type: Boolean, default: true },
  addedBy: { type: schema.Types.ObjectId, ref: "admins" },
  createdAt: { type: Date, default: Date.now },
  updatedAt: { type: Date, default: Date.now },
  updatedBy: { type: schema.Types.ObjectId, ref: "admins" },
});

module.exports = {
  charges_integration_model: mongoose.model(
    "chargesintegration",
    charges_integration_schema
  ),
};

const mongoose = require('mongoose')
const schema = mongoose.Schema

const assignTemplateSchema = new schema(
    {
        templateid: String,
        templateName: String,
        assignPOS: Boolean,
        assignCustomer: Boolean,
        createdAt: { type: Date, default: Date.now },
        updatedAt: { type: Date, default: Date.now() },
        addedBy: { type: schema.Types.ObjectId, ref: 'admins' },
        updatedBy: { type: schema.Types.ObjectId, ref: 'admins' }
    }
)

module.exports = {
    template: mongoose.model('assigntemplate', assignTemplateSchema)
}
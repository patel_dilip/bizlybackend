const mongoose = require('mongoose')
const schema = mongoose.Schema
const autoIncrement = require('mongoose-auto-increment');

const templateSchema = new schema(
    {
        templateCode: String,
        templateName: String,
        templateDescription: String,
        selectFilter: String,
        platForm: String,
        templateCreatedFor: String,
        categoryType: [],
        selectedQuestions: [{ type: schema.Types.ObjectId, ref: 'feedback' }],
        selectedQuestionsSet: [{ type: schema.Types.ObjectId ,ref:'questionset'}],
        assignPOS: { type: Boolean, default: false },
        assignCustomer: { type: Boolean, default: false },
        status: { type: Boolean, default: true },
        addedBy: { type: schema.Types.ObjectId, ref: 'admins' },
        createdAt: { type: Date, default: Date.now },
        updatedAt: { type: Date, default: Date.now },
        updatedBy: { type: schema.Types.ObjectId, ref: 'admins' }
    }
)

autoIncrement.initialize(mongoose);
templateSchema.plugin(autoIncrement.plugin, {
    model: 'template',
    field: 'templateCode',
    startAt: 1,
    incrementBy: 1
});

module.exports = {
    template: mongoose.model('template', templateSchema)
}
const mongoose = require('mongoose')
const schema = mongoose.Schema
const autoIncrement = require('mongoose-auto-increment');

const questionSetSchema = new schema(
    {
        questionSetCode: String,
        questionSetName: String,
        questionSetCreatedFor: String,
        selectedQuestions: [{ type: schema.Types.ObjectId ,ref:'feedback'}],
        categoryType: [],
        status: { type: Boolean, default: true },
        addedBy: { type: schema.Types.ObjectId, ref: 'admins' },
        createdAt: { type: Date, default: Date.now },
        updatedAt: { type: Date, default: Date.now },
        updatedBy: { type: schema.Types.ObjectId, ref: 'admins' }
    }
)

autoIncrement.initialize(mongoose);
questionSetSchema.plugin(autoIncrement.plugin, {
    model: 'questionset',
    field: 'questionSetCode',
    startAt: 1,
    incrementBy: 1
});

module.exports = {
    questionSet: mongoose.model('questionset', questionSetSchema)
}
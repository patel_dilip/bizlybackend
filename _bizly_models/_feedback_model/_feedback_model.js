const mongoose = require('mongoose')
const schema = mongoose.Schema
const autoIncrement = require('mongoose-auto-increment');

const feedbackSchema = new schema(
    {
        feedbackCode: String,
        label_Name: String,
        inputType: String,
        platForm: String,
        commentbox: String,
        YesNoButtons: Boolean,
        dropDown: [],
        radioButtons: [],
        radioChoices: [],
        multiCheckboxes: [],
        multiCheckboxesImage: [],
        slider: Number,
        starRating: Number,
        scale: Number,
        dateTime: String,
        categoryType: [],
        feedbackQuestionCreatedFor: String,
        matrixRadioButton: {
            rowMatrixiButton: [],
            columnMatrixiButton: []
        },
        status: { type: Boolean, default: true },
        addedBy: { type: schema.Types.ObjectId, ref: 'admins' },
        createdAt: { type: Date, default: Date.now },
        updatedAt: { type: Date, default: Date.now },
        updatedBy: { type: schema.Types.ObjectId, ref: 'admins' }
    }
)

autoIncrement.initialize(mongoose);
feedbackSchema.plugin(autoIncrement.plugin, {
    model: 'feedback',
    field: 'feedbackCode',
    startAt: 1,
    incrementBy: 1
});

module.exports = {
    feedback: mongoose.model('feedback', feedbackSchema)
}
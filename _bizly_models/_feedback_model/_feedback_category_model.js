const mongoose = require('mongoose')
const schema = mongoose.Schema

const feedbackCategorySchema = new schema(
    // {
    //     rootQuestionType: String,
    //     questionType: [
    //         {
    //             rootCategoryName:String,
    //             questionTypeName: String,
    //             questionSubType: [
    //                 {
    //                     questionSubTypeName: String,
    //                     questionSubChildType: [
    //                         {
    //                             questionSubChildTypeName: String,

    //                         }
    //                     ],
    //                 }
    //             ],
    //         },

    //     ],
    //     createdAt: { type: Date, default: Date.now },
    //     updatedAt: { type: Date, default: Date.now() },
    //     addedBy: { type: schema.Types.ObjectId, ref: 'admins' },
    //     updatedBy: { type: schema.Types.ObjectId, ref: 'admins' }

    // }

    {
        rootQuestionType: String,
        questionType: [
            {
                rootCategoryName: String,
                categories: [{
                    categoryName: String,
                    childCategories: [{
                        childCategoryName: String,
                        childChildCategories: [{
                            childChildCategoryName: String
                        }]
                    }]
                }]
            }
        ],
        createdAt: { type: Date, default: Date.now },
        updatedAt: { type: Date, default: Date.now() },
        addedBy: { type: schema.Types.ObjectId, ref: 'admins' },
        updatedBy: { type: schema.Types.ObjectId, ref: 'admins' }

    }






)

module.exports = {
    category: mongoose.model('feedbackcategory', feedbackCategorySchema)
}
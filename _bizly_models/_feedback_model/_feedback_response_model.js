const mongoose = require('mongoose')
const schema = mongoose.Schema

const feedbackResponseSchema = new schema(
    {

        full_Name: String,
        email: String,
        mobile: String,
        suggestion: String,
        selectedQuestion: [],
        addedBy: { type: schema.Types.ObjectId, ref: 'admins' },
        createdAt: { type: Date, default: Date.now },
        updatedAt: { type: Date, default: Date.now },
        updatedBy: { type: schema.Types.ObjectId, ref: 'admins' }

    }
)

module.exports = {
    response: mongoose.model('feedbackresponse', feedbackResponseSchema)
}
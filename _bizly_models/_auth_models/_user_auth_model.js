const mongoose = require('mongoose')
const schema = mongoose.Schema

const userSchema = new schema(
    {
        role: { type: String },
        userName: { type: String },
        firstName: { type: String },
        lastName: { type: String },
        email: { type: String },
        mobileno: { type: Number },
        password: { type: String },
        createdAt: { type: Date, default: Date.now },
        updatedAt: { type: Date, default: Date.now }
    }
)

module.exports = {
    userModel: mongoose.model('users', userSchema)
}
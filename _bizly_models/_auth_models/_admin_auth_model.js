const mongoose = require('mongoose')
const schema = mongoose.Schema

const adminSchema = new schema(
    {
        role: { type: String, default: 'admin' },
        userName: { type: String },
        password: { type: String }
    }
)

module.exports = {
    adminauthmodel: mongoose.model('admins', adminSchema)
}
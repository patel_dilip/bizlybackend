const mongoose = require('mongoose')
const schema = mongoose.Schema

const b2bCategorySchema = new schema(

    {


        name: String, //Admin
        is_selected: { type: Boolean, default: false },
        childern: [{
            name: String, //CEO
            is_selected: { type: Boolean, default: false },
            childern2: [{
                name: String,   //Manager
                is_selected: { type: Boolean, default: false },
                childern3: [{
                    name: String,   //Assistant Manager
                    is_selected: { type: Boolean, default: false },
                    childern4: [{
                        name: String,    //Employee
                        is_selected: { type: Boolean, default: false },
                        lastchildern: [{}]
                    }]
                }]

            }]


        }],
        addedBy: { type: schema.Types.ObjectId, ref: 'admins' },
        createdAt: { type: Date, default: Date.now },
        updatedAt: { type: Date, default: Date.now },
        updatedBy: { type: schema.Types.ObjectId, ref: 'admins' }
    }


)

module.exports = {
    categoryModel: mongoose.model('bizlytobizlycategory', b2bCategorySchema)
}
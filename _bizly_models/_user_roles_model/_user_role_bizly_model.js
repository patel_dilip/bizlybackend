const mongoose = require('mongoose')
const schema = mongoose.Schema

const bizlyTobizlyRoleschema = new schema(
    {
        role_ID: String,
        role_name: { type: String, lowercase: true },
        description: String,
        role_salary: Number,
        user_edited: String,
        created_by_on: { type: schema.Types.ObjectId, ref: 'admins' },
        status: { type: Boolean, default: true },
        addedBy: { type: schema.Types.ObjectId, ref: 'admins' },
        updatedBy: { type: schema.Types.ObjectId, ref: 'admins' },
        createdAt: { type: Date, default: Date.now },
        updatedAt: { type: Date, default: Date.now }
    }
)


const bizlytobizlyUsersSchema = new schema(
    {
        personal_details: {
            user_code: String,
            employee_name: String,
            contact_number: String,
            email_id: String,
            photo: String,
            first_address: String,
            second_address: String,
            state: String,
            city: String,
            country: String,
            pin_code: String,
            gender: String,
            birth_date: String,
            joiningDate: String,
            blood_group: String,
            marrital_status: String,
            emergency_contact_personName: String,
            emg_contact_number: String,
            mother_Name: String,
            father_husbandName: String,
            fam_Contact_no: String,
            status: Boolean,
            assigned_role: { type: schema.Types.ObjectId, ref: 'bizlytobizlyrole' },
            addedBy: { type: schema.Types.ObjectId, ref: 'admins' },
            updatedBy: { type: schema.Types.ObjectId, ref: 'admins' },

        },
        work_details: {
            openMon: String,
            openTue: String,
            openWed: String,
            openThur: String,
            openFri: String,
            openSat: String,
            openSun: String,

            extraopenMon: String,
            extraopenTue: String,
            extraopenWed: String,
            extraopenThur: String,
            extraopenFri: String,
            extraopenSat: String,
            extraopenSun: String,

            closeMon: String,
            closeTue: String,
            closeWed: String,
            closeThur: String,
            closeFri: String,
            closeSat: String,
            closeSun: String,

            extracloseMon: String,
            extracloseTue: String,
            extracloseWed: String,
            extracloseThur: String,
            extracloseFri: String,
            extracloseSat: String,
            extracloseSun: String,

            isMon: Boolean,
            isTues: Boolean,
            isWed: Boolean,
            isThur: Boolean,
            isFri: Boolean,
            isSat: Boolean,
            isSun: Boolean,

            Sanctioned_Leaves: Number,
            responsibilities: String,
            NDA_file: String,
            joining_letter: String,
            joining_date: String,
            assign_location: String,
            assign_city: String
        },
        bank_details: {
            account_number: String,
            IFSC_code: String,
            bank_name: String,
            bank_address: String,
            bank_state: String,
            bank_city: String,
            salary_amount: String,
            add_allownces: String,
            add_deduction: String,
            select_overtime: String,
            incentive_amount: String,
            net_salary: String,
            allownces_name: String,
            allownces_amount: String,
            deduction_name: String,
            deduction_amount: String,
            overTime_name: String,
            overTime_amount: String,
            isAllownces: Boolean,
            isDeduction: Boolean,
            isOvertime: Boolean,
        },
        legal_documents: {
            aadhar_number: String,
            aadhar_upload: String,
            licence_number: String,
            licence_upload: String,
            passport_number: String,
            passport_upload: String,
            joining_letter: String,
            joining_letter_file: String,
            add_document: String,
            add_document_file: String,
            document_name: String,
            name_document_file: String
        },
        corporate_detials: {
            joining_date: String,
            assign_city: String,
            assign_location: String,
            Joining_letter_upload: String,
            add_document: []
        },
        assets: {
            name: String,
            quantity: String,
            assetsArr: []
        },
        credentials: {
            bizUser_email: String,
            password: String,
            admin_username: String,
            admin_password: String,
        },
        createdAt: { type: Date, default: Date.now },
        updatedAt: { type: Date, default: Date.now }
    }
)


module.exports = {
    bizlytobizlyRole: mongoose.model('bizlytobizlyrole', bizlyTobizlyRoleschema),
    bizlytobizlyUser: mongoose.model('bizlytobizlyuser', bizlytobizlyUsersSchema)

}
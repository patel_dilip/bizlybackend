const mongoose = require('mongoose')
const schema = mongoose.Schema

const roleEditedHistorySchema = new schema(
    {
        objID: String,
        role_ID: String,
        role_name: String,
        role_salary: Number,
        total_users: String,
        access_control: [],
        user_edited: String,
        description: String,
        status: { type: Boolean, default: true },
        created_by_on: { type: schema.Types.ObjectId, ref: 'admins' },
        addedBy: { type: schema.Types.ObjectId, ref: 'admins' },
        updatedBy: { type: schema.Types.ObjectId, ref: 'admins' },
        createdAt: String,
        updatedAt: String
    }
)

const userEditedHistorySchema = new schema(
    {
        userobjID: String,
        personal_details: {
            user_code: String,
            employee_name: String,
            contact_number: String,
            email_id: String,
            photo: String,
            first_address: String,
            second_address: String,
            state: String,
            city: String,
            country: String,
            pin_code: String,
            gender: String,
            birth_date: String,
            joiningDate: String,
            blood_group: String,
            marrital_status: String,
            emergency_contact_personName: String,
            emg_contact_number: String,
            father_husbandName: String,
            mother_Name: String,
            status: Boolean,
            assigned_role: { type: schema.Types.ObjectId, ref: 'bizlytoposrole' },
            addedBy: { type: schema.Types.ObjectId, ref: 'admins' },
            updatedBy: { type: schema.Types.ObjectId, ref: 'admins' }

        },
        work_details: {
            openMon: String,
            openTue: String,
            openWed: String,
            openThur: String,
            openFri: String,
            openSat: String,
            openSun: String,

            extraopenMon: String,
            extraopenTue: String,
            extraopenWed: String,
            extraopenThur: String,
            extraopenFri: String,
            extraopenSat: String,
            extraopenSun: String,

            closeMon: String,
            closeTue: String,
            closeWed: String,
            closeThur: String,
            closeFri: String,
            closeSat: String,
            closeSun: String,

            extracloseMon: String,
            extracloseTue: String,
            extracloseWed: String,
            extracloseThur: String,
            extracloseFri: String,
            extracloseSat: String,
            extracloseSun: String,

            isMon: Boolean,
            isTues: Boolean,
            isWed: Boolean,
            isThur: Boolean,
            isFri: Boolean,
            isSat: Boolean,
            isSun: Boolean,

            Sanctioned_Leaves: String,
            responsibilities: String,
            NDA_file: String,
            joining_letter: String,
            joining_date: String,
            assign_location: String,
            assign_city: String
        },
        bank_details: {
            account_number: String,
            IFSC_code: String,
            bank_name: String,
            bank_address: String,
            bank_state: String,
            bank_city: String,
            salary_amount: String,
            add_allownces: String,
            add_deduction: String,
            select_overtime: String,
            incentive_amount: String,
            net_salary: String,
            allownces_name: String,
            allownces_amount: String,
            deduction_name: String,
            deduction_amount: String,
            overTime_name: String,
            overTime_amount: String,
            isAllownces: Boolean,
            isDeduction: Boolean,
            isOvertime: Boolean,
        },
        legal_document: {
            aadhar_number: String,
            aadhar_upload: String,
            licence_number: String,
            licence_upload: String,
            passport_number: String,
            passport_upload: String,
            joining_letter: String,
            joining_letter_file: String,
            add_document: String,
            add_document_file: String
        },
        corporate_details: {
            joining_date: String,
            assign_city: String,
            assign_location: String,
            Joining_letter_upload: String,
            add_document: []
        },
        assets: {
            name: String,
            quantity: String,
            assetsArr: []
        },
        credentials: {
            pos_username: String,
            admin_password: String
        },
        createdAt: String,
        updatedAt: String
    }
)
module.exports = {
    role_edited_history: mongoose.model('posroleeditedhistory', roleEditedHistorySchema),
    user_edited_history: mongoose.model('posusreeditedhistory', userEditedHistorySchema)
}
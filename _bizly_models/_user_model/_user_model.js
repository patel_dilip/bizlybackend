const mongoose = require("mongoose");
const schema = mongoose.Schema;
const autoIncrement = require("mongoose-auto-increment");

const userSchema = new schema({
  usercode: String,
  access_points: {},
  personal_details: {
    role: { type: schema.Types.ObjectId, ref: "bizlytobizlyrole" },
    firstName: { type: String },
    middleName: { type: String },
    lastName: { type: String },
    photo: { type: String },
    gender: { type: String },
    dob: { type: String },
    blood_grp: { type: String },
    marital_staus: { type: String },
    father_husband_name: { type: String },
    mother_name: { type: String }
  },
  contactDetails: {
    contact: { type: String },
    emailId: { type: String },
    address: { type: String },
    comm_address: { type: String },
    emergency: { type: String }
  },
  work_details: {
    work_schedule: { type: String },
    emp_responsibility: { type: String },
    no_disclosure: { type: String },
    sanction_leave: { type: String }
  },
  accoutDetail: {
    account_no: { type: String },
    account_type: { type: String },
    bank_name: { type: String },
    branch_name: { type: String },
    bank_city: { type: String },
    bank_address: { type: String },
    bank_ifsc_code: { type: String },
    salary: { type: String },
    join_date: { type: String },
    assigned_city: { type: String },
    assigned_loc: { type: String },
    join_letter: { type: String },
    asset_provided: { type: Boolean, default: false },
    asset_name: { type: String },
    asset_desc: { type: String },
    asset_quantity: { type: String }
  },
  documents: {
    aadhar: { type: String },
    driving_lic: { type: String },
    passport: { type: String },
    legal_docName: { type: String },
    legal_doc: { type: String }
  },
  credentials: {
    gen_loginId: { type: String },
    gen_password: { type: String },
  },
  rating: Number,
  edit_history: [],
  status: { type: Boolean, default: true },
  created_by: { type: schema.Types.ObjectId, ref: "admins" },
  createdAt: { type: Date, default: Date.now },
  updatedAt: { type: Date, default: Date.now },
  updatedBy: { type: schema.Types.ObjectId, ref: "admins" },
});

autoIncrement.initialize(mongoose);
userSchema.plugin(autoIncrement.plugin, {
  model: "bizusers",
  field: "usercode",
  startAt: 1,
  incrementBy: 1,
});

module.exports = {
  userModel: mongoose.model("bizusers", userSchema),
};

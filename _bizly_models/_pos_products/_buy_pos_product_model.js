const mongoose = require('mongoose')
const schema = mongoose.Schema
const autoIncrement = require('mongoose-auto-increment');

const buyPOSProductSchema = new schema(
    {
        posProductCode: String,
        retailBeveragesTree: [],
        addedBy: { type: schema.Types.ObjectId, ref: 'admins' },
        createdAt: { type: Date, default: Date.now },
        updatedAt: { type: Date, default: Date.now },
        updatedBy: { type: schema.Types.ObjectId, ref: 'admins' }
    }
)

autoIncrement.initialize(mongoose);
buyPOSProductSchema.plugin(autoIncrement.plugin, {
    model: 'buyposproducts',
    field: 'posProductCode',
    startAt: 1,
    incrementBy: 1
});


module.exports = {
    buyPOSProductModel: mongoose.model('buyposproducts', buyPOSProductSchema)
}
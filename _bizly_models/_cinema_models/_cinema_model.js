const mongooose = require('mongoose')
const schema = mongooose.Schema

const cinemaSchema = new schema(
    {
        cinema_code: String,
        display_name: { type: String },
        legal_name: { type: String },
        screen_series: [],
        status: { type: Boolean },
        addedBy: { type: schema.Types.ObjectId, ref: 'admins' },
        createdAt: { type: Date, default: Date.now },
        updatedAt: { type: Date, default: Date.now },
        updatedBy: { type: schema.Types.ObjectId, ref: 'admins' }
    }
)

module.exports = {
    cinemaModel: mongooose.model('cinema', cinemaSchema)
}
const mongoose = require('mongoose')
const schema = mongoose.Schema

const restaurantTypeSchema = new schema(
    {
        restaurantTypeName: { type: String },
        addedBy: { type: schema.Types.ObjectId, ref: 'admins' },
        createdAt: { type: Date, default: Date.now },
        updatedAt: { type: Date, default: Date.now },
        updatedBy: { type: schema.Types.ObjectId, ref: 'admins' }
    }
)

module.exports = {
    restaurantTypemodel: mongoose.model('restaurantType', restaurantTypeSchema)
}
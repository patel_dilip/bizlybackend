const mongoose = require('mongoose')
const schema = mongoose.Schema

const restaurantRevenueSchema = new schema(
    {
        restoRevenue: {},
        createdAt: { type: Date, default: Date.now },
        updatedAt: { type: Date, default: Date.now }
    }
)

module.exports = {
    restaurantrevenuemodel: mongoose.model('restaurantrevenue', restaurantRevenueSchema)
}
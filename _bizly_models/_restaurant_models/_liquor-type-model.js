const mongoose = require('mongoose')
const schema = mongoose.Schema

const liquorSchema = new schema(
    {
        rootCategoryName: { type: String },
        categories: [{
            categoryName: { type: String },
            childCategories: [
                {   
                    childCategoryName: { type: String },
                    childChildCategories: [{
                        childChildCategoryName: { type: String },
                        isFilterable: { type: Boolean },
                        isSearchable: { type: Boolean },
                        priority: { type: String }
                    }],
                    isFilterable: { type: Boolean },
                    isSearchable: { type: Boolean },
                    priority: { type: String }
                }
            ],
            isFilterable: { type: Boolean },
            isSearchable: { type: Boolean },
            priority: { type: String }
        }],
        isFilterable: { type: Boolean },
        isSearchable: { type: Boolean },
        priority: { type: String },
        addedBy: { type: schema.Types.ObjectId, ref: 'admins' },
        updatedAt: { type: Date, default: Date.now },
        updatedBy: { type: schema.Types.ObjectId, ref: 'admins' }
    }
)

module.exports = {
    liquorModel: mongoose.model('liquortype', liquorSchema)
}

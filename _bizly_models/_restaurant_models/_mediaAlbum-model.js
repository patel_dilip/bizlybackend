const mongoose = require('mongoose')
const schema = mongoose.Schema

const mediaAlbumSchema = new schema(
    {
        title: String,
        albums: [String],
        addedBy: { type: schema.Types.ObjectId, ref: 'admins' },
        updatedBy: { type: schema.Types.ObjectId, ref: 'admins' },
        createdAt: { type: Date, default: Date.now },
        updatedAt: { type: Date, default: Date.now }
    }
)
module.exports = {
    mediaAlbum: mongoose.model('mediaalbum', mediaAlbumSchema)
}
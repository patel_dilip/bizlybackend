const mongoose = require('mongoose')
const schema = mongoose.Schema

const selected_menuSchema = new schema(
    {
        rootCategoryName: { type: String },
        categories: [{
            categoryName: { type: String },
            childCategories: [
                {
                    childCategoryName: { type: String },
                    childChildCategories: [{
                        childChildCategoryName: { type: String },
                        isFilterable: { type: Boolean },
                        isSearchable: { type: Boolean },
                        priority: { type: Number }
                    }],
                    isFilterable: { type: Boolean },
                    isSearchable: { type: Boolean },
                    priority: { type: Number }
                }
            ],
            isFilterable: { type: Boolean },
            isSearchable: { type: Boolean },
            priority: { type: Number }
        }],
        isFilterable: { type: Boolean },
        isSearchable: { type: Boolean },
        priority: { type: Number },
        addedBy: { type: schema.Types.ObjectId, ref: 'admins' },
        updatedAt: { type: Date, default: Date.now },
        updatedBy: { type: schema.Types.ObjectId, ref: 'admins' }
    }
)

module.exports = {
    selected_menu_Model: mongoose.model('selected_menu', selected_menuSchema)
}
const mongoose = require('mongoose')
const schema = mongoose.Schema

const ambienceSchema = new schema(
    {
        title: { type: String },
        ambiences: [],
        addedBy: { type: schema.Types.ObjectId, ref: 'admins' },
        updatedBy: { type: schema.Types.ObjectId, ref: 'admins' },
        createdAt: { type: Date, default: Date.now },
        updatedAt: { type: Date, default: Date.now }
    }
)

module.exports = {
    ambiencemodel: mongoose.model('ambiences', ambienceSchema)
}
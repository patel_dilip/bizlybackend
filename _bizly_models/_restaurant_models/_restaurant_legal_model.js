const mongoose = require('mongoose')
const schema = mongoose.Schema

const restaurantLegal = new schema({
    accountNo: { type: Number },
    accountType: { type: String },
    bankName: { type: String },
    branchName: { type: String },
    city: { type: String },
    address: { type: String },
    IFSCCode: { type: String },
    documents: [{
        documentName: { type: String },
        documentNumber: { type: String },
        documentUrl: { type: String }
    }],
    otherDocuments: [
        {
            documentName: { type: String },
            documentUrl: { type: String }
        }
    ],
    addedBy: { type: schema.Types.ObjectId, ref: 'admins' },
    updatedAt: { type: Date, default: Date.now },
    createdAt: { type: Date, default: Date.now },
    updatedBy: { type: schema.Types.ObjectId, ref: 'admins' }
})

module.exports = {
    restaurantLegalModel : mongoose.model('restaurantLegalModel',restaurantLegal)
}

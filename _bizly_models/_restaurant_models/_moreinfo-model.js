const mongoose = require('mongoose')
const schema = mongoose.Schema

const moreInfoSchema = new schema(
    {
        title: { type: String },
        moreinfo: [],
        addedBy: { type: schema.Types.ObjectId, ref: 'admins' },
        updatedBy: { type: schema.Types.ObjectId, ref: 'admins' },
        createdAt: { type: Date, default: Date.now },
        updatedAt: { type: Date, default: Date.now }
    }
)

module.exports = {
    moremodel: mongoose.model('moreinfos', moreInfoSchema)
}
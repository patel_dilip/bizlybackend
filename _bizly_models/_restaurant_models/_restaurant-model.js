const mongoose = require("mongoose");
const schema = mongoose.Schema;

const restoSchema = new schema({
  outletInfo: {
    outlet_code: { type: String },
    chain_id: String,
    chain_name: String,
    outlet_id: { type: String },
    restaurant_is: { type: String },
    infrastructure_type: { type: String },
    type_of_building: { type: String },
    name_of_building: { type: String },
    address_of_building: { type: String },
    multiplex_need_qr: { type: Boolean },
    multiplex_present: Boolean,
    multiplexid: String,
    multiplex_name: String,
    food_court_name: String,
    food_court_need_qr: Boolean,
    multiplex_screen: [],
    org_code: { type: String },
    org_name: { type: String }
  },
  restoInfo: {
    restoLegalName: { type: String },
    restoName: { type: String },
    restoContactNo: { type: String },
    restoEmail: { type: String },
    restoAddress: { type: String },
    restoPriceForTwo: { type: String },
    restoLocality: [],
    restoPincode: { type: Number },
    restoRevenue: { type: String },
    restoCountryCode: { type: String },
    restoType: [],
    restoWebsite: {},
    restoRepresentativeName: { type: String },
    restoRepresentativeCountryCode: { type: String },
    restoRepresentativeContactNo: { type: Number },
    restoRepresentativeEmail: { type: String },
    restoHours: [
      {
        day: {},
        open: {},
        close: {},
        addhrOpen: {},
        addhrClose: {},
      },
    ],
    restoStatus: { type: Boolean, default: true },
  },

  establishmentType: {
    estCategories: {
      rootCategories: [],
      categories: [],
      childCategories: [],
      childchildCategories: [],
    },

    more_info: [],
    services: [],
    rulesandregulations: [],
    ambiences: [],
  },
  legalInfo: {
    accountNo: { type: Number, default: null },
    accountType: { type: String, default: null },
    bankName: { type: String, default: null },
    branchName: { type: String, default: null },
    city: { type: String, default: null },
    address: { type: String, default: null },
    IFSCCode: { type: String, default: null },
    documents: [
      {
        documentName: { type: String },
        documentNumber: { type: String },
        documentUrl: { type: String },
      },
    ],
    otherDocuments: [
      {
        documentName: { type: String },
        documentNumber: { type: String },
        documentUrl: { type: String },
      },
    ],
    selected_docs: [],
  },
  media: {
    resto_logo: { type: String },
    resto_banner: [String],
    album_image: [
      {
        name: { type: String },
        fileurl: [String],
      },
    ],
    album_video: [
      {
        name: { type: String },
        fileurl: [String],
      },
    ],
  },
  menuManagement: {
    cuisines: [],
    menu: {
      rootCategories: [],
      categories: [],
      childCategories: [],
      childchildCategories: [],
    },

    liquor: {
      rootVarients: [],
      Varients: [],
      SubVarients: [],
      SubSubVarients: [],
    },
  },
  // layout: [{
  //     title: String,
  //     table: [{

  //     }],
  //     chairs: [],
  //     walls: [],
  //     doors: [],
  //     billing: []
  // }],
  layout: {
    // title: String,
    // table: [],
    // chairs: [],
    // walls: [],
    // doors: [],
    // billing: [],
    rectArr: [],
  },
  addedBy: { type: schema.Types.ObjectId, ref: "admins" },
  createdAt: { type: Date, default: Date.now },
  updatedAt: { type: Date, default: Date.now },
  updatedBy: { type: schema.Types.ObjectId, ref: "admins" },
});

module.exports = {
  restomodel: mongoose.model("restaurants", restoSchema),
};

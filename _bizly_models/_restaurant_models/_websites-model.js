const mongoose = require('mongoose')
const schema = mongoose.Schema

const websiteSchema = new schema(
    {
        websitename: String,
        addedBy: { type: schema.Types.ObjectId, ref: 'admins' },
        updatedBy: { type: schema.Types.ObjectId, ref: 'admins' },
        createdAt: { type: Date, default: Date.now },
        updatedAt: { type: Date, default: Date.now }
    }
)

module.exports = {
    website: mongoose.model('website', websiteSchema)
}
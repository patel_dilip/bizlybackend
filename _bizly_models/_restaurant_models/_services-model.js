const mongoose = require('mongoose')
const schema = mongoose.Schema

const servicesSchema = new schema(
    {
        title: { type: String },
        services:[],
        addedBy: { type: schema.Types.ObjectId, ref: 'admins' },
        updatedBy: { type: schema.Types.ObjectId, ref: 'admins' },
        createdAt: { type: Date, default: Date.now },
        updatedAt: { type: Date, default: Date.now }
    }
)

module.exports = {
    servicemodel: mongoose.model('services', servicesSchema)
}
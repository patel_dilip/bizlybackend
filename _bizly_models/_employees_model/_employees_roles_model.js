const mongoose = require("mongoose");
const schema = mongoose.Schema;

const roleTreeSchema = new schema({
  roleTree: [],
  created_by: { type: String },
  created_on: { type: Date, default: Date.now },
  updated_by: { type: String },
  updatedat: { type: Date, default: Date.now },
});

const bizlyEmployeeRoleSchema = new schema({
  roleid: String,
  roleName: { type: String, lowercase: true },
  role_desc: { type: String },
  notesandcirculr: { type: String, default: "NA" },
  parent_key: [], //object id
  assigned_users: [], //object id
  access_points: [], //object id
  status: { type: Boolean, default: true },
  version_history: [],
  created_by: { type: String },
  created_on: { type: Date, default: Date.now },
  updated_by: { type: String },
  updatedat: { type: Date, default: Date.now },
});

module.exports = {
  roleTree: mongoose.model("roleTrees", roleTreeSchema),
  bizlyEmpRole: mongoose.model("bizlyemproles", bizlyEmployeeRoleSchema),
};

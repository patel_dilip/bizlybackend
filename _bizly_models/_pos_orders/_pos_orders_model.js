const mongoose = require("mongoose");
const schema = mongoose.Schema;
const autoIncrement = require("mongoose-auto-increment");

const posOrdersSchema = new schema({
  posOrderID: { type: String },
  Organization_ID: String,
  organization_name: String,
  representative_name: String,
  representative_email: String,
  representative_contact: Number,
  org_email: String,
  org_contact: Number,
  address: String,
  state: String,
  city: String,
  gst: { isGST: Boolean, GSTNo: String },
  posProductOrder: {
    posProductOrderID: String,
    orderDate_Time: { type: Date, default: Date.now },
    subscriptionActivationDate: { type: Date },
    subscriptionExpiryDate: { type: Date },
    noOfoutlets: { type: Number, default: 0 },
    productName: { type: schema.Types.ObjectId, ref: "chargesproduct" },
    license_No: [{ outlet_No: Number, license_No: String, outletID: String }],
    tax: Number,
    pricePerOutlet: { type: Number },
    paymentStatus: String,
    discount: { percentage: Number, amount: Number },
    DiscountedPrice: { type: Number },
    PriceafterDiscount: { type: Number },
    totalOrderAmount: Number,
    totalPaidAmountwithTax: Number,
    paidBy: String,
    subscription_Type: String,
    transcationID: String,
    transactionDate_Time: { type: Date },
    history: [],
    lastLicenceNumber: { type: Number, default: 0 },
    createdAt: { type: Date, default: Date.now },
    updatedAt: { type: Date, default: Date.now },
    updatedBy: { type: schema.Types.ObjectId, ref: "admins" },
  },
  posAddonOrder: {
    posAddOnOrderID: String,
    OrderDate_Time: { type: Date, default: Date.now },
    subscriptionActivationDate: { type: Date },
    subscriptionExpiryDate: { type: Date },
    noOfoutlets: { type: Number, default: 0 },
    addonName: [{ type: schema.Types.ObjectId, ref: "chargesaddon" }],
    addonBought: [
      {
        addonName: { type: schema.Types.ObjectId, ref: "chargesaddon" },
        license_No: [
          { outlet_No: Number, license_No: String, outletID: String },
        ],
      },
    ],
    tax: Number,
    pricePerOutlet: { type: Number },
    paymentStatus: String,
    discount: { percentage: Number, amount: Number },
    DiscountedPrice: { type: Number },
    PriceafterDiscount: { type: Number },
    totalOrderAmount: Number,
    totalPaidAmountwithTax: Number,
    paidBy: String,
    subscription_Type: String,
    transcationID: String,
    transactionDate_Time: { type: String },
    history: [],
    status: { type: Boolean, default: false },
    lastLicenceNumber: { type: Number, default: 0 },
    createdAt: { type: Date, default: Date.now },
    updatedAt: { type: Date, default: Date.now },
    updatedBy: { type: schema.Types.ObjectId, ref: "admins" },
  },
  posIntegrationOrder: {
    posIntegrationOrderID: String,
    OrderDate_Time: { type: Date, default: Date.now },
    subscriptionActivationDate: { type: Date },
    subscriptionExpiryDate: { type: Date },
    noOfoutlets: { type: Number, default: 0 },
    integrationName: [
      { type: schema.Types.ObjectId, ref: "chargesintegration" },
    ],
    integrationBought: [
      {
        integrationName: {
          type: schema.Types.ObjectId,
          ref: "chargesintegration",
        },
        license_No: [
          { outlet_No: Number, license_No: String, outletID: String },
        ],
      },
    ],
    tax: Number,
    pricePerOutlet: { type: Number },
    paymentStatus: String,
    discount: { percentage: Number, amount: Number },
    DiscountedPrice: { type: Number },
    PriceafterDiscount: { type: Number },
    totalOrderAmount: Number,
    totalPaidAmountwithTax: Number,
    paidBy: String,
    subscription_Type: String,
    transcationID: String,
    transactionDate_Time: { type: Date },
    history: [],
    status: { type: Boolean, default: false },
    lastLicenceNumber: { type: Number, default: 0 },
    createdAt: { type: Date, default: Date.now },
    updatedAt: { type: Date, default: Date.now },
    updatedBy: { type: schema.Types.ObjectId, ref: "admins" },
  },
  lastPOSProductOrderID: String,
  lastPOSAddonOrderID: String,
  lastPOSIntegrationOrderID: String,
  status: { type: Boolean, default: true },
});

const posPaymentLinkSchema = new schema({
  paymentLinkID: { type: String },
  Organization_ID: String,
  organization_name: String,
  representative_name: String,
  representative_email: String,
  representative_contact: Number,
  org_email: String,
  org_contact: Number,
  address: String,
  state: String,
  city: String,
  paymentLinkSendDateandTime: { type: Date, default: Date.now },
  subscriptionActivationDate: { type: Date },
  subscriptionExpiryDate: { type: Date },
  noOfoutlets: { type: Number, default: 0 },
  productName: { type: schema.Types.ObjectId, ref: "chargesproduct" },
  tax: Number,
  paymentStatus: String,
  discount: { percentage: Number, amount: Number },
  pricePerOutlet: { type: Number },
  DiscountedPrice: { type: Number },
  PriceafterDiscount: { type: Number },
  totalOrderAmount: Number,
  totalPaidAmountwithTax: Number,
  paidBy: String,
  PaymentLinkSend: { whatsapp: String, email: String, sms: String },
  paymentLinkSendTo: { whatsapp: [], email: [], sms: [] },
  subscription_Type: String,
  transcationID: String,
  transactionDate_Time: { type: Date },
  history: [],
  status: { type: Boolean, default: true },
  createdAt: { type: Date, default: Date.now },
  updatedAt: { type: Date, default: Date.now },
  updatedBy: { type: schema.Types.ObjectId, ref: "admins" },
});

autoIncrement.initialize(mongoose);
posOrdersSchema.plugin(autoIncrement.plugin, {
  model: "posOrders",
  field: "posOrderID",
  startAt: 1,
  incrementBy: 1,
});

posOrdersSchema.plugin(autoIncrement.plugin, {
  model: "posOrders",
  field: "Organization_ID",
  startAt: 1,
  incrementBy: 1,
});

posOrdersSchema.plugin(autoIncrement.plugin, {
  model: "posOrders",
  field: "OrderID",
  startAt: 1,
  incrementBy: 1,
});

posOrdersSchema.plugin(autoIncrement.plugin, {
  model: "posOrders",
  field: "lastPOSProductOrderID",
  startAt: 1,
  incrementBy: 1,
});

posOrdersSchema.plugin(autoIncrement.plugin, {
  model: "posOrders",
  field: "lastPOSAddonOrderID",
  startAt: 1,
  incrementBy: 1,
});

posOrdersSchema.plugin(autoIncrement.plugin, {
  model: "posOrders",
  field: "lastPOSIntegrationOrderID",
  startAt: 1,
  incrementBy: 1,
});

posPaymentLinkSchema.plugin(autoIncrement.plugin, {
  model: "posPaymentLinks",
  field: "paymentLinkID",
  startAt: 1,
  incrementBy: 1,
});

posPaymentLinkSchema.plugin(autoIncrement.plugin, {
  model: "posPaymentLinks",
  field: "Organization_ID",
  startAt: 1,
  incrementBy: 1,
});

module.exports = {
  posOrdersModel: mongoose.model("posOrders", posOrdersSchema),
  posPaymentLinkModel: mongoose.model("posPaymentLinks", posPaymentLinkSchema),
};

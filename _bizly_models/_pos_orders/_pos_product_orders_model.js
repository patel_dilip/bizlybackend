const mongoose = require("mongoose");
const schema = mongoose.Schema;
const autoIncrement = require("mongoose-auto-increment");

const posOrdersSchema = new schema({
  posOrderID: { type: String },
  Organization_ID: String,
  OrderID: String,
  OrderDate_Time: { type: Date, default: Date.now },
  subscriptionActivationDate: { type: Date },
  subscriptionExpiryDate: { type: Date },
  license_No: [{ outlet_No: Number, license_No: String, outletID: String }],
  organization_name: String,
  representative_name: String,
  representative_email: String,
  representative_contact: Number,
  org_email: String,
  org_contact: Number,
  noOfoutlets: Number,
  productName: { type: schema.Types.ObjectId, ref: "chargesproduct" },
  address: String,
  state: String,
  city: String,
  tax: Number,
  paymentStatus: String,
  discount: { percentage: Number, amount: Number },
  totalOrderAmount: Number,
  totalPaidAmountwithTax: Number,
  discounted_price: Number,
  paidBy: String,
  subscription_Type: String,
  transcationID: String,
  transactionDate_Time: { type: Date },
  cancel_order: [
    {
      cancelletionReason: String,
      cancel_order_for_Outlets: Number,
      Cancellation_Charges: Number,
      totalRefundable_amount: Number,
    },
  ],
  history: [],
  lastLicenceNumber: { type: Number, default: 0 },
  status: { type: Boolean, default: true },
  addedBy: { type: schema.Types.ObjectId, ref: "admins" },
  createdAt: { type: Date, default: Date.now },
  updatedAt: { type: Date, default: Date.now },
  updatedBy: { type: schema.Types.ObjectId, ref: "admins" },
});

autoIncrement.initialize(mongoose);
posOrdersSchema.plugin(autoIncrement.plugin, {
  model: "posProductOrders",
  field: "posOrderID",
  startAt: 1,
  incrementBy: 1,
});

posOrdersSchema.plugin(autoIncrement.plugin, {
  model: "posProductOrders",
  field: "Organization_ID",
  startAt: 1,
  incrementBy: 1,
});

posOrdersSchema.plugin(autoIncrement.plugin, {
  model: "posProductOrders",
  field: "OrderID",
  startAt: 1,
  incrementBy: 1,
});

module.exports = {
  posOrdersModel: mongoose.model("posProductOrders", posOrdersSchema),
};

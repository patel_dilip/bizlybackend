const mongoose = require('mongoose')
const schema = mongoose.Schema

const noticeTemplateSchema = new schema(
    {
        template_id: String,
        template_name: { type: String, lowercase: true },
        file: String,
        select_folder: String,
        subject: { type: String, lowercase: true },
        template_description: { type: String, lowercase: true },
        message: { type: String, lowercase: true },
        available_for_template: [],
        status: { type: Boolean, default: true },
        addedBy: { type: schema.Types.ObjectId, ref: 'admins' },
        updatedBy: { type: schema.Types.ObjectId, ref: 'admins' },
        createdAt: { type: Date, default: Date.now },
        updatedAt: { type: Date, default: Date.now }
    }
)

module.exports = {
    noticetemplate: mongoose.model('noticetemplate', noticeTemplateSchema)
}
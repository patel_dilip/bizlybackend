const mongoose = require('mongoose')
const schema = mongoose.Schema

const noticeGroupSchema = new schema(
    {

        group_For: { type: String, lowercase: true },
        group_Name: { type: String, lowercase: true },
        group_member_list: [],
        status: { type: Boolean, default: true },
        addedBy: { type: schema.Types.ObjectId, ref: 'admins' },
        updatedBy: { type: schema.Types.ObjectId, ref: 'admins' },
        createdAt: { type: Date, default: Date.now },
        updatedAt: { type: Date, default: Date.now }
    }
)

module.exports = {
    notice_group: mongoose.model('noticegroup', noticeGroupSchema)
}
const mongoose = require('mongoose')
const schema = mongoose.Schema

const noticeCreateCircularFolderschema = new schema(
    {
        userid: { type: schema.Types.ObjectId, ref: 'admins' },
        createdfolders: [],
        addedBy: { type: schema.Types.ObjectId, ref: 'admins' },
        updatedBy: { type: schema.Types.ObjectId, ref: 'admins' },
        createdAt: { type: Date, default: Date.now },
        updatedAt: { type: Date, default: Date.now }
    }
)

module.exports = {
    folder: mongoose.model('noticecreatecircularfolder', noticeCreateCircularFolderschema)
}
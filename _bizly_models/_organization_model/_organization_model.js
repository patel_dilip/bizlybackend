const mongoose = require('mongoose')
const schema = mongoose.Schema

const organizationschema = new schema(
    {
        organizationCode: String,
        organization_name: String,
        representative_name: String,
        org_email: String,
        org_contact: String,
        status: { type: Boolean, default: true },
        outlets_arr: [{ type: schema.Types.ObjectId, ref: 'restaurants' }],
        addedBy: { type: schema.Types.ObjectId, ref: 'admins' },
        updatedBy: { type: schema.Types.ObjectId, ref: 'admins' },
        createdAt: { type: Date, default: Date.now },
        updatedAt: { type: Date, default: Date.now }

    }
)

module.exports = {
    organizartion: mongoose.model('organization', organizationschema)
}
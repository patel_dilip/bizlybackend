const mongoose = require('mongoose')
const schema = mongoose.Schema
const autoIncrement = require('mongoose-auto-increment');

const attributeSetSchema = new schema(
    {
        attributeSetCode: String,
        attributeSetName: { type: String, lowercase: true },
        assignGroups: [{ type: schema.Types.ObjectId, ref: 'liquorattributegroups' }],
        attributeType: { type: String, lowercase: true },
        categoryType:[],
        status: { type: Boolean, default: true },
        addedBy: { type: schema.Types.ObjectId, ref: 'admins' },
        createdAt: { type: Date, default: Date.now },
        updatedAt: { type: Date, default: Date.now },
        updatedBy: { type: schema.Types.ObjectId, ref: 'admins' }
    }
)

autoIncrement.initialize(mongoose);
attributeSetSchema.plugin(autoIncrement.plugin, {
    model: 'liquorattributesets',
    field: 'attributeSetCode',
    startAt: 1,
    incrementBy: 1
});

module.exports = {
    attributeSetModel: mongoose.model('liquorattributesets', attributeSetSchema)
}
const mongoose = require('mongoose')
const schema = mongoose.Schema
const autoIncrement = require('mongoose-auto-increment');

const attributeSchema = new schema(
    {
        attributeCode: String,
        attributeName: { type: String, lowercase: true },
        display_name: String,
        responseType: {
            elementName: { type: String },
            fieldValue: { type: String },
            options: [{
                optionLable: { type: String },
                associateImage: { type: String }
            }]
        },
        attributeType: { type: String, lowercase: true },
        isSearchable: { type: Boolean },
        isFilterable: { type: Boolean },
        isAssign: { type: Boolean, default: false },
        status: { type: Boolean, default: true },
        addedBy: { type: schema.Types.ObjectId, ref: 'admins' },
        createdAt: { type: Date, default: Date.now },
        updatedAt: { type: Date, default: Date.now },
        updatedBy: { type: schema.Types.ObjectId, ref: 'admins' }
    }
)

autoIncrement.initialize(mongoose);
attributeSchema.plugin(autoIncrement.plugin, {
    model: 'liquorattributes',
    field: 'attributeCode',
    startAt: 1,
    incrementBy: 1
});

module.exports = {
    attributeModel: mongoose.model('liquorattributes', attributeSchema)
}
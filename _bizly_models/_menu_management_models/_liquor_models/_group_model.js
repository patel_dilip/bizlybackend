const mongoose = require('mongoose')
const schema = mongoose.Schema
const autoIncrement = require('mongoose-auto-increment');

const attributeGroupSchema = new schema(
    {
        groupCode: String,
        groupName: { type: String, lowercase: true },
        attribute_set_name:String,
        assignAttributes: [{
            type: schema.Types.ObjectId, ref: 'liquorattributes'
        }],
        attributeType: { type: String, lowercase: true },
        addedBy: { type: schema.Types.ObjectId, ref: 'admins' },
        createdAt: { type: Date, default: Date.now },
        updatedAt: { type: Date, default: Date.now },
        updatedBy: { type: schema.Types.ObjectId, ref: 'admins' }
    }
)

autoIncrement.initialize(mongoose);
attributeGroupSchema.plugin(autoIncrement.plugin, {
    model: 'liquorattributegroups',
    field: 'groupCode',
    startAt: 1,
    incrementBy: 1
});

module.exports = {
    groupModel: mongoose.model('liquorattributegroups', attributeGroupSchema)
}
const mongoose = require('mongoose')
const schema = mongoose.Schema
const autoIncrement = require('mongoose-auto-increment');

const brandSchema = new schema(
    {
        brandCode: String,
        brandName: { type: String, lowercase: true },
        country: { type: String },
        status: { type: Boolean, default: true },
        categoryType:[],
        addedBy: { type: schema.Types.ObjectId, ref: 'admins' },
        createdAt: { type: Date, default: Date.now },
        updatedAt: { type: Date, default: Date.now },
        updatedBy: { type: schema.Types.ObjectId, ref: 'admins' }
    }
)

autoIncrement.initialize(mongoose);
brandSchema.plugin(autoIncrement.plugin, {
    model: 'liquorbrands',
    field: 'brandCode',
    startAt: 1,
    incrementBy: 1
});


module.exports = {
    brandmodel: mongoose.model('liquorbrands', brandSchema)
}
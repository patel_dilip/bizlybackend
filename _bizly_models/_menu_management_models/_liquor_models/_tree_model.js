const mongoose = require("mongoose");
const schema = mongoose.Schema;
const autoIncrement = require("mongoose-auto-increment");

const liquorTreeSchema = new schema({
  treeCode: String,
  liquorTree: [],
  addedBy: { type: schema.Types.ObjectId, ref: "admins" },
  createdAt: { type: Date, default: Date.now },
  updatedAt: { type: Date, default: Date.now },
  updatedBy: { type: schema.Types.ObjectId, ref: "admins" },
});

autoIncrement.initialize(mongoose);
liquorTreeSchema.plugin(autoIncrement.plugin, {
  model: "liquortrees",
  field: "treeCode",
  startAt: 1,
  incrementBy: 1,
});

module.exports = {
  liquorTreeModel: mongoose.model("liquortrees", liquorTreeSchema),
};

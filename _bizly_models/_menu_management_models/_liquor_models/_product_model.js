const mongoose = require('mongoose')
const schema = mongoose.Schema
const autoIncrement = require('mongoose-auto-increment');

const productSchema = new schema(
    {
        productCode: String,
        productName: { type: String, lowercase: true },
        brand: { type: schema.Types.ObjectId, ref: 'liquorbrands' },
        attribute: { type: schema.Types.ObjectId, ref: 'liquorattributesets' },
        productImages: [String],
        varient: {},
        attribute_set: [],
        attributeSet_response: [],
        root: {},
        subVarient: {},
        subsubVarient: {},
        varientType: {},
        status: { type: Boolean, default: true },
        addedBy: { type: schema.Types.ObjectId, ref: 'admins' },
        createdAt: { type: Date, default: Date.now },
        updatedAt: { type: Date, default: Date.now },
        updatedBy: { type: schema.Types.ObjectId, ref: 'admins' }

    }
)

autoIncrement.initialize(mongoose);
productSchema.plugin(autoIncrement.plugin, {
    model: 'liquorproduct',
    field: 'productCode',
    startAt: 1,
    incrementBy: 1
});

module.exports = {
    productModel: mongoose.model('liquorproduct', productSchema)
}
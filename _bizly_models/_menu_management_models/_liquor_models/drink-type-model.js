const mongoose = require('mongoose')
const schema = mongoose.Schema

const drinkTypeSchema = new schema(
    {
        rootDrinkType: { type: String },
        liquorVarients: [{
            liquorVarientName: { type: String, default: null },
            liquorSubVarients: [{
                liquorSubVarientName: { type: String, default: null },
                liquorSubSubVarientType: [{
                    liquorSubSubVarientTypeName: { type: String, default: null }
                }]
            }]
        }],
        addedBy: { type: schema.Types.ObjectId, ref: 'admins' },
        updatedAt: { type: Date, default: Date.now },
        updatedBy: { type: schema.Types.ObjectId, ref: 'admins' }
    }
)

module.exports = {
    rootdrinktype: mongoose.model('rootdrinktypes', drinkTypeSchema)
}
const mongoose = require('mongoose')
const schema = mongoose.Schema

const beveragesVarientContentSchema = new schema(
    {
        contentName: { type: String, lowercase: true },
        userid: { type: schema.Types.ObjectId, ref:'admins'},
        addedBy: { type: schema.Types.ObjectId, ref: 'admins' },
        createdAt: { type: Date, default: Date.now },
        updatedAt: { type: Date, default: Date.now },
        updatedBy: { type: schema.Types.ObjectId, ref: 'admins' }
    }
)

module.exports = {
    beveragesvarientcontentmodel: mongoose.model('beverages_varientcontents', beveragesVarientContentSchema)
}
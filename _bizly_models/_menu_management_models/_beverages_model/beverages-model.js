const mongoose = require('mongoose')
const schema = mongoose.Schema
const autoIncrement = require('mongoose-auto-increment');

const beverageSchema = new schema(
    {
        beverageCode: String,
        beverageName: { type: String, lowercase: true },
        unit: { type: String },
        veg_nonveg: { type: String },
        category:{},
        rootCategory: {},
        varients: {},
        subVarient: {},
        type: {},
        images: [],
        status: { type: Boolean, default: true },
        addedBy: { type: schema.Types.ObjectId, ref: 'admins' },
        updatedBy: { type: schema.Types.ObjectId, ref: 'admins' },
        createdAt: { type: Date, default: Date.now },
        updatedAt: { type: Date, default: Date.now }

    }
)

autoIncrement.initialize(mongoose);
beverageSchema.plugin(autoIncrement.plugin, {
    model: 'beverage',
    field: 'beverageCode',
    startAt: 1,
    incrementBy: 1
});

module.exports = {
    beverages_model: mongoose.model('beverages', beverageSchema)
}
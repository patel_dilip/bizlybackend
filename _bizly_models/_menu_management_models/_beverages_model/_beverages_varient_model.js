const mongoose = require("mongoose");
const schema = mongoose.Schema;
const autoIncrement = require("mongoose-auto-increment");

const beveragesVarientSchema = new schema({
  varientCode: String,
  varientName: { type: String, lowercase: true },
  varientContent: [{ type: schema.Types.ObjectId, ref: "beverages_varientcontents" }],
  images: [String],
  userid: { type: schema.Types.ObjectId, ref: "admins" },
  varientType: [],
  status: { type: Boolean, default: true },
  addedBy: { type: schema.Types.ObjectId, ref: "admins" },
  createdAt: { type: Date, default: Date.now },
  updatedAt: { type: Date, default: Date.now },
  updatedBy: { type: schema.Types.ObjectId, ref: "admins" },
});

autoIncrement.initialize(mongoose);
beveragesVarientSchema.plugin(autoIncrement.plugin, {
  model: "beverages_varients",
  field: "varientCode",
  startAt: 1,
  incrementBy: 1,
});

module.exports = {
  beveragesvarientmodel: mongoose.model("beverages_varients", beveragesVarientSchema),
};

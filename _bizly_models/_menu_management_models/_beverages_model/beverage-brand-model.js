const mongoose = require('mongoose')
const schema = mongoose.Schema
const autoIncrement = require('mongoose-auto-increment');

const beverage_brand_schema = new schema(
    {
        brandCode: String,
        brandName: { type: String, lowercase: true },
        country: { type: String, lowercase: true },
        brandLogo: String,
        categoryType:[],
        status: { type: Boolean, default: true },
        addedBy: { type: schema.Types.ObjectId, ref: 'admins' },
        updatedBy: { type: schema.Types.ObjectId, ref: 'admins' },
        createdAt: { type: Date, default: Date.now },
        updatedAt: { type: Date, default: Date.now }

    }
)

autoIncrement.initialize(mongoose);
beverage_brand_schema.plugin(autoIncrement.plugin, {
    model: 'beveragebrand',
    field: 'brandCode',
    startAt: 1,
    incrementBy: 1
});

module.exports = {
    beverage_brand: mongoose.model('beveragebrand', beverage_brand_schema)
}
const mongoose = require('mongoose')
const schema = mongoose.Schema

const beveragesSchema = new schema(
    {
        rootCategoryName: { type: String },
        subCategories: [{
            subCategoryName: { type: String },
            subSubCategories: [{
                subSubCategoryName: { type: String },
                subSubCategoryType: [{
                    subSubCategoryTypeName: { type: String }
                }]
            }]
        }],
        beverageType: String,
        addedBy: { type: schema.Types.ObjectId, ref: 'admins' },
        updatedBy: { type: schema.Types.ObjectId, ref: 'admins' },
        updatedAt: { type: Date, default: Date.now }


    }
)

module.exports = {
    beveragesModel: mongoose.model('beveragecategory', beveragesSchema)
}
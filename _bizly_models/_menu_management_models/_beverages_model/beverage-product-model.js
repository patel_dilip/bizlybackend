const mongoose = require('mongoose')
const schema = mongoose.Schema
const autoIncrement = require('mongoose-auto-increment');

const beverage_product_schema = new schema(
    {
        productCode: String,
        productName: { type: String, lowercase: true },
        rootCategory: { type: String, lowercase: true },
        varient: { type: String, lowercase: true },
        subVarient: { type: String, lowercase: true },
        type: { type: String, lowercase: true },
        unit: { type: String },
        associatedBrand: { type: schema.Types.ObjectId, ref: 'beveragebrand' },
        productImage: String,
        attributeSet:[],
        category:{},
        status: { type: Boolean, default: true },
        addedBy: { type: schema.Types.ObjectId, ref: 'admins' },
        updatedBy: { type: schema.Types.ObjectId, ref: 'admins' },
        createdAt: { type: Date, default: Date.now },
        updatedAt: { type: Date, default: Date.now }
    }
)

autoIncrement.initialize(mongoose);
beverage_product_schema.plugin(autoIncrement.plugin, {
    model: 'beverageproduct',
    field: 'productCode',
    startAt: 1,
    incrementBy: 1
});

module.exports = {
    beverage_product_model: mongoose.model('beverageproduct', beverage_product_schema)
}
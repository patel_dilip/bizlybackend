const mongoose = require("mongoose");
const schema = mongoose.Schema;
const autoIncrement = require("mongoose-auto-increment");

const beveragesAdonSchema = new schema({
  adonCode: String,
  adonName: { type: String, lowercase: true },
  unit: { type: String },
  veg_nonveg: { type: String },
  inHouseBeverages: [{ type: schema.Types.ObjectId, ref: "beverages" }],
  adonType: [],
  // dishes: [{ type: schema.Types.ObjectId, ref: 'dishes' }],
  images: [String],
  userid: { type: schema.Types.ObjectId, ref: "admins" },
  status: { type: Boolean, default: true },
  addedBy: { type: schema.Types.ObjectId, ref: "admins" },
  createdAt: { type: Date, default: Date.now },
  updatedAt: { type: Date, default: Date.now },
  updatedBy: { type: schema.Types.ObjectId, ref: "admins" },
});

autoIncrement.initialize(mongoose);
beveragesAdonSchema.plugin(autoIncrement.plugin, {
  model: "beverages_adons",
  field: "adonCode",
  startAt: 1,
  incrementBy: 1,
});

module.exports = {
  beveragesadonmodel: mongoose.model("beverages_adons", beveragesAdonSchema),
};

const mongoose = require('mongoose')
const schema = mongoose.Schema
const autoIncrement = require('mongoose-auto-increment');

const inhouseBeveragesTreeSchema = new schema(
    {
        treeCode: String,
        inhouseBeveragesTree: [],
        addedBy: { type: schema.Types.ObjectId, ref: 'admins' },
        createdAt: { type: Date, default: Date.now },
        updatedAt: { type: Date, default: Date.now },
        updatedBy: { type: schema.Types.ObjectId, ref: 'admins' }
    }
)

autoIncrement.initialize(mongoose);
inhouseBeveragesTreeSchema.plugin(autoIncrement.plugin, {
    model: 'inhousebeveragestrees',
    field: 'treeCode',
    startAt: 1,
    incrementBy: 1
});


module.exports = {
    inhouseBeveragesTreeModel: mongoose.model('inhousebeveragestrees', inhouseBeveragesTreeSchema)
}
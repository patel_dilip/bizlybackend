const mongoose = require('mongoose')
const schema = mongoose.Schema
const autoIncrement = require('mongoose-auto-increment');

const inventoryAssetsTreeSchema = new schema(
    {
        treeCode: String,
        inventoryAssetsTree: [],
        addedBy: { type: schema.Types.ObjectId, ref: 'admins' },
        createdAt: { type: Date, default: Date.now },
        updatedAt: { type: Date, default: Date.now },
        updatedBy: { type: schema.Types.ObjectId, ref: 'admins' }
    }
)

autoIncrement.initialize(mongoose);
inventoryAssetsTreeSchema.plugin(autoIncrement.plugin, {
    model: 'inventoryassetstrees',
    field: 'treeCode',
    startAt: 1,
    incrementBy: 1
});


module.exports = {
    assetsInventoryTreeModel: mongoose.model('inventoryassetstrees', inventoryAssetsTreeSchema)
}
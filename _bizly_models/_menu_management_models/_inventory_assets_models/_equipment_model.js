const mongoose = require('mongoose')
const schema = mongoose.Schema
const autoIncrement = require('mongoose-auto-increment');

const equipmentSchema = new schema(
    {
        equipmentCode: String,
        equipmentName: { type: String, lowercase: true },
        category: {},
        status: { type: Boolean, default: true },
        root: {},
        subcategory: {},
        subsubCategory: {},
        categoryType: {},
        attribute_set: [],
        attributeSet_response: [],
        // assignattributeSet: { type: schema.Types.ObjectId, ref: 'liquorattributesets' },
        image: String,
        addedBy: { type: schema.Types.ObjectId, ref: 'admins' },
        updatedBy: { type: schema.Types.ObjectId, ref: 'admins' },
        createdAt: { type: Date, default: Date.now },
        updatedAt: { type: Date, default: Date.now }
    }
)

autoIncrement.initialize(mongoose);
equipmentSchema.plugin(autoIncrement.plugin, {
    model: 'equipment',
    field: 'equipmentCode',
    startAt: 1,
    incrementBy: 1
});

module.exports = {
    equipmentModel: mongoose.model('equipment', equipmentSchema)
}
const mongoose = require('mongoose')
const schema = mongoose.Schema
const autoIncrement = require('mongoose-auto-increment');

const varientSchema = new schema(
    {
        varientCode: String,
        varientName: { type: String, lowercase: true },
        status: { type: Boolean, default: true },
        varientType: [],
        varientContent: [{ type: schema.Types.ObjectId, ref: 'varientcontents' }],
        images: [String],
        addedBy: { type: schema.Types.ObjectId, ref: 'admins' },
        createdAt: { type: Date, default: Date.now },
        updatedAt: { type: Date, default: Date.now },
        updatedBy: { type: schema.Types.ObjectId, ref: 'admins' }
    }
)

autoIncrement.initialize(mongoose);
varientSchema.plugin(autoIncrement.plugin, {
    model: 'varients',
    field: 'varientCode',
    startAt: 1,
    incrementBy: 1
});

module.exports = {
    varientmodel: mongoose.model('varients', varientSchema)
}
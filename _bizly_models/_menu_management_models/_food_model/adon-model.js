const mongoose = require('mongoose')
const schema = mongoose.Schema
const autoIncrement = require('mongoose-auto-increment');

const adonSchema = new schema(
    {
        adonCode: String,
        adonName: { type: String, lowercase: true },
        unit: { type: String },
        veg_nonveg: { type: String },
        adonType: [],
        status: { type: Boolean, default: true },
        dishes: [{ type: schema.Types.ObjectId, ref: 'dishes' }],
        images: [String],
        addedBy: { type: schema.Types.ObjectId, ref: 'admins' },
        createdAt: { type: Date, default: Date.now },
        updatedAt: { type: Date, default: Date.now },
        updatedBy: { type: schema.Types.ObjectId, ref: 'admins' }
    }
)

autoIncrement.initialize(mongoose);
adonSchema.plugin(autoIncrement.plugin, {
    model: 'adons',
    field: 'adonCode',
    startAt: 1,
    incrementBy: 1
});

module.exports = {
    adonmodel: mongoose.model('adons', adonSchema)
}
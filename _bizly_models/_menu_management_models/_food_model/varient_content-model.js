const mongoose = require('mongoose')
const schema = mongoose.Schema

const varientContentSchema = new schema(
    {
        contentName: { type: String, lowercase: true },
        addedBy: { type: schema.Types.ObjectId, ref: 'admins' },
        createdAt: { type: Date, default: Date.now },
        updatedAt: { type: Date, default: Date.now },
        updatedBy: { type: schema.Types.ObjectId, ref: 'admins' }
    }
)

module.exports = {
    varientcontentmodel: mongoose.model('varientcontents', varientContentSchema)
}
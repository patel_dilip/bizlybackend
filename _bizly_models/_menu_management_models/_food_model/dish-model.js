const mongoose = require('mongoose')
const schema = mongoose.Schema
const autoIncrement = require('mongoose-auto-increment');

const dishSchema = new schema(
    {
        dishCode: String,
        dishName: { type: String, lowercase: true },
        unit: { type: String },
        veg_nonveg: { type: String },
        status: { type: Boolean, default: true },
        cuisines: [{ type: schema.Types.ObjectId, ref: 'cusines' }],
        varients: [{ type: schema.Types.ObjectId, ref: 'varients' }],
        images: [String],
        addedBy: { type: schema.Types.ObjectId, ref: 'admins' },
        createdAt: { type: Date, default: Date.now },
        updatedAt: { type: Date, default: Date.now },
        updatedBy: { type: schema.Types.ObjectId, ref: 'admins' }
    }
)

autoIncrement.initialize(mongoose);
dishSchema.plugin(autoIncrement.plugin, {
    model: 'dishes',
    field: 'dishCode',
    startAt: 1,
    incrementBy: 1
});

module.exports = {
    dishmodel: mongoose.model('dishes', dishSchema)
}
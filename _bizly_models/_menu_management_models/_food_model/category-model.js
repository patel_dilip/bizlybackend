const mongoose = require('mongoose')
require('./tags-model')
const schema = mongoose.Schema
const autoIncrement = require('mongoose-auto-increment');

const categorySchema = new schema(
    {
        categoryCode:String,
        categoryName: { type: String, lowercase: true },
        status: { type: Boolean, default: true },
        cuisines: [{ type: schema.Types.ObjectId, ref: 'cusines' }],
        tags: [{ type: schema.Types.ObjectId, ref: 'tags' }],
        images: [String],
        createdAt: { type: Date, default: Date.now },
        updatedAt: { type: Date, default: Date.now },
        addedBy: { type: schema.Types.ObjectId, ref: 'admins' },
        updatedBy: { type: schema.Types.ObjectId, ref: 'admins' }
    }
)

autoIncrement.initialize(mongoose);
categorySchema.plugin(autoIncrement.plugin, {
    model: 'category',
    field: 'categoryCode',
    startAt: 1,
    incrementBy: 1
});

module.exports = {
    categorymodel: mongoose.model('category', categorySchema)
}
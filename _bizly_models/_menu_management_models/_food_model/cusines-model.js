const mongoose = require('mongoose')
const schema = mongoose.Schema
const autoIncrement = require('mongoose-auto-increment');

const cusinesSchema = new schema(
    {
        cuisineCode: String,
        cuisineName: { type: String, lowercase: true },
        country: { type: String },
        state: { type: String},
        city: { type: String },
        status: { type: Boolean, default: true },
        combinationOfCusines: [{ type: schema.Types.ObjectId, ref: 'cusines' }],
        images: [String],
        addedBy: { type: schema.Types.ObjectId, ref: 'admins' },
        createdAt: { type: Date, default: Date.now },
        updatedAt: { type: Date, default: Date.now },
        updatedBy: { type: schema.Types.ObjectId, ref: 'admins' }
    }
)

autoIncrement.initialize(mongoose);
cusinesSchema.plugin(autoIncrement.plugin, {
    model: 'cusines',
    field: 'cuisineCode',
    startAt: 1,
    incrementBy: 1
});

module.exports = {
    cuisinemodel: mongoose.model('cusines', cusinesSchema)
}
const mongoose = require('mongoose')
const schema = mongoose.Schema
const autoIncrement = require('mongoose-auto-increment');
 
const retail_food_product_schema = new schema(
    {
        productCode: String,
        productName: { type: String, lowercase: true },
        rootCategory: {},
        varient: {},
        subVarient: {},
        type: {},
        // attributeSet: { type: schema.Types.ObjectId, ref: 'liquorattributesets' },
        unit: { type: String },
        associatedBrand: { type: schema.Types.ObjectId, ref: 'retailfoodbrand' },
        productImage: String,
        attributeSet: [],
        attributeSet_response: [],
        category: {},
        status: { type: Boolean, default: true },
        addedBy: { type: schema.Types.ObjectId, ref: 'admins' },
        updatedBy: { type: schema.Types.ObjectId, ref: 'admins' },
        createdAt: { type: Date, default: Date.now },
        updatedAt: { type: Date, default: Date.now }
    }
)

autoIncrement.initialize(mongoose);
retail_food_product_schema.plugin(autoIncrement.plugin, {
    model: 'retailfoodproduct',
    field: 'productCode',
    startAt: 1,
    incrementBy: 1
});

module.exports = {
    retail_food_product_model: mongoose.model('retailfoodproduct', retail_food_product_schema)
}
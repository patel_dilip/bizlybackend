const mongoose = require('mongoose')
const schema = mongoose.Schema
const autoIncrement = require('mongoose-auto-increment');

const retail_food_brand_schema = new schema(
    {
        brandCode: String,
        brandName: { type: String, lowercase: true },
        country: { type: String },
        brandLogo: String,
        categoryType:[],
        status: { type: Boolean, default: true },
        addedBy: { type: schema.Types.ObjectId, ref: 'admins' },
        updatedBy: { type: schema.Types.ObjectId, ref: 'admins' },
        createdAt: { type: Date, default: Date.now },
        updatedAt: { type: Date, default: Date.now }

    }
)

autoIncrement.initialize(mongoose);
retail_food_brand_schema.plugin(autoIncrement.plugin, {
    model: 'retailfoodbrand',
    field: 'brandCode',
    startAt: 1,
    incrementBy: 1
});

module.exports = {
    retail_food_brand: mongoose.model('retailfoodbrand', retail_food_brand_schema)
}
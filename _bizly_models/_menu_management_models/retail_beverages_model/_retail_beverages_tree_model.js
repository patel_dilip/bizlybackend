const mongoose = require('mongoose')
const schema = mongoose.Schema
const autoIncrement = require('mongoose-auto-increment');

const retailBeveragesTreeSchema = new schema(
    {
        treeCode: String,
        retailBeveragesTree: [],
        addedBy: { type: schema.Types.ObjectId, ref: 'admins' },
        createdAt: { type: Date, default: Date.now },
        updatedAt: { type: Date, default: Date.now },
        updatedBy: { type: schema.Types.ObjectId, ref: 'admins' }
    }
)

autoIncrement.initialize(mongoose);
retailBeveragesTreeSchema.plugin(autoIncrement.plugin, {
    model: 'retailbeveragestrees',
    field: 'treeCode',
    startAt: 1,
    incrementBy: 1
});


module.exports = {
    retailBeveragesTreeModel: mongoose.model('retailbeveragestrees', retailBeveragesTreeSchema)
}
const mongoose = require('mongoose')
const schema = mongoose.Schema
const autoIncrement = require('mongoose-auto-increment');
 
const retailbeverageproductSchema = new schema(
    {
        productCode: String,
        productName: { type: String, lowercase: true },
        rootCategory: {},
        varient: {},
        subVarient: {},
        type: {},
        // attributeSet: { type: schema.Types.ObjectId, ref: 'liquorattributesets' },
        attributeSet: [],
        attributeSet_response: [],
        category: {},
        unit: { type: String },
        associatedBrand: { type: schema.Types.ObjectId, ref: 'retailbeveragebrand' },
        productImage: String,
        status: { type: Boolean, default: true },
        addedBy: { type: schema.Types.ObjectId, ref: 'admins' },
        updatedBy: { type: schema.Types.ObjectId, ref: 'admins' },
        createdAt: { type: Date, default: Date.now },
        updatedAt: { type: Date, default: Date.now }
    }
)

autoIncrement.initialize(mongoose);
retailbeverageproductSchema.plugin(autoIncrement.plugin, {
    model: 'retailfoodproduct',
    field: 'productCode',
    startAt: 1,
    incrementBy: 1
});

module.exports = {
    productModel: mongoose.model('retailbeverageproduct', retailbeverageproductSchema)
}
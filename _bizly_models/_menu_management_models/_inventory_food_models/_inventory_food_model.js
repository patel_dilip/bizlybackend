const mongoose = require('mongoose')
const schema = mongoose.Schema

const inventoryFoodCategoriesSchema = new schema(
    {

        rootCategoryName: { type: String },
        subCategories: [{
            subCategoryName: { type: String },
            subSubCategories: [{
                subSubCategoryName: { type: String },
                subSubCategoryType: [{
                    subSubCategoryTypeName: { type: String },
                    isApproved: { type: Boolean, default: true }
                }]
            }]
        }],
        addedBy: { type: schema.Types.ObjectId, ref: 'admins' },
        createdAt: { type: Date, default: Date.now },
        updatedAt: { type: Date, default: Date.now },
        updatedBy: { type: schema.Types.ObjectId, ref: 'admins' }
    }
)

module.exports = {
    inventoryfoodmodel: mongoose.model('inventoryfoodcategories', inventoryFoodCategoriesSchema)
}
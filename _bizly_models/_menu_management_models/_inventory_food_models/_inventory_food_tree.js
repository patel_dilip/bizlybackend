const mongoose = require('mongoose')
const schema = mongoose.Schema
const autoIncrement = require('mongoose-auto-increment');

const inventoryFoodTreeSchema = new schema(
    {
        treeCode: String,
        foodInventoryTree: [],
        addedBy: { type: schema.Types.ObjectId, ref: 'admins' },
        createdAt: { type: Date, default: Date.now },
        updatedAt: { type: Date, default: Date.now },
        updatedBy: { type: schema.Types.ObjectId, ref: 'admins' }
    }
)

autoIncrement.initialize(mongoose);
inventoryFoodTreeSchema.plugin(autoIncrement.plugin, {
    model: 'foodinventorytrees',
    field: 'treeCode',
    startAt: 1,
    incrementBy: 1
});


module.exports = {
    foodInventoryTreeModel: mongoose.model('foodinventorytrees', inventoryFoodTreeSchema)
}
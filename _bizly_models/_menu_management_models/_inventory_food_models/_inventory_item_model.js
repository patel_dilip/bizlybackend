const mongoose = require('mongoose')
const schema = mongoose.Schema
const autoIncrement = require('mongoose-auto-increment');

const inventoryItemSchema = new schema(
    {
        itemCode: String,
        itemName: { type: String, lowercase: true },
        status: { type: Boolean, default: true },
        brand: { type: schema.Types.ObjectId, ref: 'inventorybrand' },
        ingredient: { type: schema.Types.ObjectId, ref: 'inventoryingredient' },
        quantity: [],
        // measurement:String,
        addedBy: { type: schema.Types.ObjectId, ref: 'admins' },
        createdAt: { type: Date, default: Date.now },
        updatedAt: { type: Date, default: Date.now },
        updatedBy: { type: schema.Types.ObjectId, ref: 'admins' }
    }
)

autoIncrement.initialize(mongoose);
inventoryItemSchema.plugin(autoIncrement.plugin, {
    model: 'inventoryitem',
    field: 'itemCode',
    startAt: 1,
    incrementBy: 1
});

module.exports = {
    innventoryItemModel: mongoose.model('inventoryitem', inventoryItemSchema)
}
const mongoose = require('mongoose')
const schema = mongoose.Schema
const autoIncrement = require('mongoose-auto-increment');

const inventoryIngredientSchema = new schema(
    {
        ingredientCode: String,
        ingredientName: { type: String, lowercase: true },
        ingredientcategory: {},
        root: {},
        sub: {},
        subsub: {},
        type: {},
        unit: String,
        status: { type: Boolean, default: true },
        addedBy: { type: schema.Types.ObjectId, ref: 'admins' },
        createdAt: { type: Date, default: Date.now },
        updatedAt: { type: Date, default: Date.now },
        updatedBy: { type: schema.Types.ObjectId, ref: 'admins' }
    }
)

autoIncrement.initialize(mongoose);
inventoryIngredientSchema.plugin(autoIncrement.plugin, {
    model: 'inventoryingredient',
    field: 'ingredientCode',
    startAt: 1,
    incrementBy: 1
});

module.exports = {
    inventoryIngredientmodel: mongoose.model('inventoryingredient', inventoryIngredientSchema)
}
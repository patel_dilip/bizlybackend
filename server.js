//required modules
const bodyparser = require('body-parser')
const cors = require('cors')
const multer = require('multer')
const path = require('path')
const constants = require('./config/constants')
const dbconfig = require('./config/dbconfig')

const fs = require('fs')

//Restaurant Module
const ambienceRoutes = require('./_bizly_routes/_restaurants_routes/ambience-routes')
const establishmentRoutes = require('./_bizly_routes/_restaurants_routes/establishment-type-routes')
const moreinfoRoutes = require('./_bizly_routes/_restaurants_routes/moreinfo-routes')
const restaurantrevenue = require('./_bizly_routes/_restaurants_routes/restaurant-revenue-routes')
const restoRoute = require('./_bizly_routes/_restaurants_routes/resto-routes')
const ruleAndRegulationRoutes = require('./_bizly_routes/_restaurants_routes/ruleandregulation-routes')
const serviceRoutes = require('./_bizly_routes/_restaurants_routes/service-routes')
const restaurantRoutes = require('./_bizly_routes/_restaurants_routes/restaurant-type-routes')
const restaurantLegalRoutes = require('./_bizly_routes/_restaurants_routes/restaurant-legal-routes')
const selectedestablishmentRoutes = require('./_bizly_routes/_restaurants_routes/selected-establishment-routes')
const menuRoutes = require('./_bizly_routes/_restaurants_routes/menu-routes')
const liquorRoutes = require('./_bizly_routes/_restaurants_routes/liquor-type-routes')
const selectedMenuRoutes = require('./_bizly_routes/_restaurants_routes/selected-menu-routes')
const selectedLiquorRoutes = require('./_bizly_routes/_restaurants_routes/selected-liquor-type-routes')
const mediaAlbumRoutes = require('./_bizly_routes/_restaurants_routes/mediaAlbum-routes')
const websiteRoutes = require('./_bizly_routes/_restaurants_routes/websites-routes')

//Menu Management - Food Module
const adonRoutes = require('./_bizly_routes/_menu_management_routes/food/_adon_routes')
const categoryRoutes = require('./_bizly_routes/_menu_management_routes/food/_category_routes')
const cuisineroutes = require('./_bizly_routes/_menu_management_routes/food/_cuisine_routes')
const dishRoutes = require('./_bizly_routes/_menu_management_routes/food/_dish_routes')
const tagroutes = require('./_bizly_routes/_menu_management_routes/food/_tag_routes')
const varientContentRoutes = require('./_bizly_routes/_menu_management_routes/food/_varient_content_routes')
const varientRoutes = require('./_bizly_routes/_menu_management_routes/food/_varient_routes')

//Menu Mangement - Liquor Module
const brandRoutes = require('./_bizly_routes/_menu_management_routes/liquor/_brand_routes')
const drinkTypeRoutes = require('./_bizly_routes/_menu_management_routes/liquor/_drink_type_routes')
const attributeRoutes = require('./_bizly_routes/_menu_management_routes/liquor/_attribute_routes')
const attributeGroupRoutes = require('./_bizly_routes/_menu_management_routes/liquor/_attribute_group_routes')
const attributeSetRoutes = require('./_bizly_routes/_menu_management_routes/liquor/_attribute_set_routes')
const productRoutes = require('./_bizly_routes/_menu_management_routes/liquor/_product_routes')
const liquortreeRoutes = require('./_bizly_routes/_menu_management_routes/liquor/_tree_liquor_routes')

//Menu Mangement - Food_Inventory Module
const foodInventoryRoutes = require('./_bizly_routes/_menu_management_routes/inventory_food/_inventory_food_routes')
const ingredientInventoryRoutes = require('./_bizly_routes/_menu_management_routes/inventory_food/_inventory_ingredient_routes')
const brandInventoryRoutes = require('./_bizly_routes/_menu_management_routes/inventory_food/_inventory_brand_routes')
const itemInventoryRoutes = require('./_bizly_routes/_menu_management_routes/inventory_food/_inventory_item_routes')
const foodInventoryTreeRoutes = require('./_bizly_routes/_menu_management_routes/inventory_food/_inventory_food_tree')

//Menu Management - Assets - Inventory Module
const inventoryAssetsRoutes = require('./_bizly_routes/_menu_management_routes/_inventory_assets/_inventory_assets_route')
const equipmentRoutes = require('./_bizly_routes/_menu_management_routes/_inventory_assets/_equipment_route')
const assetsInventoryTreeRoutes = require('./_bizly_routes/_menu_management_routes/_inventory_assets/_inventory_assets_tree_routes')

//Menu Management - Beverages Module
const beveragesCategoryRoutes = require('./_bizly_routes/_menu_management_routes/beverages/_beverages_category_route')
const beverageBrandRoutes = require('./_bizly_routes/_menu_management_routes/beverages/_beverage_brand_route')
const beverageProductRoutes = require('./_bizly_routes/_menu_management_routes/beverages/_beverage_product_route')
const beverageRoutes = require('./_bizly_routes/_menu_management_routes/beverages/_beverages_route')
const inhouseBeveragesTreeRoutes = require('./_bizly_routes/_menu_management_routes/beverages/_inhouse_beverages_tree_routes')

//Menu Management - Retail Food
const retailFoodRoutes = require('./_bizly_routes/_menu_management_routes/retail_food/_retail_food_routes')
const retailFoodBrandRoutes = require('./_bizly_routes/_menu_management_routes/retail_food/_retail_food_brand_routes')
const retailFoodProductRoutes = require('./_bizly_routes/_menu_management_routes/retail_food/_retail_food_product_routes')

//Menu Management - Retail Beverage
const retailBeverageBrandRoutes = require('./_bizly_routes/_menu_management_routes/retail_beverage/_retail_beverage_brand_routes')
const retailBeverageProductRoutes = require('./_bizly_routes/_menu_management_routes/retail_beverage/_retail_beverage_product_routes')
const retailBeveragesTreeRoutes = require('./_bizly_routes/_menu_management_routes/retail_beverage/_retail_beverages_tree_routes')

//User adn Admin Module
const adminRoutes = require('./_bizly_routes/_auth_routes/_admin_auth_routes')
const userRoutes = require('./_bizly_routes/_auth_routes/_user_auth_routes')

// Notice Module
const noticeGroupRoutes = require('./_bizly_routes/_notice_routes/_notice_group_routes')
const noticeRoutes = require('./_bizly_routes/_notice_routes/_notice_routes')
const noticeTemplateRoutes = require('./_bizly_routes/_notice_routes/_notice_template_routes')
const noticeCreateTemplateFolder = require('./_bizly_routes/_notice_routes/_notice_create_template_Folder_routes')
const noticeCreateCircularFolder = require('./_bizly_routes/_notice_routes/_notice_create_circular_folder_routes')

// Charges Module
const chargesAddonRoutes = require('./_bizly_routes/_charges_routes/_charges_addon_routes')
const chargesIntegrationRoutes = require('./_bizly_routes/_charges_routes/_charges_integration_routes')
const chargesproductRoutes = require('./_bizly_routes/_charges_routes/_charges_product_routes')

//User Roles Module
const userRoleBizly = require('./_bizly_routes/_user_roles_routes/_user_role_bizly_routes')
const userRolePOS = require('./_bizly_routes/_user_roles_routes/_user_role_pos_routes')
const bizlytobizlyCategory = require('./_bizly_routes/_user_roles_routes/_bizly_to_bizly_category_routes')
const bizlyeditedhistoryRoute = require('./_bizly_routes/_user_roles_routes/_bizly_edited_history_route')
const posEditedHistoryRoute = require('./_bizly_routes/_user_roles_routes/_pos_edited_history_route')

//Organization
const organizationRoutes = require('./_bizly_routes/_organization_routes/_organization_routes')

//Outlets Chain
const chainRoutes = require('./_bizly_routes/_chain_routes/_chain_routes')
const chainCategoryRoutes = require('./_bizly_routes/_chain_routes/_chain_category_routes')

//cinema module
const cinemaRoutes = require('./_bizly_routes/_cinema_routes/_cinema_routes')

//Feedback module
const feedbackRoutes = require('./_bizly_routes/_feedback_routes/_feedback_routes')
const feedbackCategoryRoutes = require('./_bizly_routes/_feedback_routes/_feedback_category_routes')
const questionsetRoutes = require('./_bizly_routes/_feedback_routes/_QuestionSet_routes')
const templateRoutes = require('./_bizly_routes/_feedback_routes/_template_routes')
const assignedTemplateRoutes = require('./_bizly_routes/_feedback_routes/_assign_template_routes')
const feedbackResponseRoutes = require('./_bizly_routes/_feedback_routes/_feedback_response_routes')

//All Categories Tree
const allCategoriesTree = require('./_bizly_routes/_menu_management_routes/_All_Categories_tree/_all_categories_tree_routes')


/*
Date : 20 Aug 2020:12:15
Author: Mahesh Bodhgire
*/
//Employees Module
//BizlyRole Sub-Module
const bizlyemproleRoutes = require('./_bizly_routes/_employees_routes/_bizlyemprole_routes/_bizlyemprole_routes')
//Beverages AddonModule
const beveragesAddonRoutes = require('./_bizly_routes/_menu_management_routes/beverages/_beverages_addon_routes')
//Beverages VarientModule
const beveragesVarientRoutes = require('./_bizly_routes/_menu_management_routes/beverages/_beverages_varient_routes')
//Beverages VarientContentModule
const beveragesVarientContentRoutes = require('./_bizly_routes/_menu_management_routes/beverages/_beverages_varient_content_routes')


const buyPOSProductRoutes = require('./_bizly_routes/_pos_product/_buy_pos_product_routes')


//uer routes
const usersRoutes = require('./_bizly_routes/_user_routes/_user_routes')

//pos orders routes
const posOrdersRoutes = require('./_bizly_routes/_pos_orders_routes/_pos_orders_routes')

//mongodb connection
dbconfig.connectToDB

//Enable CORS
constants.APP.use(cors({ credentials: true, origin: true }))
constants.APP.use(function (req, res, next) {
    var allowedOrigins = ['http://localhost:1300', 'http://bizlypos.com:1300'];
    var origin = req.headers.origin;
    if (allowedOrigins.indexOf(origin) > -1) {
        res.setHeader('Access-Control-Allow-Origin', origin);
    }
    //res.header('Access-Control-Allow-Origin', 'http://127.0.0.1:8020');
    res.header('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
    res.header('Access-Control-Allow-Headers', 'origin,Content-Type, Authorization,X-Requested-With,Accept');
    res.header('Access-Control-Allow-Credentials', true);
    return next();
});

//Bodyparsing (JSON and Multi-Form)
constants.APP.use(bodyparser.json({ limit: '1000mb' }))
constants.APP.use(bodyparser.urlencoded({ limit: '1000mb', extended: true }))

//routing
//Admin and User
constants.APP.use('/admin', adminRoutes)
constants.APP.use('/user', userRoutes)

//Menu Management - Food
constants.APP.use('/cuisine', cuisineroutes)
constants.APP.use('/tag', tagroutes)
constants.APP.use('/category', categoryRoutes)
constants.APP.use('/varientcontent', varientContentRoutes)
constants.APP.use('/varient', varientRoutes)
constants.APP.use('/dish', dishRoutes)
constants.APP.use('/adon', adonRoutes)

//Menu Management - Liquor
constants.APP.use('/drinktype', drinkTypeRoutes)
constants.APP.use('/liquorbrand', brandRoutes)
constants.APP.use('/attribute', attributeRoutes)
constants.APP.use('/attributegroup', attributeGroupRoutes)
constants.APP.use('/attributeset', attributeSetRoutes)
constants.APP.use('/product', productRoutes)
constants.APP.use('/liquortree', liquortreeRoutes)

//Menu Management -Food Inventory
constants.APP.use('/fooodinventory', foodInventoryRoutes)
constants.APP.use('/ingredientinventory', ingredientInventoryRoutes)
constants.APP.use('/brandinventory', brandInventoryRoutes)
constants.APP.use('/iteminventory', itemInventoryRoutes)
constants.APP.use('/foodinventorytree', foodInventoryTreeRoutes)

//Menu Management - Inventory Assets
constants.APP.use('/inventoryassets', inventoryAssetsRoutes)
constants.APP.use('/equipment', equipmentRoutes)
constants.APP.use('/assetsinventorytree', assetsInventoryTreeRoutes)

//Menu Management - Beverages
constants.APP.use('/beveragescategory', beveragesCategoryRoutes)
constants.APP.use('/beveragebrand', beverageBrandRoutes)
constants.APP.use('/beverageproduct', beverageProductRoutes)
constants.APP.use('/beverage', beverageRoutes)
constants.APP.use('/inhousebeveragestree', inhouseBeveragesTreeRoutes)

//Menu Management - Retail Food
constants.APP.use('/retailfood', retailFoodRoutes)
constants.APP.use('/retailfoodbrand', retailFoodBrandRoutes)
constants.APP.use('/retailfoodproduct', retailFoodProductRoutes)

//Menu Management - Retail Beverage
constants.APP.use('/retailbeveragebrand', retailBeverageBrandRoutes)
constants.APP.use('/retailbeverageproduct', retailBeverageProductRoutes)
constants.APP.use('/reatailbeveragestree', retailBeveragesTreeRoutes)

//Restaurant
constants.APP.use('/restaurant', restoRoute)
constants.APP.use('/moreinfo', moreinfoRoutes)
constants.APP.use('/services', serviceRoutes)
constants.APP.use('/ruleandregulation', ruleAndRegulationRoutes)
constants.APP.use('/ambience', ambienceRoutes)
constants.APP.use('/establishment', establishmentRoutes)
constants.APP.use('/restaurantrevenue', restaurantrevenue)
constants.APP.use('/restaurantType', restaurantRoutes)
constants.APP.use('/restaurantLegal', restaurantLegalRoutes)
constants.APP.use('/selectedestablishment', selectedestablishmentRoutes)
constants.APP.use('/menu', menuRoutes)
constants.APP.use('/liquor', liquorRoutes)
constants.APP.use('/selectedmenu', selectedMenuRoutes)
constants.APP.use('/selectedliquor', selectedLiquorRoutes)
constants.APP.use('/mediaalbum', mediaAlbumRoutes)
constants.APP.use('/website', websiteRoutes)

// Notice Module
constants.APP.use('/noticegroup', noticeGroupRoutes)
constants.APP.use('/notice', noticeRoutes)
constants.APP.use('/noticetemplate', noticeTemplateRoutes)
constants.APP.use('/noticecreatetemplatefolder', noticeCreateTemplateFolder)
constants.APP.use('/noticecreatecircularfolder', noticeCreateCircularFolder)

// Charges Module
constants.APP.use('/chargesaddon', chargesAddonRoutes)
constants.APP.use('/chargesintegration', chargesIntegrationRoutes)
constants.APP.use('/chargesproduct', chargesproductRoutes)

//User Roles Module
constants.APP.use('/userrolebizly', userRoleBizly)
constants.APP.use('/userrolePOS', userRolePOS)
constants.APP.use('/b2bcategory', bizlytobizlyCategory)
constants.APP.use('/bizlyeditedhistory', bizlyeditedhistoryRoute)
constants.APP.use('/poseditedhistory', posEditedHistoryRoute)

//Organization
constants.APP.use('/organization', organizationRoutes)

//Outlets Outlets
constants.APP.use('/chain', chainRoutes)
constants.APP.use('/chaincategory', chainCategoryRoutes)

//Cinema module
constants.APP.use('/cinema', cinemaRoutes)

//feedback module
constants.APP.use('/feedback', feedbackRoutes)
constants.APP.use('/feedbackcategory', feedbackCategoryRoutes)
constants.APP.use('/questionset', questionsetRoutes)
constants.APP.use('/template', templateRoutes)
constants.APP.use('/assigntemplate', assignedTemplateRoutes)
constants.APP.use('/feedbackresponse', feedbackResponseRoutes)

//All Categories Tree
constants.APP.use('/allcategoriestree', allCategoriesTree)

/*
Date : 20 Aug 2020:12:15
Author: Mahesh Bodhgire
*/
//Employees Module
//BizlyRole Sub-Module
constants.APP.use('/bizlyemproles', bizlyemproleRoutes)
//Beverages AddOn Route
constants.APP.use('/beveragesaddon', beveragesAddonRoutes)
//Beverages Varient Route
constants.APP.use('/beveragesvarient', beveragesVarientRoutes)
//Beverages Varient Content Route
constants.APP.use('/beveragesvarientcontent', beveragesVarientContentRoutes)

constants.APP.use('/posproduct', buyPOSProductRoutes)

constants.APP.use('/bizuser', usersRoutes)

constants.APP.use('/posorders', posOrdersRoutes)

//Start Server
constants.APP.listen(constants.PORT, () => {
    console.log('Server Started on port : ' + constants.PORT)
})

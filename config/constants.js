const express = require('express')

module.exports = {
    APP: express(),
    PORT: 1300,
    DIR: './uploads',
}
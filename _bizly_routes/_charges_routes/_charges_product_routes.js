const express = require('express')
const router = express.Router()
const multer = require('multer')
const chargesProductCntrl = require('../../_bizly_controllers/_charges_contollers/_charges_product_controller')


const DIR_CHARGESPRODUCT = '/home/bizlypos/public_html/uploads/chargesproduct';
let StorageConfig_ChargesProduct = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, DIR_CHARGESPRODUCT);
    },
    filename: (req, file, cb) => {
        cb(null, Date.now() + file.originalname);
    }
});

let upload_Chargesproduct = multer({
    storage: StorageConfig_ChargesProduct
});

router.post('/addchargesproduct', chargesProductCntrl.addChargesProduct)
router.post('/importchargesproduct', chargesProductCntrl.importChargesProduct)
router.get('/getallchargesproduct', chargesProductCntrl.getAllChargesProducts)
router.get('/getchargesproduct/:productid', chargesProductCntrl.getChargesProduct)
router.get('/getcompletedata/:productname',chargesProductCntrl.getCompleteDataByName)
router.put('/updatechargesproduct/:productid', chargesProductCntrl.updateChargeProduct)
router.put('/deletechargesproduct/:productid', chargesProductCntrl.deleteChargesProduct)
router.put('/updateproductstatus/:id/:countryname', chargesProductCntrl.updateProductStatus)
router.post('/uploadphotos', upload_Chargesproduct.array('chargesproduct', 5), (req, res) => {
    var urlArray = []
    req.files.forEach(file => {
        urlArray.push('http://www.bizlypos.com/uploads/chargesproduct/' + file.filename)
    });
    // res.send(returnUrl);
    if (urlArray.length != 0) {
        res.json({ chargesproductPhotoUrls: urlArray })
    }
})

module.exports = router
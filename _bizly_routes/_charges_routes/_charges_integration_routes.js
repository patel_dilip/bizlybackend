const express = require('express')
const router = express.Router()
const multer = require('multer')
const chargesintegrationCntrl = require('../../_bizly_controllers/_charges_contollers/_charges_integration_controller')


const DIR_CHARGESINTEGRATION = '/home/bizlypos/public_html/uploads/chargesintegration';
let StorageConfig_Chargesintegration = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, DIR_CHARGESINTEGRATION);
    },
    filename: (req, file, cb) => {
        cb(null, Date.now() + file.originalname);
    }
});

let upload_Chargesintegration = multer({
    storage: StorageConfig_Chargesintegration
});

router.post('/addchargesintegration', chargesintegrationCntrl.addChargesIntegration)
router.post('/importchargesinteration', chargesintegrationCntrl.importChargesIntegration)
router.get('/getallchargesintegration', chargesintegrationCntrl.getAllChargesIntegrations)
router.get('/getchargesintegration/:integrationid', chargesintegrationCntrl.getChargesIntegration)
router.get('/getcompletedata/:integrationname',chargesintegrationCntrl.getCompleteDataByName)
router.get('/getchargesintegrationbycountryname/:countryname', chargesintegrationCntrl.getChargesIntegrationByCountryName)
router.put('/updatechargesintegration/:integrationid', chargesintegrationCntrl.updateChargeIntegration)
router.put('/deletechargesintegration/:integrationid', chargesintegrationCntrl.deleteChargesIntegration)
router.put('/updateintegrationstatus/:id/:countryname', chargesintegrationCntrl.updateIntegrationStatus)
router.post('/uploadphotos', upload_Chargesintegration.array('chargesintegration', 5), (req, res) => {
    var urlArray = []
    req.files.forEach(file => {
        urlArray.push('http://www.bizlypos.com/uploads/chargesintegration/' + file.filename)
    });
    // res.send(returnUrl);
    if (urlArray.length != 0) {
        res.json({ chargesintegrationPhotoUrls: urlArray })
    }
})

module.exports = router
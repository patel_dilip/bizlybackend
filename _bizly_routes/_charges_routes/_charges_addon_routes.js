const express = require('express')
const router = express.Router()
const multer = require('multer')
const chargesAddonCntrl = require('../../_bizly_controllers/_charges_contollers/_charges_addon_controller')


const DIR_CHARGESADONS = '/home/bizlypos/public_html/uploads/chargesadons';
let StorageConfig_ChargesAdon = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, DIR_CHARGESADONS);
    },
    filename: (req, file, cb) => {
        cb(null, Date.now() + file.originalname);
    }
});

let upload_Chargesadon = multer({
    storage: StorageConfig_ChargesAdon
});

router.post('/addchargesaddon', chargesAddonCntrl.addChargesAddon)
router.post('/importchargesaddon', chargesAddonCntrl.importChargesAddon)
router.get('/getallchargesaddon', chargesAddonCntrl.getAllChargesAddons)
router.get('/getchargesaddon/:addonid', chargesAddonCntrl.getChargesAddon)
router.get('/getchargesaddonsbycountry/:countryname', chargesAddonCntrl.getChargesAddonsBtCountry)
router.get('/getcompletedata/:addonname',chargesAddonCntrl.getCompleteDataByName)
router.put('/updatechargesaddon/:addonid', chargesAddonCntrl.updateChargeAddon)
router.put('/deletechargesaddon/:addonid', chargesAddonCntrl.deleteChargesAddon)
router.put('/updatechargesaddonstatus/:id/:countryname', chargesAddonCntrl.updateChargeAddonStatus)
router.post('/uploadphotos', upload_Chargesadon.array('chargesadons', 5), (req, res) => {
    var urlArray = []
    req.files.forEach(file => {
        urlArray.push('http://www.bizlypos.com/uploads/chargesadons/' + file.filename)
    });
    // res.send(returnUrl);
    if (urlArray.length != 0) {
        res.json({ chargesadonPhotoUrls: urlArray })
    }
})

module.exports = router
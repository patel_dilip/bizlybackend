const express = require('express')
const router = express.Router()
const organizationCntrl = require('../../_bizly_controllers/_organization_controllers/_organization_controller')

router.post('/addorganization', organizationCntrl.addOrganization)
router.put('/addorganizationoutlets/:organizationid', organizationCntrl.addOrganizationOutlets)
router.get('/getallorganization', organizationCntrl.getAllOrganizations)
router.get('/getorganization/:organizationid', organizationCntrl.getAllOrganizationById)
router.put('/updateorganization/:organizationid', organizationCntrl.updateOrganization)
router.put('/softdeleteorganization/:organizationid', organizationCntrl.softDeleteOrganization)
router.delete('/deleteorganization/:organizationid', organizationCntrl.deleteOrganization)
router.delete('/deleteallorganization', organizationCntrl.deleteAllOrganization)
module.exports = router
const express = require('express')
const router = express.Router()
const bizlyemproleCtrl = require('../../../_bizly_controllers/_employees_controller/_bizly_employees_controller/_bizly_employees_controller')

router.post('/addbizlyemprole', bizlyemproleCtrl.addBizlyRole)
router.get('/getallbizlyroles', bizlyemproleCtrl.getAllBizlyRoles)
router.get('/getbizlyrolebyid/:bizlyroleid', bizlyemproleCtrl.getBizlyRoleByid)
router.put('/updatebizlyrole/:bizlyroleid', bizlyemproleCtrl.updatebizlyrole)
router.put('/rolesoftdelete/:bizlyroleid', bizlyemproleCtrl.rolesoftdelete)
router.post('/deleterole/:bizlyroleid', bizlyemproleCtrl.deleteRole)
router.post('/deleteallroles', bizlyemproleCtrl.deleteAllRoles)
router.put('/addroletree', bizlyemproleCtrl.addTreeData)
router.get('/getroletree', bizlyemproleCtrl.getRoleTree)
module.exports = router
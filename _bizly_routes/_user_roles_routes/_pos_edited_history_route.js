const express = require('express')
const router = express.Router()
const poseditedhistoryCntrl = require('../../_bizly_controllers/_user_roles_controller/_pos_edited_history_controller')
const middleware = require('../../middleware/isAuthentic')

// router.use(middleware)
router.get('/getalllastroleupdatedhistory/:id', poseditedhistoryCntrl.getallLastUpdatedRole)
router.get('/getalllastuserupdatedhistory/:id', poseditedhistoryCntrl.getallLastUpdatedUsers)

module.exports = router
const express = require('express')
const router = express.Router()
const userroleCntrl = require('../../_bizly_controllers/_user_roles_controller/_user_role_bizly_controller')
const multer = require('multer')

//****************************************Corporate Details*********************************************************

const DIR_Corporate_joining_letter = '/home/bizlypos/public_html/uploads/User_Role_Bizly_to_bizly/Corporate_Details/Joining_letter';
let StorageConfig_Corporate_joining_letter = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, DIR_Corporate_joining_letter);
    },
    filename: (req, file, cb) => {
        cb(null, Date.now() + file.originalname);
    }
});

let upload_Corporate_joining_letter = multer({
    storage: StorageConfig_Corporate_joining_letter,
});

const DIR_Corporate_document_file = '/home/bizlypos/public_html/uploads/User_Role_Bizly_to_bizly/Corporate_Details/document_file';
let StorageConfig_Corporate_document_file = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, DIR_Corporate_document_file);
    },
    filename: (req, file, cb) => {
        cb(null, Date.now() + file.originalname);
    }
});

let upload_Corporate_document_file = multer({
    storage: StorageConfig_Corporate_document_file,
});

//******************************************* Legal Documents ********************************************************/

const DIR_Aadhar_card = '/home/bizlypos/public_html/uploads/User_Role_Bizly_to_bizly/Legal_Documents/aadhar_card';
let StorageConfig_Aadhar_card = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, DIR_Aadhar_card);
    },
    filename: (req, file, cb) => {
        cb(null, Date.now() + file.originalname);
    }
});

let upload_Aadhar_card = multer({
    storage: StorageConfig_Aadhar_card,
});


const DIR_document_file = '/home/bizlypos/public_html/uploads/User_Role_Bizly_to_bizly/Legal_Documents/document_file';
let StorageConfig_document_file = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, DIR_document_file);
    },
    filename: (req, file, cb) => {
        cb(null, Date.now() + file.originalname);
    }
});

let upload_document_file = multer({
    storage: StorageConfig_document_file,
});

const DIR_joining_letter = '/home/bizlypos/public_html/uploads/User_Role_Bizly_to_bizly/Legal_Documents/joining_letter';
let StorageConfig_joining_letter = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, DIR_joining_letter);
    },
    filename: (req, file, cb) => {
        cb(null, Date.now() + file.originalname);
    }
});

let upload_joining_letter = multer({
    storage: StorageConfig_joining_letter,
});

const DIR_licence = '/home/bizlypos/public_html/uploads/User_Role_Bizly_to_bizly/Legal_Documents/licence';
let StorageConfig_licence = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, DIR_licence);
    },
    filename: (req, file, cb) => {
        cb(null, Date.now() + file.originalname);
    }
});

let upload_licence = multer({
    storage: StorageConfig_licence,
});

const DIR_passport = '/home/bizlypos/public_html/uploads/User_Role_Bizly_to_bizly/Legal_Documents/passport';
let StorageConfig_passport = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, DIR_passport);
    },
    filename: (req, file, cb) => {
        cb(null, Date.now() + file.originalname);
    }
});

let upload_passport = multer({
    storage: StorageConfig_passport,
});
//***********************************************************************************************************************************/


const DIR_personal_details = '/home/bizlypos/public_html/uploads/User_Role_Bizly_to_bizly/personal_details';
let StorageConfig_personal_details = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, DIR_personal_details);
    },
    filename: (req, file, cb) => {
        cb(null, Date.now() + file.originalname);
    }
});

let upload_personal_details = multer({
    storage: StorageConfig_personal_details,
});

//**************************Work Details**********************************************************

const DIR_NDA_file = '/home/bizlypos/public_html/uploads/User_Role_Bizly_to_bizly/work_details/NDA_file';
let StorageConfig_NDA_file = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, DIR_NDA_file);
    },
    filename: (req, file, cb) => {
        cb(null, Date.now() + file.originalname);
    }
});

let upload_NDA_file = multer({
    storage: StorageConfig_NDA_file,
});

const DIR_work_joining_letter = '/home/bizlypos/public_html/uploads/User_Role_Bizly_to_bizly/work_details/joining_letter';
let StorageConfig_work_joining_letter = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, DIR_work_joining_letter);
    },
    filename: (req, file, cb) => {
        cb(null, Date.now() + file.originalname);
    }
});

let upload_work_joining_letter = multer({
    storage: StorageConfig_work_joining_letter,
});

//*****************************Bizly To Bizly Roles Routes *******************************************/

router.post('/addbizlyrole', userroleCntrl.addRoles)
router.get('/getallbizlyrole', userroleCntrl.getallRoles)
router.get('/getbizlyrole/:roleid', userroleCntrl.getRoleByID)
router.put('/updatebizlyrole/:roleid', userroleCntrl.updateRole)
router.put('/deletebizlyrole/:roleid', userroleCntrl.deleteRole)


//***************************Bizly To Bizly Users Routes *********************************************/

router.post('/addbizlyuserpersonaldetails', userroleCntrl.addUsersPersonalDetails)
router.put('/addbizlyuserworkdetails/:userid', userroleCntrl.addUserWorkDetails)
router.put('/addbizlyuserbankdetails/:userid', userroleCntrl.addUsersBankDetails)
router.put('/addbizlyuserlegaldocuments/:userid', userroleCntrl.addUsersLegalDocumentsDetails)
router.put('/addbizlyusercorporatedetails/:userid', userroleCntrl.addUsersCorporateDetails)
router.put('/addbizlyuserassetsdetails/:userid', userroleCntrl.addUsersAssetsDetails)
router.put('/addbizlyusercredentialdetails/:userid', userroleCntrl.addUsersCredentialDetails)
router.get('/getallbizlyusers', userroleCntrl.getallUsers)
router.get('/getbizlyuserbyid/:userid', userroleCntrl.getuserByID)
router.put('/updatebizlyuser/:userid', userroleCntrl.updateUser)
router.put('/deletebizlyuser/:userid', userroleCntrl.deleteUser)

//******************************************Corporate Details**********************************************************/
router.post('/uploadcorporatejoiningletter', upload_Corporate_joining_letter.single('corporatejoiningletter'), (req, res) => {

    res.send('http://www.bizlypos.com/uploads/User_Role_Bizly_to_bizly/Corporate_Details/Joining_letter/' + req.file.filename)
})

router.post('/uploadcorporatedocumentfile', upload_Corporate_document_file.single('corporatedocumentfile'), (req, res) => {

    res.send('http://www.bizlypos.com/uploads/User_Role_Bizly_to_bizly/Corporate_Details/document_file/' + req.file.filename)
})

//******************************************* Legal Documents ********************************************************/

router.post('/uploadaadharcard', upload_Aadhar_card.single('aadharcard'), (req, res) => {

    res.send('http://www.bizlypos.com/uploads/User_Role_Bizly_to_bizly/Legal_Documents/aadhar_card/' + req.file.filename)
})

router.post('/uploaddocumentfile', upload_document_file.single('documentfile'), (req, res) => {

    res.send('http://www.bizlypos.com/uploads/User_Role_Bizly_to_bizly/Legal_Documents/document_file/' + req.file.filename)
})

router.post('/uploadjoiningletter', upload_joining_letter.single('joiningletter'), (req, res) => {

    res.send('http://www.bizlypos.com/uploads/User_Role_Bizly_to_bizly/Legal_Documents/joining_letter/' + req.file.filename)
})

router.post('/uploadlicence', upload_licence.single('licence'), (req, res) => {

    res.send('http://www.bizlypos.com/uploads/User_Role_Bizly_to_bizly/Legal_Documents/licence/' + req.file.filename)
})

router.post('/uploadpassport', upload_passport.single('passport'), (req, res) => {

    res.send('http://www.bizlypos.com/uploads/User_Role_Bizly_to_bizly/Legal_Documents/passport/' + req.file.filename)
})

//*********************************************************************************************************************/

router.post('/uploadpersonaldetails', upload_personal_details.single('personaldetails'), (req, res) => {

    res.send('http://www.bizlypos.com/uploads/User_Role_Bizly_to_bizly/personal_details/' + req.file.filename)
})

//*********************************Work Details******************************************************************// */

router.post('/uploadworkdetailsNDAfile', upload_NDA_file.single('workdetailsNDAfile'), (req, res) => {

    res.send('http://www.bizlypos.com/uploads/User_Role_Bizly_to_bizly/work_details/NDA_file/' + req.file.filename)
})

router.post('/uploadworkdetailsjoiningletter', upload_work_joining_letter.single('workdetailsjoiningletter'), (req, res) => {

    res.send('http://www.bizlypos.com/uploads/User_Role_Bizly_to_bizly/work_details/joining_letter/' + req.file.filename)
})

module.exports = router
const express = require('express')
const router = express.Router()
const roleHistoryCntrl = require('../../_bizly_controllers/_user_roles_controller/_bizly_edited_history_controller')
const middleware = require('../../middleware/isAuthentic')

// router.use(middleware)
router.get('/getalllastupdatedrole/:id', roleHistoryCntrl.getallLastUpdatedRole)
router.get('/getalllastupdatedusers/:id', roleHistoryCntrl.getallLastUpdatedUsers)

module.exports = router
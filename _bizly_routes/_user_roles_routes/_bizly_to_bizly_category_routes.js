const express = require('express')
const router = express.Router()
const categoryCntrl = require('../../_bizly_controllers/_user_roles_controller/_bizly_to_bizly_category_controller')

router.post('/addrootparent', categoryCntrl.addRootParent)
router.put('/addchildern/:rootparentid', categoryCntrl.addChildern)
router.put('/addchildern2/:rootparentid/:childernid', categoryCntrl.addChildern2)
router.put('/addchildern3/:rootparentid/:childern2id', categoryCntrl.addChildern3)
router.put('/addchildern4/:rootparentid/:childern2id/:childern3id', categoryCntrl.addChildern4)
router.put('/addchildernlast/:rootparentid/:childern2id/:childern3id/:childern4id', categoryCntrl.addChildernLast)
router.get('/getcategories', categoryCntrl.getALLCategories)
router.delete('/deleteallcategories', categoryCntrl.deleteAllCategory)

module.exports = router
const express = require('express')
const router = express.Router()
const posOrdersCtrl = require('../../_bizly_controllers/_pos_orders/_pos_product_orders_controller')
// const middleware = require('../../../middleware/isAuthentic')

// router.use(middleware)
router.post('/addposorder', posOrdersCtrl.addPosOrder)
router.get('/getallposproductorders', posOrdersCtrl.getAllPOSProductOrders)
router.get('/getposproductorderbyid/:posproductorderid', posOrdersCtrl.getPOSProductOrderById)
router.put('/updateposproductorderbyid/:posproductorderid', posOrdersCtrl.updatePOSProductOrderByID)
router.put('/softdeleteposproductorder/:posproductorderid', posOrdersCtrl.softDeletePOSProductOrder)
router.delete('/deleteposproductorder/:posproductorderid', posOrdersCtrl.deletePOSProductOrder)
router.delete('/deleteallposproductorders', posOrdersCtrl.deleteAllPOSProductOrders)
module.exports = router
const express = require('express')
const router = express.Router()
const posOrdersCtrl = require('../../_bizly_controllers/_pos_orders/_pos_orders_controller')
// const middleware = require('../../../middleware/isAuthentic')

// router.use(middleware)
//POS Product Orders
router.post('/addposproductorder', posOrdersCtrl.addPOSProductOrder)
router.get('/getallposproductorders', posOrdersCtrl.getAllPOSProductOrders)
router.get('/getposproductorderbyid/:posproductorderid', posOrdersCtrl.getPOSProductOrderById)
router.put('/updateposproductorder/:posorderid/:posproductorderid', posOrdersCtrl.updatePOSProductOrderByID)

// router.get('/getposproducttotaloutlets', posOrdersCtrl.getTotalPOSProductOutlets)
//POS Addon Orders
router.put('/addposaddonorders/:posorderid', posOrdersCtrl.addPOSAddonOrder)
router.get('/getallposaddonorders', posOrdersCtrl.getAllPOSAddonOrders)
router.get('/getposaddonorderbyid/:posorderid', posOrdersCtrl.getPOSAddonOrderByID)
router.put('/updateposaddonorder/:posorderid/:posaddonorderid', posOrdersCtrl.updatePOSAddonOrderByID)
//POS Integration Orders
router.put('/addposintegrationorders/:posorderid', posOrdersCtrl.addPOSIntegrationOrder)
router.get('/getallposintegrationorders', posOrdersCtrl.getAllPOSIntegrationOrders)
router.get('/getposintegrationorderbyid/:posorderid', posOrdersCtrl.getPOSIntegrationOrderByID)
router.put('/updateposintegrationorder/:posorderid/:posintegrationorderid', posOrdersCtrl.updatePOSIntegrationOrderByID)
//POS Order Stats
router.get('/getposorderstats', posOrdersCtrl.getPOSOrderStats)
//POS PaymentLink
router.post('/addpospaymentlink', posOrdersCtrl.addPOSPaymentLink)
router.get('/getallpospaymentlinks', posOrdersCtrl.getAllPOSPaymentLinks)
router.get('/getPOSPaymentLink/:pospaymentlinkid',posOrdersCtrl.getPOSPaymentLink)
module.exports = router
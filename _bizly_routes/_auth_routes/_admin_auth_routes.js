const express = require('express')
const router = express.Router()
const adminAuthCtrl = require('../../_bizly_controllers/_auth_controllers/_admin_auth_controller')

router.post('/adminsignup', adminAuthCtrl.adminSignup)
router.post('/signin', adminAuthCtrl.Signin)
router.get('/getalladmins', adminAuthCtrl.getAllAdmins)
module.exports = router
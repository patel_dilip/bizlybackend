const express = require('express')
const router = express.Router()
const userCtrl = require('../../_bizly_controllers/_auth_controllers/_user_auth_controller')

router.post('/addUser', userCtrl.addUser)
router.get('/getSingleuser/:userid', userCtrl.getSingleuser)
router.get('/getAlluser', userCtrl.getAlluser)
router.put('/updateUser/:userid', userCtrl.updateUser)
router.delete('/deleteUser/:userid', userCtrl.deleteUser)
router.put('/changepassword/:userid', userCtrl.changePassword)
// router.post('/usersignin', userCtrl.usersignin)
module.exports = router
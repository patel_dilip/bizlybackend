const express = require('express')
const router = express.Router()
const chainCategoryCntrl = require('../../_bizly_controllers/_chain_controllers/_chain_category_controller')
const middleware = require('../../middleware/isAuthentic')

// router.use(middleware)
router.post('/addrootcategory', chainCategoryCntrl.addRootCategory)
router.put('/addcategory/:rootcategoryid', chainCategoryCntrl.addCategory)
router.get('/getallcategory', chainCategoryCntrl.getAllRootCategory)
router.put('/updaterootcategoryname/:rootcategoryid', chainCategoryCntrl.updateRootCategoryName)
router.put('/updatecategoryname/:rootcategoryid/:categoresId', chainCategoryCntrl.updateCategoryName)

module.exports = router
const express = require('express')
const router = express.Router()
const chainCntrl = require('../../_bizly_controllers/_chain_controllers/_chain_controller')
const multer = require('multer')
const middleware = require('../../middleware/isAuthentic')

const DIR_chain_logo = '/home/bizlypos/public_html/uploads/outlet_chain/chain_logo';
const DIR_chain_pictures = '/home/bizlypos/public_html/uploads/outlet_chain/chain_pictures';
const DIR_chain_videos = '/home/bizlypos/public_html/uploads/outlet_chain/chain_videos';

//***********************Chain Logo***************************************
let StorageConfig_chain_logo = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, DIR_chain_logo);
    },
    filename: (req, file, cb) => {
        cb(null, Date.now() + file.originalname);
    }
});
let upload_chain_logo = multer({
    storage: StorageConfig_chain_logo,
});

//***********************chain pictures***************************************
let StorageConfig_chain_pictures = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, DIR_chain_pictures);
    },
    filename: (req, file, cb) => {
        cb(null, Date.now() + file.originalname);
    }
});
let upload_chain_pictures = multer({
    storage: StorageConfig_chain_pictures,
});


//***********************chain_videos***************************************
let StorageConfig_chain_videos = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, DIR_chain_videos);
    },
    filename: (req, file, cb) => {
        cb(null, Date.now() + file.originalname);
    }
});
let upload_chain_videos = multer({
    storage: StorageConfig_chain_videos,
});

// router.use(middleware)
router.post('/addchaininfo', chainCntrl.addChainInfo)
router.get('/getallchains', chainCntrl.getAllChains)
router.get('/getchain/:chainid', chainCntrl.getChainById)
router.put('/updatemediainfo/:chainid', chainCntrl.updateMediaInfo)
router.put('/updateoutlets/:chainid', chainCntrl.updateOutlets)
router.put('/updatemoreinfo/:chainid', chainCntrl.updateestablishmentMoreInfo)
router.put('/updateServices/:chainid', chainCntrl.updateestablishmentServices)
router.put('/updaterules/:chainid', chainCntrl.updateestablishmentRules)
router.put('/updateambience/:chainid', chainCntrl.updateestablishmentAmbience)
router.put('/updatecuisine/:chainid', chainCntrl.updateestablishmentCuisines)
router.put('/updatechaininfo/:chainid', chainCntrl.updateChainInfo)
router.delete('/deletechain/:chainid', chainCntrl.deleteChain)
router.delete('/deleteallchain', chainCntrl.deleteAllChain)

router.post('/uploadchainlogo', upload_chain_logo.single('chainlogo'), (req, res) => {
    res.send('http://www.bizlypos.com/uploads/outlet_chain/chain_logo/' + req.file.filename)
})

router.post('/uploadchainpicture', upload_chain_pictures.single('chainpicture'), (req, res) => {
    res.send('http://www.bizlypos.com/uploads/outlet_chain/chain_pictures/' + req.file.filename)
})

router.post('/uploadchainvideo', upload_chain_videos.single('chainvideo'), (req, res) => {
    res.send('http://www.bizlypos.com/uploads/outlet_chain/chain_videos/' + req.file.filename)
})
module.exports = router
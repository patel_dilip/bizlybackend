const express = require('express')
const router = express.Router()
const mediaCntrl = require('../../_bizly_controllers/_restaurants_controllers/mediaAlbum-controller')

router.post('/addmediaalbum', mediaCntrl.addmediaAlbum)
router.get('/getallmediaalbums', mediaCntrl.getAllMediaAlbums)
router.get('/getmediaalbum/:mediaid', mediaCntrl.getMediaAlbumbyID)
router.put('/updatemediaalbum/:mediaid', mediaCntrl.updateMediaAlbum)
router.delete('/deletemediaalbum/:mediaid', mediaCntrl.deleteMediaAlbum)

module.exports = router
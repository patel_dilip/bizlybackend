
const express = require('express')
const router = express.Router()
const restoRevenueCtrl = require('../../_bizly_controllers/_restaurants_controllers/restaurant-revenue-controller')

router.post('/addrestaurantrevenue', restoRevenueCtrl.addRestoRevenue)
router.get('/getrestaurantrevenue/:restorevenueid', restoRevenueCtrl.getRestoRevenue)
router.get('/getallrestaurantrevenues', restoRevenueCtrl.getAllRestoRevenues)
router.put('/updaterestaurantrevenue/:restorevenueid', restoRevenueCtrl.updateRestoRevenue)
router.delete('/deleterestaurantrevenue/:restorevenueid', restoRevenueCtrl.deleteRestoRevenue)
router.delete('/deleteallrestaurantrevenue', restoRevenueCtrl.deleteAllRestoRevenue)

module.exports = router
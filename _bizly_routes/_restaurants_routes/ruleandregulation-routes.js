const express = require('express')
const router = express.Router()
const ruleAndRegulationCtrl = require('../../_bizly_controllers/_restaurants_controllers/rulesandregulations-controller')

router.post('/addruleandregulation',ruleAndRegulationCtrl.addRuleAndRegulation)
router.get('/getruleandregulation/:ruleandregulationid',ruleAndRegulationCtrl.getRuleAndRegulation)
router.get('/getallruleandregulations', ruleAndRegulationCtrl.getAllRulesAndRegulations)
router.put('/updateruleandregulation/:ruleandregulationid', ruleAndRegulationCtrl.updateRuleAndRegulation)
router.delete('/deleteruleandregulationbyid/:ruleandregulationid', ruleAndRegulationCtrl.deleteRuleAndRegulationById)
router.delete('/deleteallruleandregulation', ruleAndRegulationCtrl.deleteALLRuleAndRegulation)
module.exports=router
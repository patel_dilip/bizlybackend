const express = require('express')
const router = express.Router()
const restaurantLegalCtrl = require('../../_bizly_controllers/_restaurants_controllers/restaurant-legal-controller')
const multer = require('multer')
var appRoot = require('app-root-path');


// const DIR_GSTFILE = appRoot + '/uploads/cuisine';
// const DIR_PANFILE = appRoot + '/uploads/cuisine';
// const DIR_FSSAIFILE = appRoot + '/uploads/cuisine';
// const DIR_DOCUMENTS = appRoot + '/uploads/cuisine';
const DIR_GSTFILE = '/home/bizlypos/public_html/uploads/gstfiles';
const DIR_PANFILE = '/home/bizlypos/public_html/uploads/panfiles';
const DIR_FSSAIFILE = '/home/bizlypos/public_html/uploads/fssaifiles';
const DIR_DOCUMENTS = '/home/bizlypos/public_html/uploads/otherdocuments';
const DIR_NOC = '/home/bizlypos/public_html/uploads/noc';
const DIR_Liquor_License = '/home/bizlypos/public_html/uploads/liquorlicense';
const DIR_Eating_House_License = '/home/bizlypos/public_html/uploads/eatinghouselicense';
const DIR_Music_License = '/home/bizlypos/public_html/uploads/Musiclicense';
const DIR_Trademark_for_Cafe_License = '/home/bizlypos/public_html/uploads/TrademarkforCafeLicense';
const DIR_CEC = '/home/bizlypos/public_html/uploads/CEC';
const DIR_Shops_and_Establishment_Act = '/home/bizlypos/public_html/uploads/ShopsandEstablishmentAct';
const DIR_Signage_License = '/home/bizlypos/public_html/uploads/Signagelicense';
const DIR_Lift_Clearance = '/home/bizlypos/public_html/uploads/LiftClearance';
const DIR_Health_Trade_License = '/home/bizlypos/public_html/uploads/HealthTradeLicense';

//GST
let StorageConfig_GSTFile = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, DIR_GSTFILE);
    },
    filename: (req, file, cb) => {
        cb(null, Date.now() + file.originalname);
    }
});

//PAN
let StorageConfig_PANFile = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, DIR_PANFILE);
    },
    filename: (req, file, cb) => {
        cb(null, Date.now() + file.originalname);
    }
});

//FSSAI
let StorageConfig_FSSAIFile = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, DIR_FSSAIFILE);
    },
    filename: (req, file, cb) => {
        cb(null, Date.now() + file.originalname);
    }
});

//OTHER
let StorageConfig_OTHERFile = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, DIR_DOCUMENTS);
    },
    filename: (req, file, cb) => {
        cb(null, Date.now() + file.originalname);
    }
});

//NOC
let StorageConfig_NOC = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, DIR_NOC);
    },
    filename: (req, file, cb) => {
        cb(null, Date.now() + file.originalname);
    }
});

//Liquor_License
let StorageConfig_Liquor_License = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, DIR_Liquor_License);
    },
    filename: (req, file, cb) => {
        cb(null, Date.now() + file.originalname);
    }
});

//Eating_House_License
let StorageConfig_Eating_House_License = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, DIR_Eating_House_License);
    },
    filename: (req, file, cb) => {
        cb(null, Date.now() + file.originalname);
    }
});

//Music_License
let StorageConfig_Music_License = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, DIR_Music_License);
    },
    filename: (req, file, cb) => {
        cb(null, Date.now() + file.originalname);
    }
});

//Trademark_for_Cafe_License
let StorageConfig_Trademark_for_Cafe_License = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, DIR_Trademark_for_Cafe_License);
    },
    filename: (req, file, cb) => {
        cb(null, Date.now() + file.originalname);
    }
});

//CEC
let StorageConfig_CEC = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, DIR_CEC);
    },
    filename: (req, file, cb) => {
        cb(null, Date.now() + file.originalname);
    }
});

//Shops_and_Establishment_Act
let StorageConfig_Shops_and_Establishment_Act = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, DIR_Shops_and_Establishment_Act);
    },
    filename: (req, file, cb) => {
        cb(null, Date.now() + file.originalname);
    }
});

//Signage_License
let StorageConfig_Signage_License = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, DIR_Signage_License);
    },
    filename: (req, file, cb) => {
        cb(null, Date.now() + file.originalname);
    }
});

//Lift_Clearance
let StorageConfig_Lift_Clearance = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, DIR_Lift_Clearance);
    },
    filename: (req, file, cb) => {
        cb(null, Date.now() + file.originalname);
    }
});

//Health_Trade_License
let StorageConfig_Health_Trade_License = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, DIR_Health_Trade_License);
    },
    filename: (req, file, cb) => {
        cb(null, Date.now() + file.originalname);
    }
});

let upload_gstfile = multer({
    storage: StorageConfig_GSTFile,
});

let upload_panfile = multer({
    storage: StorageConfig_PANFile,
});

let upload_fssaifile = multer({
    storage: StorageConfig_FSSAIFile,
});

let upload_otherfile = multer({
    storage: StorageConfig_OTHERFile,
});

let upload_NOC = multer({
    storage: StorageConfig_NOC,
});

let upload_Liquor_License = multer({
    storage: StorageConfig_Liquor_License,
});

let upload_Eating_House_License = multer({
    storage: StorageConfig_Eating_House_License,
});

let upload_Music_License = multer({
    storage: StorageConfig_Music_License,
});

let upload_Trademark_for_Cafe_License = multer({
    storage: StorageConfig_Trademark_for_Cafe_License,
});

let upload_CEC = multer({
    storage: StorageConfig_CEC,
});

let upload_Shops_and_Establishment_Act = multer({
    storage: StorageConfig_Shops_and_Establishment_Act,
});

let upload_Signage_License = multer({
    storage: StorageConfig_Signage_License,
});

let upload_Lift_Clearance = multer({
    storage: StorageConfig_Lift_Clearance,
});

let upload_Health_Trade_License = multer({
    storage: StorageConfig_Health_Trade_License,
});

router.post('/addRestaurantLegal', restaurantLegalCtrl.addRestaurantLegal)
router.get('/getRestaurantLegal/:restaurantLegaleid', restaurantLegalCtrl.getRestaurantLegal)
router.get('/getAllRestaurantLegal', restaurantLegalCtrl.getAllRestaurantLegal)
router.put('/updateRestaurantLegal/:restaurantLegaleid', restaurantLegalCtrl.updateRestaurantLegal)
router.delete('/deleteRestaurantLegal/:restaurantLegaleid', restaurantLegalCtrl.deleteRestaurantLegal)
router.post('/deleterestaurantgstfile', restaurantLegalCtrl.deleteRestaurantGSTfiles)
router.post('/deleterestaurantpanfile', restaurantLegalCtrl.deleteRestaurantPANFiles)
router.post('/deleterestaurantfssaifile', restaurantLegalCtrl.deleteRestaurantFSSAIIFiles)
router.post('/deleterestaurantotherdocumentfile', restaurantLegalCtrl.deleteRestaurantOtherDocument)
router.post('/deleterestaurantnocfile', restaurantLegalCtrl.deleteRestaurantNOCFile)
router.post('/deleterestaurantliquorlicensefile', restaurantLegalCtrl.deleteRestaurantLiquorLicenseFile)
router.post('/deleterestauranteatinghouselicensefile', restaurantLegalCtrl.deleteRestaurantEatingHouseLicenseFile)
router.post('/deleterestaurantmusiclicensefile', restaurantLegalCtrl.deleteRestaurantMusicLicenseFile)
router.post('/deleterestauranttrademarkforcafelicensefile', restaurantLegalCtrl.deleteRestaurantTrademarkForCafeLicenseFile)
router.post('/deleterestaurantcecfile', restaurantLegalCtrl.deleteRestaurantCECLicenseFile)
router.post('/deleterestaurantshopandestablishmentactfile', restaurantLegalCtrl.deleteRestaurantShopandEstablishmentActFile)
router.post('/deleterestaurantsignagelicensefile', restaurantLegalCtrl.deleteRestaurantSignageFile)
router.post('/deleterestaurantlifeclerancefile', restaurantLegalCtrl.deleteRestaurantLiftClearanceFile)
router.post('/deleterestauranthealthtradelicensefile', restaurantLegalCtrl.deleteRestaurantHealthTradeLicenseFile)
router.post('/uploadgst', upload_gstfile.single('gstfile'), (req, res) => {
    res.send('http://www.bizlypos.com/uploads/gstfiles/' + req.file.filename)
})

router.post('/uploadpan', upload_panfile.single('panfile'), (req, res) => {
    res.send('http://www.bizlypos.com/uploads/panfiles/' + req.file.filename)
})

router.post('/uploadfssai', upload_fssaifile.single('fssaifile'), (req, res) => {
    res.send('http://www.bizlypos.com/uploads/fssaifiles/' + req.file.filename)
})

router.post('/uploadother', upload_otherfile.single('otherfile'), (req, res) => {
    res.send('http://www.bizlypos.com/uploads/otherdocuments/' + req.file.filename)
})

router.post('/uploadnoc', upload_NOC.single('noc'), (req, res) => {
    res.send('http://www.bizlypos.com/uploads/noc/' + req.file.filename)
})

router.post('/uploadliquorlicense', upload_Liquor_License.single('liquorlicense'), (req, res) => {
    res.send('http://www.bizlypos.com/uploads/liquorlicense/' + req.file.filename)
})

router.post('/uploadeatinghouselicense', upload_Eating_House_License.single('eatinghouselicense'), (req, res) => {
    res.send('http://www.bizlypos.com/uploads/eatinghouselicense/' + req.file.filename)
})

router.post('/uploadmusiclicense', upload_Music_License.single('musiclicense'), (req, res) => {
    res.send('http://www.bizlypos.com/uploads/Musiclicense/' + req.file.filename)
})

router.post('/uploadtrademarkforcafelicense', upload_Trademark_for_Cafe_License.single('trademarkforcafelicense'), (req, res) => {
    res.send('http://www.bizlypos.com/uploads/TrademarkforCafeLicense/' + req.file.filename)
})

router.post('/uploadcec', upload_CEC.single('cec'), (req, res) => {
    res.send('http://www.bizlypos.com/uploads/CEC/' + req.file.filename)
})

router.post('/uploadshopsandestablishmentact', upload_Shops_and_Establishment_Act.single('shopsandestablishmentact'), (req, res) => {
    res.send('http://www.bizlypos.com/uploads/ShopsandEstablishmentAct/' + req.file.filename)
})

router.post('/uploadsignagelicense', upload_Signage_License.single('signagelicense'), (req, res) => {
    res.send('http://www.bizlypos.com/uploads/Signagelicense/' + req.file.filename)
})

router.post('/uploadliftclearance', upload_Lift_Clearance.single('liftclearance'), (req, res) => {
    res.send('http://www.bizlypos.com/uploads/LiftClearance/' + req.file.filename)
})

router.post('/uploadhealthtradelicense', upload_Health_Trade_License.single('healthtradelicense'), (req, res) => {
    res.send('http://www.bizlypos.com/uploads/HealthTradeLicense/' + req.file.filename)
})

module.exports = router
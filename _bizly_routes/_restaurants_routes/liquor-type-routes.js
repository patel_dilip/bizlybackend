const express = require('express')
const router = express.Router()
const liquorTypeCtrl = require('../../_bizly_controllers/_restaurants_controllers/liquor-type-controller')

router.post('/addliquortyperootcategory', liquorTypeCtrl.addRootDrinkName)
router.put('/addliquortypecategories/:rootDrinkId', liquorTypeCtrl.addVarients)
router.put('/addliquortypechildcategories/:rootDrinkId/:varientId', liquorTypeCtrl.addSubVarients)
router.put('/addliquortypechildchildcategories/:rootDrinkId/:subvarientid', liquorTypeCtrl.addSubSubVarients)
router.get('/getallliquortypes', liquorTypeCtrl.getAllLiquortypes)
router.get('/getliquortype/:liquortypeid', liquorTypeCtrl.getLiquortype)
router.put('/updaterootdrinktypename/:rootCategoryId', liquorTypeCtrl.updateRootDrinkName)
router.put('/updatecategoryname/:rootCategoryId/:categoresId', liquorTypeCtrl.updateCategoryName)
router.put('/updatechildcategoriesname/:rootCategoryId/:childcategoryid', liquorTypeCtrl.updateChildCategoriesName)
router.put('/updatechildchildcategoryname/:rootCategoryId/:childcategoryid/:childChildCategoryid', liquorTypeCtrl.updateChildChildCategoryName)

module.exports = router
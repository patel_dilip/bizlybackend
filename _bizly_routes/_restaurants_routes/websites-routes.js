const express = require('express')
const router = express.Router()
const websiteCntrl = require('../../_bizly_controllers/_restaurants_controllers/websites-controller')

router.post('/addwebsite',websiteCntrl.addWebsite)
router.put('/updatewebsite/:websiteid',websiteCntrl.updateWebsites)

module.exports=router
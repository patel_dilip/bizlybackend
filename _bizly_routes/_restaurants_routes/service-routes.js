const express = require('express')
const router = express.Router()
const serviceCtrl = require('../../_bizly_controllers/_restaurants_controllers/service-controller')

router.post('/addservice', serviceCtrl.addService)
router.get('/getservice/:serviceId', serviceCtrl.getService)
router.get('/getallservies', serviceCtrl.getAllServices)
router.put('/updateservice/:serviceId', serviceCtrl.updateService)
router.delete('/deleteservice/:serviceId', serviceCtrl.deleteServiceById)
router.delete('/deleteAllservice', serviceCtrl.deleteALLService)
module.exports = router
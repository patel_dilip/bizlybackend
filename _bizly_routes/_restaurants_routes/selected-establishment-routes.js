const express = require('express')
const router = express.Router()
const selected_establishmentCtrl = require('../../_bizly_controllers/_restaurants_controllers/selected-establishment-controller')

router.post('/addselectedestablishmentrootcategory',selected_establishmentCtrl.addRootCategory)
router.put('/addselectedestablishmentcategories/:rootCategoryId', selected_establishmentCtrl.addCategories)
router.put('/addselectedestablishmentchildcategories/:rootCategoryId/:categoresId', selected_establishmentCtrl.addChildCategories)
router.put('/addselectedestablishmentchildchildcategories/:rootCategoryId/:childcategoryid', selected_establishmentCtrl.addChildChildCategories)
router.get('/getallselectedestablishments', selected_establishmentCtrl.getAllEstablishments)
router.get('/getselectedestablishment/:establishmentid', selected_establishmentCtrl.getEstablishment)
router.delete('/deleteselectedestablishment/:establishmentid',selected_establishmentCtrl.deleteSelectedEstablishment)
module.exports=router
const express = require('express')
const router = express.Router()
const menuCntrl = require('../../_bizly_controllers/_restaurants_controllers/menu-controller')

router.post('/addmenurootcategory', menuCntrl.addMenu)
router.put('/addmenucategories/:rootCategoryId', menuCntrl.addCategories)
router.put('/addmenuchildcategories/:rootCategoryId/:categoresId', menuCntrl.addChildCategories)
router.put('/addmenuchildchildcategories/:rootCategoryId/:childcategoryid', menuCntrl.addChildChildCategories)
router.get('/getallmenus', menuCntrl.getAllMenus)
router.get('/getmenu/:menuid', menuCntrl.getMenu)
router.put('/updaterootcategory/:rootCategoryId', menuCntrl.updateRootCategoryName)
router.put('/updatecategoryname/:rootCategoryId/:categoresId', menuCntrl.updateCategoryName)
router.put('/updatechildcategoriesname/:rootCategoryId/:childcategoryid', menuCntrl.updateChildCategoriesName)
router.put('/updatechildchildcategoryname/:rootCategoryId/:childcategoryid/:childChildCategoryid', menuCntrl.updateChildChildCategoryName)

module.exports = router
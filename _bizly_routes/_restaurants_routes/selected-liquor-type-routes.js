const express = require('express')
const router = express.Router()
const selected_liquor_Ctrl = require('../../_bizly_controllers/_restaurants_controllers/selected-liquor-type-controller')

router.post('/addselectedliquorrootcategory',selected_liquor_Ctrl.addRootCategory)
router.put('/addselectedliquorcategories/:rootCategoryId', selected_liquor_Ctrl.addCategories)
router.put('/addselectedliquorchildcategories/:rootCategoryId/:categoresId', selected_liquor_Ctrl.addChildCategories)
router.put('/addselectedliquorchildchildcategories/:rootCategoryId/:childcategoryid', selected_liquor_Ctrl.addChildChildCategories)
router.get('/getallselectedliquors', selected_liquor_Ctrl.getAllSelectedLiquortype)
router.get('/getselectedliquor/:liquorid', selected_liquor_Ctrl.getSelectedLiquretype)
router.delete('/deleteselectedliquor/:liquorid',selected_liquor_Ctrl.deleteSelectedLiquortype)
module.exports=router
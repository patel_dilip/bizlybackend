const express = require('express')
const router = express.Router()
const ambienceCtrl = require('../../_bizly_controllers/_restaurants_controllers/ambience-controller')

router.post('/addambience', ambienceCtrl.addAmbience)
router.get('/getambience/:ambienceid', ambienceCtrl.getAmbience)
router.get('/getallambiences', ambienceCtrl.getAllAmbiences)
router.put('/updateambience/:ambienceid', ambienceCtrl.updateAmbience)
router.delete('/deleteambience/:ambienceid', ambienceCtrl.deleteAmbienceById)
router.delete('/deleteallambience', ambienceCtrl.deleteALLAmbience)
module.exports = router
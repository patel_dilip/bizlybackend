const express = require('express')
const router = express.Router()
const restaurantTypeCtrl = require('../../_bizly_controllers/_restaurants_controllers/restaurantType-controller')

router.post('/addRestaurantType',restaurantTypeCtrl.addRestaurantType)
router.get('/getRestaurantType/:restaurantTypeid',restaurantTypeCtrl.getRestaurantType)
router.get('/getAllrestaurantType',restaurantTypeCtrl.getAllrestaurantType)
router.put('/updateRestaurantType/:restaurantTypeid',restaurantTypeCtrl.updateRestaurantType)
router.delete('/deleteRestaurantType/:restaurantTypeid',restaurantTypeCtrl.deleteRestaurantType)

module.exports = router
const express = require('express')
const router = express.Router()
const selected_menu_Ctrl = require('../../_bizly_controllers/_restaurants_controllers/selected-menu-controller')

router.post('/addselectedmenurootcategory',selected_menu_Ctrl.addRootCategory)
router.put('/addselectedmenucategories/:rootCategoryId', selected_menu_Ctrl.addCategories)
router.put('/addselectedmenuchildcategories/:rootCategoryId/:categoresId', selected_menu_Ctrl.addChildCategories)
router.put('/addselectedmenuchildchildcategories/:rootCategoryId/:childcategoryid', selected_menu_Ctrl.addChildChildCategories)
router.get('/getallselectedmenus', selected_menu_Ctrl.getAllSelectedMenus)
router.get('/getselectedmenu/:menuid', selected_menu_Ctrl.getSelectedMenu)
router.delete('/deleteselectedmenu/:menuid',selected_menu_Ctrl.deleteSelectedMenu)
module.exports=router

const express = require('express')
const router = express.Router()
const restoCntrl = require('../../_bizly_controllers/_restaurants_controllers/resto-controller')
const multer = require('multer')
var appRoot = require('app-root-path');

// const DIR_RESTO_LOGO = appRoot + '/uploads/cuisine';
const DIR_RESTO_LOGO = '/home/bizlypos/public_html/uploads/reataurant_logos';
let StorageConfig_Resto_Logo = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, DIR_RESTO_LOGO);
    },
    filename: (req, file, cb) => {
        cb(null, Date.now() + file.originalname);
    }
});

let upload_resto_logo = multer({
    storage: StorageConfig_Resto_Logo,
});

// const DIR_RESTO_RESTO_BANNER = appRoot + '/uploads/cuisine';
const DIR_RESTO_RESTO_BANNER = '/home/bizlypos/public_html/uploads/reataurant_banners';
let StorageConfig_Resto_Banner = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, DIR_RESTO_RESTO_BANNER);
    },
    filename: (req, file, cb) => {
        cb(null, Date.now() + file.originalname);
    }
});

let upload_resto_banner = multer({
    storage: StorageConfig_Resto_Banner,
});

// const DIR_RESTO_RESTO_ALBUM_IMAGES = appRoot + '/uploads/cuisine';
const DIR_RESTO_RESTO_ALBUM_IMAGES = '/home/bizlypos/public_html/uploads/reataurant_album_images';
let StorageConfig_Resto_Album_Images = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, DIR_RESTO_RESTO_ALBUM_IMAGES);
    },
    filename: (req, file, cb) => {
        cb(null, Date.now() + file.originalname);
    }
});

let upload_resto_album_images = multer({
    storage: StorageConfig_Resto_Album_Images,
});

// const DIR_RESTO_RESTO_ALBUM_VIDEOS = appRoot + '/uploads/cuisine';
const DIR_RESTO_RESTO_ALBUM_VIDEOS = '/home/bizlypos/public_html/uploads/reataurant_album_videos';
let StorageConfig_Resto_Album_Videos = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, DIR_RESTO_RESTO_ALBUM_VIDEOS);
    },
    filename: (req, file, cb) => {
        cb(null, Date.now() + file.originalname);
    }
});

let upload_resto_album_videos = multer({
    storage: StorageConfig_Resto_Album_Videos,
});

//ADD
router.post('/addrestaurantoulets', restoCntrl.addRestaurnatOutlets)
router.put('/addrestaurantinfo/:restoid', restoCntrl.addRestaurantInfo)
router.put('/addrestaurantestablishments/:restoid', restoCntrl.addRestaurantEstablishments)
router.put('/addrestaurantmenumanagementmenu/:restoid', restoCntrl.addRestaurantmenuManagementMenu)
router.put('/addrestaurantmenumanagementliquor/:restoid', restoCntrl.addRestaurantmenuManagementliquor)
router.put('/addrestaurantmenumanagementcuisines/:restoid', restoCntrl.addRestaurantmenuManagementcuisines)
router.put('/addrestaurantmoreinfo/:restoid', restoCntrl.addrestaurantMoreinfo)
router.put('/addrestaurantservices/:restoid', restoCntrl.addrestaurantServices)
router.put('/addrestaurantrulesandregulations/:restoid', restoCntrl.addrestaurantRulesandregulations)
router.put('/addrestaurantambiences/:restoid', restoCntrl.addrestaurantAmbiences)
router.put('/addrestaurantlegaldata/:restoid', restoCntrl.addRestaurantLegalData)
// router.put('/addrestaurantorganization/:restoid', restoCntrl.addrestaurantOrganization)
// router.put('/addrestaurantorganizationoutlets/:restoid/:organizationid', restoCntrl.addrestaurantOrganizationOutlets)
router.put('/addrestaurantlayout/:restoid', restoCntrl.addRestaurantLayout)

//Get
router.get('/getrestaurant/:restoid', restoCntrl.getRestaurant)
router.get('/getallrestaurants', restoCntrl.getAllRestaurants)

//Update
router.put('/updateOutlet/:restoid', restoCntrl.updateRestaurant)
// router.put('/updaterestoinfo/:restoid', restoCntrl.updateRestaurantInfo)
router.put('/updateEstcategory/:restoid', restoCntrl.updaterestaurantESTcategory)
router.put('/updatemoreinfo/:restoid', restoCntrl.updaterestaurantMoreinfo)
router.put('/updateservices/:restoid', restoCntrl.updaterestaurantServices)
router.put('/updaterulesandregulation/:restoid', restoCntrl.updaterestaurantRulesandregulations)
router.put('/updateambiences/:restoid', restoCntrl.updaterestaurantAmbiences)
router.put('/updatecuisines/:restoid', restoCntrl.updateRestaurantmenuManagementcuisines)

//Delete
router.put('/deleterestaurant/:restoid', restoCntrl.deleteRestaurant)
router.delete('/deleterestaurantbyid/:restoid', restoCntrl.deleteRestaurantByid)
router.delete('/deleteallrestaurant', restoCntrl.deleteAllRestaurant)
router.post('/deleterestaurantlogoimage', restoCntrl.deleteRestaurantLogoImage)
router.post('/deleterestaurantbannerimage', restoCntrl.deleteRestaurantBannerImage)
router.post('/deleterestaurantalbumimages', restoCntrl.deleteRestaurantAlbumsImage)
router.post('/deleterestaurantalbumvideos', restoCntrl.deleteRestaurantAlbumsVideos)

// Image upload
router.post('/uploadrestaurantlogo', upload_resto_logo.single('restologo'), (req, res) => {
    res.send('http://www.bizlypos.com/uploads/reataurant_logos/' + req.file.filename)
})
router.post('/uploadrestaurantbanner', upload_resto_banner.single('restobanner'), (req, res) => {
    res.send('http://www.bizlypos.com/uploads/reataurant_banners/' + req.file.filename)
})
router.post('/uploadrestoalbumimages', upload_resto_album_images.single('restoalbumimages'), (req, res) => {
    res.send('http://www.bizlypos.com/uploads/reataurant_album_images/' + req.file.filename)
})
router.post('/uploadrestoalbumvideos', upload_resto_album_videos.single('restoalbumvideos'), (req, res) => {
    res.send('http://www.bizlypos.com/uploads/reataurant_album_videos/' + req.file.filename)
})
router.put('/addmedia/:restoid', restoCntrl.addRestaurantMedia)

module.exports = router;


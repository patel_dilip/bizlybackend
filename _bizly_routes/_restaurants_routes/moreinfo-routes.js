
const express = require('express')
const router = express.Router()
const moreinfoCntrl = require('../../_bizly_controllers/_restaurants_controllers/moreinfo-controller')

router.post('/addmoreinfo', moreinfoCntrl.addMoreInfo)
router.get('/getmoreinfo/:moreinfoid', moreinfoCntrl.getMoreInfo)
router.get('/getallmoreinfos', moreinfoCntrl.getAllMoreInfos)
router.put('/updatemoreinfo/:moreinfoid', moreinfoCntrl.updateMoreInfo)
router.delete('/deleteallmoreinfo', moreinfoCntrl.deleteALLMoreInfo)
router.delete('/deletemoreinfobyid/:moreinfoid', moreinfoCntrl.deleteMoreInfByID)
module.exports = router
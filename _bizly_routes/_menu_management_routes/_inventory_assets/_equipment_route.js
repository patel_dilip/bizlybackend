const express = require('express')
const router = express.Router()
const multer = require('multer')
const middleware = require('../../../middleware/isAuthentic')
const equipmentCntrl = require('../../../_bizly_controllers/_menu_management_controllers/_inventory_assets/_equipment_controller')


const DIR_EQUIPMENT = '/home/bizlypos/public_html/uploads/equipment';
let StorageConfig_EQUIPMENT = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, DIR_EQUIPMENT);
    },
    filename: (req, file, cb) => {
        cb(null, Date.now() + file.originalname);
    }
});

let upload_equipment = multer({
    storage: StorageConfig_EQUIPMENT,
});

// router.use(middleware)
router.post('/addequipment', equipmentCntrl.addEquipment)
router.get('/getallequipments', equipmentCntrl.getAllEquipments)
router.get('/getequipment/:equipmentid', equipmentCntrl.getEquipment)
router.get('/checkequipmentalreadyexists/:equipmentName', equipmentCntrl.checkEquipmentAlreadyExists)
router.put('/updateequipment/:equipmentid', equipmentCntrl.updateEquipment)
router.delete('/deleteequipment/:equipmentid', equipmentCntrl.deleteEquipment)
router.put('/softdeleteequipment/:equipmentid', equipmentCntrl.softDeleteEquipment)
router.post('/uploadphotos', upload_equipment.single('equipment'), (req, res) => {

    var url = 'http://www.bizlypos.com/uploads/equipment/' + req.file.filename
    res.json({ EquipmentUrls: url })

})

module.exports = router
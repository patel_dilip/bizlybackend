const express = require('express')
const router = express.Router()
const inventoryAssetsCntrl = require('../../../_bizly_controllers/_menu_management_controllers/_inventory_assets/_inventory_assets_controller')
const middleware = require('../../../middleware/isAuthentic')

// router.use(middleware)
router.post('/addrootinventoryassetscategoryname', inventoryAssetsCntrl.addRootInventoryAssetsCategoryName)
router.put('/addinventoryassetssubcategory/:rootcategoryid', inventoryAssetsCntrl.addSubcategory)
router.put('/addinventoryassetssubsubcategory/:rootcategoryid/:subcategoryid', inventoryAssetsCntrl.addSubSubCategories)
router.put('/addinventoryassetssubsubcategorytype/:rootcategoryid/:subsubcategoryid', inventoryAssetsCntrl.addSubSubCategoryType)
router.get('/getrootinventoryassetscategory/:rootcategoryid', inventoryAssetsCntrl.getRootInventoryAssetsCategories)
router.get('/getallrootinventoryassetscategory', inventoryAssetsCntrl.getAllRootInventoryAssetsCategories)
router.put('/updateRootInventoryAssetsCategoryName/:rootcategoryid', inventoryAssetsCntrl.updateRootInventoryAssetsCategoryName)
router.put('/updatesubcategoryname/:rootcategoryid/:subcategoryid', inventoryAssetsCntrl.updateSubcategoryName)
router.put('/updatesubsubcategoriesname/:rootcategoryid/:subcategoryid/:subsubcategoryid', inventoryAssetsCntrl.updateSubSubCategoriesName)
router.put('/updatesubsubcategorytypename/:rootcategoryid/:subcategoryid/:subsubcategoryid/:subsubcategorytypeid', inventoryAssetsCntrl.updateSubSubCategoryTypeName)

module.exports = router
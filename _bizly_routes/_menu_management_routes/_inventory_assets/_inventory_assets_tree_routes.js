const express = require('express')
const router = express.Router()
const inventoryAssetsTreeCtrl = require('../../../_bizly_controllers/_menu_management_controllers/_inventory_assets/_inventory_assets_tree_controller')
const middleware = require('../../../middleware/isAuthentic')

// router.use(middleware)
router.post('/addassetsinventorytree', inventoryAssetsTreeCtrl.addTree)
router.get('/getassetsinventorytree', inventoryAssetsTreeCtrl.getTree)
router.put('/updateassetsinventorytree/:treeid', inventoryAssetsTreeCtrl.updateTree)
module.exports = router
const express = require('express')
const router = express.Router()
const multer = require('multer')
const brandCntrl = require('../../../_bizly_controllers/_menu_management_controllers/retail_beverage/_retail_beverage_brand_controller')
const middleware = require('../../../middleware/isAuthentic')

//Set Location to upload attribute option image

const DIR_retail_beverage_brand = '/home/bizlypos/public_html/uploads/retail_beverage/retail_beverage_brand';

//storage configuration
let StorageConfig_retail_beverage_brand = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, DIR_retail_beverage_brand);
    },
    filename: (req, file, cb) => {
        cb(null, Date.now() + file.originalname);
    }
});

let upload_retail_beverage_brand = multer({
    storage: StorageConfig_retail_beverage_brand,
});


// router.use(middleware)
router.post('/addretailbeveragebrand', brandCntrl.addRetailBeverageBrand)
router.get('/getallretailbeveragebrand', brandCntrl.getAllRetailBeverageBrand)
router.get('/getretailbeveragebrand/:brandid', brandCntrl.getRetailBeverageBrandById)
router.get('/checkretailbeveragebrandalreadyexists/:brandName', brandCntrl.checkretailBeverageBrandAlreadyExists)
router.post('/getretailbeveragebrandfromcategory', brandCntrl.getRetailBeverageBrandFromCategories)
router.put('/updateretailbeveragebrand/:brandid', brandCntrl.updateRetailBeverageBrand)
router.put('/deleteretailbeveragebrand/:brandid', brandCntrl.deleteRetailBeverageBrand)
router.post('/uploadphoto', upload_retail_beverage_brand.single('retailbeveragebrand'), (req, res) => {
    res.send('http://www.bizlypos.com/uploads/retail_beverage/retail_beverage_brand/' + req.file.filename)
})

module.exports = router
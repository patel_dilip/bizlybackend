const express = require('express')
const router = express.Router()
const multer = require('multer')
const productCntrl = require('../../../_bizly_controllers/_menu_management_controllers/retail_beverage/_retail_beverage_product_controller')
const middleware = require('../../../middleware/isAuthentic')

//Set Location to upload attribute option image

const DIR_retail_beverage_product = '/home/bizlypos/public_html/uploads/retail_beverage/retail_beverage_product';

//storage configuration
let StorageConfig_retail_beverage_product = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, DIR_retail_beverage_product);
    },
    filename: (req, file, cb) => {
        cb(null, Date.now() + file.originalname);
    }
});

let upload_retail_beverage_product = multer({
    storage: StorageConfig_retail_beverage_product,
});

// router.use(middleware)
router.post('/addretailbeverageproduct', productCntrl.addRetailBeverageProduct)
router.get('/getallretailbeverageproduct', productCntrl.getAllRetailBeverageProduct)
router.get('/getretailbeverageproduct/:productid', productCntrl.getRetailBeverageProductById)
router.get('/checkretailbeverageproductalreadyexists/:productName', productCntrl.checkretailBeverageProductAlreadyExists)
router.put('/updateretailbeverageproduct/:productid', productCntrl.updateRetailBeverageProduct)
router.put('/deleteretailbeverageproduct/:productid', productCntrl.deleteRetailBeverageProduct)
router.post('/uploadphoto', upload_retail_beverage_product.single('retailbeverageproduct'), (req, res) => {
    res.send('http://www.bizlypos.com/uploads/retail_beverage/retail_beverage_product/' + req.file.filename)
})

module.exports = router
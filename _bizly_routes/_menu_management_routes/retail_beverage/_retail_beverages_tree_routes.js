const express = require('express')
const router = express.Router()
const retailBeveragesTreeCtrl = require('../../../_bizly_controllers/_menu_management_controllers/retail_beverage/_retail_beverages_tree_controller')
const middleware = require('../../../middleware/isAuthentic')

// router.use(middleware)
router.post('/addretailbeveragestree', retailBeveragesTreeCtrl.addTree)
router.get('/getretailbeveragestree', retailBeveragesTreeCtrl.getTree)
router.put('/updateretailbeveragestree/:treeid', retailBeveragesTreeCtrl.updateTree)
module.exports = router
const express = require('express')
const router = express.Router()
const multer = require('multer')
const productCntrl = require('../../../_bizly_controllers/_menu_management_controllers/liquor/_product_controller')
const middleware = require('../../../middleware/isAuthentic')

const DIR_PRODUCT = '/home/bizlypos/public_html/uploads/products';
let StorageConfig_Product = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, DIR_PRODUCT);
    },
    filename: (req, file, cb) => {
        cb(null, Date.now() + file.originalname);
    }
});

let upload_product = multer({
    storage: StorageConfig_Product,
});

// router.use(middleware)
router.post('/addproduct', productCntrl.addproduct)
router.get('/getallproducts', productCntrl.getAllProducts)
router.get('/getproduct/:productid', productCntrl.getProduct)
router.get('/checkproductalreadyexists/:productName', productCntrl.checkProductAlreadyExists)
router.put('/updateproduct/:productid', productCntrl.updateProducts)
router.delete('/deleteproduct/:productid', productCntrl.deleteProduct)
router.put('/softdeleteproduct/:productid', productCntrl.softDeleteProduct)
router.post('/uploadphotos', upload_product.array('product', 5), (req, res) => {
    var urlArray = []
    req.files.forEach(file => {
        urlArray.push('http://www.bizlypos.com/uploads/products/' + file.filename)
    });
    // res.send(returnUrl);
    if (urlArray.length != 0) {
        res.json({ adonPhotoUrls: urlArray })
    }
})

module.exports = router

const express = require('express')
const router = express.Router()
const attributeCtrl = require('../../../_bizly_controllers/_menu_management_controllers/liquor/_attribute_controller')
const multer = require('multer')
const middleware = require('../../../middleware/isAuthentic')
var appRoot = require('app-root-path');

//Set Location to upload attribute option image
// const DIR_ATTRIBUTEOPTIONS = appRoot + '/uploads/cuisine';
const DIR_ATTRIBUTEOPTIONS = '/home/bizlypos/public_html/uploads/attributeoptions';

//storage configuration
let StorageConfig_AttributeOptions = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, DIR_ATTRIBUTEOPTIONS);
    },
    filename: (req, file, cb) => {
        cb(null, Date.now() + file.originalname);
    }
});

let upload_attribute_option = multer({
    storage: StorageConfig_AttributeOptions,
});

// router.use(middleware)
router.post('/addAttribute', attributeCtrl.addAttribute)
router.get('/getAttribute/:attributeid', attributeCtrl.getAttribute)
router.get('/getAllAttribute', attributeCtrl.getAllAttribute)
router.get('/checkattributealreadyexists/:attributeName/:attributeType', attributeCtrl.checkAttributeAlreadyExists)
router.put('/updateAttribute/:attributeid', attributeCtrl.updateAttribute)
router.delete('/deleteAttribute/:attributeid', attributeCtrl.deleteAttribute)
router.put('/issearchable/:attributeid', attributeCtrl.isSearchableToggle)
router.put('/isfilterable/:attributeid', attributeCtrl.isFilterableToggle)
router.put('/softdeleteAttribute/:attributeid', attributeCtrl.softDeleteAttribute)
router.post('/uploadphoto', upload_attribute_option.single('attributeoption'), (req, res) => {
    res.send('http://www.bizlypos.com/uploads/attributeoptions/' + req.file.filename)
})
module.exports = router
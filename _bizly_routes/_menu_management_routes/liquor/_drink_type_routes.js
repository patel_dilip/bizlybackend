const express = require('express')
const router = express.Router()
const drinkTypeCtrl = require('../../../_bizly_controllers/_menu_management_controllers/liquor/_drink_type_controller')
const middleware = require('../../../middleware/isAuthentic')

// router.use(middleware)
router.post('/adddrinktype', drinkTypeCtrl.addDrinkType)
router.get('/getdrinktype/:drinktypeid', drinkTypeCtrl.getDrinkType)
router.get('/getalldrinktypes', drinkTypeCtrl.getAllDrinkTypes)
router.delete('/deletedrinktype/:drinktypeid', drinkTypeCtrl.deleteDrinkType)
router.put('/addliquorvarient/:drinktypeid', drinkTypeCtrl.addLiquorVarient)
router.put('/addliquorsubvarient/:drinktypeid/:liquorvarientid', drinkTypeCtrl.addLiquorSubVarient)
router.put('/addliquorsubsubvarient/:drinktypeid/:liquorsubvarientid', drinkTypeCtrl.addLiquorSubSubVarient)
router.get('/getliquorvarient/:drinktypeid', drinkTypeCtrl.getLiquorVarient) //optional
router.put('/updatedrinktypename/:rootdrinktypeid', drinkTypeCtrl.updateDrinkTypeName)
router.put('/updateliquorvarientname/:rootdrinktypeid/:liquorvarientid', drinkTypeCtrl.updateliquorVarientName)
router.put('/updateliquorsubvarientsname/:rootdrinktypeid/:liquorvarientid/:liquorsubvarientid', drinkTypeCtrl.updateliquorSubVarientsName)
router.put('/updateliquorsubsubvarienttypename/:rootdrinktypeid/:liquorvarientid/:liquorsubvarientid/:liquorsubsubvarienttypeid', drinkTypeCtrl.updateliquorSubSubVarientTypeName)

module.exports = router
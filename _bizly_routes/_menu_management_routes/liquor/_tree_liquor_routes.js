const express = require("express");
const router = express.Router();
const liquorTreeCtrl = require("../../../_bizly_controllers/_menu_management_controllers/liquor/_tree_controller");
const middleware = require("../../../middleware/isAuthentic");

// router.use(middleware)
router.post("/addliquortree", liquorTreeCtrl.addTree);
router.get("/getliquortree", liquorTreeCtrl.getTree);
router.put("/updateliquortree/:treeid", liquorTreeCtrl.updateTree);
router.put("/addliquortree", liquorTreeCtrl.addLiquorTree);
module.exports = router;

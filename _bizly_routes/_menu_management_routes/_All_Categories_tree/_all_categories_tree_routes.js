const express = require('express')
const router = express.Router()
const categoryCntrl = require('../../../_bizly_controllers/_menu_management_controllers/_All_Categories_tree/_all_categories_tree_controller')
const middleware = require('../../../middleware/isAuthentic')

// router.use(middleware)
router.get('/getallCategories', categoryCntrl.getAllCategories)
module.exports = router
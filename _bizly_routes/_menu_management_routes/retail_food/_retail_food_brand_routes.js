const express = require('express')
const router = express.Router()
const multer = require('multer')
const retailFoodbrandCntrl = require('../../../_bizly_controllers/_menu_management_controllers/retail_food/_retail_food_brand_controller')
const middleware = require('../../../middleware/isAuthentic')

//Set Location to upload attribute option image

const DIR_RETAILFOODBRAND = '/home/bizlypos/public_html/uploads/retailfoodbrand';

//storage configuration
let StorageConfig_RetailFoodBrand = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, DIR_RETAILFOODBRAND);
    },
    filename: (req, file, cb) => {
        cb(null, Date.now() + file.originalname);
    }
});

let upload_Retail_Food_Brand = multer({
    storage: StorageConfig_RetailFoodBrand,
});

// router.use(middleware)
router.post('/addretailfoodbrand', retailFoodbrandCntrl.addRetailFoodBrand)
router.get('/getallretailfoodbrand', retailFoodbrandCntrl.getAllRetailFoodBrand)
router.get('/getretailfoodbrand/:brandid', retailFoodbrandCntrl.getRetailFoodBrand)
router.post('/getretailfoodbrandfromcategories', retailFoodbrandCntrl.getRetailfoodBrandFromCategories)
router.get('/checkretailfoodbrandalreadyexists/:brandName', retailFoodbrandCntrl.checkretailFoodBrandAlreadyExists)
router.put('/updateretailfoodbrand/:brandid', retailFoodbrandCntrl.updateRetailFoodBrand)
router.delete('/deleteretailfoodbrand/:brandid', retailFoodbrandCntrl.deleteRetailFoodBrand)
router.put('/softdeleteretailfoodbrand/:brandid', retailFoodbrandCntrl.softDeleteRetailFoodBrand)
router.post('/uploadphoto', upload_Retail_Food_Brand.single('retailfoodbrand'), (req, res) => {
    res.send('http://www.bizlypos.com/uploads/retailfoodbrand/' + req.file.filename)
})

module.exports = router
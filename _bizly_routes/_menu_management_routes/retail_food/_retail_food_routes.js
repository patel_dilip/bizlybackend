const express = require('express')
const router = express.Router()
const retailFoodCntrl = require('../../../_bizly_controllers/_menu_management_controllers/retail_food/_retail_food_controller')
const middleware = require('../../../middleware/isAuthentic')

// router.use(middleware)
router.post('/addrootretailfoodcategoryname', retailFoodCntrl.addRootRetilFoodCategoryName)
router.put('/addretailfoodsubcategory/:rootcategoryid', retailFoodCntrl.addSubcategory)
router.put('/addretailfoodsubsubcategory/:rootcategoryid/:subcategoryid', retailFoodCntrl.addSubSubCategories)
router.put('/addretailfoodsubsubcategorytype/:rootcategoryid/:subsubcategoryid', retailFoodCntrl.addSubSubCategoryType)
router.get('/getrootretailfoodcategory/:rootcategoryid', retailFoodCntrl.getRootRetailFoodCategories)
router.get('/getallrootretailfoodcategory', retailFoodCntrl.getAllRetailFoodCategories)
router.put('/updateRootRetilFoodCategoryName/:rootcategoryid', retailFoodCntrl.updateRootRetilFoodCategoryName)
router.put('/updatesubcategoryname/:rootcategoryid/:subcategoryid', retailFoodCntrl.updateSubcategoryName)
router.put('/updateSubSubCategoriesName/:rootcategoryid/:subcategoryid/:subsubcategoryid', retailFoodCntrl.updateSubSubCategoriesName)
router.put('/updateSubSubCategoryTypeName/:rootcategoryid/:subcategoryid/:subsubcategoryid/:subsubcategorytypeid', retailFoodCntrl.updateSubSubCategoryTypeName)

module.exports = router
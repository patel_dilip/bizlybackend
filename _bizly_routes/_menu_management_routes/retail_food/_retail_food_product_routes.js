const express = require('express')
const router = express.Router()
const multer = require('multer')
const retailFoodproductCntrl = require('../../../_bizly_controllers/_menu_management_controllers/retail_food/_retail_food_product_controller')
const middleware = require('../../../middleware/isAuthentic')

//Set Location to upload attribute option image

const DIR_RETAILFOODPRODUCT = '/home/bizlypos/public_html/uploads/retailfoodproduct';

//storage configuration
let StorageConfig_RetailFoodProduct = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, DIR_RETAILFOODPRODUCT);
    },
    filename: (req, file, cb) => {
        cb(null, Date.now() + file.originalname);
    }
});

let upload_Retail_food_Product = multer({
    storage: StorageConfig_RetailFoodProduct,
});

// router.use(middleware)
router.post('/addretailfoodproduct', retailFoodproductCntrl.addRetailFoodProduct)
router.get('/getallretailfoodproduct', retailFoodproductCntrl.getAllRetailfoodProduct)
router.get('/getretailfoodproduct/:productid', retailFoodproductCntrl.getRetailFoodProduct)
router.get('/checkretailfoodbproductalreadyexists/:productName', retailFoodproductCntrl.checkretailFoodProductAlreadyExists)
router.put('/updateretailfoodproduct/:productid', retailFoodproductCntrl.updateRetailFoodProduct)
router.delete('/deleteretailfoodproduct/:productid', retailFoodproductCntrl.deleteRetailfoodproduct)
router.put('/softdeleteretailfoodproduct/:productid', retailFoodproductCntrl.softDeleteRetailfoodproduct)
router.post('/uploadphoto', upload_Retail_food_Product.single('retailfoodproduct'), (req, res) => {
    res.send('http://www.bizlypos.com/uploads/retailfoodproduct/' + req.file.filename)
})

module.exports = router
const express = require('express')
const router = express.Router()
const multer = require('multer')
const beverageproductCntrl = require('../../../_bizly_controllers/_menu_management_controllers/beverages/_beverage_product_controller')
const middleware = require('../../../middleware/isAuthentic')

//Set Location to upload attribute option image

const DIR_BEVERAGEPRODUCT = '/home/bizlypos/public_html/uploads/beverageproduct';

//storage configuration
let StorageConfig_BeverageProduct = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, DIR_BEVERAGEPRODUCT);
    },
    filename: (req, file, cb) => {
        cb(null, Date.now() + file.originalname);
    }
});

let upload_Beverage_Product = multer({
    storage: StorageConfig_BeverageProduct,
});

// router.use(middleware)
router.post('/addbeverageproduct', beverageproductCntrl.addBeverageProduct)
router.get('/getallbeverageproduct', beverageproductCntrl.getAllBeverageProduct)
router.get('/getbeverageproduct/:productid', beverageproductCntrl.getBeverageProduct)
router.get('/checkbeverageproductalreadyexists/:productName', beverageproductCntrl.checkBeverageProductAlreadyExists)
router.put('/updatebeverageproduct/:productid', beverageproductCntrl.updateBeverageProduct)
router.delete('/deletebeverageproduct/:productid', beverageproductCntrl.deleteBeverageproduct)
router.put('/softdeletebeverageproduct/:productid', beverageproductCntrl.softDeleteBeverageproduct)
router.post('/uploadphoto', upload_Beverage_Product.single('beverageproduct'), (req, res) => {
    res.send('http://www.bizlypos.com/uploads/beverageproduct/' + req.file.filename)
})

module.exports = router
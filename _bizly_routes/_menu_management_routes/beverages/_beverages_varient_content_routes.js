const express = require("express");
const router = express.Router();
const beveragesVarientContentCtrl = require("../../../_bizly_controllers/_menu_management_controllers/beverages/_beverages_varient_content_controller");
const middleware = require("../../../middleware/isAuthentic");

// router.use(middleware)
router.post(
  "/addbeveragesvarientcontent",
  beveragesVarientContentCtrl.addContent
);
router.get(
  "/getbeveragesvarientcontent/:contentid",
  beveragesVarientContentCtrl.getContent
);
router.get(
  "/getallbeveragesvarientcontents",
  beveragesVarientContentCtrl.getAllContents
);
router.get(
  "/checkbeveragesvarientcontentalreadyexits/:contentName",
  beveragesVarientContentCtrl.checkVarientContentAlreadyExists
);
router.put(
  "/updatebeveragesvarientcontent/:contentid",
  beveragesVarientContentCtrl.updateContent
);
router.delete(
  "/deletebeveragesvarientcontent/:contentid",
  beveragesVarientContentCtrl.deleteContent
);

module.exports = router;

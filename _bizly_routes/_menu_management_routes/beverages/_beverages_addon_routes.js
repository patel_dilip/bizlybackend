const express = require("express");
const router = express.Router();
const beveragesAddonCtrl = require("../../../_bizly_controllers/_menu_management_controllers/beverages/_beverages_addon_controller");
const multer = require("multer");
// const middleware = require('../../../middleware/isAuthentic')
var appRoot = require("app-root-path");

// const DIR_BEVERAGESADONS = appRoot + '/uploads/cuisine';
const DIR_BEVERAGESADONS = "/home/bizlypos/public_html/uploads/beveragesadons";
let StorageConfig_BeveragesAdon = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, DIR_BEVERAGESADONS);
  },
  filename: (req, file, cb) => {
    cb(null, Date.now() + file.originalname);
  },
});

let upload_beveragesadon = multer({
  storage: StorageConfig_BeveragesAdon,
});

// router.use(middleware)
router.post("/addbeveragesaddon", beveragesAddonCtrl.addBeveragesAddon);
router.get("/getbeveragesaddon/:adonid", beveragesAddonCtrl.getBeveragesAddon);
router.get("/getallbeveragesaddons", beveragesAddonCtrl.getAllBeveragesAdons);
router.get(
  "/checkbeveragesaddonexists/:adonName",
  beveragesAddonCtrl.checkBeveragesAddonAlreadyExists
);
router.put(
  "/updatebeveragesaddon/:adonid",
  beveragesAddonCtrl.updateBeveragesAddon
);
router.delete(
  "/deletebeveragesaddon/:adonid",
  beveragesAddonCtrl.deleteBeveragesAddon
);
router.put(
  "/softdeletebeveragesaddon/:adonid",
  beveragesAddonCtrl.softDeleteBeveragesAddon
);
router.post(
  "/uploadbeveragesphotos",
  upload_beveragesadon.array("beveragesaddons", 5),
  (req, res) => {
    var urlArray = [];
    req.files.forEach((file) => {
      urlArray.push(
        "http://www.bizlypos.com/uploads/beveragesadons/" + file.filename
      );
    });
    // res.send(returnUrl);
    if (urlArray.length != 0) {
      res.json({ beveragesAddonPhotoUrls: urlArray });
    }
  }
);

module.exports = router;

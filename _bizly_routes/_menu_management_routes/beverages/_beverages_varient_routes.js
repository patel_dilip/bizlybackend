const express = require('express')
const router = express.Router()
const beveragesVarientCtrl  = require('../../../_bizly_controllers/_menu_management_controllers/beverages/_beverages_varient_controller')
const multer = require('multer')
const middleware = require('../../../middleware/isAuthentic')
var appRoot = require('app-root-path');


// const DIR_VARIENTS = appRoot + '/uploads/cuisine';
const DIR_VARIENTS = '/home/bizlypos/public_html/uploads/beveragesvarients';
let StorageConfig_Varients = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, DIR_VARIENTS);
    },
    filename: (req, file, cb) => {
        cb(null, Date.now() + file.originalname);
    }
});

let upload_varients = multer({
    storage: StorageConfig_Varients,
});

// router.use(middleware)
router.post('/addbeveragesvarient', beveragesVarientCtrl.addVarient)
router.get('/getbeveragesvarient/:varientid', beveragesVarientCtrl.getVarient)
router.get('/getallbeveragesvarients', beveragesVarientCtrl.getAllVarients)
router.get('/checkbeveragesvarientalreadyexists/:varientName', beveragesVarientCtrl.checkVarientAlreadyExists)
router.put('/updatebeveragesvarient/:varientid', beveragesVarientCtrl.updateVarient)
router.delete('/deletebeveragesvarient/:varientid', beveragesVarientCtrl.deleteVarient)
router.put('/softdeletebeveragesvarient/:varientid', beveragesVarientCtrl.softDeleteVarient)
router.post('/uploadbeveragesvarientphotos', upload_varients.array('beveragesvarients', 5), (req, res) => {
    var urlArray = []
    req.files.forEach(file => {
        urlArray.push('http://www.bizlypos.com/uploads/beveragesvarients/' + file.filename)
    });
    // res.send(returnUrl);
    if (urlArray.length != 0) {
        res.json({ varientPhotoUrls: urlArray })
    }
})

module.exports = router
const express = require('express')
const router = express.Router()
const multer = require('multer')
const beveragebrandCntrl = require('../../../_bizly_controllers/_menu_management_controllers/beverages/_beverage_brand_controller')
const middleware = require('../../../middleware/isAuthentic')

//Set Location to upload attribute option image

const DIR_BEVERAGEBRAND = '/home/bizlypos/public_html/uploads/beveragebrand';

//storage configuration
let StorageConfig_BeverageBrand = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, DIR_BEVERAGEBRAND);
    },
    filename: (req, file, cb) => {
        cb(null, Date.now() + file.originalname);
    }
});

let upload_Beverage_Brand = multer({
    storage: StorageConfig_BeverageBrand,
});

// router.use(middleware)
router.post('/addbeveragebrand', beveragebrandCntrl.addBeverageBrand)
router.get('/getallbeveragebrand', beveragebrandCntrl.getAllBeverageBrand)
router.get('/getbeveragebrand/:brandid', beveragebrandCntrl.getBeverageBrand)
router.post('/getbeveragebrandfromcategory', beveragebrandCntrl.getBeverageBrandFromCategories)
router.get('/checkbeveragebrandalreadyexists/:brandName', beveragebrandCntrl.checkBeverageBrandAlreadyExists)
router.put('/updatebeveragebrand/:brandid', beveragebrandCntrl.updateBeverageBrand)
router.delete('/deletebeveragebrand/:brandid', beveragebrandCntrl.deleteBeverageBrand)
router.put('/softdeletebeveragebrand/:brandid', beveragebrandCntrl.softDeleteBeverageBrand)
router.post('/uploadphoto', upload_Beverage_Brand.single('beveragebrand'), (req, res) => {
    res.send('http://www.bizlypos.com/uploads/beveragebrand/' + req.file.filename)
})

module.exports = router
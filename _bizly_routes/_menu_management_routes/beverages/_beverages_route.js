const express = require('express')
const router = express.Router()
const multer = require('multer')
const beverageCntrl = require('../../../_bizly_controllers/_menu_management_controllers/beverages/_beverage_controller')
const middleware = require('../../../middleware/isAuthentic')

const DIR_BEVERAGES = '/home/bizlypos/public_html/uploads/beverages';
let StorageConfig_Beverages = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, DIR_BEVERAGES);
    },
    filename: (req, file, cb) => {
        cb(null, Date.now() + file.originalname);
    }
});

let upload_Beverages = multer({
    storage: StorageConfig_Beverages,
});

// router.use(middleware)
router.post('/addbeverages', beverageCntrl.addBeverages)
router.get('/getallbeverages', beverageCntrl.getAllBeverages)
router.get('/getbeverage/:beverageid', beverageCntrl.getBeverage)
router.get('/checkbeveragealreadyexists/:beverageName', beverageCntrl.checkBeverageAlreadyExists)
router.put('/updatebeverage/:beverageid', beverageCntrl.updateBeverages)
router.delete('/deletebeverage/:beverageid', beverageCntrl.deleteBeverage)
router.put('/softdeletebeverage/:beverageid', beverageCntrl.softDeleteBeverage)
router.post('/uploadphotos', upload_Beverages.array('beverages', 5), (req, res) => {
    var urlArray = []
    req.files.forEach(file => {
        urlArray.push('http://www.bizlypos.com/uploads/beverages/' + file.filename)
    });
    // res.send(returnUrl);
    if (urlArray.length != 0) {
        res.json({ BeveragePhotoUrls: urlArray })
    }
})

module.exports = router
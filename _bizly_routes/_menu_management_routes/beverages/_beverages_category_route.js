const express = require('express')
const router = express.Router()
const beveragesCntrl = require('../../../_bizly_controllers/_menu_management_controllers/beverages/_beverages_category_controller')
const middleware = require('../../../middleware/isAuthentic')

// router.use(middleware)
router.post('/addrootbeveragescategoryname', beveragesCntrl.addRootBeveragesCategoryName)
router.put('/addbeveragescategorysubcategory/:rootcategoryid', beveragesCntrl.addSubcategory)
router.put('/addbeveragescategorysubsubcategory/:rootcategoryid/:subcategoryid', beveragesCntrl.addSubSubCategories)
router.put('/addbeveragescategorysubsubcategorytype/:rootcategoryid/:subsubcategoryid', beveragesCntrl.addSubSubCategoryType)
router.get('/getrootbeveragescategory/:rootcategoryid', beveragesCntrl.getRootBeveragesCategories)
router.get('/getallrootbeveragescategory', beveragesCntrl.getAllBeveragesCategories)
router.put('/updaterootbeveragescategoryname/:rootcategoryid', beveragesCntrl.updateRootBeveragesCategoryName)
router.put('/updatesubcategoryname/:rootcategoryid/:subcategoryid', beveragesCntrl.updateSubcategoryName)
router.put('/updatesubsubcategoriesname/:rootcategoryid/:subcategoryid/:subsubcategoryid', beveragesCntrl.updateSubSubCategoriesName)
router.put('/updatesubsubcategorytypename/:rootcategoryid/:subcategoryid/:subsubcategoryid/:subsubcategorytypeid', beveragesCntrl.updateSubSubCategoryTypeName)
module.exports = router
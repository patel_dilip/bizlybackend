const express = require('express')
const router = express.Router()
const inhouseBeveragesTreeCtrl = require('../../../_bizly_controllers/_menu_management_controllers/beverages/_inhouse_beverages_tree_controller')
const middleware = require('../../../middleware/isAuthentic')

// router.use(middleware)
router.post('/addinhousebeveragestree', inhouseBeveragesTreeCtrl.addTree)
router.get('/getinhousebeveragestree', inhouseBeveragesTreeCtrl.getTree)
router.put('/updateinhousebeveragestree/:treeid', inhouseBeveragesTreeCtrl.updateTree)
module.exports = router
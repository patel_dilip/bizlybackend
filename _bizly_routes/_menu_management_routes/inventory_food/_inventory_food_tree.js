const express = require('express')
const router = express.Router()
const inventoryFoodTreeCtrl = require('../../../_bizly_controllers/_menu_management_controllers/inventory_food/_inventory_food_tree_controller')
const middleware = require('../../../middleware/isAuthentic')

// router.use(middleware)
router.post('/addfoodinventorytree', inventoryFoodTreeCtrl.addTree)
router.get('/getfoodinventorytree', inventoryFoodTreeCtrl.getTree)
router.put('/updatefoodinventorytree/:treeid', inventoryFoodTreeCtrl.updateTree)
module.exports = router
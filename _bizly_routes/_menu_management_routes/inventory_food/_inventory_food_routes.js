const express = require('express')
const router = express.Router()
const inventroyfoodetCtrl = require('../../../_bizly_controllers/_menu_management_controllers/inventory_food/_inventory_food_controller')
const middleware = require('../../../middleware/isAuthentic')

// router.use(middleware)
router.post('/addfoodcategoryname', inventroyfoodetCtrl.addRootInventoryFoodCategoryName)
router.put('/addsubcategory/:rootcategoryid', inventroyfoodetCtrl.addSubcategory)
router.put('/addsubsubcategory/:rootcategoryid/:subcategoryid', inventroyfoodetCtrl.addSubSubCategories)
router.put('/addsubsubcategorytype/:rootcategoryid/:subsubcategoryid', inventroyfoodetCtrl.addSubSubCategoryType)
router.get('/getfoodcategory/:rootcategoryid', inventroyfoodetCtrl.getRootInventoryFoodCategories)
router.get('/getallfoodcategories', inventroyfoodetCtrl.getAllRootInventoryFoodCategories)
router.put('/updaterootinventoryfoodcategoryname/:rootcategoryid', inventroyfoodetCtrl.updateRootInventoryFoodCategoryName)
router.put('/updatesubcategoryname/:rootcategoryid/:subcategoryid',inventroyfoodetCtrl.updateSubcategoryName)
router.put('/updatesubsubcategoriesname/:rootcategoryid/:subsubcategoryid',inventroyfoodetCtrl.updateSubSubCategoriesName)
router.put('/updatesubsubcategorytypename/:rootcategoryid/:subsubcategoryid/:subsubcategorytypeid',inventroyfoodetCtrl.updateSubSubCategoryTypeName)

module.exports = router
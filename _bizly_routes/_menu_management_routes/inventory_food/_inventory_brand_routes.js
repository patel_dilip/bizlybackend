const express = require('express')
const router = express.Router()
const inventroybrandCtrl = require('../../../_bizly_controllers/_menu_management_controllers/inventory_food/_inventory_brand_controller')
const middleware = require('../../../middleware/isAuthentic')

// router.use(middleware)
router.post('/addinventorybrand',inventroybrandCtrl.addInventoryBrand)
router.get('/getinventorybrand/:inventorybrandid',inventroybrandCtrl.getInventoryBrand)
router.get('/getallinventorybrand',inventroybrandCtrl.getAllInventoryBrand)
router.get('/checkinventroybrandalreadyexists/:BrandName', inventroybrandCtrl.checkInevntoryBrandAlreadyExists)
router.put('/updateinventorybrand/:inventorybrandid',inventroybrandCtrl.updateInventoryBrand)
router.delete('/deleteinventorybrand/:inventorybrandid',inventroybrandCtrl.deleteInventoryBrand)

module.exports = router
const express = require('express')
const router = express.Router()
const inventoryItemCtrl = require('../../../_bizly_controllers/_menu_management_controllers/inventory_food/_inventory_item_controller')
const middleware = require('../../../middleware/isAuthentic')

// router.use(middleware)
router.post('/addinventoryitem', inventoryItemCtrl.addInventoryItem)
router.get('/getinventoryitem/:inventoryitemid', inventoryItemCtrl.getInventoryItem)
router.get('/getallinventoryitem', inventoryItemCtrl.getAllInventoryItem)
router.get('/checkinventroyitemalreadyexists/:itemName', inventoryItemCtrl.checkInevntoryItemAlreadyExists)
router.put('/updateinventoryitem/:inventoryitemid', inventoryItemCtrl.updateInventoryItem)
router.delete('/deleteinventoryitem/:inventoryitemid', inventoryItemCtrl.deleteInventoryItem)
router.put('/softdeleteinventoryitem/:inventoryitemid', inventoryItemCtrl.softDeleteInventoryItem)
module.exports = router
const express = require('express')
const router = express.Router()
const categoryCtrl = require('../../../_bizly_controllers/_menu_management_controllers/food/_category_controller')
const multer = require('multer')
const middleware = require('../../../middleware/isAuthentic')
var appRoot = require('app-root-path');


// const DIR_CUISINES = appRoot + '/uploads/cuisine';
const DIR_CATEGORIES = '/home/bizlypos/public_html/uploads/categories';
let StorageConfig_Categories = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, DIR_CATEGORIES);
    },
    filename: (req, file, cb) => {
        cb(null, Date.now() + file.originalname);
    }
});

let upload_category = multer({
    storage: StorageConfig_Categories,
});

// router.use(middleware)
router.post('/addcategory', categoryCtrl.addCategory)
router.get('/getcategory/:categoryid', categoryCtrl.getCategory)
router.get('/getallcategories', categoryCtrl.getAllCategories)
router.get('/checkcategorynamealreadyexists/:categoryName', categoryCtrl.checkCategoryAlreadyExists)
router.put('/updatecategory/:categoryid', categoryCtrl.updateCategory)
router.delete('/deletecategory/:categoryid', categoryCtrl.deleteCategory)
router.put('/softdeletecategory/:categoryid', categoryCtrl.softDeleteCategory)
router.post('/uploadphotos', upload_category.array('categories', 5), (req, res) => {
    var urlArray = []
    req.files.forEach(file => {
        urlArray.push('http://www.bizlypos.com/uploads/categories/' + file.filename)
    });
    // res.send(returnUrl);
    if (urlArray.length != 0) {
        res.json({ cuisinePhotoUrls: urlArray })
    }
})

module.exports = router
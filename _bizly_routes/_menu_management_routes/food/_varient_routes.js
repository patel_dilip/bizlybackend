const express = require('express')
const router = express.Router()
const varientCtrl = require('../../../_bizly_controllers/_menu_management_controllers/food/_varient_controller')
const multer = require('multer')
const middleware = require('../../../middleware/isAuthentic')
var appRoot = require('app-root-path');


// const DIR_VARIENTS = appRoot + '/uploads/cuisine';
const DIR_VARIENTS = '/home/bizlypos/public_html/uploads/varients';
let StorageConfig_Varients = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, DIR_VARIENTS);
    },
    filename: (req, file, cb) => {
        cb(null, Date.now() + file.originalname);
    }
});

let upload_varients = multer({
    storage: StorageConfig_Varients,
});

// router.use(middleware)
router.post('/addvarient', varientCtrl.addVarient)
router.get('/getvarient/:varientid', varientCtrl.getVarient)
router.get('/getallvarients', varientCtrl.getAllVarients)
router.get('/checkvarientalreadyexists/:varientName', varientCtrl.checkVarientAlreadyExists)
router.put('/updatevarient/:varientid', varientCtrl.updateVarient)
router.delete('/deletevarient/:varientid', varientCtrl.deleteVarient)
router.put('/softdeletevarient/:varientid', varientCtrl.softDeleteVarient)
router.post('/uploadphotos', upload_varients.array('varients', 5), (req, res) => {
    var urlArray = []
    req.files.forEach(file => {
        urlArray.push('http://www.bizlypos.com/uploads/varients/' + file.filename)
    });
    // res.send(returnUrl);
    if (urlArray.length != 0) {
        res.json({ varientPhotoUrls: urlArray })
    }
})

module.exports = router
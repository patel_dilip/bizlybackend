const express = require('express')
const router = express.Router()
const dishCtrl = require('../../../_bizly_controllers/_menu_management_controllers/food/_dish_controller')
const multer = require('multer')
const middleware = require('../../../middleware/isAuthentic')
var appRoot = require('app-root-path');

// const DIR_DISHES = appRoot + '/uploads/cuisine';
const DIR_DISHES = '/home/bizlypos/public_html/uploads/dishes';
let StorageConfig_Dishes = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, DIR_DISHES);
    },
    filename: (req, file, cb) => {
        cb(null, Date.now() + file.originalname);
    }
});

let upload_dish = multer({
    storage: StorageConfig_Dishes,
});

// router.use(middleware)
router.post('/adddish', dishCtrl.addDish)
router.get('/getdish/:dishid', dishCtrl.getDish)
router.get('/getalldishes', dishCtrl.getAllDishes)
router.get('/checkdishalreadyexists/:dishName', dishCtrl.checkDishAlreadyExists)
router.put('/updatedish/:dishid', dishCtrl.updateDish)
router.delete('/deletedish/:dishid', dishCtrl.deleteDish)
router.put('/softdeletedish/:dishid', dishCtrl.softDeleteDish)
router.post('/uploadphotos', upload_dish.array('dishes', 5), (req, res) => {
    var urlArray = []
    req.files.forEach(file => {
        urlArray.push('http://www.bizlypos.com/uploads/dishes/' + file.filename)
    });
    // res.send(returnUrl);
    if (urlArray.length != 0) {
        res.json({ dishPhotoUrls: urlArray })
    }
})

module.exports = router
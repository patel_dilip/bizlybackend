const express = require('express')
const router = express.Router()
const adonCtrl = require('../../../_bizly_controllers/_menu_management_controllers/food/_adon_controller')
const multer = require('multer')
const middleware = require('../../../middleware/isAuthentic')
var appRoot = require('app-root-path');


// const DIR_ADONS = appRoot + '/uploads/cuisine';
const DIR_ADONS = '/home/bizlypos/public_html/uploads/adons';
let StorageConfig_Adon = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, DIR_ADONS);
    },
    filename: (req, file, cb) => {
        cb(null, Date.now() + file.originalname);
    }
});

let upload_adon = multer({
    storage: StorageConfig_Adon,
});

// router.use(middleware) 
router.post('/addadon', adonCtrl.addAdon)
router.get('/getadon/:adonid', adonCtrl.getAdon)
router.get('/getalladons', adonCtrl.getAllAdons)
router.get('/checkadonalreadyexists/:adonName', adonCtrl.checkAddonAlreadyExists)
router.put('/updateadon/:adonid', adonCtrl.updateAdon)
router.delete('/deleteadon/:adonid', adonCtrl.deleteAdon)
router.put('/softdeleteadon/:adonid', adonCtrl.softDeleteAdon)
router.post('/uploadphotos', upload_adon.array('adons', 5), (req, res) => {
    var urlArray = []
    req.files.forEach(file => {
        urlArray.push('http://www.bizlypos.com/uploads/adons/' + file.filename)
    });
    // res.send(returnUrl);
    if (urlArray.length != 0) {
        res.json({ adonPhotoUrls: urlArray })
    }
})

module.exports = router
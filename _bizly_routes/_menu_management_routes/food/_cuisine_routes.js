const express = require('express')
const router = express.Router()
const cuisineCtrl = require('../../../_bizly_controllers/_menu_management_controllers/food/_cusines_controller')
const multer = require('multer')
const middleware = require('../../../middleware/isAuthentic')
var appRoot = require('app-root-path');


// const DIR_CUISINES = appRoot + '/uploads/cuisine';
const DIR_CUISINES = '/home/bizlypos/public_html/uploads/cuisines';
let StorageConfig_Cuisines = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, DIR_CUISINES);
    },
    filename: (req, file, cb) => {
        cb(null, Date.now() + file.originalname);
    }
});

let upload_cuisine = multer({
    storage: StorageConfig_Cuisines,
});

// router.use(middleware)
router.post('/addcuisine', cuisineCtrl.addCuisine)
router.post('/addexcelCuisines', cuisineCtrl.addExcelCuisines)  
router.get('/getcuisine/:cuisineid', cuisineCtrl.getCuisine)
router.get('/getallcuisines', cuisineCtrl.getAllCuisines)
router.get('/checkcuisinealreadyexists/:cuisineName',cuisineCtrl.checkCuisineAlreadyExists)
router.put('/updatecuisine/:cuisineid', cuisineCtrl.updateCuisine)
router.delete('/deletecuisine/:cuisineid', cuisineCtrl.deleteCuisine)
router.put('/softdeletecuisone/:cuisineid', cuisineCtrl.softDeleteCuisine)
router.post('/uploadphotos', upload_cuisine.array('cuisines', 5), (req, res) => {
    var urlArray = []
    req.files.forEach(file => {
        urlArray.push('http://www.bizlypos.com/uploads/cuisines/' + file.filename)
    });
    // res.send(returnUrl);
    if (urlArray.length != 0) {
        res.json({ cuisinePhotoUrls: urlArray })
    }
})


module.exports = router
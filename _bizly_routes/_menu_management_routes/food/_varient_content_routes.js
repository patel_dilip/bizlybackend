const express = require('express')
const router = express.Router()
const varientContentCtrl = require('../../../_bizly_controllers/_menu_management_controllers/food/_varient_content_controller')
const middleware = require('../../../middleware/isAuthentic')

// router.use(middleware)
router.post('/addcontent', varientContentCtrl.addContent)
router.get('/getcontent/:contentid', varientContentCtrl.getContent)
router.get('/getallcontents', varientContentCtrl.getAllContents)
router.get('/checkvarientcontentalreadyexits/:contentName',varientContentCtrl.checkVarientContentAlreadyExists)
router.put('/updatecontent/:contentid', varientContentCtrl.updateContent)
router.delete('/deletecontent/:contentid', varientContentCtrl.deleteContent)

module.exports = router
const express = require('express')
const router = express.Router()
const tagCtrl = require('../../../_bizly_controllers/_menu_management_controllers/food/_tag_controller')
const middleware = require('../../../middleware/isAuthentic')

// router.use(middleware)
router.post('/addtag', tagCtrl.addTag)
router.get('/gettag/:tagid', tagCtrl.getTag)
router.get('/getalltags', tagCtrl.getAllTags)
router.get('/checktagalreadyexists/:tagName',tagCtrl.checkTagAlreadyExists)
router.put('/updatetag/:tagid', tagCtrl.updateTag)
router.delete('/deletetag/:tagid', tagCtrl.deleteTag)

module.exports = router
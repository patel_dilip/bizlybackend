const express = require('express')
const router = express.Router()
const resposneCntrl = require('../../_bizly_controllers/_feedback_controllers/_feedback_response_controller')
const middleware = require('../../middleware/isAuthentic')

// router.use(middleware)
router.post('/addfeedbackresponse', resposneCntrl.addFeedbackResponse)
router.get('/getallfeedbackresposne', resposneCntrl.getAllFeedbackResponse)
router.get('/getfeedbackresponsebyid/:responseid', resposneCntrl.getFeedbackResponseById)
router.put('/updatefeedbackresponse/:responseid', resposneCntrl.updateFeedbackResponse)
router.delete('/deletefeedbackresponse/:responseid', resposneCntrl.deleteFeedbackResponse)

module.exports = router
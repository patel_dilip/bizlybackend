const express = require('express')
const router = express.Router()
const questionsetCntrl = require('../../_bizly_controllers/_feedback_controllers/_QuestionSet_controller')
const middleware = require('../../middleware/isAuthentic')

// router.use(middleware)

router.post('/addquestionset', questionsetCntrl.addQuestionSet)
router.post('/getquestionsetfromcategory', questionsetCntrl.getQuestionSetfromCategory)
router.get('/getallquestionset', questionsetCntrl.getAllQuestionSet)
router.get('/getquestionsetbyid/:questionsetid', questionsetCntrl.getQuestionSetById)
router.put('/updatequestionset/:questionsetid', questionsetCntrl.updateQuestionSet)
router.put('/changequestionsetstatus/:questionsetid', questionsetCntrl.questionSetStatus)
router.delete('/deletequestionset/:questionsetid', questionsetCntrl.deleteQuestionSet)

module.exports = router
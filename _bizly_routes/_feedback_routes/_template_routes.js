const express = require('express')
const router = express.Router()
const templateCntrl = require('../../_bizly_controllers/_feedback_controllers/_template_controller')
const middleware = require('../../middleware/isAuthentic')

// router.use(middleware)
router.post('/addtemplate', templateCntrl.addTemplate)
router.get('/getAlltemplate', templateCntrl.getAllTemplate)
router.get('/gettemplate/:templateid', templateCntrl.getTemplateById)
router.put('/updatetemplate/:templateid', templateCntrl.updateTemplate)
router.put('/assignedtempaltetoposorcustomer/:templateid', templateCntrl.assignedTemplatetoposORcustomer)
router.put('/changetempaltestatus/:templateid', templateCntrl.templateStatus)
router.delete('/deletetempalte/:templateid', templateCntrl.deleteTemplate)

module.exports = router
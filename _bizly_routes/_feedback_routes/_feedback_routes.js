const express = require('express')
const multer = require('multer')
const router = express.Router()
const feedbackCntrl = require('../../_bizly_controllers/_feedback_controllers/_feedback_controller')
const middleware = require('../../middleware/isAuthentic')

// router.use(middleware)

const DIR_associated_file = '/home/bizlypos/public_html/uploads/feedback';
let StorageConfig_associated_file = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, DIR_associated_file);
    },
    filename: (req, file, cb) => {
        cb(null, Date.now() + file.originalname);
    }
});

let upload_associated_file = multer({
    storage: StorageConfig_associated_file,
});

router.post('/addfeedback', feedbackCntrl.addFeedback)
router.post('/getfeedbackquestionfromcategory', feedbackCntrl.getfeedbackQuestionfromCategory)
router.get('/getallfeedbacks', feedbackCntrl.getAllFeedbacks)
router.get('/getfeedback/:feedbackid', feedbackCntrl.getFeedbackById)
router.get('/getestserviceliquorcategories', feedbackCntrl.getEstServiceLiquorcategories)
router.get('/getbizlytocustomerandbizlytoposdata/:createdfor', feedbackCntrl.getAllBizlytoCustomerAndBizlytoPosData)
router.put('/updatefeedback/:feedbackid', feedbackCntrl.updateFeedback)
router.put('/softdeletefeedbackquestion/:feedbackid', feedbackCntrl.softDeleteFeedback)
router.delete('/deletefeedback/:feedbackid', feedbackCntrl.deleteFeedback)
router.post('/uploadassociateimage', upload_associated_file.single('associatedimage'), (req, res) => {

    res.send('http://www.bizlypos.com/uploads/feedback/' + req.file.filename)
})

module.exports = router
const express = require('express')
const router = express.Router()
const assigntempCntrl = require('../../_bizly_controllers/_feedback_controllers/_assign_template_controller')
const middleware = require('../../middleware/isAuthentic')

// router.use(middleware)
router.post('/addassigntemplate', assigntempCntrl.addAssignTemplate)
router.get('/getallassignedtemplate', assigntempCntrl.getAllAssignedTemplate)
router.get('/getassignedtemplate/:templateid', assigntempCntrl.getAssignedTemplateById)
router.get('/getallposassingnedtemplate', assigntempCntrl.getAllPOSassignTemplate)
router.get('/getallcustomerassignedtemplate', assigntempCntrl.getAllCustomerAssignTemplate)

module.exports = router
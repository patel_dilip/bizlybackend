const express = require('express')
const router = express.Router()
const categoryCntrl = require('../../_bizly_controllers/_feedback_controllers/_feedback_category_controller')
const middleware = require('../../middleware/isAuthentic')

// router.use(middleware)
router.post('/addrootquestiontype', categoryCntrl.addRootQuestionType)
router.put('/addquestiontype/:rootquestiontypeid', categoryCntrl.addQuestionType)
router.put('/addcategory/:rootquestiontypeid/:questiontypeid', categoryCntrl.addCategories)
router.put('/addchildcategory/:rootquestiontypeid/:categoryid', categoryCntrl.addchildCategory)
router.put('/addchildchildcategory/:rootquestiontypeid/:categoryid/:childcategoryid', categoryCntrl.addchildchildCategories)
router.get('/getallfeedbackCategory', categoryCntrl.getAllFeedbackCategories)
router.get('/getfeedbackcategory/:rootquestiontypeid', categoryCntrl.getFeedbackCategories)
module.exports = router
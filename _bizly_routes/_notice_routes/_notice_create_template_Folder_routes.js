const express = require('express')
const router = express.Router()
const noticecreatefoldreCntrl = require('../../_bizly_controllers/_notice_controllers/_notice_create_template_Folder_controller')

router.post('/noticecreatetemplatefolder', noticecreatefoldreCntrl.noticeCreateTemplateFolder)
router.get('/getnoticecreatetemplatefolder/:userid', noticecreatefoldreCntrl.getNoticeCreateTemplateFolder)

module.exports = router
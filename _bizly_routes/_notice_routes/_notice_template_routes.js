const express = require('express')
const router = express.Router()
const multer = require('multer')
const noticeTemplateCntrl = require('../../_bizly_controllers/_notice_controllers/_notice_template_controller')

const DIR_NOTICE_TEMPLATE = '/home/bizlypos/public_html/uploads/noticetemplate';


let StorageConfig_notice_template = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, DIR_NOTICE_TEMPLATE);
    },
    filename: (req, file, cb) => {
        cb(null, Date.now() + file.originalname);
    }
});

let upload_notice_template = multer({
    storage: StorageConfig_notice_template,
});

router.post('/addnoticetemplate', noticeTemplateCntrl.addNoticeTemplate)
router.get('/getallnoticetemplates', noticeTemplateCntrl.getAllTemplates)
router.get('/getnoticetemplate/:noticetemplateid', noticeTemplateCntrl.getTemplateByID)
router.put('/updatenoticetemplate/:noticetemplateid', noticeTemplateCntrl.updateNoticeTemplate)
router.put('/deletenoticetemplate/:noticetemplateid', noticeTemplateCntrl.deleteNoticeTemplate)

router.post('/uploadnoticetemplate', upload_notice_template.single('noticetemplate'), (req, res) => {
    res.send('http://www.bizlypos.com/uploads/noticetemplate/' + req.file.filename)
})

module.exports = router
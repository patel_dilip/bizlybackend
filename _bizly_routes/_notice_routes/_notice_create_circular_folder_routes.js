const express = require('express')
const router = express.Router()
const noticecreatefoldreCntrl = require('../../_bizly_controllers/_notice_controllers/_notice_create_circular_folder_controller')

router.post('/noticecreatecircularfolder', noticecreatefoldreCntrl.noticeCreateCircularFolder)
router.get('/getnoticecreatecircularfolder/:userid', noticecreatefoldreCntrl.getNoticeCreateCircularFolder)

module.exports = router
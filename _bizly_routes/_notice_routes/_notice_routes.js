const express = require('express')
const router = express.Router()
const multer = require('multer')
const noticeCntr = require('../../_bizly_controllers/_notice_controllers/_notice_controller')

const DIR_NOTICE = '/home/bizlypos/public_html/uploads/notice';


let StorageConfig_notice = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, DIR_NOTICE);
    },
    filename: (req, file, cb) => {
        cb(null, Date.now() + file.originalname);
    }
});

let upload_notice = multer({
    storage: StorageConfig_notice,
});


router.post('/addnotice', noticeCntr.addNotice)
router.get('/getallnotices', noticeCntr.getAllNotices)
router.get('/getnotice/:noticeid', noticeCntr.getNoticeByID)
router.put('/updatenotice/:noticeid', noticeCntr.updateNotice)
router.put('/deletenotice/:noticeid', noticeCntr.deleteNotice)
router.post('/createfolder',noticeCntr.createFolder)

router.post('/uploadnotice', upload_notice.single('notice'), (req, res) => {
    res.send('http://www.bizlypos.com/uploads/notice/' + req.file.filename)
})

module.exports = router
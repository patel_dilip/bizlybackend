const express = require('express')
const router = express.Router()
const noticeGroupCntrl = require('../../_bizly_controllers/_notice_controllers/_notice_group_controller')

router.post('/addnoticegroup', noticeGroupCntrl.addNoticeGroup)
router.get('/getallnoticegroups', noticeGroupCntrl.getAllNoticeGroups)
router.get('/getnoticegroup/:noticegroupid', noticeGroupCntrl.getNoticeGroupByID)
router.put('/updatenoticegroup/:noticegroupid', noticeGroupCntrl.updateNoticeGroup)
router.put('/deletenoticegroup/:noticegroupid', noticeGroupCntrl.deleteNoticeGroup)

module.exports = router
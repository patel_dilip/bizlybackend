const express = require('express')
const router = express.Router()
const buyPOSProductCtrl = require('../../_bizly_controllers/_pos_product/_buy_pos_product_controller')
// const middleware = require('../../../middleware/isAuthentic')

// router.use(middleware)
router.post('/buyposproduct', buyPOSProductCtrl.buyposproduct)
module.exports = router
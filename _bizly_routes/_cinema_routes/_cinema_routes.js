const express = require('express')
const router = express.Router()
const cinemaCntrl = require('../../_bizly_controllers/_cinema_controller/_cinema_controller')
const middleware = require('../../middleware/isAuthentic')

// router.use(middleware)
router.post('/addcinemainfo', cinemaCntrl.addcinemaInfo)
// router.put('/addseriesinfo/:cinemaid', cinemaCntrl.addSeriesInfo)
router.get('/getallcinemas', cinemaCntrl.getAllCinemaData)
router.get('/getcienma/:cinemaid', cinemaCntrl.getCinemaData)
router.put('/updatecinema/:cinemaid', cinemaCntrl.updateCinemaData)
router.put('/softdeletecinema/:cinemaid', cinemaCntrl.softdeleteCinemaData)
router.delete('/deletecinema/:cinemaid', cinemaCntrl.deleteCinemaData)
router.delete('/deleteallcinema', cinemaCntrl.deleteAllCinemaData)

module.exports = router
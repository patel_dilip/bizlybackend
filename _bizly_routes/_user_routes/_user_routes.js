const express = require("express");
const router = express.Router();
const userCtrl = require("../../_bizly_controllers/_user_controller/_user_controller");
const multer = require("multer");

//user photo upload
const userPhoto = "/home/bizlypos/public_html/uploads/userphotos";
let StorageConfig_User_Photo = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, userPhoto);
  },
  filename: (req, file, cb) => {
    cb(null, Date.now() + file.originalname);
  },
});

let upload_user_photo = multer({
  storage: StorageConfig_User_Photo,
});

//legal document file upload
const legalDocument = "/home/bizlypos/public_html/uploads/legaldocuments";
let StorageConfig_LegalDocument = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, legalDocument);
  },
  filename: (req, file, cb) => {
    cb(null, Date.now() + file.originalname);
  },
});

let upload_legal_document = multer({
  storage: StorageConfig_LegalDocument,
});

router.post("/adduser", userCtrl.addUser);
router.get("/getallusers", userCtrl.getAllUsers);
router.get("/getuser/:userid", userCtrl.getUser);
router.put("/updateuser/:userid", userCtrl.updateUser);
router.put("/softdeleteuser/:userid", userCtrl.softDeleteUser);
router.delete("/deleteuser/:userid", userCtrl.deleteUser);
router.delete("/deleteallusers", userCtrl.deleteAllUsers);
router.post(
  "/uploaduserphoto",
  upload_user_photo.single("userphoto"),
  (req, res) => {
    res.send("http://www.bizlypos.com/uploads/userphotos/" + req.file.filename);
  }
);
router.post(
    "/uploadlegaldocument",
    upload_legal_document.single("legaldocument"),
    (req, res) => {
      res.send("http://www.bizlypos.com/uploads/legaldocuments/" + req.file.filename);
    }
  );


module.exports = router;

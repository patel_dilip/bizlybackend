const questionSetModel = require('../../_bizly_models/_feedback_model/_QuestionSet_model').questionSet

const addQuestionSet = (req, res) => {
    var set = new questionSetModel()
    set.questionSetName = req.body.questionSetName
    set.questionSetCreatedFor = req.body.questionSetCreatedFor
    set.selectedQuestions = req.body.selectedQuestions
    set.categoryType = req.body.categoryType
    set.addedBy = req.body.userid
    set.updatedBy = req.body.updatedBy

    set.save((err) => {
        if (err) {
            res.json({ success: false, msg: err })
        } else {
            res.json({ success: true, msg: "Question Set Added" })
        }
    })
}

const getAllQuestionSet = (req, res) => {
    questionSetModel.find({}).
        populate('selectedQuestions').
        populate('addedBy', 'userName').
        populate('updatedBy', 'userName').
        sort('-createdAt').
        exec((err, data) => {
            if (err) {
                res.json({ success: false, msg: err })
            } else if (data == null, data.length == 0) {
                res.json({ success: false, msg: "Question set Not Found" })
            } else {
                res.json({ success: true, msg: "Question set found", data: data })
            }
        })
}

const getQuestionSetById = (req, res) => {
    questionSetModel.find({ _id: req.params.questionsetid }).
        populate('selectedQuestions').
        populate('addedBy', 'userName').
        populate('updatedBy', 'userName').
        exec((err, data) => {
            if (err) {
                res.json({ success: false, msg: err })
            } else if (data == null, data.length == 0) {
                res.json({ success: false, msg: "Question set Not Found" })
            } else {
                res.json({ success: true, msg: "Question set found", data: data })
            }
        })
}

const getQuestionSetfromCategory = (req, res) => {
    questionSetModel.find({ questionSetCreatedFor: req.body.createdfor }, (err, data) => {
        if (err) {
            res.json({ success: false, msg: err })
        } else if (data == null || data.length == 0) {
            res.json({ success: false, msg: "Data not found" })
        } else {

            var arr1 = []
            var arr2 = []
            var arr3 = []
            var arr4 = []
            var arr5 = []
            for (let i = 0; i < data.length; i++) {
                if (data[i].categoryType.length > 0) {
                    for (let j = 0; j < data[i].categoryType.length; j++) {
                        if (data[i].categoryType[j].rootQuestionType == req.body.string) {
                            var promise1 = new Promise((resolve, reject) => {
                                arr1.push(data[i])
                                resolve(arr1)
                            })
                        } else if (data[i].categoryType[j].questionType.length > 0) {
                            for (let k = 0; k < data[i].categoryType[j].questionType.length; k++) {
                                if (data[i].categoryType[j].questionType[k].rootCategoryName == req.body.string) {
                                    var promise2 = new Promise((resolve, reject) => {
                                        arr2.push(data[i])
                                        resolve(arr2)
                                    })
                                } else if (data[i].categoryType[j].questionType[k].categories.length > 0) {
                                    for (let l = 0; l < data[i].categoryType[j].questionType[k].categories.length; l++) {
                                        if (data[i].categoryType[j].questionType[k].categories[l].categoryName == req.body.string) {
                                            var promise3 = new Promise((resolve, reject) => {
                                                arr3.push(data[i])
                                                resolve(arr3)
                                            })
                                        } else if (data[i].categoryType[j].questionType[k].categories[l].childCategories.length > 0) {
                                            for (let m = 0; m < data[i].categoryType[j].questionType[k].categories[l].childCategories.length; m++) {
                                                if (data[i].categoryType[j].questionType[k].categories[l].childCategories[m].childCategoryName == req.body.string) {
                                                    var promise4 = new Promise((resolve, reject) => {
                                                        arr4.push(data[i])
                                                        resolve(arr4)
                                                    })
                                                } else if (data[i].categoryType[j].questionType[k].categories[l].childCategories[m].childChildCategories.length > 0) {
                                                    for (let n = 0; n < data[i].categoryType[j].questionType[k].categories[l].childCategories[m].childChildCategories.length; n++) {
                                                        if (data[i].categoryType[j].questionType[k].categories[l].childCategories[m].childChildCategories[n].childChildCategoryName == req.body.string) {
                                                            var promise5 = new Promise((resolve, reject) => {
                                                                arr5.push(data[i])
                                                                resolve(arr5)
                                                            })
                                                        } else {
                                                            continue;
                                                        }
                                                    }
                                                } else {
                                                    console.log("child child category length=0");
                                                }
                                            }
                                        } else {
                                            console.log("child category length=0")
                                        }
                                    }
                                } else {
                                    console.log("Categories length=0");
                                }
                            }
                        } else {
                            console.log("Question type length=0")
                        }
                    }
                } else {
                    console.log("Data length =0");

                }

            }
            Promise.all([promise1, promise2, promise3, promise4, promise5]).then((promiseData) => {
                promiseData.filter(finalData => {
                    if (finalData == null) {
                        console.log('negative')
                    } else {
                        res.json({ success: true, msg: "Questions found", data: finalData })
                    }
                })

            }).catch((err) => {
                res.json({ success: false, msg: "err in promise data", error: err })
            })
        }
    })
}

const updateQuestionSet = (req, res) => {
    questionSetModel.findOneAndUpdate({ _id: req.params.questionsetid },
        {
            $set: {
                questionSetName: req.body.questionSetName,
                questionSetCreatedFor: req.body.questionSetCreatedFor,
                selectedQuestions: req.body.selectedQuestions,
                categoryType: req.body.categoryType,
                updatedBy: req.body.userid,
                updatedAt: Date.now(),
            }
        }, (err, udata) => {
            if (err) {
                res.json({ success: false, msg: err })
            } else if (udata == null, udata.length == 0) {
                res.json({ success: false, msg: "Question Set Not Found" })
            } else {
                res.json({ success: true, msg: "Question Set Updated" })
            }
        })
}

const questionSetStatus = (req, res) => {
    questionSetModel.findByIdAndUpdate(req.params.questionsetid,
        {
            $set: {
                status: req.body.status
            }
        }, (err, data) => {
            if (err) {
                res.json({ success: false, msg: err })
            } else if (data == null || data.length == 0) {
                res.json({ success: false, msg: "Question set not found" })
            } else {
                res.json({ success: true, msg: "Ouestion set Status change" })
            }
        })
}

const deleteQuestionSet = (req, res) => {
    questionSetModel.findOneAndDelete({ _id: req.params.questionsetid }, (err, data) => {
        if (err) {
            res.json({ success: false, msg: err })
        } else {
            res.json({ success: true, msg: "Question Set Deleted" })
        }
    })
}

module.exports = {
    addQuestionSet,
    getAllQuestionSet,
    getQuestionSetById,
    getQuestionSetfromCategory,
    updateQuestionSet,
    questionSetStatus,
    deleteQuestionSet
}
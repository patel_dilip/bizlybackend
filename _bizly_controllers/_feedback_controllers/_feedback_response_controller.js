const feedbackResposneModel = require('../../_bizly_models/_feedback_model/_feedback_response_model').response

const addFeedbackResponse = (req, res) => {
    var response = new feedbackResposneModel()
    response.full_Name = req.body.full_Name
    response.email = req.body.email
    response.mobile = req.body.mobile
    response.suggestion = req.body.suggestion
    response.selectedQuestion = req.body.selectedQuestion
    response.addedBy = req.body.userid
    response.updatedBy = req.body.userid

    response.save((err) => {
        if (err) {
            res.json({ success: false, msg: err })
        } else {
            res.json({ success: true, msg: "Response Added" })
        }
    })
}

const getAllFeedbackResponse = (req, res) => {
    feedbackResposneModel.find().
        populate('addedBy', 'username').
        populate('updatedBy', 'username').
        exec((err, data) => {
            if (err) {
                res.json({ success: false, msg: err })
            } else if (data == null || data.length == 0) {
                res.json({ success: false, msg: "feedback Response not found" })
            } else {
                res.json({ success: true, msg: "Feedback Resposne Found", data: data })
            }
        })
}

const getFeedbackResponseById = (req, res) => {
    feedbackResposneModel.findById(req.params.responseid).
        populate('addedBy', 'username').
        populate('updatedBy', 'username').
        exec((err, data) => {
            if (err) {
                res.json({ success: false, msg: err })
            } else if (data == null || data.length == 0) {
                res.json({ success: false, msg: "feedback Response not found" })
            } else {
                res.json({ success: true, msg: "Feedback Resposne Found", data: data })
            }
        })
}

const updateFeedbackResponse = (req, res) => {
    feedbackResposneModel.findOneAndUpdate({ _id: req.params.responseid },
        {
            $set: {
                full_Name: req.body.full_Name,
                email: req.body.email,
                mobile: req.body.mobile,
                suggestion: req.body.suggestion,
                selectedQuestion: req.body.selectedQuestion,
                updatedBy: req.body.userid,
                updatedAt: Date.now
            }
        }, (err, udata) => {
            if (err) {
                res.json({ success: false, msg: err })
            } else if (udata == null || udata.length == 0) {
                res.json({ success: false, msg: "feedback response not found" })
            } else {
                res.json({ success: true, msg: "Feedback Response Updated" })
            }
        })
}

const deleteFeedbackResponse = (req, res) => {
    feedbackResposneModel.findByIdAndDelete(req.params.responseid, (err, data) => {
        if (err) {
            res.json({ success: false, msg: err })
        } else if (data == null || data.length == 0) {
            res.json({ success: false, msg: "feedback response not found" })
        } else {
            res.json({ success: true, msg: "Resposne Deleted" })
        }
    })
}

module.exports = {
    addFeedbackResponse,
    getAllFeedbackResponse,
    getFeedbackResponseById,
    updateFeedbackResponse,
    deleteFeedbackResponse
}
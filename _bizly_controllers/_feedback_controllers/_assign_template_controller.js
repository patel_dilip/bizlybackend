const assignTemplateModel = require('../../_bizly_models/_feedback_model/_assign_template_model').template

const addAssignTemplate = (req, res) => {
    var template = new assignTemplateModel()
    template.templateid = req.body.templateid
    template.templateName = req.body.templateName
    template.assignPOS = req.body.assignPOS
    template.assignCustomer = req.body.assignCustomer
    template.addedBy = req.body.userid
    template.updatedBy = req.body.userid

    template.save((err) => {
        if (err) {
            res.json({ success: false, msg: err })
        } else {
            res.json({ success: true, msg: "Template Assigned" })
        }
    })
}

const getAllAssignedTemplate = (req, res) => {
    assignTemplateModel.find().
        populate('addedBy', 'userName').
        populate('updatedBy', 'userName').
        exec((err, data) => {
            if (err) {
                res.json({ success: false, msg: err })
            } else if (data == null || data.length == 0) {
                res.json({ success: false, msg: "Data Not Found" })
            } else {
                res.json({ success: false, msg: "Assign Template Found", data: data })
            }
        })
}

const getAssignedTemplateById = (req, res) => {
    assignTemplateModel.find({ _id: req.params.templateid }).
        populate('addedBy', 'userName').
        populate('updatedBy', 'userName').
        exec((err, data) => {
            if (err) {
                res.json({ success: false, msg: err })
            } else if (data == null || data.length == 0) {
                res.json({ success: false, msg: "Data Not Found" })
            } else {
                res.json({ success: false, msg: "Assign Template Found", data: data })
            }
        })
}

const getAllPOSassignTemplate = (req, res) => {
    assignTemplateModel.find({ assignPOS: true }).
        populate('addedBy', 'userName').
        populate('updatedBy', 'userName').
        exec((err, data) => {
            if (err) {
                res.json({ success: false, msg: err })
            } else if (data == null || data.length == 0) {
                res.json({ success: false, msg: "Data Not Found" })
            } else {
                res.json({ success: false, msg: "POS Assigned Template Found", data: data })
            }
        })
}

const getAllCustomerAssignTemplate = (req, res) => {
    assignTemplateModel.find({ assignCustomer: true }).
        populate('addedBy', 'userName').
        populate('updatedBy', 'userName').
        exec((err, data) => {
            if (err) {
                res.json({ success: false, msg: err })
            } else if (data == null || data.length == 0) {
                res.json({ success: false, msg: "Data Not Found" })
            } else {
                res.json({ success: false, msg: "Customer Assigned Template Found", data: data })
            }
        })
}

module.exports = {
    addAssignTemplate,
    getAllAssignedTemplate,
    getAssignedTemplateById,
    getAllPOSassignTemplate,
    getAllCustomerAssignTemplate
}
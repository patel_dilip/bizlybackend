const feedbackCategoryModel = require('../../_bizly_models/_feedback_model/_feedback_category_model').category

const addRootQuestionType = (req, res) => {
    var root = new feedbackCategoryModel()
    root.rootQuestionType = req.body.rootQuestionType
    // root.questionType = req.body.questionType
    root.addedBy = req.body.userid
    root.updatedBy = req.body.userid

    root.save((err) => {
        if (err) {
            res.json({ success: false, msg: err })
        } else {
            res.json({ success: true, msg: 'root question type added' })
        }
    })
}


const addQuestionType = (req, res) => {
    feedbackCategoryModel.findByIdAndUpdate(req.params.rootquestiontypeid,
        { $push: { questionType: req.body } },
        (err, data) => {
            if (err) {
                res.json({ success: false, msg: err })
            } else if (data == null) {
                res.json({ success: false, msg: "root question type not found" })
            } else {
                res.json({ success: true, msg: "Question Type added" })
            }
        })
}

const addCategories = (req, res) => {
    feedbackCategoryModel.updateOne({ _id: req.params.rootquestiontypeid, 'questionType._id': req.params.questiontypeid },
        { $push: { 'questionType.$.categories': req.body } },
        (err, data) => {
            if (err) {
                res.json({ success: false, msg: err })
            } else if (data == null) {
                res.json({ success: false, msg: "rootcategory not found" })
            } else {
                res.json({ success: true, msg: "Category added" })
            }
        })
}

const addchildCategory = (req, res) => {
    feedbackCategoryModel.updateOne({ _id: req.params.rootquestiontypeid, 'questionType.categories._id': req.params.categoryid },
        {
            $push: {
                'questionType.$.categories.$[outer].childCategories': req.body
            }
        },
        {
            "arrayFilters": [
                {
                    "outer._id": req.params.categoryid,
                }
            ]
        }, (err, data) => {
            if (err) {
                res.json({ success: false, msg: err })
            } else if (data == null) {
                res.json({ success: false, msg: "rootcategory not found" })
            } else {
                res.json({ success: true, msg: "child category added" })
            }
        })
}

const addchildchildCategories = (req, res) => {
    feedbackCategoryModel.updateOne({
        _id: req.params.rootquestiontypeid,
        'questionType.categories._id': req.params.categoryid,
        'questionType.categories.childCategories._id': req.params.childcategoryid
    },
        {
            $push: {
                'questionType.$.categories.$[outer].childCategories.$[inner].childChildCategories': req.body
            }
        },
        {
            "arrayFilters": [
                {
                    "outer._id": req.params.categoryid,
                },
                {
                    'inner._id': req.params.childcategoryid,
                }
            ]
        }, (err, data) => {
            if (err) {
                res.json({ success: false, msg: err })
            } else if (data == null) {
                res.json({ success: false, msg: "rootcategory not found" })
            } else {
                res.json({ success: true, msg: "child child category added" })
            }
        })
}

const getFeedbackCategories = async (req, res) => {
    feedbackCategoryModel.findOne({ _id: req.params.rootquestiontypeid }).
        populate('addedBy', 'userName').
        populate('updatedBy', 'userName').
        exec(function (err, rootcategory) {
            if (err) {
                throw err
            } else if (rootcategory == null) {
                res.json({ success: false, msg: 'rootcategory not found' })
            } else {
                res.json({ success: true, msg: 'rootcategory found', data: rootcategory })
            }

        })
}

const getAllFeedbackCategories = (req, res) => {
    feedbackCategoryModel.find().
        populate('addedBy', 'userName').
        populate('updatedBy', 'userName').
        exec(function (err, rootcategories) {
            if (err) {
                throw err
            } else if (rootcategories == null || rootcategories.length == 0) {
                res.json({ success: false, msg: "rootcategories Not Found" })
            } else {
                res.json({ success: true, msg: "rootcategories Found", data: rootcategories })
            }
        })
}

module.exports = {
    addRootQuestionType,
    addQuestionType,
    addCategories,
    addchildCategory,
    addchildchildCategories,
    getAllFeedbackCategories,
    getFeedbackCategories
}
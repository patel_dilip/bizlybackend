const feedbackModel = require('../../_bizly_models/_feedback_model/_feedback_model').feedback
const questionSetModel = require('../../_bizly_models/_feedback_model/_QuestionSet_model').questionSet
const templateModel = require('../../_bizly_models/_feedback_model/_template_model').template
const establishmentModel = require('../../_bizly_models/_restaurant_models/_establishment-types-model').establishmentmodel
const serviceModel = require('../../_bizly_models/_restaurant_models/_services-model').servicemodel
const liquorModel = require('../../_bizly_models/_restaurant_models/_liquor-type-model').liquorModel

const addFeedback = (req, res) => {
    var promiseArr = []
    for (let i = 0; i < req.body.length; i++) {
        promiseArr.push(new Promise((resolve, reject) => {
            var feedback = new feedbackModel()
            feedback.label_Name = req.body[i].label_Name
            feedback.inputType = req.body[i].inputType
            feedback.platForm = req.body[i].platForm
            feedback.commentbox = req.body[i].commentbox
            feedback.YesNoButtons = req.body[i].YesNoButtons
            feedback.dropDown = req.body[i].dropDown
            feedback.radioButtons = req.body[i].radioButtons
            feedback.multiChoices = req.body[i].multiChoices
            feedback.multiCheckboxes = req.body[i].multiCheckboxes
            feedback.multiCheckboxesImage = req.body[i].multiCheckboxesImage
            feedback.slider = req.body[i].slider
            feedback.starRating = req.body[i].starRating
            feedback.scale = req.body[i].scale
            feedback.dateTime = req.body[i].dateTime
            feedback.feedbackQuestionCreatedFor = req.body[i].feedbackQuestionCreatedFor
            feedback.categoryType = req.body[i].categoryType
            feedback.matrixRadioButton = req.body[i].matrixRadioButton
            feedback.addedBy = req.body[i].userid
            feedback.updatedBy = req.body[i].userid

            feedback.save((err) => {
                if (err) {
                    reject(err)
                } else {
                    resolve(feedback)
                }
            })
        }))
    }
    Promise.all(promiseArr).then((promiseArr) => {
        res.json({ success: true, msg: "Feedback form Added" })
    }).catch((err) => {
        res.json({ success: false, msg: "error in promise arr", error: err })
    })
}

const getAllFeedbacks = (req, res) => {
    feedbackModel.find()
        .populate('addedBy', 'userName')
        .populate('updatedBy', 'userName')
        .sort('-createdAt')
        .exec((err, data) => {
            if (err) {
                res.json({ success: false, msg: err })
            } else if (data == null || data.length == 0) {
                res.json({ success: false, msg: "Feedbacks Not Found" })
            } else {
                res.json({ success: true, msg: "feedback data found", data: data })
            }
        })
}

const getFeedbackById = (req, res) => {
    feedbackModel.findById(req.params.feedbackid)
        .populate('addedBy', 'userName')
        .populate('updatedBy', 'userName')
        .exec((err, data) => {
            if (err) {
                res.json({ success: false, msg: err })
            } else if (data == null || data.length == 0) {
                res.json({ success: false, msg: "Feedbacks Not Found" })
            } else {
                res.json({ success: true, msg: "feedback data found", data: data })
            }
        })
}

const getEstServiceLiquorcategories = async (req, res) => {
    try {
        let finalObj = new Array();
        establishmentModel.find()
            .then((estData) => {
                finalObj.push({
                    rootQuestionType: "establishment Data",
                    questionType: estData
                })
                liquorModel.find()
                    .then((liquorData) => {
                        finalObj.push({
                            rootQuestionType: "Liquor Data",
                            questionType: liquorData
                        })
                        res.json({
                            success: true,
                            data: finalObj
                        })
                    }).catch((err) => {
                        res.json({ success: false, msg: "err in liquor data", error: err })
                    })

            }).catch((err) => {
                res.json({ success: false, msg: "err in est data", error: err })
            })
    } catch (ex) {
        res.json({ success: false, error: ex.message })
    }

}

const getfeedbackQuestionfromCategory = (req, res) => {
    feedbackModel.find({ feedbackQuestionCreatedFor: req.body.createdfor }, (err, data) => {
        if (err) {
            res.json({ success: false, msg: err })
        } else if (data == null || data.length == 0) {
            res.json({ success: false, msg: "Data not found" })
        } else {

            var arr1 = []
            var arr2 = []
            var arr3 = []
            var arr4 = []
            var arr5 = []
            for (let i = 0; i < data.length; i++) {
                if (data[i].categoryType.length > 0) {
                    for (let j = 0; j < data[i].categoryType.length; j++) {
                        if (data[i].categoryType[j].rootQuestionType == req.body.string) {
                            var promise1 = new Promise((resolve, reject) => {
                                arr1.push(data[i])
                                resolve(arr1)
                            })
                        } else if (data[i].categoryType[j].questionType.length > 0) {
                            for (let k = 0; k < data[i].categoryType[j].questionType.length; k++) {
                                if (data[i].categoryType[j].questionType[k].rootCategoryName == req.body.string) {
                                    var promise2 = new Promise((resolve, reject) => {
                                        arr2.push(data[i])
                                        resolve(arr2)
                                    })
                                } else if (data[i].categoryType[j].questionType[k].categories.length > 0) {
                                    for (let l = 0; l < data[i].categoryType[j].questionType[k].categories.length; l++) {
                                        if (data[i].categoryType[j].questionType[k].categories[l].categoryName == req.body.string) {
                                            var promise3 = new Promise((resolve, reject) => {
                                                arr3.push(data[i])
                                                resolve(arr3)
                                            })
                                        } else if (data[i].categoryType[j].questionType[k].categories[l].childCategories.length > 0) {
                                            for (let m = 0; m < data[i].categoryType[j].questionType[k].categories[l].childCategories.length; m++) {
                                                if (data[i].categoryType[j].questionType[k].categories[l].childCategories[m].childCategoryName == req.body.string) {
                                                    var promise4 = new Promise((resolve, reject) => {
                                                        arr4.push(data[i])
                                                        resolve(arr4)
                                                    })
                                                } else if (data[i].categoryType[j].questionType[k].categories[l].childCategories[m].childChildCategories.length > 0) {
                                                    for (let n = 0; n < data[i].categoryType[j].questionType[k].categories[l].childCategories[m].childChildCategories.length; n++) {
                                                        if (data[i].categoryType[j].questionType[k].categories[l].childCategories[m].childChildCategories[n].childChildCategoryName == req.body.string) {
                                                            var promise5 = new Promise((resolve, reject) => {
                                                                arr5.push(data[i])
                                                                resolve(arr5)
                                                            })
                                                        } else {
                                                            continue;
                                                        }
                                                    }
                                                } else {
                                                    console.log("child child category length=0");
                                                }
                                            }
                                        } else {
                                            console.log("child category length=0")
                                        }
                                    }
                                } else {
                                    console.log("Categories length=0");
                                }
                            }
                        } else {
                            console.log("Question type length=0")
                        }
                    }
                } else {
                    console.log("Data length =0");

                }

            }
            Promise.all([promise1, promise2, promise3, promise4, promise5]).then((promiseData) => {
                promiseData.filter(finalData => {
                    if (finalData == null) {
                        console.log('negative')
                    } else {
                        res.json({ success: true, msg: "Questions found", data: finalData })
                    }
                })

            }).catch((err) => {
                res.json({ success: false, msg: "err in promise data", error: err })
            })
        }
    })
}

const getAllBizlytoCustomerAndBizlytoPosData = async (req, res) => {
    try {
        feedbackModel.find({ feedbackQuestionCreatedFor: req.params.createdfor })
            .populate('addedBy', 'userName')
            .populate('updatedBy', 'userName')
            .sort('-createdAt')
            .then((feedbackData) => {
                questionSetModel.find({ questionSetCreatedFor: req.params.createdfor })
                    .populate('addedBy', 'userName')
                    .populate('updatedBy', 'userName')
                    .sort('-createdAt')
                    .then((questionSetData) => {
                        templateModel.find({ templateCreatedFor: req.params.createdfor })
                            .populate('addedBy', 'userName')
                            .populate('updatedBy', 'userName')
                            .sort('-createdAt')
                            .then((templateData) => {
                                res.json({
                                    success: true,
                                    allFeedbackquestions: feedbackData,
                                    allQuestionSet: questionSetData,
                                    allTemplate: templateData
                                })
                            }).catch((err) => {
                                res.json({ success: false, msg: "error in Question set", error: err })
                            })
                    }).catch((err) => {
                        res.json({ success: false, msg: "error in Question set", error: err })
                    })
            }).catch((err) => {
                res.json({ success: false, msg: "error in feedback", error: err })
            })
    } catch (err) {
        res.json({ success: false, msg: err })
    }

}

const updateFeedback = (req, res) => {
    feedbackModel.findByIdAndUpdate(req.params.feedbackid,
        {
            $set: {
                label_Name: req.body.label_Name,
                inputType: req.body.inputType,
                platForm: req.body.platForm,
                commentbox: req.body.commentbox,
                feedbackQuestionCreatedFor: req.body.feedbackQuestionCreatedFor,
                YesNoButtons: req.body.YesNoButtons,
                dropDown: req.body.dropDown,
                radioButtons: req.body.radioButtons,
                multiChoices: req.body.multiChoices,
                multiCheckboxes: req.body.multiCheckboxes,
                multiCheckboxesImage: req.body.multiCheckboxesImage,
                slider: req.body.slider,
                starRating: req.body.starRating,
                scale: req.body.scale,
                dateTime: req.body.dateTime,
                categoryType: req.body.categoryType,
                matrixRadioButton: req.body.matrixRadioButton,
                updatedAt: Date.now()

            }
        }, (err, udata) => {
            if (err) {
                res.json({ success: false, msg: err })
            } else if (udata == null || udata.length == 0) {
                res.json({ success: false, msg: "feedback not found" })
            } else {
                res.json({ success: true, msg: "feedback updated" })
            }
        })
}

const softDeleteFeedback = (req, res) => {
    feedbackModel.findByIdAndUpdate(req.params.feedbackid,
        {
            $set: {
                status: req.body.status
            }
        }, (err, data) => {
            if (err) {
                res.json({ success: false, msg: err })
            } else {
                res.json({ success: true, msg: "feedback delete" })
            }
        })
}

const deleteFeedback = (req, res) => {
    feedbackModel.findByIdAndDelete(req.params.feedbackid, (err, data) => {
        if (err) {
            res.json({ success: false, msg: err })
        } else {
            res.json({ success: true, msg: "feedback delete" })
        }
    })
}

module.exports = {
    addFeedback,
    getAllFeedbacks,
    getFeedbackById,
    getEstServiceLiquorcategories,
    getfeedbackQuestionfromCategory,
    getAllBizlytoCustomerAndBizlytoPosData,
    updateFeedback,
    softDeleteFeedback,
    deleteFeedback
}
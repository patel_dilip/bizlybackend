const templateModel = require('../../_bizly_models/_feedback_model/_template_model').template

const addTemplate = (req, res) => {
    var template = new templateModel()
    template.templateName = req.body.templateName
    template.templateDescription = req.body.templateDescription
    template.selectFilter = req.body.selectFilter
    template.platForm = req.body.platForm
    template.templateCreatedFor = req.body.templateCreatedFor
    template.categoryType = req.body.categoryType
    template.selectedQuestions = req.body.selectedQuestions
    template.selectedQuestionsSet = req.body.selectedQuestionsSet
    template.addedBy = req.body.userid
    template.updatedBy = req.body.userid

    template.save((err) => {
        if (err) {
            res.json({ success: false, msg: err })
        } else {
            res.json({ success: true, msg: "Template Added" })
        }
    })
}

const getAllTemplate = (req, res) => {
    templateModel.find().
        populate('selectedQuestions').
        populate('selectedQuestionsSet').
        populate('addedBy', 'userName').
        populate('updatedBy', 'userName').
        sort('-createdAt').
        exec((err, data) => {
            if (err) {
                res.json({ success: false, msg: err })
            } else if (data == null || data.length == 0) {
                res.json({ success: false, msg: "Template not found" })
            } else {
                res.json({ success: true, msg: "template Found", data: data })
            }
        })
}

const getTemplateById = (req, res) => {
    templateModel.find({ _id: req.params.templateid }).
        populate('selectedQuestions').
        populate('selectedQuestionsSet').
        populate('addedBy', 'userName').
        populate('updatedBy', 'userName').
        exec((err, data) => {
            if (err) {
                res.json({ success: false, msg: err })
            } else if (data == null || data.length == 0) {
                res.json({ success: false, msg: "Template not found" })
            } else {
                res.json({ success: true, msg: "template Found", data: data })
            }
        })
}

const updateTemplate = (req, res) => {
    templateModel.findOneAndUpdate({ _id: req.params.templateid },
        {
            $set: {
                templateName: req.body.templateName,
                templateDescription: req.body.templateDescription,
                selectFilter: req.body.selectFilter,
                platForm: req.body.platForm,
                templateCreatedFor: req.body.templateCreatedFor,
                categoryType: req.body.categoryType,
                selectedQuestions: req.body.selectedQuestions,
                selectedQuestionsSet: req.body.selectedQuestionsSet,
                updatedAt: Date.now()
            }
        }, (err, udata) => {
            if (err) {
                res.json({ success: false, msg: err })
            } else if (udata == null || udata.length == 0) {
                res.json({ success: false, msg: "Template not found" })
            } else {
                res.json({ success: true, msg: "Template Updated" })
            }
        })
}

const assignedTemplatetoposORcustomer = (req, res) => {
    templateModel.findOneAndUpdate({ _id: req.params.templateid },
        {
            $set: {
                assignPOS: req.body.assignPOS,
                assignCustomer: req.body.assignCustomer
            }
        }, (err, data) => {
            if (err) {
                res.json({ success: false, msg: err })
            } else if (data == null || data.length == 0) {
                res.json({ success: false, msg: "template not found" })
            } else {
                res.json({ success: true, msg: "Template Assigned" })
            }
        })
}

const templateStatus = (req, res) => {
    templateModel.findByIdAndUpdate(req.params.templateid,
        {
            $set: {
                status: req.body.status
            }
        }, (err, data) => {
            if (err) {
                res.json({ success: false, msg: err })
            } else if (data == null || data.length == 0) {
                res.json({ success: false, msg: "template not found" })
            } else {
                res.json({ success: true, msg: "Template Status change" })
            }
        })
}

const deleteTemplate = (req, res) => {
    templateModel.findByIdAndDelete(req.params.templateid, (err, data) => {
        if (err) {
            res.json({ success: false, msg: err })
        } else {
            res.json({ success: true, msg: "Template Deleted" })
        }
    })
}

module.exports = {
    addTemplate,
    getAllTemplate,
    getTemplateById,
    updateTemplate,
    assignedTemplatetoposORcustomer,
    templateStatus,
    deleteTemplate
}
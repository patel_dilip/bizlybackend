const posOrdersModel = require("../../_bizly_models/_pos_orders/_pos_product_orders_model")
  .posOrdersModel;

const addPosOrder = (req, res) => {
  posOrdersModel.findOne().exec((err, data) => {
    if (err) {
      console.log(err);
    } else if (data == null) {
      var lastLicenceNumber = 0;
      var licenceNos = [];
      var currentDate = new Date();
      var dateAfter30Days = new Date(currentDate);

      dateAfter30Days.setDate(dateAfter30Days.getDate() + 365);
      // console.log(currentDate, dateAfter30Days);

      lastLicenceNumber = 0;

      var posOrder = new posOrdersModel(req.body);

      //   license_Nos
      for (let i = 0; i < req.body.noOfoutlets; i++) {
        // console.log(lastLicenceNumber);
        licenceNos.push({
          outlet_No: i + 1,
          license_No: lastLicenceNumber + i + 1,
          outletID: lastLicenceNumber + i + 1,
        });
        posOrder.lastLicenceNumber = lastLicenceNumber + i + 1;
        // posOrder.license_No.outlet_No = i + 1;
        // posOrder.license_No.license_No = lastLicenceNumber + i + 1;
        // posOrder.license_No.outletID = lastLicenceNumber + i + 1;
      }
      // console.log(posOrder.lastLicenceNumber);

      posOrder.subscriptionActivationDate = currentDate;
      posOrder.subscriptionExpiryDate = dateAfter30Days;
      posOrder.license_No = licenceNos;
      posOrder.addedBy = req.body.userid;
      posOrder.updatedBy = req.body.userid;

      posOrder.save((err) => {
        if (err) {
          res.json({ sucess: false, msg: err });
        } else {
          res.json({ sucess: true });
        }
      });
    } else {
      var lastLicenceNumber = 0;
      var licenceNos = [];
      var currentDate = new Date();
      var dateAfter30Days = new Date(currentDate);

      dateAfter30Days.setDate(dateAfter30Days.getDate() + 365);
      // console.log(currentDate, dateAfter30Days);

      lastLicenceNumber = data["lastLicenceNumber"];

      var posOrder = new posOrdersModel(req.body);

      //   license_No
      for (let i = 0; i < req.body.noOfoutlets; i++) {
        // console.log(lastLicenceNumber);
        licenceNos.push({
          outlet_No: i + 1,
          license_No: lastLicenceNumber + i + 1,
          outletID: lastLicenceNumber + i + 1,
        });
        posOrder.lastLicenceNumber = lastLicenceNumber + i + 1;
        // posOrder.license_No.outlet_No = i + 1;
        // posOrder.license_No.license_No = lastLicenceNumber + i + 1;
        // posOrder.license_No.outletID = lastLicenceNumber + i + 1;
      }
      // console.log(posOrder.lastLicenceNumber);

      posOrder.subscriptionActivationDate = currentDate;
      posOrder.subscriptionExpiryDate = dateAfter30Days;
      posOrder.license_No = licenceNos;
      posOrder.addedBy = req.body.userid;
      posOrder.updatedBy = req.body.userid;

      posOrder.save((err) => {
        if (err) {
          res.json({ sucess: false, msg: err });
        } else {
          res.json({ sucess: true });
        }
      });
    }
  });
};

const getAllPOSProductOrders = async (req, res) => {
  posOrdersModel
    .find()
    .populate("productName", "productName")
    .populate("addedBy", "userName")
    .populate("updatedBy", "userName")
    .exec((err, data) => {
      if (err) {
        res.json({ sucess: false, msg: err });
      } else if (data == null || data.length == 0) {
        res.json({ sucess: false });
      } else {
        res.json({ sucess: true, data: data });
      }
    });
};

const getPOSProductOrderById = async (req, res) => {
  posOrdersModel
    .findById(req.params.posproductorderid)
    .populate("productName", "productName")
    .populate("addedBy", "userName")
    .populate("updatedBy", "userName")
    .exec((err, data) => {
      if (err) {
        res.json({ sucess: false, msg: err });
      } else if (data == null) {
        res.json({ sucess: false });
      } else {
        res.json({ sucess: true, data: data });
      }
    });
};

const updatePOSProductOrderByID = async (req, res) => {
  posOrdersModel.findByIdAndUpdate(
    req.params.posproductorderid,
    { $set: req.body },
    (err, posOrders) => {
      if (err) {
        res.json({ sucess: false, msg: err });
      } else {
        res.json({ sucess: true });
      }
    }
  );
};

const softDeletePOSProductOrder = async (req, res) => {
  posOrdersModel.findByIdAndUpdate(
    req.params.posproductorderid,
    { $set: { status: req.body.status } },
    (err) => {
      if (err) {
        res.json({ sucess: false, msg: err });
      } else {
        res.json({ sucess: true });
      }
    }
  );
};

const deletePOSProductOrder = async (req, res) => {
  posOrdersModel.deleteOne({ _id: req.params.posproductorderid }, (err) => {
    if (err) {
      res.json({ sucess: false, msg: err });
    } else {
      res.json({ sucess: true });
    }
  });
};

const deleteAllPOSProductOrders = async (req, res) => {
  posOrdersModel.deleteMany({}, (err) => {
    if (err) {
      res.json({ sucess: false, msg: err });
    } else {
      res.json({ sucess: true });
    }
  });
};

module.exports = {
  addPosOrder,
  getAllPOSProductOrders,
  getPOSProductOrderById,
  updatePOSProductOrderByID,
  softDeletePOSProductOrder,
  deletePOSProductOrder,
  deleteAllPOSProductOrders
};

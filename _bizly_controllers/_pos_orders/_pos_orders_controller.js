const posOrdersModel = require("../../_bizly_models/_pos_orders/_pos_orders_model")
  .posOrdersModel;
const posPaymentLinkModel = require("../../_bizly_models/_pos_orders/_pos_orders_model")
  .posPaymentLinkModel;

/* -----------------------------------------POS Product Orders----------------------------------------- */
const addPOSProductOrder = (req, res) => {
  posOrdersModel
    .findOne()
    .sort({ posOrderID: -1 })
    .exec((err, data) => {
      if (err) {
        console.log(err);
      } else if (data == null) {
        var lastLicenceNumber = 0;
        var licenceNos = [];
        var currentDate = new Date();
        var dateAfter30Days = new Date(currentDate);
        var posOrder = new posOrdersModel(req.body);

        //Generate Expiry Date
        if (req.body.posProductOrder.subscription_Type == "Monthly") {
          dateAfter30Days.setDate(dateAfter30Days.getDate() + 30);
        }
        if (req.body.posProductOrder.subscription_Type == "Yearly") {
          dateAfter30Days.setDate(dateAfter30Days.getDate() + 365);
        }
        lastLicenceNumber = 0;
        //   license_Nos
        for (let i = 0; i < req.body.posProductOrder.noOfoutlets; i++) {
          licenceNos.push({
            outlet_No: i + 1,
            license_No: lastLicenceNumber + i + 1,
            outletID: lastLicenceNumber + i + 1,
          });
          posOrder.posProductOrder.lastLicenceNumber =
            lastLicenceNumber + i + 1;
        }

        posOrder.posProductOrder.posProductOrderID = 1;
        posOrder.posProductOrder.subscriptionActivationDate = currentDate;
        posOrder.posProductOrder.subscriptionExpiryDate = dateAfter30Days;
        posOrder.posProductOrder.license_No = licenceNos;
        //   posOrder.posAddonOrder = null
        //   posOrder.posIntegrationOrder = null
        posOrder.save((err) => {
          if (err) {
            res.json({ sucess: false, msg: err });
          } else {
            res.json({ sucess: true });
          }
        });
      } else {
        var lastLicenceNumber;
        var licenceNos = [];
        var currentDate = new Date();
        var dateAfter30Days = new Date(currentDate);
        var posOrder = new posOrdersModel(req.body);

        //Generate Expiry Date
        if (req.body.posProductOrder.subscription_Type == "Monthly") {
          dateAfter30Days.setDate(dateAfter30Days.getDate() + 30);
        }
        if (req.body.posProductOrder.subscription_Type == "Yearly") {
          dateAfter30Days.setDate(dateAfter30Days.getDate() + 365);
        }

        lastLicenceNumber = data["posProductOrder"]["lastLicenceNumber"];
        console.log(lastLicenceNumber);
        //   license_Nos
        for (let i = 0; i < req.body.posProductOrder.noOfoutlets; i++) {
          licenceNos.push({
            outlet_No: i + 1,
            license_No: lastLicenceNumber + i + 1,
            outletID: lastLicenceNumber + i + 1,
          });
          posOrder.posProductOrder.lastLicenceNumber =
            lastLicenceNumber + i + 1;
          console.log(posOrder.posProductOrder.lastLicenceNumber);
        }

        posOrder.posProductOrder.subscriptionActivationDate = currentDate;
        posOrder.posProductOrder.subscriptionExpiryDate = dateAfter30Days;
        posOrder.posProductOrder.license_No = licenceNos;
        posOrder.posProductOrder.posProductOrderID =
          data["lastPOSProductOrderID"] + 1;

        posOrder.save((err) => {
          if (err) {
            res.json({ sucess: false, msg: err });
          } else {
            res.json({ sucess: true });
          }
        });
      }
    });
};

const getAllPOSProductOrders = async (req, res) => {
  posOrdersModel
    .find({ status: true })
    .populate("posProductOrder.productName", "productName")
    .select("-posAddonOrder -posIntegrationOrder")
    .exec((err, data) => {
      if (err) {
        res.json({ sucess: false, msg: err });
      } else if (data == null || data.length == 0) {
        res.json({ sucess: false });
      } else {
        res.json({ sucess: true, data: data });
      }
    });
};

const getPOSProductOrderById = async (req, res) => {
  posOrdersModel
    .findById(req.params.posproductorderid)
    .populate("posProductOrder.productName", "productName")
    .select("-posAddonOrder -posIntegrationOrder")
    .exec((err, data) => {
      if (err) {
        res.json({ sucess: false, msg: err });
      } else if (data == null) {
        res.json({ sucess: false });
      } else {
        res.json({ sucess: true, data: data });
      }
    });
};

const getPOSOrderStats = async (req, res) => {
  var totalData = {};
  posOrdersModel.aggregate(
    [
      {
        $match: {
          status: true,
        },
      },
      {
        $group: {
          _id: null,
          totalPOSProductOrders: { $sum: 1 },
          totalPOSProductOutlets: { $sum: "$posProductOrder.noOfoutlets" },
          totalPOSProductOrderRevenueAmt: {
            $sum: "$posProductOrder.PriceafterDiscount",
          },
          totalPOSProductOrderDiscount: {
            $sum: "$posProductOrder.DiscountedPrice",
          },
        },
      },
    ],
    (err, result) => {
      if (err) {
        res.json({ sucess: false, msg: err });
      } else if (result == null) {
        res.json({ sucess: false });
      } else {
        totalData.posProductData = result[0];
        posOrdersModel.aggregate(
          [
            {
              $match: {
                "posAddonOrder.status": true,
              },
            },
            {
              $group: {
                _id: null,
                totalAddonOutlets: { $sum: "$posAddonOrder.noOfoutlets" },
                totalPOSAddonOrderRevenueAmt: {
                  $sum: "$posAddonOrder.PriceafterDiscount",
                },
                totalPOSAddonOrderDiscount: {
                  $sum: "$posAddonOrder.DiscountedPrice",
                },
                totalPOSAddonOrders: { $sum: 1 },
              },
            },
          ],
          (err, result) => {
            if (err) {
              res.json({ sucess: false, msg: err });
            } else if (result == null) {
              res.json({ sucess: false });
            } else {
              totalData.posAddonData = result[0];
              posOrdersModel.aggregate(
                [
                  {
                    $match: {
                      "posIntegrationOrder.status": true,
                    },
                  },
                  {
                    $group: {
                      _id: null,
                      totalIntegrationOutets: {
                        $sum: "$posIntegrationOrder.noOfoutlets",
                      },
                      totalIntegrationOrderRevenueAmt: {
                        $sum: "$posIntegrationOrder.PriceafterDiscount",
                      },
                      totalPOSIntegrationOrderDiscount: {
                        $sum: "$posIntegrationOrder.DiscountedPrice",
                      },
                      totalPOSIntegrationOrders: { $sum: 1 },
                    },
                  },
                ],
                (err, result) => {
                  if (err) {
                    res.json({ sucess: false, msg: err });
                  } else if (result == null) {
                    res.json({ sucess: false });
                  } else {
                    totalData.posIntegrationData = result[0];
                    res.json({ sucess: true, data: totalData });
                  }
                }
              );
              // res.json({ sucess: true, data: totalData });
            }
          }
        );
      }
    }
  );
};

const updatePOSProductOrderByID = async (req, res) => {
  posOrdersModel.findOneAndUpdate(
    {
      _id: req.params.posorderid,
      "posProductOrder.posProductOrderID": req.params.posproductorderid,
    },
    {
      $set: {
        // $push: {
        //   "posProductOrder.history": {
        //     $each: req.body.history,
        //   },
        // },
        "posProductOrder.history": req.body.history,
        "posProductOrder.noOfoutlets": req.body.noOfoutlets,
        "posProductOrder.DiscountedPrice": req.body.DiscountedPrice,
        "posProductOrder.PriceafterDiscount": req.body.PriceafterDiscount,
        "posProductOrder.totalOrderAmount": req.body.totalOrderAmount,
        "posProductOrder.totalPaidAmountwithTax":
          req.body.totalPaidAmountwithTax,
      },
    },
    (err, data) => {
      if (err) {
        res.json({ sucess: false, msg: err });
      } else if (data == null) {
        res.json({ sucess: false });
      } else {
        res.json({ sucess: true, data: data });
      }
    }
  );
};
/* -----------------------------------------POS Addon Orders----------------------------------------- */
const addPOSAddonOrder = async (req, res) => {
  posOrdersModel.findOne({ _id: req.params.posorderid }).exec((err, data) => {
    if (err) {
      console.log(err);
    } else if (data["posAddonOrder"]["lastLicenceNumber"] == 0) {
      var lastLicenceNumber = 0;
      var currentDate = new Date();
      var dateAfter30Days = new Date(currentDate);
      var posAddonOrderObject = {};
      var addonBought = [];

      //Generate Expiry Date
      if (req.body.subscription_Type == "Monthly") {
        dateAfter30Days.setDate(dateAfter30Days.getDate() + 30);
      }
      if (req.body.subscription_Type == "Yearly") {
        dateAfter30Days.setDate(dateAfter30Days.getDate() + 365);
      }
      lastLicenceNumber = 0;
      //   license_Nos
      for (let a = 0; a < req.body.addonName.length; a++) {
        for (let i = 0; i < req.body.noOfoutlets; i++) {
          addonBought.push({
            addonName: req.body.addonName[a],
            license_No: {
              outlet_No: i + 1,
              license_No: lastLicenceNumber + i + 1,
              outletID: lastLicenceNumber + i + 1,
            },
          });
          lastLicenceNumber = lastLicenceNumber + i + 1;
        }
      }

      posAddonOrderObject = {
        noOfoutlets: req.body.noOfoutlets,
        addonName: req.body.addonName,
        tax: req.body.tax,
        paymentStatus: req.body.paymentStatus,
        discount: req.body.discount,
        totalOrderAmount: req.body.totalOrderAmount,
        totalPaidAmountwithTax: req.body.totalPaidAmountwithTax,
        DiscountedPrice: req.body.DiscountedPrice,
        pricePerOutlet: req.body.pricePerOutlet,
        PriceafterDiscount: req.body.PriceafterDiscount,
        paidBy: req.body.paidBy,
        subscription_Type: req.body.subscription_Type,
        transcationID: req.body.transcationID,
        transactionDate_Time: req.body.transactionDate_Time,
        subscriptionActivationDate: currentDate,
        subscriptionExpiryDate: dateAfter30Days,
        addonBought: addonBought,
        OrderDate_Time: new Date(),
        lastLicenceNumber: lastLicenceNumber,
        createdAt: new Date(),
        status: req.body.status,
        posAddOnOrderID: data["lastPOSAddonOrderID"],
      };

      posOrdersModel.findByIdAndUpdate(
        { _id: req.params.posorderid },
        { $set: { posAddonOrder: posAddonOrderObject } },
        (err, data) => {
          if (err) {
            console.log(err);
          } else {
            res.json({ sucess: true });
          }
        }
      );
    } else {
      res.json({ sucess: false });
    }
  });
};

const getAllPOSAddonOrders = async (req, res) => {
  posOrdersModel
    .find({})
    .populate("posAddonOrder.addonName", "AddonsName")
    .populate("posAddonOrder.addonBought.addonName", "AddonsName")
    .select("-posIntegrationOrder -posProductOrder")
    .exec((err, data) => {
      if (err) {
        res.json({ sucess: false, msg: err });
      } else if (data == null || data.length == 0) {
        res.json({ sucess: false });
      } else {
        res.json({ sucess: true, data: data });
      }
    });
};

const getPOSAddonOrderByID = async (req, res) => {
  posOrdersModel
    .findOne({ _id: req.params.posorderid })
    .populate("posAddonOrder.addonName", "AddonsName")
    .populate("posAddonOrder.addonBought.addonName", "AddonsName")
    .select("-posIntegrationOrder -posProductOrder")
    .exec((err, data) => {
      if (err) {
        res.json({ sucess: false, msg: err });
      } else if (data == null || data.length == 0) {
        res.json({ sucess: false });
      } else {
        res.json({ sucess: true, data: data });
      }
    });
};

const updatePOSAddonOrderByID = async (req, res) => {
  posOrdersModel.findOneAndUpdate(
    {
      _id: req.params.posorderid,
      "posAddonOrder.posAddOnOrderID": req.params.posaddonorderid,
    },
    {
      $set: {
        // $push: {
        //   "posProductOrder.history": {
        //     $each: req.body.history,
        //   },
        // },
        "posAddonOrder.history": req.body.history,
        "posAddonOrder.noOfoutlets": req.body.noOfoutlets,
        "posAddonOrder.DiscountedPrice": req.body.DiscountedPrice,
        "posAddonOrder.PriceafterDiscount": req.body.PriceafterDiscount,
        "posAddonOrder.totalOrderAmount": req.body.totalOrderAmount,
        "posAddonOrder.totalPaidAmountwithTax": req.body.totalPaidAmountwithTax,
      },
    },
    (err, data) => {
      if (err) {
        res.json({ sucess: false, msg: err });
      } else if (data == null) {
        res.json({ sucess: false });
      } else {
        res.json({ sucess: true, data: data });
      }
    }
  );
};
/* -----------------------------------------POS Integration Orders----------------------------------------- */
const addPOSIntegrationOrder = async (req, res) => {
  posOrdersModel.findOne({ _id: req.params.posorderid }).exec((err, data) => {
    if (err) {
      console.log(err);
    } else if (data["posIntegrationOrder"]["lastLicenceNumber"] == 0) {
      var lastLicenceNumber = 0;
      var currentDate = new Date();
      var dateAfter30Days = new Date(currentDate);
      var posIntegrationOrderObject = {};
      var integrationBought = [];

      //Generate Expiry Date
      if (req.body.subscription_Type == "Monthly") {
        dateAfter30Days.setDate(dateAfter30Days.getDate() + 30);
      }
      if (req.body.subscription_Type == "Yearly") {
        dateAfter30Days.setDate(dateAfter30Days.getDate() + 365);
      }
      lastLicenceNumber = 0;
      //   license_Nos
      for (let a = 0; a < req.body.integrationName.length; a++) {
        for (let i = 0; i < req.body.noOfoutlets; i++) {
          integrationBought.push({
            integrationName: req.body.integrationName[a],
            license_No: {
              outlet_No: i + 1,
              license_No: lastLicenceNumber + i + 1,
              outletID: lastLicenceNumber + i + 1,
            },
          });
          lastLicenceNumber = lastLicenceNumber + i + 1;
        }
      }

      posIntegrationOrderObject = {
        noOfoutlets: req.body.noOfoutlets,
        integrationName: req.body.integrationName,
        tax: req.body.tax,
        paymentStatus: req.body.paymentStatus,
        discount: req.body.discount,
        totalOrderAmount: req.body.totalOrderAmount,
        totalPaidAmountwithTax: req.body.totalPaidAmountwithTax,
        DiscountedPrice: req.body.DiscountedPrice,
        pricePerOutlet: req.body.pricePerOutlet,
        PriceafterDiscount: req.body.PriceafterDiscount,
        paidBy: req.body.paidBy,
        subscription_Type: req.body.subscription_Type,
        transcationID: req.body.transcationID,
        transactionDate_Time: req.body.transactionDate_Time,
        subscriptionActivationDate: currentDate,
        subscriptionExpiryDate: dateAfter30Days,
        integrationBought: integrationBought,
        OrderDate_Time: new Date(),
        lastLicenceNumber: lastLicenceNumber,
        createdAt: new Date(),
        status: req.body.status,
        posIntegrationOrderID: data["lastPOSIntegrationOrderID"],
      };

      posOrdersModel.findByIdAndUpdate(
        { _id: req.params.posorderid },
        { $set: { posIntegrationOrder: posIntegrationOrderObject } },
        (err, data) => {
          if (err) {
            console.log(err);
          } else {
            res.json({ sucess: true });
          }
        }
      );
    } else {
      res.json({ sucess: false });
    }
  });
};

const getAllPOSIntegrationOrders = async (req, res) => {
  posOrdersModel
    .find({})
    .populate("posIntegrationOrder.integrationName", "integrationName")
    .populate(
      "posIntegrationOrder.integrationBought.integrationName",
      "integrationName"
    )
    .select("-posAddonOrder -posProductOrder")
    .exec((err, data) => {
      if (err) {
        res.json({ sucess: false, msg: err });
      } else if (data == null || data.length == 0) {
        res.json({ sucess: false });
      } else {
        res.json({ sucess: true, data: data });
      }
    });
};

const getPOSIntegrationOrderByID = async (req, res) => {
  posOrdersModel
    .findOne({ _id: req.params.posorderid })
    .populate("posIntegrationOrder.integrationName", "integrationName")
    .populate(
      "posIntegrationOrder.integrationBought.integrationName",
      "integrationName"
    )
    .select("-posAddonOrder -posProductOrder")
    .exec((err, data) => {
      if (err) {
        res.json({ sucess: false, msg: err });
      } else if (data == null || data.length == 0) {
        res.json({ sucess: false });
      } else {
        res.json({ sucess: true, data: data });
      }
    });
};

const updatePOSIntegrationOrderByID = async (req, res) => {
  posOrdersModel.findOneAndUpdate(
    {
      _id: req.params.posorderid,
      "posIntegrationOrder.posIntegrationOrderID":
        req.params.posintegrationorderid,
    },
    {
      $set: {
        // $push: {
        //   "posProductOrder.history": {
        //     $each: req.body.history,
        //   },
        // },
        "posIntegrationOrder.history": req.body.history,
        "posIntegrationOrder.noOfoutlets": req.body.noOfoutlets,
        "posIntegrationOrder.DiscountedPrice": req.body.DiscountedPrice,
        "posIntegrationOrder.PriceafterDiscount": req.body.PriceafterDiscount,
        "posIntegrationOrder.totalOrderAmount": req.body.totalOrderAmount,
        "posIntegrationOrder.totalPaidAmountwithTax":
          req.body.totalPaidAmountwithTax,
      },
    },
    (err, data) => {
      if (err) {
        res.json({ sucess: false, msg: err });
      } else if (data == null) {
        res.json({ sucess: false });
      } else {
        res.json({ sucess: true, data: data });
      }
    }
  );
};

const addPOSPaymentLink = async (req, res) => {
  posPaymentLinkModel
    .findOne()
    .sort({ paymentLinkID: -1 })
    .exec((err, data) => {
      if (err) {
        console.log(err);
      } else if (data == null) {
        var currentDate = new Date();
        var dateAfter30Days = new Date(currentDate);
        var posPaymentLink = new posPaymentLinkModel(req.body);

        //Generate Expiry Date
        if (req.body.subscription_Type == "Monthly") {
          dateAfter30Days.setDate(dateAfter30Days.getDate() + 30);
        }
        if (req.body.subscription_Type == "Yearly") {
          dateAfter30Days.setDate(dateAfter30Days.getDate() + 365);
        }

        posPaymentLink.subscriptionActivationDate = currentDate;
        posPaymentLink.subscriptionExpiryDate = dateAfter30Days;

        posPaymentLink.save((err) => {
          if (err) {
            res.json({ sucess: false, msg: err });
          } else {
            res.json({ sucess: true });
          }
        });
      } else {
        var currentDate = new Date();
        var dateAfter30Days = new Date(currentDate);
        var posPaymentLink = new posPaymentLinkModel(req.body);

        //Generate Expiry Date
        if (req.body.subscription_Type == "Monthly") {
          dateAfter30Days.setDate(dateAfter30Days.getDate() + 30);
        }
        if (req.body.subscription_Type == "Yearly") {
          dateAfter30Days.setDate(dateAfter30Days.getDate() + 365);
        }

        posPaymentLink.subscriptionActivationDate = currentDate;
        posPaymentLink.subscriptionExpiryDate = dateAfter30Days;

        posPaymentLink.save((err) => {
          if (err) {
            res.json({ sucess: false, msg: err });
          } else {
            res.json({ sucess: true });
          }
        });
      }
    });
};

const getAllPOSPaymentLinks = async (req, res) => {
  posPaymentLinkModel
    .find({ status: true })
    .populate("productName", "productName")
    .exec((err, data) => {
      if (err) {
        res.json({ sucess: false, msg: err });
      } else if (data == null || data.length == 0) {
        res.json({ sucess: false });
      } else {
        res.json({ sucess: true, data: data });
      }
    });
};

const getPOSPaymentLink = (req, res) => {
  posPaymentLinkModel
    .findOne({ _id: req.params.pospaymentlinkid, status: true })
    .populate("productName", "productName")
    .exec((err, data) => {
      if (err) {
        res.json({ sucess: false, msg: err });
      } else if (data == null || data.length == 0) {
        res.json({ sucess: false });
      } else {
        res.json({ sucess: true, data: data });
      }
    });
};
module.exports = {
  addPOSProductOrder,
  getAllPOSProductOrders,
  getPOSProductOrderById,
  addPOSAddonOrder,
  getAllPOSAddonOrders,
  getPOSAddonOrderByID,
  addPOSIntegrationOrder,
  getAllPOSIntegrationOrders,
  getPOSIntegrationOrderByID,
  getPOSOrderStats,
  addPOSPaymentLink,
  getAllPOSPaymentLinks,
  getPOSPaymentLink,
  updatePOSProductOrderByID,
  updatePOSAddonOrderByID,
  updatePOSIntegrationOrderByID,
};

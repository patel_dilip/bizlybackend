const beveragesVarientModel = require("../../../_bizly_models/_menu_management_models/_beverages_model/_beverages_varient_model")
  .beveragesvarientmodel;
const dishModel = require("../../../_bizly_models/_menu_management_models/_food_model/dish-model")
  .dishmodel;

const addVarient = (req, res) => {
  beveragesVarientModel.findOne(
    { varientName: req.body.varientName },
    (err, data) => {
      if (err) {
        res.json({ sucess: false, msg: err });
      } else if (data) {
        res.json({ sucess: false });
      } else {
        var varient = new beveragesVarientModel();
        varient.varientName = req.body.varientName;
        varient.varientContent = req.body.varientContent;
        varient.varientType = req.body.varientType;
        varient.images = req.body.images;
        varient.userid = req.body.userid;
        varient.addedBy = req.body.userid;
        varient.updatedBy = req.body.userid;

        varient.save((err) => {
          if (err) {
            throw err;
          } else {
            res.json({ sucess: true });
          }
        });
      }
    }
  );
};

const getVarient = async (req, res) => {
  beveragesVarientModel
    .findOne({ _id: req.params.varientid })
    .populate("varientContent")
    .populate("addedBy", "userName")
    .populate("updatedBy", "userName")
    .populate("userid", "userName")
    .sort("-updatedAt")
    .exec(function (err, varient) {
      if (err) {
        res.json({ sucess: false, msg: err });
      } else if (varient == null) {
        res.json({ sucess: false });
      } else {
        res.json({ sucess: true, data: varient });
      }
    });
};

const getAllVarients = (req, res) => {
  beveragesVarientModel
    .find()
    .populate("varientContent")
    .populate("addedBy", "userName")
    .populate("updatedBy", "userName")
    .populate("userid", "userName")
    .sort("-updatedAt")
    .exec(function (err, varients) {
      if (err) {
        res.json({ sucess: false });
      } else if (varients == null || varients.length == 0) {
        res.json({ sucess: false });
      } else {
        res.json({ sucess: true, data: varients });
      }
    });
};

// check Varient Already Exists
const checkVarientAlreadyExists = (req, res) => {
  beveragesVarientModel.findOne(
    { varientName: req.params.varientName },
    (err, data) => {
      if (err) {
        res.json({ success: false, msg: err });
      } else if (data) {
        res.json({ success: true });
      } else {
        res.json({ success: false });
      }
    }
  );
};

const updateVarient = (req, res) => {
  beveragesVarientModel.findByIdAndUpdate(
    req.params.varientid,
    {
      $set: {
        varientName: req.body.varientName,
        varientContent: req.body.varientContent,
        varientType: req.body.varientType,
        images: req.body.images,
        updatedBy: req.body.userid,
        userid: req.body.userid,
        updatedAt: Date.now(),
      },
    },
    (err, ivarient) => {
      if (err) {
        res.json({ sucess: false, msg: err});
      } else if (ivarient == null) {
        res.json({ sucess: false });
      } else {
        res.json({ sucess: true });
      }
    }
  );
};

const deleteVarient = (req, res) => {
  beveragesVarientModel.findByIdAndDelete(req.params.varientid, (err) => {
    if (err) {
        res.json({ sucess: false, msg: err});
    } else {
      res.json({ sucess: true });
    }
  });
};

const softDeleteVarient = (req, res) => {
  try {
    beveragesVarientModel
      .findByIdAndUpdate(req.params.varientid, {
        $set: { status: req.body.status },
      })
      .then((varientData) => {
        let varientID = varientData._id;
        dishModel
          .find({}, { varients: 1, _id: 1 })
          .then((varientsData) => {
            var promiseArr = [];
            for (let i = 0; i < varientsData.length; i++) {
              for (let j = 0; j < varientsData[i].varients.length; j++) {
                if (
                  JSON.stringify(
                    varientsData[i].varients[j] == JSON.stringify(varientID)
                  )
                ) {
                  promiseArr.push(
                    new Promise((resolve, reject) => {
                      dishModel
                        .updateOne(
                          { _id: varientsData[i]._id },
                          { $pull: { varients: varientID } }
                        )
                        .then((udata) => {
                          resolve(udata);
                        })
                        .catch((err) => {
                          reject(err);
                        });
                    })
                  );
                }
              }
            }
            Promise.all(promiseArr)
              .then((promiseArr) => {
                res.json({ success: true });
              })
              .catch((err) => {
                res.json({
                  success: false,
                  error: err,
                });
              });
          })
          .catch((err) => {
            res.json({
              success: false,
              error: err,
            });
          });
      })
      .catch((err) => {
        res.json({ success: false, error: err });
      });
  } catch (ex) {
    return res.json({ success: false, error: ex.message });
  }
};
module.exports = {
  addVarient,
  getVarient,
  getAllVarients,
  checkVarientAlreadyExists,
  updateVarient,
  deleteVarient,
  softDeleteVarient,
};

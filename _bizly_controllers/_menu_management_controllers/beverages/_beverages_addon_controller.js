const beveragesaddonModel = require("../../../_bizly_models/_menu_management_models/_beverages_model/_beverages_addon_model")
  .beveragesadonmodel;

const addBeveragesAddon = (req, res) => {
  beveragesaddonModel.findOne({ adonName: req.body.adonName }, (err, data) => {
    if (err) {
      res.json({ success: false, msg: err });
    } else if (data) {
      res.json({ success: false, msg: "Addon already exists" });
    } else {
      var beveragesAddon = new beveragesaddonModel();
      beveragesAddon.adonName = req.body.adonName;
      beveragesAddon.unit = req.body.unit;
      beveragesAddon.veg_nonveg = req.body.veg_nonveg;
      beveragesAddon.inHouseBeverages = req.body.inHouseBeverages;
      beveragesAddon.adonType = req.body.adonType;
      //   adon.dishes = req.body.dishes;
      beveragesAddon.images = req.body.images;
      beveragesAddon.addedBy = req.body.userid;
      beveragesAddon.updatedBy = req.body.userid;
      beveragesAddon.userid = req.body.userid;
      beveragesAddon.save((err) => {
        if (err) {
          res.json({ sucess: false, msg: err });
        } else {
          res.json({ sucess: true });
        }
      });
    }
  });
};

const getBeveragesAddon = async (req, res) => {
  beveragesaddonModel
    .findOne({ _id: req.params.adonid })
    // .populate("dishes", "dishName")
    .populate("inHouseBeverages")
    .populate("userid", "userName")
    .populate("addedBy", "userName")
    .populate("updatedBy", "userName")
    .sort("-updatedAt")
    .exec(function (err, addon) {
      if (err) {
        res.json({ sucess: false, msg: err });
      } else if (addon == null) {
        res.json({ sucess: false });
      } else {
        res.json({ sucess: true, data: addon });
      }
    });
};

const getAllBeveragesAdons = (req, res) => {
  beveragesaddonModel
    .find()
    // .populate("dishes", "dishName")
    .populate("inHouseBeverages")
    .populate("userid", "userName")
    .populate("addedBy", "userName")
    .populate("updatedBy", "userName")
    .sort("-updatedAt")
    .exec(function (err, addons) {
      if (err) {
        res.json({ sucess: false, msg: err });
      } else if (addons == null || addons.length == 0) {
        res.json({ sucess: false });
      } else {
        res.json({ sucess: true, data: addons });
      }
    });
};
// check Addon Already Exists
const checkBeveragesAddonAlreadyExists = (req, res) => {
  beveragesaddonModel.findOne(
    { adonName: req.params.adonName },
    (err, data) => {
      if (err) {
        res.json({ success: false, msg: err });
      } else if (data) {
        res.json({ success: true });
      } else {
        res.json({ success: false });
      }
    }
  );
};

const updateBeveragesAddon = (req, res) => {
  beveragesaddonModel.findByIdAndUpdate(
    req.params.adonid,
    {
      $set: {
        adonName: req.body.adonName,
        unit: req.body.unit,
        veg_nonveg: req.body.veg_nonveg,
        inHouseBeverages: req.body.inHouseBeverages,
        adonType: req.body.adonType,
        // dishes: req.body.dishes,
        images: req.body.images,
        userid: req.body.userid,
        updatedBy: req.body.userid,
        updatedAt: Date.now(),
      },
    },
    (err, uadon) => {
      if (err) {
        res.json({ sucess: false, msg: err });
      } else if (uadon == null) {
        res.json({ sucess: false });
      } else {
        res.json({ sucess: true });
      }
    }
  );
};

const deleteBeveragesAddon = (req, res) => {
  beveragesaddonModel.findOneAndDelete(req.params.adonid, (err) => {
    if (err) {
      res.json({ sucess: false, msg: err });
    } else {
      res.json({ sucess: true });
    }
  });
};

const softDeleteBeveragesAddon = (req, res) => {
  beveragesaddonModel.findOneAndUpdate(
    req.params.adonid,
    { $set: { status: req.body.status } },
    (err) => {
      if (err) {
        res.json({ sucess: false, msg: err });
      } else {
        res.json({ sucess: true });
      }
    }
  );
};

module.exports = {
  addBeveragesAddon,
  getBeveragesAddon,
  getAllBeveragesAdons,
  checkBeveragesAddonAlreadyExists,
  updateBeveragesAddon,
  deleteBeveragesAddon,
  softDeleteBeveragesAddon,
};

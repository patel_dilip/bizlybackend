const inhouseBeveragesTreeModel = require("../../../_bizly_models/_menu_management_models/_beverages_model/_inhouse_beverages_tree_model")
  .inhouseBeveragesTreeModel;

const addTree = (req, res) => {
  var tree = new inhouseBeveragesTreeModel();
  tree.inhouseBeveragesTree = req.body.inhouseBeveragesTree;
  tree.addedBy = req.body.userid;
  tree.updatedBy = req.body.userid;

  tree.save((err) => {
    if (err) {
      res.json({ sucess: false, msg: err });
    } else {
      res.json({ sucess: true });
    }
  });
};

const getTree = async (req, res) => {
  inhouseBeveragesTreeModel
    .findOne()
    .populate("addedBy", "userName")
    .populate("updatedBy", "userName")
    .sort("-updatedAt")
    .exec(function (err, tree) {
      if (err) {
        res.json({ sucess: false, msg: err });
      } else if (tree == null) {
        res.json({ sucess: false });
      } else {
        res.json({ sucess: true, data: tree });
      }
    });
};

const updateTree = (req, res) => {
  inhouseBeveragesTreeModel.findByIdAndUpdate(
    req.params.treeid,
    {
      $set: {
        inhouseBeveragesTree: req.body.inhouseBeveragesTree,
        updatedBy: req.body.userid,
      },
    },
    (err) => {
      if (err) {
        res.json({ sucess: false, msg: err });
      } else {
        res.json({ sucess: true });
      }
    }
  );
};

module.exports = {
  addTree,
  getTree,
  updateTree,
};

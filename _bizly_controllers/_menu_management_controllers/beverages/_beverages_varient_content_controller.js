const beveragesVarientContentCtrl = require("../../../_bizly_models/_menu_management_models/_beverages_model/_beverages_varient_content_model")
  .beveragesvarientcontentmodel;
const addContent = (req, res) => {
  var content = new beveragesVarientContentCtrl(req.body);
  // content.contentName = req.body.contentName
  content.save((err) => {
    if (err) {
      throw err;
    } else {
      res.json({ sucess: true });
    }
  });
};

const getContent = async (req, res) => {
  beveragesVarientContentCtrl
    .findOne({ _id: req.params.contentid })
    .populate("userid", "userName")
    .populate("addedBy", "userName")
    .populate("updatedBy", "userName")
    .sort("-updatedAt")
    .exec(function (err, content) {
      if (err) {
        res.json({ sucess: false });
      } else if (content == null) {
        res.json({ sucess: false });
      } else {
        res.json({ sucess: true, data: content });
      }
    });
};

const getAllContents = (req, res) => {
  beveragesVarientContentCtrl
    .find()
    .populate("userid", "userName")
    .populate("addedBy", "userName")
    .populate("updatedBy", "userName")
    .sort("-updatedAt")
    .exec(function (err, contents) {
      if (err) {
        res.json({ sucess: false });
      } else if (contents == null || contents.length == 0) {
        res.json({ sucess: false });
      } else {
        res.json({ sucess: true, data: contents });
      }
    });
};

// check Varient Content Already Exists
const checkVarientContentAlreadyExists = (req, res) => {
  beveragesVarientContentCtrl.findOne(
    { contentName: req.params.contentName },
    (err, data) => {
      if (err) {
        res.json({ success: false, msg: err });
      } else if (data) {
        res.json({ success: true });
      } else {
        res.json({ success: false });
      }
    }
  );
};

const updateContent = (req, res) => {
  beveragesVarientContentCtrl.findByIdAndUpdate(
    req.params.contentid,
    {
      $set: {
        contentName: req.body.contentName,
        updatedBy: req.body.userid,
        updatedAt: Date.now(),
      },
    },
    (err, ucontent) => {
      if (err) {
        throw err;
      } else if (ucontent == null) {
        res.json({ sucess: false });
      } else {
        res.json({ sucess: true });
      }
    }
  );
};

const deleteContent = (req, res) => {
  beveragesVarientContentCtrl.findByIdAndDelete(req.params.contentid, (err) => {
    if (err) {
      res.json({ sucess: false });
    } else {
      res.json({ sucess: true });
    }
  });
};

module.exports = {
  addContent,
  getContent,
  getAllContents,
  checkVarientContentAlreadyExists,
  updateContent,
  deleteContent,
};

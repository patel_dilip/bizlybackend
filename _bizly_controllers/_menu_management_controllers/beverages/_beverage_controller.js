const beverageModel = require('../../../_bizly_models/_menu_management_models/_beverages_model/beverages-model').beverages_model

const addBeverages = (req, res) => {
    var beverage = new beverageModel()
    beverage.beverageName = req.body.beverageName
    beverage.unit = req.body.unit
    beverage.veg_nonveg = req.body.veg_nonveg
    beverage.rootCategory = req.body.rootCategory
    beverage.varients = req.body.varients
    beverage.category = req.body.category
    beverage.subVarient = req.body.subVarient
    beverage.type = req.body.type
    beverage.images = req.body.images
    beverage.addedBy = req.body.userid
    beverage.updatedBy = req.body.userid

    beverage.save((err) => {
        if (err) {
            res.json({ success: false, msg: err })
        } else {
            res.json({ success: true, msg: "Beverages added" })
        }
    })
}

const getAllBeverages = (req, res) => {
    beverageModel.find({}).
        populate('addedBy', 'userName').
        populate('updatedBy', 'userName').
        sort('-updatedAt').
        exec((err, data) => {
            if (err) {
                res.json({ success: false, msg: err })
            } else if (data == null || data.length == 0) {
                res.json({ success: false, msg: "Beverages not found" })
            } else {
                res.json({ success: true, msg: "Beverages Found", data: data })
            }
        })
}

const getBeverage = (req, res) => {
    beverageModel.findOne(req.params.beverageid).
        populate('addedBy', 'userName').
        populate('updatedBy', 'userName').
        sort('-updatedAt').
        exec((err, data) => {
            if (err) {
                res.json({ success: false, msg: err })
            } else if (data == null || data.length == 0) {
                res.json({ success: false, msg: "Beverages not found" })
            } else {
                res.json({ success: true, msg: "Beverages Found", data: data })
            }
        })
}

// check Beverage Already Exists
const checkBeverageAlreadyExists = (req, res) => {
    beverageModel.findOne({ beverageName: req.params.beverageName }, (err, data) => {
        if (err) {
            res.json({ success: false, msg: err })
        } else if (data) {
            res.json({ success: true, msg: "beverage Name already Exists" })
        } else {
            res.json({ success: false, msg: "Beverage Name not available" })
        }
    })
}

const updateBeverages = (req, res) => {
    beverageModel.findByIdAndUpdate(req.params.beverageid,
        {
            $set: {
                beverageName: req.body.beverageName,
                unit: req.body.unit,
                veg_nonveg: req.body.veg_nonveg,
                rootCategory: req.body.rootCategory,
                varients: req.body.varients,
                subVarient: req.body.subVarient,
                type: req.body.type,
                category : req.body.category,
                images: req.body.images,
                updatedBy: req.body.userid,
                updatedAt: Date.now()
            }
        }, (err, udata) => {
            if (err) {
                res.json({ success: false, msg: err })
            } else if (udata == null || udata.length == 0) {
                res.json({ success: false, msg: "Beverages not found" })
            } else {
                res.json({ success: true, msg: "Beverages Updated" })
            }
        })
}

const deleteBeverage = (req, res) => {
    beverageModel.findByIdAndDelete(req.params.beverageid, (err, data) => {
        if (err) {
            res.json({ success: false, msg: err })
        } else {
            res.json({ success: true, msg: "Beverage Deleted" })
        }
    })
}

const softDeleteBeverage = (req, res) => {
    beverageModel.findByIdAndUpdate(req.params.beverageid, { $set: { status: req.body.status } }, (err, data) => {
        if (err) {
            res.json({ success: false, msg: err })
        } else {
            res.json({ success: true, msg: "Beverage soft Deleted" })
        }
    })
}

module.exports = {
    addBeverages,
    getAllBeverages,
    getBeverage,
    checkBeverageAlreadyExists,
    updateBeverages,
    deleteBeverage,
    softDeleteBeverage
}
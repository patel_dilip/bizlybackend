const beverage_product_model = require('../../../_bizly_models/_menu_management_models/_beverages_model/beverage-product-model').beverage_product_model

const addBeverageProduct = (req, res) => {
    var product = new beverage_product_model()
    product.productName = req.body.productName
    product.rootCategory = req.body.rootCategory
    product.varient = req.body.varient
    product.subVarient = req.body.subVarient
    product.type = req.body.type
    product.unit = req.body.unit
    product.attributeSet = req.body.attributeSet
    product.category = req.body.category
    product.associatedBrand = req.body.associatedBrand
    product.productImage = req.body.productImage
    product.addedBy = req.body.userid
    product.updatedBy = req.body.userid

    product.save((err) => {
        if (err) {
            res.json({ success: false, msg: err })
        } else {
            res.json({ success: true, msg: "Beverage product added" })
        }
    })
}

const getAllBeverageProduct = (req, res) => {
    beverage_product_model.find({}).
        populate('associatedBrand', 'brandName').
        populate('addedBy', 'userName').
        populate('updatedBy', 'userName').
        sort('-updatedAt').
        exec((err, productData) => {
            if (err) {
                res.json({ sucess: false, msg: err })
            } else if (productData == null || productData.length == 0) {
                res.json({ success: false, msg: "Beverage Product data not found" })
            } else {
                res.json({ success: true, msg: "Beverage product data found", data: productData })
            }
        })
}

const getBeverageProduct = (req, res) => {
    beverage_product_model.find(req.params.productid).
        populate('associatedBrand', 'brandName').
        populate('addedBy', 'userName').
        populate('updatedBy', 'userName').
        sort('-updatedAt').
        exec((err, productData) => {
            if (err) {
                res.json({ sucess: false, msg: err })
            } else if (productData == null || productData.length == 0) {
                res.json({ success: false, msg: "Beverage Product data not found" })
            } else {
                res.json({ success: true, msg: "Beverage product data found", data: productData })
            }
        })
}

// check Beverage Product Already Exists
const checkBeverageProductAlreadyExists = (req, res) => {
    beverage_product_model.findOne({ productName: req.params.productName }, (err, data) => {
        if (err) {
            res.json({ success: false, msg: err })
        } else if (data) {
            res.json({ success: true, msg: "Product Name already Exists" })
        } else {
            res.json({ success: false, msg: "Product Name not available" })
        }
    })
}

const updateBeverageProduct = (req, res) => {
    beverage_product_model.findByIdAndUpdate(req.params.productid,
        {
            $set: {
                productName: req.body.productName,
                rootCategory: req.body.rootCategory,
                varient: req.body.varient,
                subVarient: req.body.subVarient,
                type: req.body.type,
                unit: req.body.unit,
                attributeSet : req.body.attributeSet,
                category:req.body.category,
                associatedBrand: req.body.associatedBrand,
                productImage: req.body.productImage,
                updatedBy: req.body.userid,
                updatedAt: Date.now()
            }
        }, (err, udata) => {
            if (err) {
                res.json({ success: false, msg: err })
            } else if (udata == null || udata.length == 0) {
                res.json({ success: false, msg: "Beverage product data not found" })
            } else {
                res.json({ success: true, msg: "Beverage Data updated" })
            }
        })
}

const deleteBeverageproduct = (req, res) => {
    beverage_product_model.findByIdAndDelete(req.params.productid, (err, data) => {
        if (err) {
            res.json({ success: false, msg: err })
        } else {
            res.json({ success: true, msg: "Beverage Data Deleted" })
        }
    })
}

const softDeleteBeverageproduct = (req, res) => {
    beverage_product_model.findByIdAndUpdate(req.params.productid, { $set: { status: req.body.status } }, (err, data) => {
        if (err) {
            res.json({ success: false, msg: err })
        } else {
            res.json({ success: true, msg: "Beverage Data soft Deleted" })
        }
    })
}

module.exports = {
    addBeverageProduct,
    getAllBeverageProduct,
    getBeverageProduct,
    checkBeverageProductAlreadyExists,
    updateBeverageProduct,
    deleteBeverageproduct,
    softDeleteBeverageproduct
}
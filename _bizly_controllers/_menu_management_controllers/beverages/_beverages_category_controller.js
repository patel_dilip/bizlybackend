const beveragesModel = require('../../../_bizly_models/_menu_management_models/_beverages_model/beverages-category-model').beveragesModel
const beverages = require('../../../_bizly_models/_menu_management_models/_beverages_model/beverages-model').beverages_model
const retailBeverageBrandModel = require('../../../_bizly_models/_menu_management_models/retail_beverages_model/_retail_beverage_brand_model').brandModel
const retailBeverageProductModel = require('../../../_bizly_models/_menu_management_models/retail_beverages_model/_retail_beverage_product_model').productModel
const attributeSetModel = require('../../../_bizly_models/_menu_management_models/_liquor_models/_attribute_set_model').attributeSetModel

const addRootBeveragesCategoryName = (req, res) => {
    beveragesModel.findOne({ rootCategoryName: req.body.rootCategoryName }, (err, data) => {
        if (err) {
            res.json({ sucess: false, msg: err })
        } else if (data) {
            res.json({ sucess: false, msg: "root category name already exists" })
        } else {
            var rootcategory = new beveragesModel()
            rootcategory.rootCategoryName = req.body.rootCategoryName
            rootcategory.beverageType = req.body.beverageType
            rootcategory.addedBy = req.body.userid
            rootcategory.updatedBy = req.body.userid

            rootcategory.save((err) => {
                if (err) {
                    throw err
                } else {
                    res.json({ sucess: true, msg: 'rootcategory added' })
                }
            })
        }
    })

}

const addSubcategory = (req, res) => {
    beveragesModel.findOne({ 'subCategories.subCategoryName': req.body.subCategoryName }, (err, data) => {
        if (err) {
            res.json({ sucess: false, msg: err })
        } else if (data) {
            res.json({ sucess: false, msg: "sub category name already exists" })
        } else {
            beveragesModel.findByIdAndUpdate(req.params.rootcategoryid,
                { $push: { subCategories: req.body } },
                (err, usubcategory) => {
                    if (err) {
                        throw err
                    } else if (usubcategory == null) {
                        res.json({ sucess: false, msg: "rootcategory not found" })
                    } else {
                        res.json({ sucess: true, msg: "subcategory added" })
                    }
                })
        }
    })
}

const addSubSubCategories = (req, res) => {
    beveragesModel.findOne({ 'subCategories.subSubCategories.subSubCategoryName': req.body.subSubCategoryName }, (err, data) => {
        if (err) {
            res.json({ sucess: false, msg: err })
        } else if (data) {
            res.json({ sucess: false, msg: "sub sub category name already exists" })
        } else {
            beveragesModel.updateOne({ _id: req.params.rootcategoryid, "subCategories._id": req.params.subcategoryid },
                { $addToSet: { "subCategories.$.subSubCategories": { subSubCategoryName: req.body.subSubCategoryName } } },
                (err, usubsubcategory) => {
                    if (err) {
                        throw err
                    } else if (usubsubcategory == null) {
                        res.json({ sucess: false, msg: "rootcategory not found" })
                    } else {
                        res.json({ sucess: true, msg: "subsubcategory added" })
                    }
                })
        }
    })
}

const addSubSubCategoryType = (req, res) => {
    beveragesModel.findOne({ 'subCategories.subSubCategories.subSubCategoryType.subSubCategoryTypeName': req.body.subSubCategoryTypeName }, (err, data) => {
        if (err) {
            res.json({ sucess: false, msg: err })
        } else if (data) {
            res.json({ sucess: false, msg: "sub sub category type name already exists" })
        } else {
            beveragesModel.updateOne(
                {
                    _id: req.params.rootcategoryid,
                    "subCategories.subSubCategories._id": req.params.subsubcategoryid
                },
                {
                    $push: {
                        "subCategories.$.subSubCategories.$[outer].subSubCategoryType": req.body

                    }
                },
                {
                    "arrayFilters": [
                        {
                            "outer._id": req.params.subsubcategoryid,
                        }
                    ]
                },
                (err, usubsubcategorytype) => {
                    if (err) {
                        throw err
                    } else if (usubsubcategorytype == null) {
                        res.json({ sucess: false, msg: "rootcategory not found" })
                    } else {
                        res.json({ sucess: true, msg: "subsubcategorytype added" })
                    }
                })
        }
    })
}

const getRootBeveragesCategories = async (req, res) => {
    beveragesModel.findOne({ _id: req.params.rootcategoryid }).
        populate('addedBy', 'userName').
        populate('updatedBy', 'userName').
        exec(function (err, rootcategory) {
            if (err) {
                throw err
            } else if (rootcategory == null) {
                res.json({ sucess: false, msg: 'rootcategory not found' })
            } else {
                res.json({ sucess: true, msg: 'rootcategory found', data: rootcategory })
            }

        })
}

const getAllBeveragesCategories = (req, res) => {
    beveragesModel.find().
        populate('addedBy', 'userName').
        populate('updatedBy', 'userName').
        exec(function (err, rootcategories) {
            if (err) {
                throw err
            } else if (rootcategories == null || rootcategories.length == 0) {
                res.json({ sucess: false, msg: "rootcategories Not Found" })
            } else {
                res.json({ sucess: true, msg: "rootcategories Found", data: rootcategories })
            }
        })
}

const updateRootBeveragesCategoryName = (req, res) => {
    //Update for in House Beverages
    if (req.body.beverageType == "inHouseBeverage") {
        beveragesModel.findOneAndUpdate({ _id: req.params.rootcategoryid, beverageType: "inHouseBeverage" },
            { $set: { rootCategoryName: req.body.rootCategoryName } },
            (err, data) => {
                if (err) {
                    res.json({ success: false, msg: err })
                } else if (data == null || data.length == 0) {
                    res.json({ success: false, msg: "Data not found" })
                } else {
                    beverages.find({ "rootCategory.id": req.params.rootcategoryid })
                        .then((beverageData) => {
                            promiseArr = []
                            for (let i = 0; i < beverageData.length; i++) {
                                promiseArr.push(new Promise((resolve, reject) => {
                                    if (beverageData[i].rootCategory.id == req.params.rootcategoryid && beverageData[i].category.id == req.params.rootcategoryid) {
                                        beverages.findOneAndUpdate({ _id: beverageData[i]._id },
                                            {
                                                $set: {
                                                    "category.category": req.body.rootCategoryName,
                                                    "rootCategory.rootCategory": req.body.rootCategoryName
                                                }
                                            }).then((Budata) => {
                                                resolve(Budata)
                                            }).catch((err) => {
                                                reject(err)
                                            })
                                    } else {
                                        beverages.findOneAndUpdate({ _id: beverageData[i]._id },
                                            {
                                                $set: {
                                                    "rootCategory.rootCategory": req.body.rootCategoryName
                                                }
                                            }).then((Budata) => {
                                                resolve(Budata)
                                            }).catch((err) => {
                                                reject(err)
                                            })
                                    }
                                }))
                            }
                            Promise.all(promiseArr).then((promiseArr) => {
                                res.json({ success: true, msg: "Root name Updated" })
                            }).catch((err) => {
                                res.json({ success: false, msg: "error in promise arr", error: err })
                            })
                        }).catch((err) => {
                            res.json({ success: false, msg: "error in beverage data", error: err })
                        })
                }
            })
    } else {
        //Update for retail beverages 
        beveragesModel.findOneAndUpdate({ _id: req.params.rootcategoryid, beverageType: "retailBeverage" },
            { $set: { rootCategoryName: req.body.rootCategoryName } },
            (err, data) => {
                if (err) {
                    res.json({ success: false, msg: err })
                } else if (data == null || data.length == 0) {
                    res.json({ success: false, msg: "Data not found" })
                } else {
                    retailBeverageBrandModel.find()
                        .then((RBBrandData) => {
                            promiseArr1 = []
                            for (let i = 0; i < RBBrandData.length; i++) {
                                if (RBBrandData[i].categoryType.length > 0) {
                                    for (let j = 0; j < RBBrandData[i].categoryType.length; j++) {
                                        promiseArr1.push(new Promise((resolve, reject) => {
                                            retailBeverageBrandModel.findOneAndUpdate({
                                                _id: RBBrandData[i]._id,
                                                "categoryType._id": req.params.rootcategoryid
                                            },
                                                {
                                                    $set: {
                                                        "categoryType.$.rootCategoryName": req.body.rootCategoryName
                                                    }
                                                }).then((RBBudata) => {
                                                    resolve(RBBudata)
                                                }).catch((err) => {
                                                    reject(err)
                                                })
                                        }))
                                    }
                                } else {
                                    continue
                                }
                            }
                            Promise.all(promiseArr1).then((promiseArr1) => {
                                retailBeverageProductModel.find({ "rootCategory.id": req.params.rootcategoryid })
                                    .then((RBProductData) => {
                                        promiseArr2 = []
                                        for (let i = 0; i < RBProductData.length; i++) {
                                            promiseArr2.push(new Promise((resolve, reject) => {
                                                if (RBProductData[i].rootCategory.id == req.params.rootcategoryid && RBProductData[i].category.id == req.params.rootcategoryid) {
                                                    retailBeverageProductModel.findOneAndUpdate({ _id: RBProductData[i]._id },
                                                        {
                                                            $set: {
                                                                "category.category": req.body.rootCategoryName,
                                                                "rootCategory.rootCategory": req.body.rootCategoryName
                                                            }
                                                        }).then((RBPudata) => {
                                                            resolve(RBPudata)
                                                        }).catch((err) => {
                                                            reject(err)
                                                        })
                                                } else {
                                                    retailBeverageProductModel.findOneAndUpdate({ _id: RBProductData[i]._id },
                                                        {
                                                            $set: {
                                                                "rootCategory.rootCategory": req.body.rootCategoryName
                                                            }
                                                        }).then((RBPudata) => {
                                                            resolve(RBPudata)
                                                        }).catch((err) => {
                                                            reject(err)
                                                        })
                                                }
                                            }))
                                        }
                                        Promise.all(promiseArr2).then((promiseArr2) => {
                                            attributeSetModel.find({ attributeType: "retailbeverages" })
                                                .then((attributeData) => {
                                                    promiseArr3 = []
                                                    for (let i = 0; i < attributeData.length; i++) {
                                                        if (attributeData[i].categoryType.length > 0) {
                                                            for (let j = 0; j < attributeData[i].categoryType.length; j++) {
                                                                promiseArr3.push(new Promise((resolve, reject) => {
                                                                    attributeSetModel.findOneAndUpdate({
                                                                        _id: attributeData[i]._id,
                                                                        "categoryType._id": req.params.rootcategoryid
                                                                    }, {
                                                                            $set: {
                                                                                "categoryType.$.rootCategoryName": req.body.rootCategoryName
                                                                            }
                                                                        }).then((Audata) => {
                                                                            resolve(Audata)
                                                                        }).catch((err) => {
                                                                            reject(err)
                                                                        })
                                                                }))
                                                            }
                                                        } else {
                                                            continue
                                                        }
                                                    }
                                                    Promise.all(promiseArr3).then((promiseArr3) => {
                                                        res.json({ success: true, msg: "Root name updated" })
                                                    }).catch((err) => {
                                                        res.json({ success: false, msg: "error in promise arr 3", error: err })
                                                    })
                                                }).catch((err) => {
                                                    res.json({ success: false, msg: "error in attribute data", error: err })
                                                })
                                        }).catch((err) => {
                                            res.json({ success: false, msg: "error in promise arr 2", error: err })
                                        })
                                    }).catch((err) => {
                                        res.json({ success: false, msg: "error in retail beverage product data", error: err })
                                    })
                            }).catch((err) => {
                                res.json({ success: false, msg: "error in promise arr 1", error: err })
                            })
                        }).catch((err) => {
                            res.json({ success: false, msg: "error in retail beverage brand data", error: err })
                        })
                }
            })
    }
}

const updateSubcategoryName = (req, res) => {
    //Update for in House Beverages
    if (req.body.beverageType == "inHouseBeverage") {
        beveragesModel.findOne({ _id: req.params.rootcategoryid, beverageType: "inHouseBeverage" })
            .then((result) => {
                let promiseArr = []
                for (let i = 0; i < result.subCategories.length; i++) {
                    promiseArr.push(new Promise((resolve, reject) => {
                        beveragesModel.findOneAndUpdate({
                            _id: req.params.rootcategoryid,
                            "subCategories._id": req.params.subcategoryid
                        }, {
                                $set: {
                                    "subCategories.$.subCategoryName": req.body.subCategoryName
                                }
                            }).then((udata) => {
                                resolve(udata)
                            }).catch((err) => {
                                reject(err)
                            })
                    }))

                }
                Promise.all(promiseArr).then((promiseArr) => {
                    beverages.find({ "varients.id": req.params.subcategoryid })
                        .then((beverageData) => {
                            promiseArr1 = []
                            for (let i = 0; i < beverageData.length; i++) {
                                promiseArr1.push(new Promise((resolve, reject) => {
                                    if (beverageData[i].varients.id == req.params.subcategoryid && beverageData[i].category.id == req.params.subcategoryid) {
                                        beverages.findOneAndUpdate({ _id: beverageData[i]._id },
                                            {
                                                $set: {
                                                    "category.category": req.body.subCategoryName,
                                                    "varients.varients": req.body.subCategoryName
                                                }
                                            }).then((Budata) => {
                                                resolve(Budata)
                                            }).catch((err) => {
                                                reject(err)
                                            })
                                    } else {
                                        beverages.findOneAndUpdate({ _id: beverageData[i]._id },
                                            {
                                                $set: {
                                                    "varients.varients": req.body.subCategoryName
                                                }
                                            }).then((Budata) => {
                                                resolve(Budata)
                                            }).catch((err) => {
                                                reject(err)
                                            })
                                    }
                                }))
                            }
                            Promise.all(promiseArr1).then((promiseArr1) => {
                                res.json({ success: true, msg: "Sub category name updated" })
                            }).catch((err) => {
                                res.json({ success: false, msg: "error in promise arr 1", error: err })
                            })
                        }).catch((err) => {
                            res.json({ success: false, msg: "error in beverage data", error: err })
                        })
                }).catch((err) => {
                    res.json({ success: false, msg: "error in promise", error: err })
                })

            }).catch((err) => {
                res.json({ success: false, msg: "error in result", error: err })
            })
    } else {
        //Update for retail beverages
        beveragesModel.findOne({ _id: req.params.rootcategoryid, beverageType: "retailBeverage" })
            .then((result) => {
                let promiseArr2 = []
                for (let i = 0; i < result.subCategories.length; i++) {
                    promiseArr2.push(new Promise((resolve, reject) => {
                        beveragesModel.findOneAndUpdate({
                            _id: req.params.rootcategoryid,
                            "subCategories._id": req.params.subcategoryid
                        }, {
                                $set: {
                                    "subCategories.$.subCategoryName": req.body.subCategoryName
                                }
                            }).then((udata) => {
                                resolve(udata)
                            }).catch((err) => {
                                reject(err)
                            })
                    }))

                }
                Promise.all(promiseArr2).then((promiseArr2) => {
                    retailBeverageBrandModel.find()
                        .then((RBBrandData) => {
                            promiseArr3 = []
                            for (let i = 0; i < RBBrandData.length; i++) {
                                if (RBBrandData[i].categoryType.length > 0) {
                                    for (let j = 0; j < RBBrandData[i].categoryType.length; j++) {
                                        if (RBBrandData[i].categoryType[j].subCategories.length > 0) {
                                            for (let k = 0; k < RBBrandData[i].categoryType[j].subCategories.length; k++) {
                                                promiseArr3.push(new Promise((resolve, reject) => {
                                                    retailBeverageBrandModel.findOneAndUpdate({
                                                        _id: RBBrandData[i]._id,
                                                        "categoryType._id": req.params.rootcategoryid,
                                                        "categoryType.subCategories._id": req.params.subcategoryid
                                                    },
                                                        {
                                                            $set: {
                                                                "categoryType.$.subCategories.$[outer].subCategoryName": req.body.subCategoryName
                                                            }
                                                        },
                                                        {
                                                            "arrayFilters": [
                                                                {
                                                                    "outer._id": req.params.subcategoryid
                                                                }
                                                            ]
                                                        }).then((RBBudata) => {
                                                            resolve(RBBudata)
                                                        }).catch((err) => {
                                                            reject(err)
                                                        })
                                                }))
                                            }
                                        } else {
                                            continue
                                        }
                                    }
                                } else {
                                    continue
                                }
                            }
                            Promise.all(promiseArr3).then((promiseArr3) => {
                                retailBeverageProductModel.find({ "varient.id": req.params.subcategoryid })
                                    .then((RBProductData) => {
                                        promiseArr4 = []
                                        for (let i = 0; i < RBProductData.length; i++) {
                                            promiseArr4.push(new Promise((resolve, reject) => {
                                                if (RBProductData[i].varient.id == req.params.subcategoryid && RBProductData[i].category.id == req.params.subcategoryid) {
                                                    retailBeverageProductModel.findOneAndUpdate({ _id: RBProductData[i]._id },
                                                        {
                                                            $set: {
                                                                "category.category": req.body.subCategoryName,
                                                                "varient.varient": req.body.subCategoryName
                                                            }
                                                        }).then((RBPudata) => {
                                                            resolve(RBPudata)
                                                        }).catch((err) => {
                                                            reject(err)
                                                        })
                                                } else {
                                                    retailBeverageProductModel.findOneAndUpdate({ _id: RBProductData[i]._id },
                                                        {
                                                            $set: {
                                                                "varient.varient": req.body.subCategoryName
                                                            }
                                                        }).then((RBPudata) => {
                                                            resolve(RBPudata)
                                                        }).catch((err) => {
                                                            reject(err)
                                                        })
                                                }
                                            }))
                                        }
                                        Promise.all(promiseArr4).then((promiseArr4) => {
                                            attributeSetModel.find({ attributeType: "retailbeverages" })
                                                .then((attributeData) => {
                                                    promiseArr5 = []
                                                    for (let i = 0; i < attributeData.length; i++) {
                                                        if (attributeData[i].categoryType.length > 0) {
                                                            for (let j = 0; j < attributeData[i].categoryType.length; j++) {
                                                                if (attributeData[i].categoryType[j].subCategories.length > 0) {
                                                                    for (let k = 0; k < attributeData[i].categoryType[j].subCategories.length; k++) {
                                                                        promiseArr5.push(new Promise((resolve, reject) => {
                                                                            attributeSetModel.findOneAndUpdate({
                                                                                _id: attributeData[i]._id,
                                                                                "categoryType._id": req.params.rootcategoryid,
                                                                                "categoryType.subCategories._id": req.params.subcategoryid
                                                                            }, {
                                                                                    $set: {
                                                                                        "categoryType.$.subCategories.$[outer].subCategoryName": req.body.subCategoryName
                                                                                    }
                                                                                },
                                                                                {
                                                                                    "arrayFilters": [
                                                                                        {
                                                                                            "outer._id": req.params.subcategoryid
                                                                                        }
                                                                                    ]
                                                                                }).then((Audata) => {
                                                                                    resolve(Audata)
                                                                                }).catch((err) => {
                                                                                    reject(err)
                                                                                })
                                                                        }))
                                                                    }
                                                                } else {
                                                                    continue
                                                                }
                                                            }
                                                        } else {
                                                            continue
                                                        }
                                                    }
                                                    Promise.all(promiseArr5).then((promiseArr5) => {
                                                        res.json({ success: true, msg: "Sub category name updated" })
                                                    }).catch((err) => {
                                                        res.json({ success: false, msg: "error in promise 5", error: err })
                                                    })
                                                }).catch((err) => {
                                                    res.json({ success: false, msg: "error in attribute data", error: err })
                                                })
                                        }).catch((err) => {
                                            res.json({ success: false, msg: "error in promise 4", error: err })
                                        })
                                    }).catch((err) => {
                                        res.json({ success: false, msg: "error in retail beverage product data", error: err })
                                    })
                            }).catch((err) => {
                                res.json({ success: false, msg: "error in promise 3", error: err })
                            })
                        }).catch((err) => {
                            res.json({ success: false, msg: "error in retail beverage brand data", error: err })
                        })
                }).catch((err) => {
                    res.json({ success: false, msg: "error in promise 2", error: err })
                })

            }).catch((err) => {
                res.json({ success: false, msg: "error in result", error: err })
            })
    }
}

const updateSubSubCategoriesName = (req, res) => {
    //Update for in house beverage
    if (req.body.beverageType == "inHouseBeverage") {
        beveragesModel.findOne({ _id: req.params.rootcategoryid, beverageType: "inHouseBeverage" })
            .then((result) => {
                let promiseArr = []
                for (let i = 0; i < result.subCategories.length; i++) {
                    if (result.subCategories[i].subSubCategories.length > 0) {
                        for (let j = 0; j < result.subCategories[i].subSubCategories.length; j++) {
                            promiseArr.push(new Promise((resolve, reject) => {
                                beveragesModel.findOneAndUpdate({
                                    _id: req.params.rootcategoryid,
                                    "subCategories.subSubCategories._id": req.params.subsubcategoryid
                                }, {
                                        $set: {
                                            "subCategories.$.subSubCategories.$[outer].subSubCategoryName": req.body.subSubCategoryName
                                        }
                                    },
                                    {
                                        "arrayFilters": [
                                            {
                                                "outer._id": req.params.subsubcategoryid,
                                            }
                                        ]
                                    }).then((udata) => {
                                        resolve(udata)
                                    }).catch((err) => {
                                        reject(err)
                                    })
                            }))
                        }
                    } else {
                        continue;
                    }
                }
                Promise.all(promiseArr).then((promiseArr) => {
                    beverages.find({ "subVarient.id": req.params.subsubcategoryid })
                        .then((beverageData) => {
                            promiseArr1 = []
                            for (let i = 0; i < beverageData.length; i++) {
                                promiseArr1.push(new Promise((resolve, reject) => {
                                    if (beverageData[i].subVarient.id == req.params.subsubcategoryid && beverageData[i].category.id == req.params.subsubcategoryid) {
                                        beverages.findOneAndUpdate({ _id: beverageData[i]._id },
                                            {
                                                $set: {
                                                    "category.category": req.body.subSubCategoryName,
                                                    "subVarient.subVarient": req.body.subSubCategoryName
                                                }
                                            }).then((Budata) => {
                                                resolve(Budata)
                                            }).catch((err) => {
                                                reject(err)
                                            })
                                    } else {
                                        beverages.findOneAndUpdate({ _id: beverageData[i]._id },
                                            {
                                                $set: {
                                                    "subVarient.subVarient": req.body.subSubCategoryName
                                                }
                                            }).then((Budata) => {
                                                resolve(Budata)
                                            }).catch((err) => {
                                                reject(err)
                                            })
                                    }
                                }))
                            }
                            Promise.all(promiseArr1).then((promiseArr1) => {
                                res.json({ success: true, msg: "Sub sub category name updated" })
                            }).catch((err) => {
                                res.json({ success: false, msg: "error in promise arr 1", error: err })
                            })
                        }).catch((err) => {
                            res.json({ success: false, msg: "error in beverage data", error: err })
                        })
                }).catch((err) => {
                    res.json({ success: false, msg: "error in promise", error: err })
                })

            }).catch((err) => {
                res.json({ success: false, msg: "error in result", error: err })
            })
    } else {
        //Update for retail beverage
        beveragesModel.findOne({ _id: req.params.rootcategoryid, beverageType: "retailBeverage" })
            .then((result) => {
                let promiseArr2 = []
                for (let i = 0; i < result.subCategories.length; i++) {
                    if (result.subCategories[i].subSubCategories.length > 0) {
                        for (let j = 0; j < result.subCategories[i].subSubCategories.length; j++) {
                            promiseArr2.push(new Promise((resolve, reject) => {
                                beveragesModel.findOneAndUpdate({
                                    _id: req.params.rootcategoryid,
                                    "subCategories.subSubCategories._id": req.params.subsubcategoryid
                                }, {
                                        $set: {
                                            "subCategories.$.subSubCategories.$[outer].subSubCategoryName": req.body.subSubCategoryName
                                        }
                                    },
                                    {
                                        "arrayFilters": [
                                            {
                                                "outer._id": req.params.subsubcategoryid,
                                            }
                                        ]
                                    }).then((udata) => {
                                        resolve(udata)
                                    }).catch((err) => {
                                        reject(err)
                                    })
                            }))
                        }
                    } else {
                        continue;
                    }
                }
                Promise.all(promiseArr2).then((promiseArr2) => {
                    retailBeverageBrandModel.find()
                        .then((RBBrandData) => {
                            promiseArr3 = []
                            for (let i = 0; i < RBBrandData.length; i++) {
                                if (RBBrandData[i].categoryType.length > 0) {
                                    for (let j = 0; j < RBBrandData[i].categoryType.length; j++) {
                                        if (RBBrandData[i].categoryType[j].subCategories.length > 0) {
                                            for (let k = 0; k < RBBrandData[i].categoryType[j].subCategories.length; k++) {
                                                if (RBBrandData[i].categoryType[j].subCategories[k].subSubCategories.length > 0) {
                                                    for (let l = 0; l < RBBrandData[i].categoryType[j].subCategories[k].subSubCategories.length; l++) {
                                                        promiseArr3.push(new Promise((resolve, reject) => {
                                                            retailBeverageBrandModel.findOneAndUpdate({
                                                                _id: RBBrandData[i]._id,
                                                                "categoryType._id": req.params.rootcategoryid,
                                                                "categoryType.subCategories._id": req.params.subcategoryid,
                                                                "categoryType.subCategories.subSubCategories._id": req.params.subsubcategoryid
                                                            },
                                                                {
                                                                    $set: {
                                                                        "categoryType.$.subCategories.$[outer].subSubCategories.$[inner].subSubCategoryName": req.body.subSubCategoryName
                                                                    }
                                                                },
                                                                {
                                                                    "arrayFilters": [
                                                                        {
                                                                            "outer._id": req.params.subcategoryid
                                                                        },
                                                                        {
                                                                            "inner._id": req.params.subsubcategoryid
                                                                        }
                                                                    ]
                                                                }).then((RBBudata) => {
                                                                    resolve(RBBudata)
                                                                }).catch((err) => {
                                                                    reject(err)
                                                                })
                                                        }))
                                                    }
                                                } else {
                                                    continue
                                                }
                                            }
                                        } else {
                                            continue
                                        }
                                    }
                                } else {
                                    continue
                                }
                            }
                            Promise.all(promiseArr3).then((promiseArr3) => {
                                retailBeverageProductModel.find({ "subVarient.id": req.params.subsubcategoryid })
                                    .then((RBProductData) => {
                                        promiseArr4 = []
                                        for (let i = 0; i < RBProductData.length; i++) {
                                            promiseArr4.push(new Promise((resolve, reject) => {
                                                if (RBProductData[i].subVarient.id == req.params.subsubcategoryid && RBProductData[i].category.id == req.params.subsubcategoryid) {
                                                    retailBeverageProductModel.findOneAndUpdate({ _id: RBProductData[i]._id },
                                                        {
                                                            $set: {
                                                                "category.category": req.body.subSubCategoryName,
                                                                "subVarient.subVarient": req.body.subSubCategoryName
                                                            }
                                                        }).then((RBPudata) => {
                                                            resolve(RBPudata)
                                                        }).catch((err) => {
                                                            reject(err)
                                                        })
                                                } else {
                                                    retailBeverageProductModel.findOneAndUpdate({ _id: RBProductData[i]._id },
                                                        {
                                                            $set: {
                                                                "subVarient.subVarient": req.body.subSubCategoryName
                                                            }
                                                        }).then((RBPudata) => {
                                                            resolve(RBPudata)
                                                        }).catch((err) => {
                                                            reject(err)
                                                        })
                                                }
                                            }))
                                        }
                                        Promise.all(promiseArr4).then((promiseArr4) => {
                                            attributeSetModel.find({ attributeType: "retailbeverages" })
                                                .then((attributeData) => {
                                                    promiseArr5 = []
                                                    for (let i = 0; i < attributeData.length; i++) {
                                                        if (attributeData[i].categoryType.length > 0) {
                                                            for (let j = 0; j < attributeData[i].categoryType.length; j++) {
                                                                if (attributeData[i].categoryType[j].subCategories.length > 0) {
                                                                    for (let k = 0; k < attributeData[i].categoryType[j].subCategories.length; k++) {
                                                                        if (attributeData[i].categoryType[j].subCategories[k].subSubCategories.length > 0) {
                                                                            for (let l = 0; l < attributeData[i].categoryType[j].subCategories[k].subSubCategories.length; l++) {
                                                                                promiseArr5.push(new Promise((resolve, reject) => {
                                                                                    attributeSetModel.findOneAndUpdate({
                                                                                        _id: attributeData[i]._id,
                                                                                        "categoryType._id": req.params.rootcategoryid,
                                                                                        "categoryType.subCategories._id": req.params.subcategoryid,
                                                                                        "categoryType.subCategories.subSubCategories._id": req.params.subsubcategoryid
                                                                                    }, {
                                                                                            $set: {
                                                                                                "categoryType.$.subCategories.$[outer].subSubCategories.$[inner].subSubCategoryName": req.body.subSubCategoryName
                                                                                            }
                                                                                        },
                                                                                        {
                                                                                            "arrayFilters": [
                                                                                                {
                                                                                                    "outer._id": req.params.subcategoryid
                                                                                                },
                                                                                                {
                                                                                                    "inner._id": req.params.subsubcategoryid
                                                                                                }
                                                                                            ]
                                                                                        }).then((Audata) => {
                                                                                            resolve(Audata)
                                                                                        }).catch((err) => {
                                                                                            reject(err)
                                                                                        })
                                                                                }))
                                                                            }
                                                                        } else {
                                                                            continue
                                                                        }
                                                                    }
                                                                } else {
                                                                    continue
                                                                }
                                                            }
                                                        } else {
                                                            continue
                                                        }
                                                    }
                                                    Promise.all(promiseArr5).then((promiseArr5) => {
                                                        res.json({ success: true, msg: "Sub sub category name updated" })
                                                    }).catch((err) => {
                                                        res.json({ success: false, msg: "error in promise 5", error: err })
                                                    })
                                                }).catch((err) => {
                                                    res.json({ success: false, msg: "error in attribute data", error: err })
                                                })
                                        }).catch((err) => {
                                            res.json({ success: false, msg: "error in promise 4", error: err })
                                        })
                                    }).catch((err) => {
                                        res.json({ success: false, msg: "error in retail beverage product data", error: err })
                                    })
                            }).catch((err) => {
                                res.json({ success: false, msg: "error in promise 3", error: err })
                            })
                        }).catch((err) => {
                            res.json({ success: false, msg: "error in retail beverage brand data", error: err })
                        })
                }).catch((err) => {
                    res.json({ success: false, msg: "error in promise 2", error: err })
                })

            }).catch((err) => {
                res.json({ success: false, msg: "error in result", error: err })
            })
    }
}

const updateSubSubCategoryTypeName = (req, res) => {
    //Update for in house beverages
    if (req.body.beverageType == "inHouseBeverage") {
        beveragesModel.findOne({ _id: req.params.rootcategoryid, beverageType: "inHouseBeverage" })
            .then((result) => {
                let promiseArr = []
                for (let i = 0; i < result.subCategories.length; i++) {
                    if (result.subCategories[i].subSubCategories.length > 0) {
                        for (let j = 0; j < result.subCategories[i].subSubCategories.length; j++) {
                            if (result.subCategories[i].subSubCategories[j].subSubCategoryType.length > 0) {
                                for (let k = 0; k < result.subCategories[i].subSubCategories[j].subSubCategoryType.length; k++) {
                                    promiseArr.push(new Promise((resolve, reject) => {
                                        beveragesModel.findOneAndUpdate({
                                            _id: req.params.rootcategoryid,
                                            "subCategories.subSubCategories._id": req.params.subsubcategoryid,
                                            "subCategories.subSubCategories.subSubCategoryType._id": req.params.subsubcategorytypeid
                                        }, {
                                                $set: {
                                                    "subCategories.$.subSubCategories.$[outer].subSubCategoryType.$[inner].subSubCategoryTypeName": req.body.subSubCategoryTypeName
                                                }
                                            },
                                            {
                                                "arrayFilters": [
                                                    {
                                                        "outer._id": req.params.subsubcategoryid,
                                                    },
                                                    {
                                                        "inner._id": req.params.subsubcategorytypeid
                                                    }
                                                ]
                                            }).then((udata) => {
                                                resolve(udata)
                                            }).catch((err) => {
                                                reject(err)
                                            })
                                    }))
                                }
                            } else {
                                continue;
                            }
                        }
                    } else {
                        continue;
                    }
                }
                Promise.all(promiseArr).then((promiseArr) => {
                    beverages.find({ "type.id": req.params.subsubcategorytypeid })
                        .then((beverageData) => {
                            promiseArr1 = []
                            for (let i = 0; i < beverageData.length; i++) {
                                promiseArr1.push(new Promise((resolve, reject) => {
                                    if (beverageData[i].type.id == req.params.subsubcategorytypeid && beverageData[i].category.id == req.params.subsubcategorytypeid) {
                                        beverages.findOneAndUpdate({ _id: beverageData[i]._id },
                                            {
                                                $set: {
                                                    "category.category": req.body.subSubCategoryTypeName,
                                                    "type.type": req.body.subSubCategoryTypeName
                                                }
                                            }).then((Budata) => {
                                                resolve(Budata)
                                            }).catch((err) => {
                                                reject(err)
                                            })
                                    } else {
                                        beverages.findOneAndUpdate({ _id: beverageData[i]._id },
                                            {
                                                $set: {
                                                    "type.type": req.body.subSubCategoryTypeName
                                                }
                                            }).then((Budata) => {
                                                resolve(Budata)
                                            }).catch((err) => {
                                                reject(err)
                                            })
                                    }
                                }))
                            }
                            Promise.all(promiseArr1).then((promiseArr) => {
                                res.json({ success: true, msg: "Sub sub category type name updated" })
                            }).catch((err) => {
                                res.json({ success: false, msg: "error in promise arr 1", error: err })
                            })
                        }).catch((err) => {
                            res.json({ success: false, msg: "error in beverage data", error: err })
                        })
                }).catch((err) => {
                    res.json({ success: false, msg: "error in promise", error: err })
                })

            }).catch((err) => {
                res.json({ success: false, msg: "error in result", error: err })
            })
    } else {
        //Update for retail beverages
        beveragesModel.findOne({ _id: req.params.rootcategoryid, beverageType: "retailBeverage" })
            .then((result) => {
                let promiseArr2 = []
                for (let i = 0; i < result.subCategories.length; i++) {
                    if (result.subCategories[i].subSubCategories.length > 0) {
                        for (let j = 0; j < result.subCategories[i].subSubCategories.length; j++) {
                            if (result.subCategories[i].subSubCategories[j].subSubCategoryType.length > 0) {
                                for (let k = 0; k < result.subCategories[i].subSubCategories[j].subSubCategoryType.length; k++) {
                                    promiseArr2.push(new Promise((resolve, reject) => {
                                        beveragesModel.findOneAndUpdate({
                                            _id: req.params.rootcategoryid,
                                            "subCategories.subSubCategories._id": req.params.subsubcategoryid,
                                            "subCategories.subSubCategories.subSubCategoryType._id": req.params.subsubcategorytypeid
                                        }, {
                                                $set: {
                                                    "subCategories.$.subSubCategories.$[outer].subSubCategoryType.$[inner].subSubCategoryTypeName": req.body.subSubCategoryTypeName
                                                }
                                            },
                                            {
                                                "arrayFilters": [
                                                    {
                                                        "outer._id": req.params.subsubcategoryid,
                                                    },
                                                    {
                                                        "inner._id": req.params.subsubcategorytypeid
                                                    }
                                                ]
                                            }).then((udata) => {
                                                resolve(udata)
                                            }).catch((err) => {
                                                reject(err)
                                            })
                                    }))
                                }
                            } else {
                                continue;
                            }
                        }
                    } else {
                        continue;
                    }
                }
                Promise.all(promiseArr2).then((promiseArr2) => {
                    retailBeverageBrandModel.find()
                        .then((RBBrandData) => {
                            promiseArr3 = []
                            for (let i = 0; i < RBBrandData.length; i++) {
                                if (RBBrandData[i].categoryType.length > 0) {
                                    for (let j = 0; j < RBBrandData[i].categoryType.length; j++) {
                                        if (RBBrandData[i].categoryType[j].subCategories.length > 0) {
                                            for (let k = 0; k < RBBrandData[i].categoryType[j].subCategories.length; k++) {
                                                if (RBBrandData[i].categoryType[j].subCategories[k].subSubCategories.length > 0) {
                                                    for (let l = 0; l < RBBrandData[i].categoryType[j].subCategories[k].subSubCategories.length; l++) {
                                                        if (RBBrandData[i].categoryType[j].subCategories[k].subSubCategories[l].subSubCategoryType.length > 0) {
                                                            for (let m = 0; m < RBBrandData[i].categoryType[j].subCategories[k].subSubCategories[l].subSubCategoryType.length; m++) {
                                                                promiseArr3.push(new Promise((resolve, reject) => {
                                                                    retailBeverageBrandModel.findOneAndUpdate({
                                                                        _id: RBBrandData[i]._id,
                                                                        "categoryType._id": req.params.rootcategoryid,
                                                                        "categoryType.subCategories._id": req.params.subcategoryid,
                                                                        "categoryType.subCategories.subSubCategories._id": req.params.subsubcategoryid,
                                                                        "categoryType.subCategories.subSubCategories.subSubCategoryType._id": req.params.subsubcategorytypeid
                                                                    },
                                                                        {
                                                                            $set: {
                                                                                "categoryType.$.subCategories.$[outer].subSubCategories.$[inner].subSubCategoryType.$[middle].subSubCategoryTypeName": req.body.subSubCategoryTypeName
                                                                            }
                                                                        },
                                                                        {
                                                                            "arrayFilters": [
                                                                                {
                                                                                    "outer._id": req.params.subcategoryid
                                                                                },
                                                                                {
                                                                                    "inner._id": req.params.subsubcategoryid
                                                                                },
                                                                                {
                                                                                    "middle._id": req.params.subsubcategorytypeid
                                                                                }
                                                                            ]
                                                                        }).then((RBBudata) => {
                                                                            resolve(RBBudata)
                                                                        }).catch((err) => {
                                                                            reject(err)
                                                                        })
                                                                }))
                                                            }
                                                        } else {
                                                            continue
                                                        }
                                                    }
                                                } else {
                                                    continue
                                                }
                                            }
                                        } else {
                                            continue
                                        }
                                    }
                                } else {
                                    continue
                                }
                            }
                            Promise.all(promiseArr3).then((promiseArr3) => {
                                retailBeverageProductModel.find({ "type.id": req.params.subsubcategorytypeid })
                                    .then((RBProductData) => {
                                        promiseArr4 = []
                                        for (let i = 0; i < RBProductData.length; i++) {
                                            promiseArr4.push(new Promise((resolve, reject) => {
                                                if (RBProductData[i].type.id == req.params.subsubcategorytypeid && RBProductData[i].category.id == req.params.subsubcategorytypeid) {
                                                    retailBeverageProductModel.findOneAndUpdate({ _id: RBProductData[i]._id },
                                                        {
                                                            $set: {
                                                                "category.category": req.body.subSubCategoryTypeName,
                                                                "type.type": req.body.subSubCategoryTypeName
                                                            }
                                                        }).then((RBPudata) => {
                                                            resolve(RBPudata)
                                                        }).catch((err) => {
                                                            reject(err)
                                                        })
                                                } else {
                                                    retailBeverageProductModel.findOneAndUpdate({ _id: RBProductData[i]._id },
                                                        {
                                                            $set: {
                                                                "type.type": req.body.subSubCategoryTypeName
                                                            }
                                                        }).then((RBPudata) => {
                                                            resolve(RBPudata)
                                                        }).catch((err) => {
                                                            reject(err)
                                                        })
                                                }
                                            }))
                                        }
                                        Promise.all(promiseArr4).then((promiseArr4) => {
                                            attributeSetModel.find({ attributeType: "retailbeverages" })
                                                .then((attributeData) => {
                                                    promiseArr5 = []
                                                    for (let i = 0; i < attributeData.length; i++) {
                                                        if (attributeData[i].categoryType.length > 0) {
                                                            for (let j = 0; j < attributeData[i].categoryType.length; j++) {
                                                                if (attributeData[i].categoryType[j].subCategories.length > 0) {
                                                                    for (let k = 0; k < attributeData[i].categoryType[j].subCategories.length; k++) {
                                                                        if (attributeData[i].categoryType[j].subCategories[k].subSubCategories.length > 0) {
                                                                            for (let l = 0; l < attributeData[i].categoryType[j].subCategories[k].subSubCategories.length; l++) {
                                                                                if (attributeData[i].categoryType[j].subCategories[k].subSubCategories[l].subSubCategoryType.length > 0) {
                                                                                    for (let m = 0; m < attributeData[i].categoryType[j].subCategories[k].subSubCategories[l].subSubCategoryType.length; m++) {
                                                                                        promiseArr5.push(new Promise((resolve, reject) => {
                                                                                            attributeSetModel.findOneAndUpdate({
                                                                                                _id: attributeData[i]._id,
                                                                                                "categoryType._id": req.params.rootcategoryid,
                                                                                                "categoryType.subCategories._id": req.params.subcategoryid,
                                                                                                "categoryType.subCategories.subSubCategories._id": req.params.subsubcategoryid,
                                                                                                "categoryType.subCategories.subSubCategories.subSubCategoryType._id": req.params.subsubcategorytypeid
                                                                                            }, {
                                                                                                    $set: {
                                                                                                        "categoryType.$.subCategories.$[outer].subSubCategories.$[inner].subSubCategoryType.$[middle].subSubCategoryTypeName": req.body.subSubCategoryTypeName
                                                                                                    }
                                                                                                },
                                                                                                {
                                                                                                    "arrayFilters": [
                                                                                                        {
                                                                                                            "outer._id": req.params.subcategoryid
                                                                                                        },
                                                                                                        {
                                                                                                            "inner._id": req.params.subsubcategoryid
                                                                                                        },
                                                                                                        {
                                                                                                            "middle._id": req.params.subsubcategorytypeid
                                                                                                        }
                                                                                                    ]
                                                                                                }).then((Audata) => {
                                                                                                    resolve(Audata)
                                                                                                }).catch((err) => {
                                                                                                    reject(err)
                                                                                                })
                                                                                        }))
                                                                                    }
                                                                                } else {
                                                                                    continue
                                                                                }
                                                                            }
                                                                        } else {
                                                                            continue
                                                                        }
                                                                    }
                                                                } else {
                                                                    continue
                                                                }
                                                            }
                                                        } else {
                                                            continue
                                                        }
                                                    }
                                                    Promise.all(promiseArr5).then((promiseArr5) => {
                                                        res.json({ success: true, msg: "Sub sub category type name updated" })
                                                    }).catch((err) => {
                                                        res.json({ success: false, msg: "error in promise arr 5", error: err })
                                                    })
                                                }).catch((err) => {
                                                    res.json({ success: false, msg: "error in attribute data", error: err })
                                                })
                                        }).catch((err) => {
                                            res.json({ success: false, msg: "error in promise arr 4", error: err })
                                        })
                                    }).catch((err) => {
                                        res.json({ success: false, msg: "error in retail beverage product data", error: err })
                                    })
                            }).catch((err) => {
                                res.json({ success: false, msg: "error in promise arr 3", error: err })
                            })
                        }).catch((err) => {
                            res.json({ success: false, msg: "error in retail beverage brand data", error: err })
                        })
                }).catch((err) => {
                    res.json({ success: false, msg: "error in promise arr 2", error: err })
                })

            }).catch((err) => {
                res.json({ success: false, msg: "error in result", error: err })
            })
    }
}

module.exports = {
    addRootBeveragesCategoryName,
    addSubcategory,
    addSubSubCategories,
    addSubSubCategoryType,
    getRootBeveragesCategories,
    getAllBeveragesCategories,
    updateRootBeveragesCategoryName,
    updateSubcategoryName,
    updateSubSubCategoriesName,
    updateSubSubCategoryTypeName
}
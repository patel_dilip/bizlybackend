const inventory_assets_model = require('../../../_bizly_models/_menu_management_models/_inventory_assets_models/_inventory_assets_model').inventoryAssetsModel
const beverageModel = require('../../../_bizly_models/_menu_management_models/_beverages_model/beverages-category-model').beveragesModel
const inventory_food_model = require('../../../_bizly_models/_menu_management_models/_inventory_food_models/_inventory_food_model').inventoryfoodmodel
const liquorModel = require('../../../_bizly_models/_menu_management_models/_liquor_models/drink-type-model').rootdrinktype
const retail_Food_model = require('../../../_bizly_models/_menu_management_models/retail_food_models/_retail_food_model').retailfoodModel

const getAllCategories = async (req, res) => {
    try {

        beverageModel.find()
            .populate('addedBy', 'userName')
            .populate('updatedBy', 'userName')
            .then((beverageData) => {
                inventory_food_model.find()
                    .populate('addedBy', 'userName')
                    .populate('updatedBy', 'userName')
                    .then((inventoryFoodData) => {
                        liquorModel.find()
                            .populate('addedBy', 'userName')
                            .populate('updatedBy', 'userName')
                            .then((liquorData) => {
                                retail_Food_model.find()
                                    .populate('addedBy', 'userName')
                                    .populate('updatedBy', 'userName')
                                    .then((retailFoodData) => {
                                        res.json({
                                            success: true,
                                            BeverageCategory: beverageData,
                                            InventoryFoodCategory: inventoryFoodData,
                                            LiquorCategory: liquorData,
                                            retailFoodCategory: retailFoodData
                                        })
                                    }).catch((err) => {
                                        return res.json({ success: false, msg: "error in retail food data", error: err })
                                    })
                            }).catch((err) => {
                                return res.json({ success: false, msg: "error in Liquor data", error: err })
                            })
                    }).catch((err) => {
                        return res.json({ success: false, msg: "error in Inventory food data", error: err })
                    })
            }).catch((err) => {
                return res.json({ success: false, msg: "error in beverage data", error: err })
            })

    } catch (ex) {
        return res.json({ success: false, error: ex.error })
    }
}
module.exports = {
    getAllCategories
}
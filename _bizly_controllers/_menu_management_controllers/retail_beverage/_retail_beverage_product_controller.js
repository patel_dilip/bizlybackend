const retailbeverageModel = require('../../../_bizly_models/_menu_management_models/retail_beverages_model/_retail_beverage_product_model').productModel

const addRetailBeverageProduct = (req, res) => {
    retailbeverageModel.findOne({ productName: req.body.productName }, (err, data) => {
        if (err) {
            res.json({ success: false, msg: err })
        } else if (data) {
            res.json({ success: false, msg: "retail product name already exists" })
        } else {
            var product = new retailbeverageModel()
            product.productName = req.body.productName
            product.rootCategory = req.body.rootCategory
            product.varient = req.body.varient
            product.subVarient = req.body.subVarient
            product.type = req.body.type
            product.attributeSet = req.body.attributeSet
            product.attributeSet_response = req.body.attributeSet_response
            product.category = req.body.category
            product.unit = req.body.unit
            product.associatedBrand = req.body.associatedBrand
            product.productImage = req.body.productImage
            product.addedBy = req.body.userid
            product.updatedBy = req.body.userid

            product.save((err) => {
                if (err) {
                    res.json({ success: false, msg: err })
                } else {
                    res.json({ success: true, msg: "Retail Beverage product added" })
                }
            })
        }
    })
}

const getAllRetailBeverageProduct = (req, res) => {
    retailbeverageModel.find()
        .populate('associatedBrand')
        .populate('addedBy', 'userName')
        .populate('updatedBy', 'userName')
        .exec((err, data) => {
            if (err) {
                res.json({ success: false, msg: err })
            } else if (data == null || data.length == 0) {
                res.json({ success: false, msg: "retail Beverage product Data Not Found" })
            } else {
                res.json({ success: true, msg: "retail Beverage product Data Found", data: data })
            }
        })
}

const getRetailBeverageProductById = (req, res) => {
    retailbeverageModel.find(req.params.productid)
        .populate('associatedBrand')
        .populate('addedBy', 'userName')
        .populate('updatedBy', 'userName')
        .exec((err, data) => {
            if (err) {
                res.json({ success: false, msg: err })
            } else if (data == null || data.length == 0) {
                res.json({ success: false, msg: "retail Beverage product Data Not Found" })
            } else {
                res.json({ success: true, msg: "retail Beverage product Data Found", data: data })
            }
        })
}

// check retail beverage product Already Exists
const checkretailBeverageProductAlreadyExists = (req, res) => {
    retailbeverageModel.findOne({ productName: req.params.productName }, (err, data) => {
        if (err) {
            res.json({ success: false, msg: err })
        } else if (data) {
            res.json({ success: true, msg: "Product Name already Exists" })
        } else {
            res.json({ success: false, msg: "Product Name not available" })
        }
    })
}

const updateRetailBeverageProduct = (req, res) => {
    retailbeverageModel.findByIdAndUpdate(req.params.productid, req.body, (err, udata) => {
        if (err) {
            res.json({ success: false, msg: err })
        } else {
            res.json({ success: true, msg: "retail beverage product updated" })
        }
    })
}

const deleteRetailBeverageProduct = (req, res) => {
    retailbeverageModel.findByIdAndUpdate(req.params.productid, { $set: { status: req.body.status } }, (err, udata) => {
        if (err) {
            res.json({ success: false, msg: err })
        } else {
            res.json({ success: true, msg: "retail beverage product deleted" })
        }
    })
}

module.exports = {
    addRetailBeverageProduct,
    getAllRetailBeverageProduct,
    getRetailBeverageProductById,
    checkretailBeverageProductAlreadyExists,
    updateRetailBeverageProduct,
    deleteRetailBeverageProduct
}
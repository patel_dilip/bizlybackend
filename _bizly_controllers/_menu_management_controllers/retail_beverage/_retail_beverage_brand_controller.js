const retailbeverageModel = require('../../../_bizly_models/_menu_management_models/retail_beverages_model/_retail_beverage_brand_model').brandModel

const addRetailBeverageBrand = (req, res) => {
    retailbeverageModel.findOne({ brandName: req.body.brandName }, (err, data) => {
        if (err) {
            res.json({ success: false, msg: err })
        } else if (data) {
            res.json({ success: false, msg: "retail brand name already exists" })
        } else {
            var brand = new retailbeverageModel()
            brand.brandName = req.body.brandName
            brand.country = req.body.country
            brand.brandLogo = req.body.brandLogo
            brand.categoryType = req.body.categoryType
            brand.addedBy = req.body.userid
            brand.updatedBy = req.body.userid

            brand.save((err) => {
                if (err) {
                    res.json({ success: false, msg: err })
                } else {
                    res.json({ success: true, msg: "retail beverage brand added" })
                }
            })
        }
    })
}

const getAllRetailBeverageBrand = (req, res) => {
    retailbeverageModel.find()
        .populate('addedBy', 'userName')
        .populate('updatedBy', 'userName')
        .exec((err, data) => {
            if (err) {
                res.json({ success: false, msg: err })
            } else if (data == null || data.length == 0) {
                res.json({ success: false, msg: "retail Beverage brand Data Not Found" })
            } else {
                res.json({ success: true, msg: "retail Beverage brand Data Found", data: data })
            }
        })
}

const getRetailBeverageBrandById = (req, res) => {
    retailbeverageModel.find(req.params.brandid)
        .populate('addedBy', 'userName')
        .populate('updatedBy', 'userName')
        .exec((err, data) => {
            if (err) {
                res.json({ success: false, msg: err })
            } else if (data == null || data.length == 0) {
                res.json({ success: false, msg: "retail Beverage brand Data Not Found" })
            } else {
                res.json({ success: true, msg: "retail Beverage brand Data Found", data: data })
            }
        })
}

const getRetailBeverageBrandFromCategories = (req, res) => {
    retailbeverageModel.find({}, (err, brandData) => {
        var arr1 = []
        var arr2 = []
        var arr3 = []
        var arr4 = []
        for (let i = 0; i < brandData.length; i++) {
            for (let j = 0; j < brandData[i].categoryType.length; j++) {
                if (brandData[i].categoryType[j]._id == req.body.id &&
                    brandData[i].categoryType[j].rootCategoryName == req.body.string &&
                    brandData[i].categoryType[j].showChildren == true) {
                    var promise1 = new Promise((resolve, reject) => {
                        arr1.push(brandData[i])
                        resolve(arr1)
                    })

                } else if (brandData[i].categoryType[j].subCategories.length > 0) {
                    for (let k = 0; k < brandData[i].categoryType[j].subCategories.length; k++) {
                        if (brandData[i].categoryType[j].subCategories[k]._id == req.body.id &&
                            brandData[i].categoryType[j].subCategories[k].subCategoryName == req.body.string &&
                            brandData[i].categoryType[j].subCategories[k].showChildren == true) {

                            var promise2 = new Promise((resolve, reject) => {
                                arr2.push(brandData[i])
                                resolve(arr2)
                            })
                        } else if (brandData[i].categoryType[j].subCategories[k].subSubCategories.length > 0) {
                            for (let l = 0; l < brandData[i].categoryType[j].subCategories[k].subSubCategories.length; l++) {
                                if (brandData[i].categoryType[j].subCategories[k].subSubCategories[l]._id == req.body.id &&
                                    brandData[i].categoryType[j].subCategories[k].subSubCategories[l].subSubCategoryName == req.body.string &&
                                    brandData[i].categoryType[j].subCategories[k].subSubCategories[l].showChildren == true) {

                                    var promise3 = new Promise((resolve, reject) => {
                                        arr3.push(brandData[i])
                                        resolve(arr3)
                                    })
                                } else if (brandData[i].categoryType[j].subCategories[k].subSubCategories[l].subSubCategoryType.length > 0) {
                                    for (let m = 0; m < brandData[i].categoryType[j].subCategories[k].subSubCategories[l].subSubCategoryType.length; m++) {
                                        if (brandData[i].categoryType[j].subCategories[k].subSubCategories[l].subSubCategoryType[m]._id == req.body.id &&
                                            brandData[i].categoryType[j].subCategories[k].subSubCategories[l].subSubCategoryType[m].subSubCategoryTypeName == req.body.string &&
                                            brandData[i].categoryType[j].subCategories[k].subSubCategories[l].subSubCategoryType[m].showChildren == true) {

                                            var promise4 = new Promise((resolve, reject) => {
                                                arr4.push(brandData[i])
                                                resolve(arr4)
                                            })
                                        } else {
                                            // res.json({success:false , msg:req.body.string + "not found"})
                                        }
                                    }
                                } else {
                                    console.log("sub Sub Category Type length =0")
                                }
                            }
                        } else {
                            console.log("sub Sub Categories length =0")
                        }
                    }

                } else {
                    console.log("sub Categories length = 0")
                }
            }
        }
        Promise.all([promise1, promise2, promise3, promise4]).then((finalData) => {
            finalData.filter(data => {
                if (data == null || data == undefined) {
                    console.log("negative")
                } else {
                    res.json({ success: true, data: data })
                }
            })
        }).catch((err) => {
            res.json({ success: false, msg: "Error in final data" })
        })
    })
}

// check retail beverage brand Already Exists
const checkretailBeverageBrandAlreadyExists = (req, res) => {
    retailbeverageModel.findOne({ brandName: req.params.brandName }, (err, data) => {
        if (err) {
            res.json({ success: false, msg: err })
        } else if (data) {
            res.json({ success: true, msg: "Brand Name already Exists" })
        } else {
            res.json({ success: false, msg: "Brand Name not available" })
        }
    })
}

const updateRetailBeverageBrand = (req, res) => {
    retailbeverageModel.findByIdAndUpdate(req.params.brandid, req.body, (err, udata) => {
        if (err) {
            res.json({ success: false, msg: err })
        } else {
            res.json({ success: true, msg: "retail beverage brand updated" })
        }
    })
}

const deleteRetailBeverageBrand = (req, res) => {
    retailbeverageModel.findByIdAndUpdate(req.params.brandid, { $set: { status: req.body.status } }, (err, udata) => {
        if (err) {
            res.json({ success: false, msg: err })
        } else {
            res.json({ success: true, msg: "retail beverage brand deleted" })
        }
    })
}

module.exports = {
    addRetailBeverageBrand,
    getAllRetailBeverageBrand,
    getRetailBeverageBrandById,
    getRetailBeverageBrandFromCategories,
    checkretailBeverageBrandAlreadyExists,
    updateRetailBeverageBrand,
    deleteRetailBeverageBrand
}
const retailBeveragesTreeModel = require("../../../_bizly_models/_menu_management_models/retail_beverages_model/_retail_beverages_tree_model")
  .retailBeveragesTreeModel;

const addTree = (req, res) => {
  var tree = new retailBeveragesTreeModel();
  tree.retailBeveragesTree = req.body.retailBeveragesTree;
  tree.addedBy = req.body.userid;
  tree.updatedBy = req.body.userid;

  tree.save((err) => {
    if (err) {
      res.json({ sucess: false, msg: err });
    } else {
      res.json({ sucess: true });
    }
  });
};

const getTree = async (req, res) => {
  retailBeveragesTreeModel
    .findOne()
    .populate("addedBy", "userName")
    .populate("updatedBy", "userName")
    .sort("-updatedAt")
    .exec(function (err, tree) {
      if (err) {
        res.json({ sucess: false, msg: err });
      } else if (tree == null) {
        res.json({ sucess: false });
      } else {
        res.json({ sucess: true, data: tree });
      }
    });
};

const updateTree = (req, res) => {
  retailBeveragesTreeModel.findByIdAndUpdate(
    req.params.treeid,
    {
      $set: {
        retailBeveragesTree: req.body.retailBeveragesTree,
        updatedBy: req.body.userid,
      },
    },
    (err) => {
      if (err) {
        res.json({ sucess: false, msg: err });
      } else {
        res.json({ sucess: true });
      }
    }
  );
};

module.exports = {
  addTree,
  getTree,
  updateTree,
};

const retail_food_brand_model = require('../../../_bizly_models/_menu_management_models/retail_food_models/_retail_food_brand_model').retail_food_brand

const addRetailFoodBrand = (req, res) => {
    retail_food_brand_model.findOne({ brandName: req.body.brandName }, (err, data) => {
        if (err) {
            res.json({ success: false, msg: err })
        } else if (data) {
            res.json({ success: false, msg: "retail food brand name already exists" })
        } else {
            var brand = new retail_food_brand_model()
            brand.brandName = req.body.brandName
            brand.country = req.body.country
            brand.brandLogo = req.body.brandLogo
            brand.categoryType = req.body.categoryType,
            brand.addedBy = req.body.userid
            brand.updatedBy = req.body.userid

            brand.save((err) => {
                if (err) {
                    res.json({ success: false, msg: err })
                } else {
                    res.json({ success: true, msg: "retail food Brand added" })
                }
            })
        }
    })
}

const getAllRetailFoodBrand = (req, res) => {
    retail_food_brand_model.find({}).
        populate('addedBy', 'userName').
        populate('updatedBy', 'userName').
        sort('-createdAt').
        exec((err, brandData) => {
            if (err) {
                res.json({ sucess: false, msg: err })
            } else if (brandData == null || brandData.length == 0) {
                res.json({ success: false, msg: "retail food Brand data not found" })
            } else {
                res.json({ success: true, msg: "retail food Brand data found", data: brandData })
            }
        })
}

const getRetailFoodBrand = (req, res) => {
    retail_food_brand_model.findOne(req.params.brandid).
        populate('addedBy', 'userName').
        populate('updatedBy', 'userName').
        exec((err, brandData) => {
            if (err) {
                res.json({ success: false, msg: err })
            } else if (brandData == null || brandData.length == 0) {
                res.json({ success: false, msg: "retail food Brand data not found" })
            } else {
                res.json({ success: true, msg: "retail food Brand data found", data: brandData })
            }
        })
}

const getRetailfoodBrandFromCategories = (req, res) => {
    retail_food_brand_model.find({}, (err, brandData) => {
        var arr1 = []
        var arr2 = []
        var arr3 = []
        var arr4 = []
        for (let i = 0; i < brandData.length; i++) {
            for (let j = 0; j < brandData[i].categoryType.length; j++) {
                if (brandData[i].categoryType[j]._id == req.body.id &&
                    brandData[i].categoryType[j].rootCategoryName == req.body.string &&
                    brandData[i].categoryType[j].showChildren == true) {
                    var promise1 = new Promise((resolve, reject) => {
                        arr1.push(brandData[i])
                        resolve(arr1)
                    })

                } else if (brandData[i].categoryType[j].subCategories.length > 0) {
                    for (let k = 0; k < brandData[i].categoryType[j].subCategories.length; k++) {
                        if (brandData[i].categoryType[j].subCategories[k]._id == req.body.id &&
                            brandData[i].categoryType[j].subCategories[k].subCategoryName == req.body.string &&
                            brandData[i].categoryType[j].subCategories[k].showChildren == true) {

                            var promise2 = new Promise((resolve, reject) => {
                                arr2.push(brandData[i])
                                resolve(arr2)
                            })
                        } else if (brandData[i].categoryType[j].subCategories[k].subSubCategories.length > 0) {
                            for (let l = 0; l < brandData[i].categoryType[j].subCategories[k].subSubCategories.length; l++) {
                                if (brandData[i].categoryType[j].subCategories[k].subSubCategories[l]._id == req.body.id &&
                                    brandData[i].categoryType[j].subCategories[k].subSubCategories[l].subSubCategoryName == req.body.string &&
                                    brandData[i].categoryType[j].subCategories[k].subSubCategories[l].showChildren == true) {

                                    var promise3 = new Promise((resolve, reject) => {
                                        arr3.push(brandData[i])
                                        resolve(arr3)
                                    })
                                } else if (brandData[i].categoryType[j].subCategories[k].subSubCategories[l].subSubCategoryType.length > 0) {
                                    for (let m = 0; m < brandData[i].categoryType[j].subCategories[k].subSubCategories[l].subSubCategoryType.length; m++) {
                                        if (brandData[i].categoryType[j].subCategories[k].subSubCategories[l].subSubCategoryType[m]._id == req.body.id &&
                                            brandData[i].categoryType[j].subCategories[k].subSubCategories[l].subSubCategoryType[m].subSubCategoryTypeName == req.body.string &&
                                            brandData[i].categoryType[j].subCategories[k].subSubCategories[l].subSubCategoryType[m].showChildren == true) {

                                            var promise4 = new Promise((resolve, reject) => {
                                                arr4.push(brandData[i])
                                                resolve(arr4)
                                            })
                                        } else {
                                            // res.json({success:false , msg:req.body.string + "not found"})
                                        }
                                    }
                                } else {
                                    console.log("sub Sub Category Type length =0")
                                }
                            }
                        } else {
                            console.log("sub Sub Categories length =0")
                        }
                    }

                } else {
                    console.log("sub Categories length = 0")
                }
            }
        }
        Promise.all([promise1, promise2, promise3, promise4]).then((finalData) => {
            finalData.filter(data => {
                if (data == null || data == undefined) {
                    console.log("negative")
                } else {
                    res.json({ success: true, data: data })
                }
            })
        }).catch((err) => {
            res.json({ success: false, msg: "Error in final data" })
        })
    })
}

// check retail food brand Already Exists
const checkretailFoodBrandAlreadyExists = (req, res) => {
    retail_food_brand_model.findOne({ brandName: req.params.brandName }, (err, data) => {
        if (err) {
            res.json({ success: false, msg: err })
        } else if (data) {
            res.json({ success: true, msg: "Brand Name already Exists" })
        } else {
            res.json({ success: false, msg: "Brand Name not available" })
        }
    })
}

const updateRetailFoodBrand = (req, res) => {
    retail_food_brand_model.findByIdAndUpdate(req.params.brandid, req.body, (err, udata) => {
        if (err) {
            res.json({ success: false, msg: err })
        } else if (udata == null || udata.length == 0) {
            res.json({ success: false, msg: "retail food Brand data not found" })
        } else {
            res.json({ success: true, msg: "retail food Data updated" })
        }
    })
}

const deleteRetailFoodBrand = (req, res) => {
    retail_food_brand_model.findByIdAndDelete(req.params.brandid, (err, data) => {
        if (err) {
            res.json({ success: false, msg: err })
        } else {
            res.json({ success: true, msg: "retail food Data Deleted" })
        }
    })
}

const softDeleteRetailFoodBrand = (req, res) => {
    retail_food_brand_model.findByIdAndUpdate(req.params.brandid, { $set: { status: req.body.status } }, (err, data) => {
        if (err) {
            res.json({ success: false, msg: err })
        } else {
            res.json({ success: true, msg: "retail food Data soft Deleted" })
        }
    })
}
module.exports = {

    addRetailFoodBrand,
    getAllRetailFoodBrand,
    getRetailFoodBrand,
    getRetailfoodBrandFromCategories,
    checkretailFoodBrandAlreadyExists,
    updateRetailFoodBrand,
    deleteRetailFoodBrand,
    softDeleteRetailFoodBrand
}
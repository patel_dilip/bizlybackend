const retail_food_product_model = require('../../../_bizly_models/_menu_management_models/retail_food_models/_retail_food_product_model').retail_food_product_model

const addRetailFoodProduct = (req, res) => {
    retail_food_product_model.findOne({ productName: req.body.productName }, (err, data) => {
        if (err) {
            res.json({ success: false, msg: err })
        } else if (data) {
            res.json({ success: false, msg: "retail product name already exists" })
        } else {
            var product = new retail_food_product_model()
            product.productName = req.body.productName
            product.rootCategory = req.body.rootCategory
            product.varient = req.body.varient
            product.subVarient = req.body.subVarient
            product.type = req.body.type
            product.attributeSet = req.body.attributeSet
            product.attributeSet_response = req.body.attributeSet_response
            product.category = req.body.category
            product.unit = req.body.unit
            product.associatedBrand = req.body.associatedBrand
            product.productImage = req.body.productImage
            product.addedBy = req.body.userid
            product.updatedBy = req.body.userid

            product.save((err) => {
                if (err) {
                    res.json({ success: false, msg: err })
                } else {
                    res.json({ success: true, msg: "retail food product added" })
                }
            })
        }
    })

}
 
const getAllRetailfoodProduct = (req, res) => {
    retail_food_product_model.find({}).
        populate('associatedBrand', 'brandName').
        populate('addedBy', 'userName').
        populate('updatedBy', 'userName').
        sort('-updatedAt').
        exec((err, productData) => {
            if (err) {
                res.json({ sucess: false, msg: err })
            } else if (productData == null || productData.length == 0) {
                res.json({ success: false, msg: "retail food Product data not found" })
            } else {
                res.json({ success: true, msg: "Retail food product data found", data: productData })
            }
        })
}

const getRetailFoodProduct = (req, res) => {
    retail_food_product_model.find(req.params.productid).
        populate('associatedBrand', 'brandName').
        populate('addedBy', 'userName').
        populate('updatedBy', 'userName').
        sort('-updatedAt').
        exec((err, productData) => {
            if (err) {
                res.json({ sucess: false, msg: err })
            } else if (productData == null || productData.length == 0) {
                res.json({ success: false, msg: "Retail food Product data not found" })
            } else {
                res.json({ success: true, msg: "Retail food product data found", data: productData })
            }
        })
}

// check retail food product Already Exists
const checkretailFoodProductAlreadyExists = (req, res) => {
    retail_food_product_model.findOne({ productName: req.params.productName }, (err, data) => {
        if (err) {
            res.json({ success: false, msg: err })
        } else if (data) {
            res.json({ success: true, msg: "Product Name already Exists" })
        } else {
            res.json({ success: false, msg: "Product Name not available" })
        }
    })
}

const updateRetailFoodProduct = (req, res) => {
    retail_food_product_model.findByIdAndUpdate(req.params.productid,
        {
            $set: {
                productName: req.body.productName,
                rootCategory: req.body.rootCategory,
                varient: req.body.varient,
                subVarient: req.body.subVarient,
                type: req.body.type,
                attributeSet: req.body.attributeSet,
                attributeSet_response: req.body.attributeSet_response,
                category: req.body.category,
                unit: req.body.unit,
                associatedBrand: req.body.associatedBrand,
                productImage: req.body.productImage,
                updatedBy: req.body.userid,
                updatedAt: Date.now()
            }
        }, (err, udata) => {
            if (err) {
                res.json({ success: false, msg: err })
            } else if (udata == null || udata.length == 0) {
                res.json({ success: false, msg: "Retail food product data not found" })
            } else {
                res.json({ success: true, msg: "Retail food Data updated" })
            }
        })
}

const deleteRetailfoodproduct = (req, res) => {
    retail_food_product_model.findByIdAndDelete(req.params.productid, (err, data) => {
        if (err) {
            res.json({ success: false, msg: err })
        } else {
            res.json({ success: true, msg: "Retail food Data Deleted" })
        }
    })
}

const softDeleteRetailfoodproduct = (req, res) => {
    retail_food_product_model.findByIdAndUpdate(req.params.productid, { $set: { status: req.body.status } }, (err, data) => {
        if (err) {
            res.json({ success: false, msg: err })
        } else {
            res.json({ success: true, msg: "Retail food Data soft Deleted" })
        }
    })
}
module.exports = {

    addRetailFoodProduct,
    getAllRetailfoodProduct,
    getRetailFoodProduct,
    checkretailFoodProductAlreadyExists,
    updateRetailFoodProduct,
    deleteRetailfoodproduct,
    softDeleteRetailfoodproduct
}
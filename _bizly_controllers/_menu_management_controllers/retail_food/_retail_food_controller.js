const retailfoodModel = require('../../../_bizly_models/_menu_management_models/retail_food_models/_retail_food_model').retailfoodModel
const retailfoodbrandModel = require('../../../_bizly_models/_menu_management_models/retail_food_models/_retail_food_brand_model').retail_food_brand
const retailfoodProductModel = require('../../../_bizly_models/_menu_management_models/retail_food_models/_retail_food_product_model').retail_food_product_model
const attributeSetModel = require('../../../_bizly_models/_menu_management_models/_liquor_models/_attribute_set_model').attributeSetModel

const addRootRetilFoodCategoryName = (req, res) => {
    retailfoodModel.findOne({ rootCategoryName: req.body.rootCategoryName }, (err, data) => {
        if (err) {
            res.json({ sucess: false, msg: err })
        } else if (data) {
            res.json({ sucess: false, msg: "root category name already exists" })
        } else {
            var rootcategory = new retailfoodModel()
            rootcategory.rootCategoryName = req.body.rootCategoryName
            rootcategory.addedBy = req.body.userid
            rootcategory.updatedBy = req.body.userid

            rootcategory.save((err) => {
                if (err) {
                    throw err
                } else {
                    res.json({ sucess: true, msg: 'rootcategory added' })
                }
            })
        }
    })
}

const addSubcategory = (req, res) => {
    retailfoodModel.findOne({ 'subCategories.subCategoryName': req.body.subCategoryName }, (err, data) => {
        if (err) {
            res.json({ sucess: false, msg: err })
        } else if (data) {
            res.json({ sucess: false, msg: "sub category name already exists" })
        } else {
            retailfoodModel.findByIdAndUpdate(req.params.rootcategoryid,
                { $push: { subCategories: req.body } },
                (err, usubcategory) => {
                    if (err) {
                        throw err
                    } else if (usubcategory == null) {
                        res.json({ sucess: false, msg: "rootcategory not found" })
                    } else {
                        res.json({ sucess: true, msg: "subcategory added" })
                    }
                })
        }
    })
}

const addSubSubCategories = (req, res) => {
    retailfoodModel.findOne({ 'subCategories.subSubCategories.subSubCategoryName': req.body.subSubCategoryName }, (err, data) => {
        if (err) {
            res.json({ sucess: false, msg: err })
        } else if (data) {
            res.json({ sucess: false, msg: "sub sub category name already exists" })
        } else {
            retailfoodModel.updateOne({ _id: req.params.rootcategoryid, "subCategories._id": req.params.subcategoryid },
                { $addToSet: { "subCategories.$.subSubCategories": { subSubCategoryName: req.body.subSubCategoryName } } },
                (err, usubsubcategory) => {
                    if (err) {
                        throw err
                    } else if (usubsubcategory == null) {
                        res.json({ sucess: false, msg: "rootcategory not found" })
                    } else {
                        res.json({ sucess: true, msg: "subsubcategory added" })
                    }
                })
        }
    })
}

const addSubSubCategoryType = (req, res) => {
    retailfoodModel.findOne({ "subCategories.subSubCategories.subSubCategoryType.subSubCategoryTypeName": req.body.subSubCategoryTypeName }, (err, data) => {
        if (err) {
            res.json({ sucess: false, msg: err })
        } else if (data) {
            res.json({ sucess: false, msg: "sub sub category type name already exists" })
        } else {
            retailfoodModel.updateOne(
                {
                    _id: req.params.rootcategoryid,
                    "subCategories.subSubCategories._id": req.params.subsubcategoryid
                },
                {
                    $push: {
                        "subCategories.$.subSubCategories.$[outer].subSubCategoryType": req.body

                    }
                },
                {
                    "arrayFilters": [
                        {
                            "outer._id": req.params.subsubcategoryid,
                        }
                    ]
                },
                (err, usubsubcategorytype) => {
                    if (err) {
                        throw err
                    } else if (usubsubcategorytype == null) {
                        res.json({ sucess: false, msg: "rootcategory not found" })
                    } else {
                        res.json({ sucess: true, msg: "subsubcategorytype added" })
                    }
                })
        }
    })
}

const getRootRetailFoodCategories = async (req, res) => {
    retailfoodModel.findOne({ _id: req.params.rootcategoryid }).
        populate('addedBy', 'userName').
        populate('updatedBy', 'userName').
        exec(function (err, rootcategory) {
            if (err) {
                throw err
            } else if (rootcategory == null) {
                res.json({ sucess: false, msg: 'rootcategory not found' })
            } else {
                res.json({ sucess: true, msg: 'rootcategory found', data: rootcategory })
            }

        })
}

const getAllRetailFoodCategories = (req, res) => {
    retailfoodModel.find().
        populate('addedBy', 'userName').
        populate('updatedBy', 'userName').
        exec(function (err, rootcategories) {
            if (err) {
                throw err
            } else if (rootcategories == null || rootcategories.length == 0) {
                res.json({ sucess: false, msg: "rootcategories Not Found" })
            } else {
                res.json({ sucess: true, msg: "rootcategories Found", data: rootcategories })
            }
        })
}

const updateRootRetilFoodCategoryName = (req, res) => {
    retailfoodModel.findOneAndUpdate({ _id: req.params.rootcategoryid },
        { $set: { rootCategoryName: req.body.rootCategoryName } },
        (err, data) => {
            if (err) {
                res.json({ success: false, msg: err })
            } else if (data == null || data.length == 0) {
                res.json({ success: false, msg: "Data not found" })
            } else {
                retailfoodbrandModel.find()
                    .then((RFBrandData) => {
                        promiseArr = []
                        for (let i = 0; i < RFBrandData.length; i++) {
                            if (RFBrandData[i].categoryType.length > 0) {
                                for (let j = 0; j < RFBrandData[i].categoryType.length; j++) {
                                    promiseArr.push(new Promise((resolve, reject) => {
                                        retailfoodbrandModel.findOneAndUpdate({
                                            _id: RFBrandData[i]._id,
                                            "categoryType._id": req.params.rootcategoryid
                                        }, {
                                                $set: {
                                                    "categoryType.$.rootCategoryName": req.body.rootCategoryName
                                                }
                                            }).then((RFBudata) => {
                                                resolve(RFBudata)
                                            }).catch((err) => {
                                                reject(err)
                                            })
                                    }))
                                }
                            } else {
                                continue
                            }
                        }
                        Promise.all(promiseArr).then((promiseArr) => {
                            retailfoodProductModel.find({ "rootCategory.id": req.params.rootcategoryid })
                                .then((RFProductData) => {
                                    promiseArr1 = []
                                    for (let i = 0; i < RFProductData.length; i++) {
                                        promiseArr1.push(new Promise((resolve, reject) => {
                                            if (RFProductData[i].rootCategory.id == req.params.rootcategoryid && RFProductData[i].category.id == req.params.rootcategoryid) {
                                                retailfoodProductModel.findOneAndUpdate({ _id: RFProductData[i]._id },
                                                    {
                                                        $set: {
                                                            "rootCategory.rootCategory": req.body.rootCategoryName,
                                                            "category.category": req.body.rootCategoryName,
                                                        }
                                                    }).then((RFPudata) => {
                                                        resolve(RFPudata)
                                                    }).catch((err) => {
                                                        reject(err)
                                                    })
                                            } else {
                                                retailfoodProductModel.findOneAndUpdate({
                                                    _id: RFProductData[i]._id
                                                },
                                                    {
                                                        $set: {
                                                            "rootCategory.rootCategory": req.body.rootCategoryName
                                                        }
                                                    }).then((RFPudata) => {
                                                        resolve(RFPudata)
                                                    }).catch((err) => {
                                                        reject(err)
                                                    })
                                            }
                                        }))
                                    }
                                    Promise.all(promiseArr1).then((promiseArr1) => {
                                        attributeSetModel.find({ attributeType: "retailfood" })
                                            .then((attributeData) => {
                                                promiseArr2 = []
                                                for (let i = 0; i < attributeData.length; i++) {
                                                    if (attributeData[i].categoryType.length > 0) {
                                                        for (let j = 0; j < attributeData[i].categoryType.length; j++) {
                                                            promiseArr2.push(new Promise((resolve, reject) => {
                                                                attributeSetModel.findOneAndUpdate({
                                                                    _id: attributeData[i]._id,
                                                                    "categoryType._id": req.params.rootcategoryid
                                                                }, {
                                                                        $set: {
                                                                            "categoryType.$.rootCategoryName": req.body.rootCategoryName
                                                                        }
                                                                    }).then((Audata) => {
                                                                        resolve(Audata)
                                                                    }).catch((err) => {
                                                                        reject(err)
                                                                    })
                                                            }))
                                                        }
                                                    } else {
                                                        continue
                                                    }
                                                }
                                                Promise.all(promiseArr2).then((promiseArr2) => {
                                                    res.json({ success: true, msg: "Root name Updated" })
                                                }).catch((err) => {
                                                    res.json({ success: false, msg: "error in Promise arr 2", error: err })
                                                })
                                            }).catch((err) => {
                                                res.json({ success: false, msg: "error in Attribute data", error: err })
                                            })
                                    }).catch((err) => {
                                        res.json({ success: false, msg: "error in Promise arr 1", error: err })
                                    })
                                }).catch((err) => {
                                    res.json({ success: false, msg: "error in retail food Product data", error: err })
                                })

                        }).catch((err) => {
                            res.json({ success: false, msg: "error in Promise arr", error: err })
                        })
                    }).catch((err) => {
                        res.json({ success: false, msg: "error in retail food brand data", error: err })
                    })
            }
        })
}

const updateSubcategoryName = (req, res) => {
    retailfoodModel.findOne({ _id: req.params.rootcategoryid })
        .then((result) => {
            let promiseArr = []
            for (let i = 0; i < result.subCategories.length; i++) {
                promiseArr.push(new Promise((resolve, reject) => {
                    retailfoodModel.findOneAndUpdate({
                        _id: req.params.rootcategoryid,
                        "subCategories._id": req.params.subcategoryid
                    }, {
                            $set: {
                                "subCategories.$.subCategoryName": req.body.subCategoryName
                            }
                        }).then((udata) => {
                            resolve(udata)
                        }).catch((err) => {
                            reject(err)
                        })
                }))

            }
            Promise.all(promiseArr).then((promiseArr) => {
                retailfoodbrandModel.find()
                    .then((RFBrandData) => {
                        promiseArr1 = []
                        for (let i = 0; i < RFBrandData.length; i++) {
                            if (RFBrandData[i].categoryType.length > 0) {
                                for (let j = 0; j < RFBrandData[i].categoryType.length; j++) {
                                    if (RFBrandData[i].categoryType[j].subCategories.length > 0) {
                                        for (let k = 0; k < RFBrandData[i].categoryType[j].subCategories.length; k++) {
                                            promiseArr1.push(new Promise((resolve, reject) => {
                                                retailfoodbrandModel.findOneAndUpdate({
                                                    _id: RFBrandData[i]._id,
                                                    "categoryType._id": req.params.rootcategoryid,
                                                    "categoryType.subCategories._id": req.params.subcategoryid

                                                }, {
                                                        $set: {
                                                            "categoryType.$.subCategories.$[outer].subCategoryName": req.body.subCategoryName
                                                        }
                                                    }, {
                                                        "arrayFilters": [
                                                            {
                                                                "outer._id": req.params.subcategoryid
                                                            }
                                                        ]
                                                    }).then((RFBudata) => {
                                                        resolve(RFBudata)
                                                    }).catch((err) => {
                                                        reject(err)
                                                    })
                                            }))
                                        }
                                    } else {
                                        continue
                                    }
                                }
                            } else {
                                continue
                            }
                        }
                        Promise.all(promiseArr1).then((promiseArr1) => {
                            retailfoodProductModel.find({ "varient.id": req.params.subcategoryid })
                                .then((RFProductData) => {
                                    promiseArr2 = []
                                    for (let i = 0; i < RFProductData.length; i++) {
                                        promiseArr2.push(new Promise((resolve, reject) => {
                                            if (RFProductData[i].varient.id == req.params.subcategoryid && RFProductData[i].category.id == req.params.subcategoryid) {
                                                retailfoodProductModel.findOneAndUpdate({ _id: RFProductData[i]._id },
                                                    {
                                                        $set: {
                                                            "varient.varient": req.body.subCategoryName,
                                                            "category.category": req.body.subCategoryName,
                                                        }
                                                    }).then((RFPudata) => {
                                                        resolve(RFPudata)
                                                    }).catch((err) => {
                                                        reject(err)
                                                    })
                                            } else {
                                                retailfoodProductModel.findOneAndUpdate({
                                                    _id: RFProductData[i]._id
                                                },
                                                    {
                                                        $set: {
                                                            "varient.varient": req.body.subCategoryName
                                                        }
                                                    }).then((RFPudata) => {
                                                        resolve(RFPudata)
                                                    }).catch((err) => {
                                                        reject(err)
                                                    })
                                            }
                                        }))
                                    }
                                    Promise.all(promiseArr2).then((promiseArr2) => {
                                        attributeSetModel.find({ attributeType: "retailfood" })
                                            .then((attributeData) => {
                                                promiseArr3 = []
                                                for (let i = 0; i < attributeData.length; i++) {
                                                    if (attributeData[i].categoryType.length > 0) {
                                                        for (let j = 0; j < attributeData[i].categoryType.length; j++) {
                                                            if (attributeData[i].categoryType[j].subCategories.length > 0) {
                                                                for (let k = 0; k < attributeData[i].categoryType[j].subCategories.length; k++) {
                                                                    promiseArr3.push(new Promise((resolve, reject) => {
                                                                        attributeSetModel.findOneAndUpdate({
                                                                            _id: attributeData[i]._id,
                                                                            "categoryType._id": req.params.rootcategoryid,
                                                                            "categoryType.subCategories._id": req.params.subcategoryid
                                                                        }, {
                                                                                $set: {
                                                                                    "categoryType.$.subCategories.$[outer].subCategoryName": req.body.subCategoryName
                                                                                }
                                                                            }, {
                                                                                "arrayFilters": [
                                                                                    {
                                                                                        "outer._id": req.params.subcategoryid
                                                                                    }
                                                                                ]
                                                                            }).then((Audata) => {
                                                                                resolve(Audata)
                                                                            }).catch((err) => {
                                                                                reject(err)
                                                                            })
                                                                    }))
                                                                }
                                                            } else {
                                                                continue
                                                            }
                                                        }
                                                    } else {
                                                        continue
                                                    }
                                                }
                                                Promise.all(promiseArr3).then((promiseArr3) => {
                                                    res.json({ success: true, msg: "Sub category name updated" })
                                                }).catch((err) => {
                                                    res.json({ success: false, msg: "error in promise arr 3", error: err })
                                                })
                                            }).catch((err) => {
                                                res.json({ success: false, msg: "error in Attribute data", error: err })
                                            })
                                    }).catch((err) => {
                                        res.json({ success: false, msg: "error in promise arr 2", error: err })
                                    })
                                }).catch((err) => {
                                    res.json({ success: false, msg: "error in retail food Product data", error: err })
                                })
                        }).catch((err) => {
                            res.json({ success: false, msg: "error in promise arr 1", error: err })
                        })
                    }).catch((err) => {
                        res.json({ success: false, msg: "error in retail food brand data", error: err })
                    })

            }).catch((err) => {
                res.json({ success: false, msg: "error in promise", error: err })
            })

        }).catch((err) => {
            res.json({ success: false, msg: "error in result", error: err })
        })
}

const updateSubSubCategoriesName = (req, res) => {
    retailfoodModel.findOne({ _id: req.params.rootcategoryid })
        .then((result) => {
            let promiseArr = []
            for (let i = 0; i < result.subCategories.length; i++) {
                if (result.subCategories[i].subSubCategories.length > 0) {
                    for (let j = 0; j < result.subCategories[i].subSubCategories.length; j++) {
                        promiseArr.push(new Promise((resolve, reject) => {
                            retailfoodModel.findOneAndUpdate({
                                _id: req.params.rootcategoryid,
                                "subCategories.subSubCategories._id": req.params.subsubcategoryid
                            }, {
                                    $set: {
                                        "subCategories.$.subSubCategories.$[outer].subSubCategoryName": req.body.subSubCategoryName
                                    }
                                },
                                {
                                    "arrayFilters": [
                                        {
                                            "outer._id": req.params.subsubcategoryid,
                                        }
                                    ]
                                }).then((udata) => {
                                    resolve(udata)
                                }).catch((err) => {
                                    reject(err)
                                })
                        }))
                    }
                } else {
                    continue;
                }
            }
            Promise.all(promiseArr).then((promiseArr) => {
                retailfoodbrandModel.find()
                    .then((RFBrandData) => {
                        promiseArr1 = []
                        for (let i = 0; i < RFBrandData.length; i++) {
                            if (RFBrandData[i].categoryType.length > 0) {
                                for (let j = 0; j < RFBrandData[i].categoryType.length; j++) {
                                    if (RFBrandData[i].categoryType[j].subCategories.length > 0) {
                                        for (let k = 0; k < RFBrandData[i].categoryType[j].subCategories.length; k++) {
                                            if (RFBrandData[i].categoryType[j].subCategories[k].subSubCategories.length > 0) {
                                                for (let l = 0; l < RFBrandData[i].categoryType[j].subCategories[k].subSubCategories.length; l++) {
                                                    promiseArr1.push(new Promise((resolve, reject) => {
                                                        retailfoodbrandModel.findOneAndUpdate({
                                                            _id: RFBrandData[i]._id,
                                                            "categoryType._id": req.params.rootcategoryid,
                                                            "categoryType.subCategories._id": req.params.subcategoryid,
                                                            "categoryType.subCategories.subSubCategories._id": req.params.subsubcategoryid

                                                        }, {
                                                                $set: {
                                                                    "categoryType.$.subCategories.$[outer].subSubCategories.$[inner].subSubCategoryName": req.body.subSubCategoryName
                                                                }
                                                            }, {
                                                                "arrayFilters": [
                                                                    {
                                                                        "outer._id": req.params.subcategoryid
                                                                    },
                                                                    {
                                                                        "inner._id": req.params.subsubcategoryid
                                                                    }
                                                                ]
                                                            }).then((RFBudata) => {
                                                                resolve(RFBudata)
                                                            }).catch((err) => {
                                                                reject(err)
                                                            })
                                                    }))
                                                }
                                            } else {
                                                continue
                                            }
                                        }
                                    } else {
                                        continue
                                    }
                                }
                            } else {
                                continue
                            }
                        }
                        Promise.all(promiseArr1).then((promiseArr1) => {
                            retailfoodProductModel.find({ "subVarient.id": req.params.subsubcategoryid })
                                .then((RFProductData) => {
                                    promiseArr2 = []
                                    for (let i = 0; i < RFProductData.length; i++) {
                                        promiseArr2.push(new Promise((resolve, reject) => {
                                            if (RFProductData[i].subVarient.id == req.params.subsubcategoryid && RFProductData[i].category.id == req.params.subsubcategoryid) {
                                                retailfoodProductModel.findOneAndUpdate({ _id: RFProductData[i]._id },
                                                    {
                                                        $set: {
                                                            "subVarient.subVarient": req.body.subSubCategoryName,
                                                            "category.category": req.body.subSubCategoryName,
                                                        }
                                                    }).then((RFPudata) => {
                                                        resolve(RFPudata)
                                                    }).catch((err) => {
                                                        reject(err)
                                                    })
                                            } else {
                                                retailfoodProductModel.findOneAndUpdate({
                                                    _id: RFProductData[i]._id
                                                },
                                                    {
                                                        $set: {
                                                            "subVarient.subVarient": req.body.subSubCategoryName
                                                        }
                                                    }).then((RFPudata) => {
                                                        resolve(RFPudata)
                                                    }).catch((err) => {
                                                        reject(err)
                                                    })
                                            }
                                        }))
                                    }
                                    Promise.all(promiseArr2).then((err) => {
                                        attributeSetModel.find({ attributeType: "retailfood" })
                                            .then((attributeData) => {
                                                promiseArr3 = []
                                                for (let i = 0; i < attributeData.length; i++) {
                                                    if (attributeData[i].categoryType.length > 0) {
                                                        for (let j = 0; j < attributeData[i].categoryType.length; j++) {
                                                            if (attributeData[i].categoryType[j].subCategories.length > 0) {
                                                                for (let k = 0; k < attributeData[i].categoryType[j].subCategories.length; k++) {
                                                                    if (attributeData[i].categoryType[j].subCategories[k].subSubCategories.length > 0) {
                                                                        for (let l = 0; l < attributeData[i].categoryType[j].subCategories[k].subSubCategories.length; l++) {
                                                                            promiseArr3.push(new Promise((resolve, reject) => {
                                                                                attributeSetModel.findOneAndUpdate({
                                                                                    _id: attributeData[i]._id,
                                                                                    "categoryType._id": req.params.rootcategoryid,
                                                                                    "categoryType.subCategories._id": req.params.subcategoryid,
                                                                                    "categoryType.subCategories.subSubCategories._id": req.params.subsubcategoryid
                                                                                }, {
                                                                                        $set: {
                                                                                            "categoryType.$.subCategories.$[outer].subSubCategories.$[inner].subSubCategoryName": req.body.subSubCategoryName
                                                                                        }
                                                                                    }, {
                                                                                        "arrayFilters": [
                                                                                            {
                                                                                                "outer._id": req.params.subcategoryid
                                                                                            },
                                                                                            {
                                                                                                "inner._id": req.params.subsubcategoryid
                                                                                            }
                                                                                        ]
                                                                                    }).then((Audata) => {
                                                                                        resolve(Audata)
                                                                                    }).catch((err) => {
                                                                                        reject(err)
                                                                                    })
                                                                            }))
                                                                        }
                                                                    } else {
                                                                        continue
                                                                    }
                                                                }
                                                            } else {
                                                                continue
                                                            }
                                                        }
                                                    } else {
                                                        continue
                                                    }
                                                }
                                                Promise.all(promiseArr3).then((promiseArr3) => {
                                                    res.json({ success: true, msg: "Sub sub category name updated" })
                                                }).catch((err) => {
                                                    res.json({ success: false, msg: "error in promise arr 3", error: err })
                                                })
                                            }).catch((err) => {
                                                res.json({ success: false, msg: "error in attribute data", error: err })
                                            })
                                    }).catch((err) => {
                                        res.json({ success: false, msg: "error in promise arr 2", error: err })
                                    })
                                }).catch((err) => {
                                    res.json({ success: false, msg: "error in retail food Product data", error: err })
                                })

                        }).catch((err) => {
                            res.json({ success: false, msg: "error in promise arr 1", error: err })
                        })
                    }).catch((err) => {
                        res.json({ success: false, msg: "error in retail food brand data", error: err })
                    })
            }).catch((err) => {
                res.json({ success: false, msg: "error in promise arr", error: err })
            })

        }).catch((err) => {
            res.json({ success: false, msg: "error in result", error: err })
        })
}

const updateSubSubCategoryTypeName = (req, res) => {
    retailfoodModel.findOne({ _id: req.params.rootcategoryid })
        .then((result) => {
            let promiseArr = []
            for (let i = 0; i < result.subCategories.length; i++) {
                if (result.subCategories[i].subSubCategories.length > 0) {
                    for (let j = 0; j < result.subCategories[i].subSubCategories.length; j++) {
                        if (result.subCategories[i].subSubCategories[j].subSubCategoryType.length > 0) {
                        } else {
                            continue;
                        }
                        promiseArr.push(new Promise((resolve, reject) => {
                            retailfoodModel.findOneAndUpdate({
                                _id: req.params.rootcategoryid,
                                "subCategories.subSubCategories._id": req.params.subsubcategoryid,
                                "subCategories.subSubCategories.subSubCategoryType._id": req.params.subsubcategorytypeid
                            }, {
                                    $set: {
                                        "subCategories.$.subSubCategories.$[outer].subSubCategoryType.$[inner].subSubCategoryTypeName": req.body.subSubCategoryTypeName
                                    }
                                },
                                {
                                    "arrayFilters": [
                                        {
                                            "outer._id": req.params.subsubcategoryid,
                                        },
                                        {
                                            "inner._id": req.params.subsubcategorytypeid
                                        }
                                    ]
                                }).then((udata) => {
                                    resolve(udata)
                                }).catch((err) => {
                                    reject(err)
                                })
                        }))
                    }
                } else {
                    continue;
                }
            }
            Promise.all(promiseArr).then((promiseArr) => {
                retailfoodbrandModel.find()
                    .then((RFBrandData) => {
                        promiseArr1 = []
                        for (let i = 0; i < RFBrandData.length; i++) {
                            if (RFBrandData[i].categoryType.length > 0) {
                                for (let j = 0; j < RFBrandData[i].categoryType.length; j++) {
                                    if (RFBrandData[i].categoryType[j].subCategories.length > 0) {
                                        for (let k = 0; k < RFBrandData[i].categoryType[j].subCategories.length; k++) {
                                            if (RFBrandData[i].categoryType[j].subCategories[k].subSubCategories.length > 0) {
                                                for (let l = 0; l < RFBrandData[i].categoryType[j].subCategories[k].subSubCategories.length; l++) {
                                                    if (RFBrandData[i].categoryType[j].subCategories[k].subSubCategories[l].subSubCategoryType.length > 0) {
                                                        for (let m = 0; m < RFBrandData[i].categoryType[j].subCategories[k].subSubCategories[l].subSubCategoryType.length; m++) {
                                                            promiseArr1.push(new Promise((resolve, reject) => {
                                                                retailfoodbrandModel.findOneAndUpdate({
                                                                    _id: RFBrandData[i]._id,
                                                                    "categoryType._id": req.params.rootcategoryid,
                                                                    "categoryType.subCategories._id": req.params.subcategoryid,
                                                                    "categoryType.subCategories.subSubCategories._id": req.params.subsubcategoryid,
                                                                    "categoryType.subCategories.subSubCategories.subSubCategoryType._id": req.params.subsubcategorytypeid

                                                                }, {
                                                                        $set: {
                                                                            "categoryType.$.subCategories.$[outer].subSubCategories.$[inner].subSubCategoryType.$[middle].subSubCategoryTypeName": req.body.subSubCategoryTypeName
                                                                        }
                                                                    }, {
                                                                        "arrayFilters": [
                                                                            {
                                                                                "outer._id": req.params.subcategoryid
                                                                            },
                                                                            {
                                                                                "inner._id": req.params.subsubcategoryid
                                                                            },
                                                                            {
                                                                                "middle._id": req.params.subsubcategorytypeid
                                                                            }
                                                                        ]
                                                                    }).then((RFBudata) => {
                                                                        resolve(RFBudata)
                                                                    }).catch((err) => {
                                                                        reject(err)
                                                                    })
                                                            }))
                                                        }
                                                    } else {
                                                        continue
                                                    }
                                                }
                                            } else {
                                                continue
                                            }
                                        }
                                    } else {
                                        continue
                                    }
                                }
                            } else {
                                continue
                            }
                        }
                        Promise.all(promiseArr1).then((promiseArr1) => {
                            retailfoodProductModel.find({ "type.id": req.params.subsubcategorytypeid })
                                .then((RFProductData) => {
                                    promiseArr2 = []
                                    for (let i = 0; i < RFProductData.length; i++) {
                                        promiseArr2.push(new Promise((resolve, reject) => {
                                            if (RFProductData[i].type.id == req.params.subsubcategorytypeid && RFProductData[i].category.id == req.params.subsubcategorytypeid) {
                                                retailfoodProductModel.findOneAndUpdate({ _id: RFProductData[i]._id },
                                                    {
                                                        $set: {
                                                            "type.type": req.body.subSubCategoryTypeName,
                                                            "category.category": req.body.subSubCategoryTypeName,
                                                        }
                                                    }).then((RFPudata) => {
                                                        resolve(RFPudata)
                                                    }).catch((err) => {
                                                        reject(err)
                                                    })
                                            } else {
                                                retailfoodProductModel.findOneAndUpdate({
                                                    _id: RFProductData[i]._id
                                                },
                                                    {
                                                        $set: {
                                                            "type.type": req.body.subSubCategoryTypeName
                                                        }
                                                    }).then((RFPudata) => {
                                                        resolve(RFPudata)
                                                    }).catch((err) => {
                                                        reject(err)
                                                    })
                                            }
                                        }))
                                    }
                                    Promise.all(promiseArr2).then((promiseArr2) => {
                                        attributeSetModel.find({ attributeType: "retailfood" })
                                            .then((attributeData) => {
                                                promiseArr3 = []
                                                for (let i = 0; i < attributeData.length; i++) {
                                                    if (attributeData[i].categoryType.length > 0) {
                                                        for (let j = 0; j < attributeData[i].categoryType.length; j++) {
                                                            if (attributeData[i].categoryType[j].subCategories.length > 0) {
                                                                for (let k = 0; k < attributeData[i].categoryType[j].subCategories.length; k++) {
                                                                    if (attributeData[i].categoryType[j].subCategories[k].subSubCategories.length > 0) {
                                                                        for (let l = 0; l < attributeData[i].categoryType[j].subCategories[k].subSubCategories.length; l++) {
                                                                            if (attributeData[i].categoryType[j].subCategories[k].subSubCategories[l].subSubCategoryType.length > 0) {
                                                                                for (let m = 0; m < attributeData[i].categoryType[j].subCategories[k].subSubCategories[l].subSubCategoryType.length; m++) {
                                                                                    promiseArr3.push(new Promise((resolve, reject) => {
                                                                                        attributeSetModel.findOneAndUpdate({
                                                                                            _id: attributeData[i]._id,
                                                                                            "categoryType._id": req.params.rootcategoryid,
                                                                                            "categoryType.subCategories._id": req.params.subcategoryid,
                                                                                            "categoryType.subCategories.subSubCategories._id": req.params.subsubcategoryid,
                                                                                            "categoryType.subCategories.subSubCategories.subSubCategoryType._id": req.params.subsubcategorytypeid
                                                                                        }, {
                                                                                                $set: {
                                                                                                    "categoryType.$.subCategories.$[outer].subSubCategories.$[inner].subSubCategoryType.$[middle].subSubCategoryTypeName": req.body.subSubCategoryTypeName
                                                                                                }
                                                                                            }, {
                                                                                                "arrayFilters": [
                                                                                                    {
                                                                                                        "outer._id": req.params.subcategoryid
                                                                                                    },
                                                                                                    {
                                                                                                        "inner._id": req.params.subsubcategoryid
                                                                                                    },
                                                                                                    {
                                                                                                        "middle._id": req.params.subsubcategorytypeid
                                                                                                    }
                                                                                                ]
                                                                                            }).then((Audata) => {
                                                                                                resolve(Audata)
                                                                                            }).catch((err) => {
                                                                                                reject(err)
                                                                                            })
                                                                                    }))
                                                                                }
                                                                            } else {
                                                                                continue
                                                                            }
                                                                        }
                                                                    } else {
                                                                        continue
                                                                    }
                                                                }
                                                            } else {
                                                                continue
                                                            }
                                                        }
                                                    } else {
                                                        continue
                                                    }
                                                }
                                                Promise.all(promiseArr3).then((promiseArr3) => {
                                                    res.json({ success: true, msg: "Sub sub category type name updated" })
                                                }).catch((err) => {
                                                    res.json({ success: false, msg: "error in promise arr 3", error: err })
                                                })
                                            }).catch((err) => {
                                                res.json({ success: false, msg: "error in attribute data", error: err })
                                            })
                                    }).catch((err) => {
                                        res.json({ success: false, msg: "error in promise arr 2", error: err })
                                    })
                                }).catch((err) => {
                                    res.json({ success: false, msg: "error in retail food product data", error: err })
                                })
                        }).catch((err) => {
                            res.json({ success: false, msg: "error in promise arr 1", error: err })
                        })
                    }).catch((err) => {
                        res.json({ success: false, msg: "error in retail food brand data", error: err })
                    })
            }).catch((err) => {
                res.json({ success: false, msg: "error in promise", error: err })
            })

        }).catch((err) => {
            res.json({ success: false, msg: "error in result", error: err })
        })
}

module.exports = {
    addRootRetilFoodCategoryName,
    addSubcategory,
    addSubSubCategories,
    addSubSubCategoryType,
    getRootRetailFoodCategories,
    getAllRetailFoodCategories,
    updateRootRetilFoodCategoryName,
    updateSubcategoryName,
    updateSubSubCategoriesName,
    updateSubSubCategoryTypeName
}
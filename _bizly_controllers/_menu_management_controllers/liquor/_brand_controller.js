const brandModel = require('../../../_bizly_models/_menu_management_models/_liquor_models/brand-model').brandmodel

const addBrand = (req, res) => {
    brandModel.findOne({ brandName: req.body.brandName }, (err, data) => {
        if (err) {
            res.json({ sucess: false, msg: err })
        } else if (data) {
            res.json({ sucess: false, msg: "Brand already exists" })
        } else {
            var brand = new brandModel()
            brand.brandName = req.body.brandName
            brand.country = req.body.country
            brand.categoryType = req.body.categoryType
            brand.addedBy = req.body.userid
            brand.updatedBy = req.body.userid

            brand.save((err) => {
                if (err) {
                    throw err
                } else {
                    res.json({ sucess: true, msg: 'brand added' })
                }
            })
        }
    })
}

const getBrand = async (req, res) => {
    brandModel.findOne({ _id: req.params.brandid }).
        populate('addedBy', 'userName').
        populate('updatedBy', 'userName').
        sort('-updatedAt').
        exec(function (err, brand) {
            if (err) {
                throw err
            } else if (brand == null) {
                res.json({ sucess: false, msg: 'brand not found' })
            } else {
                res.json({ sucess: true, msg: 'brand found', data: brand })
            }

        })
}

const getAllBrands = (req, res) => {
    brandModel.find().
        populate('addedBy', 'userName').
        populate('updatedBy', 'userName').
        sort('-updatedAt').
        exec(function (err, brands) {
            if (err) {
                throw err
            } else if (brands == null || brands.length == 0) {
                res.json({ sucess: false, msg: "brands Not Found" })
            } else {
                res.json({ sucess: true, msg: "brands Found", data: brands })
            }
        })
}

const getBrandNameFromCategories = (req, res) => {
    brandModel.find({}, (err, brandData) => {
        var arr1 = []
        var arr2 = []
        var arr3 = []
        var arr4 = []
        for (let i = 0; i < brandData.length; i++) {
            for (let j = 0; j < brandData[i].categoryType.length; j++) {
                if (brandData[i].categoryType[j]._id == req.body.id &&
                    brandData[i].categoryType[j].rootDrinkType == req.body.string &&
                    brandData[i].categoryType[j].showChildren == true) {
                    var promise1 = new Promise((resolve, reject) => {
                        arr1.push(brandData[i])
                        resolve(arr1)
                    })

                } else if (brandData[i].categoryType[j].liquorVarients.length > 0) {
                    for (let k = 0; k < brandData[i].categoryType[j].liquorVarients.length; k++) {
                        if (brandData[i].categoryType[j].liquorVarients[k]._id == req.body.id &&
                            brandData[i].categoryType[j].liquorVarients[k].liquorVarientName == req.body.string &&
                            brandData[i].categoryType[j].liquorVarients[k].showChildren == true) {

                            var promise2 = new Promise((resolve, reject) => {
                                arr2.push(brandData[i])
                                resolve(arr2)
                            })
                        } else if (brandData[i].categoryType[j].liquorVarients[k].liquorSubVarients.length > 0) {
                            for (let l = 0; l < brandData[i].categoryType[j].liquorVarients[k].liquorSubVarients.length; l++) {
                                if (brandData[i].categoryType[j].liquorVarients[k].liquorSubVarients[l]._id == req.body.id &&
                                    brandData[i].categoryType[j].liquorVarients[k].liquorSubVarients[l].liquorSubVarientName == req.body.string &&
                                    brandData[i].categoryType[j].liquorVarients[k].liquorSubVarients[l].showChildren == true) {

                                    var promise3 = new Promise((resolve, reject) => {
                                        arr3.push(brandData[i])
                                        resolve(arr3)
                                    })
                                } else if (brandData[i].categoryType[j].liquorVarients[k].liquorSubVarients[l].liquorSubSubVarientType.length > 0) {
                                    for (let m = 0; m < brandData[i].categoryType[j].liquorVarients[k].liquorSubVarients[l].liquorSubSubVarientType.length; m++) {
                                        if (brandData[i].categoryType[j].liquorVarients[k].liquorSubVarients[l].liquorSubSubVarientType[m]._id == req.body.id &&
                                            brandData[i].categoryType[j].liquorVarients[k].liquorSubVarients[l].liquorSubSubVarientType[m].liquorSubSubVarientTypeName == req.body.string &&
                                            brandData[i].categoryType[j].liquorVarients[k].liquorSubVarients[l].liquorSubSubVarientType[m].showChildren == true) {

                                            var promise4 = new Promise((resolve, reject) => {
                                                arr4.push(brandData[i])
                                                resolve(arr4)
                                            })
                                        } else {
                                            // res.json({success:false , msg:req.body.string + "not found"})
                                        }
                                    }
                                } else {
                                    console.log("liquor sub sub varient type length =0")
                                }
                            }
                        } else {
                            console.log("liquor sub varient length =0")
                        }
                    }

                } else {
                    console.log("liquor varient length = 0")
                }
            }
        }
        Promise.all([promise1, promise2, promise3, promise4]).then((finalData) => {
            finalData.filter(data => {
                if (data == null || data == undefined) {
                    console.log("negative")
                } else {
                    res.json({ success: true, data: data })
                }
            })
        }).catch((err) => {
            res.json({ success: false, msg: "Error in final data" })
        })
    })
}

// check Liquor Brand Already Exists
const checkBrandAlreadyExists = (req, res) => {
    brandModel.findOne({ brandName: req.params.brandName }, (err, data) => {
        if (err) {
            res.json({ success: false, msg: err })
        } else if (data) {
            res.json({ success: true, msg: "Liquor Brand Name already Exists" })
        } else {
            res.json({ success: false, msg: "Liquor Brand Name not available" })
        }
    })
}

const updateBrand = (req, res) => {
    brandModel.findByIdAndUpdate(req.params.brandid,
        {
            $set: {
                brandName: req.body.brandName,
                country: req.body.country,
                categoryType: req.body.categoryType,
                updatedBy: req.body.userid,
                updatedAt: Date.now()
            }
        }, (err, ubrand) => {
            if (err) {
                throw err
            } else if (ubrand == null) {
                res.json({ sucess: false, msg: "brand not found" })
            } else {
                res.json({ sucess: true, msg: "brand updated" })
            }
        })
}

const deleteBrand = (req, res) => {
    brandModel.findByIdAndDelete(req.params.brandid, (err) => {
        if (err) {
            res.json({ sucess: false, msg: "brand not found" })
        } else {
            res.json({ sucess: true, msg: "brand Deleted" })
        }
    })
}

const softDeleteBrand = (req, res) => {
    brandModel.findByIdAndUpdate(req.params.brandid, { $set: { status: req.body.status } }, (err) => {
        if (err) {
            res.json({ sucess: false, msg: "brand not found" })
        } else {
            res.json({ sucess: true, msg: "brand soft Deleted" })
        }
    })
}

module.exports = {
    addBrand,
    getBrand,
    getAllBrands,
    checkBrandAlreadyExists,
    updateBrand,
    deleteBrand,
    getBrandNameFromCategories,
    softDeleteBrand
}
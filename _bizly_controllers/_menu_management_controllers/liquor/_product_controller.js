const productModel = require('../../../_bizly_models/_menu_management_models/_liquor_models/_product_model').productModel

const addproduct = (req, res) => {
    productModel.findOne({ productName: req.body.productName }, (err, data) => {
        if (err) {
            res.json({ success: false, msg: err })
        } else if (data) {
            res.json({ success: false, msg: "Product already exists" })
        } else {
            var product = new productModel()
            product.productName = req.body.productName,
                product.brand = req.body.brand,
                product.attribute = req.body.attribute,
                product.varient = req.body.varient,
                product.attribute_set = req.body.attribute_set,
                product.attributeSet_response = req.body.attributeSet_response,
                product.root = req.body.root,
                product.subVarient = req.body.subVarient,
                product.subsubVarient = req.body.subsubVarient,
                product.varientType = req.body.varientType,
                product.productImages = req.body.productImages,
                product.addedBy = req.body.userid,
                product.updatedBy = req.body.userid

            product.save((err) => {
                if (err) {
                    res.json({ success: false, msg: err })
                } else {
                    res.json({ success: true, msg: "Product Added" })
                }
            })
        }
    })
}

const getAllProducts = (req, res) => {
    productModel.find({}).
        populate('brand', 'brandName').
        populate('attribute', 'attributeSetName').
        populate('addedBy', 'userName').
        populate('updatedBy', 'userName').
        sort('-updatedAt').
        exec((err, products) => {
            if (err) {
                res.json({ success: false, msg: err })
            } else if (products == null || products.length == 0) {
                res.json({ success: false, msg: "Product Not Found" })
            } else {
                res.json({ success: true, msg: "All Products Found", data: products })
            }
        })
}

const getProduct = (req, res) => {
    productModel.findById(req.params.productid).
        populate('brand', 'brandName').
        populate('attribute', 'attributeSetName').
        populate('addedBy', 'userName').
        populate('updatedBy', 'userName').
        sort('-updatedAt').
        exec((err, products) => {
            if (err) {
                res.json({ success: false, msg: err })
            } else if (products == null || products.length == 0) {
                res.json({ success: false, msg: "Product Not Found" })
            } else {
                res.json({ success: true, msg: "All Products Found", data: products })
            }
        })
}

// check Liquor Product Already Exists
const checkProductAlreadyExists = (req, res) => {
    productModel.findOne({ productName: req.params.productName }, (err, data) => {
        if (err) {
            res.json({ success: false, msg: err })
        } else if (data) {
            res.json({ success: true, msg: "Liquor Product Name already Exists" })
        } else {
            res.json({ success: false, msg: "Liquor Product Name not available" })
        }
    })
}

const updateProducts = (req, res) => {
    productModel.findByIdAndUpdate(req.params.productid,
        {
            $set: {
                productName: req.body.productName,
                brand: req.body.brand,
                attribute: req.body.attribute,
                productImages: req.body.productImages,
                attribute_set: req.body.attribute_set,
                attributeSet_response: req.body.attributeSet_response,
                root: req.body.root,
                subVarient: req.body.subVarient,
                subsubVarient: req.body.subsubVarient,
                varientType: req.body.varientType,
                updatedBy: req.body.userid,
                updatedAt: Date.now()
            }
        }, (err, uproducts) => {
            if (err) {
                res.json({ success: false, msg: err })
            } else if (uproducts == null || uproducts.length == 0) {
                res.json({ success: false, msg: "Product Not Found" })
            } else {
                res.json({ success: true, msg: "Product Updated" })
            }
        })
}

const deleteProduct = (req, res) => {
    productModel.findByIdAndDelete(req.params.productid, (err) => {
        if (err) {
            res.json({ success: false, msg: err })
        } else {
            res.json({ success: true, msg: "Product Deleted" })
        }
    })
}

const softDeleteProduct = (req, res) => {
    productModel.findByIdAndUpdate(req.params.productid, { $set: { status: req.body.status } }, (err) => {
        if (err) {
            res.json({ success: false, msg: err })
        } else {
            res.json({ success: true, msg: "Product soft Deleted" })
        }
    })
}

module.exports = {
    addproduct,
    getAllProducts,
    getProduct,
    checkProductAlreadyExists,
    updateProducts,
    deleteProduct,
    softDeleteProduct
}
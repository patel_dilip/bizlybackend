const attributeModel = require('../../../_bizly_models/_menu_management_models/_liquor_models/_attribute-model').attributeModel
const attributeGroupModel = require('../../../_bizly_models/_menu_management_models/_liquor_models/_group_model').groupModel

const addAttribute = (req, res) => {
    attributeModel.findOne({ attributeName: req.body.attributeName,attributeType:req.body.attributeType }, (err, data) => {
        if (err) {
            res.json({ sucess: false, msg: err })
        } else if (data) {
            res.json({ sucess: false, msg: "Attribute already exists" })
        } else {
            var attribute = new attributeModel()
            attribute.attributeName = req.body.attributeName
            attribute.display_name = req.body.display_name
            attribute.responseType = req.body.responseType
            attribute.attributeType = req.body.attributeType
            attribute.isSearchable = req.body.isSearchable
            attribute.isFilterable = req.body.isFilterable
            attribute.addedBy = req.body.userid
            attribute.updatedBy = req.body.userid

            attribute.save((err) => {
                if (err) {
                    throw err
                } else {
                    res.json({ sucess: true, msg: 'Attribute added' })
                }
            })
        }
    })
}

const getAttribute = async (req, res) => {
    attributeModel.findOne({ _id: req.params.attributeid }).
        populate('addedBy', 'userName').
        populate('updatedBy', 'userName').
        sort('-updatedAt').
        exec(function (err, attribute) {
            if (err) {
                throw err
            } else if (attribute == null) {
                res.json({ sucess: false, msg: 'Attribute not found' })
            } else {
                res.json({ sucess: true, msg: 'Attribute found', data: attribute })
            }

        })
}

const getAllAttribute = (req, res) => {
    attributeModel.find().
        populate('addedBy', 'userName').
        populate('updatedBy', 'userName').
        sort('-updatedAt').
        exec(function (err, attribute) {
            if (err) {
                throw err
            } else if (attribute == null || attribute.length == 0) {
                res.json({ sucess: false, msg: "Attribute Not Found" })
            } else {
                res.json({ sucess: true, msg: "Attribute Found", data: attribute })
            }
        })
}

// check Attribute Already Exists
const checkAttributeAlreadyExists = (req, res) => {
    attributeModel.findOne({ attributeName: req.params.attributeName, attributeType: req.params.attributeType }, (err, data) => {
        if (err) {
            res.json({ success: false, msg: err })
        } else if (data) {
            res.json({ success: true, msg: "Attribute Name already Exists" })
        } else {
            res.json({ success: false, msg: "Attribute Name not available" })
        }
    })
}

const updateAttribute = (req, res) => {
    attributeModel.findByIdAndUpdate(req.params.attributeid,
        {
            $set: {
                attributeName: req.body.attributeName,
                responseType: req.body.responseType,
                attributeType: req.body.attributeType,
                updatedBy: req.body.userid,
                updatedAt: Date.now()
            }
        }, (err, Uattribute) => {
            if (err) {
                throw err
            } else if (Uattribute == null) {
                res.json({ sucess: false, msg: "Attribute not found" })
            } else {
                res.json({ sucess: true, msg: "Attribute updated" })
            }
        })
}

const deleteAttribute = (req, res) => {
    attributeModel.findByIdAndDelete(req.params.attributeid, (err) => {
        if (err) {
            res.json({ sucess: false, msg: err })
        } else {
            res.json({ sucess: true, msg: "Attribute deleted" })
        }
    })
}

const isSearchableToggle = (req, res) => {
    attributeModel.findByIdAndUpdate(req.params.attributeid, { $set: { isSearchable: req.body.isSearchable } }, (err, udata) => {
        if (err) {
            res.json({ sucess: false, msg: err })
        } else {
            res.json({ sucess: true, msg: 'updated' })
        }
    })
}

const isFilterableToggle = (req, res) => {
    attributeModel.findByIdAndUpdate(req.params.attributeid, { $set: { isFilterable: req.body.isFilterable } }, (err, udata) => {
        if (err) {
            res.json({ sucess: false, msg: err })
        } else {
            res.json({ sucess: true, msg: 'updated' })
        }
    })
}

const softDeleteAttribute = (req, res) => {
    attributeModel.findByIdAndUpdate(req.params.attributeid, { $set: { status: req.body.status } }, (err) => {
        if (err) {
            res.json({ sucess: false, msg: "Attribute not found" })
        } else {
            res.json({ sucess: true, msg: "Attribute Deleted" })
        }
    })
}

module.exports = {
    addAttribute,
    getAttribute,
    getAllAttribute,
    checkAttributeAlreadyExists,
    updateAttribute,
    deleteAttribute,
    isSearchableToggle,
    isFilterableToggle,
    softDeleteAttribute
} 
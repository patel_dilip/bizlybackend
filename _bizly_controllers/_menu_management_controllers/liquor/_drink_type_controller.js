const drinkTypeModel = require('../../../_bizly_models/_menu_management_models/_liquor_models/drink-type-model').rootdrinktype
const attributesetModel = require('../../../_bizly_models/_menu_management_models/_liquor_models/_attribute_set_model').attributeSetModel
const liquorbrandModel = require('../../../_bizly_models/_menu_management_models/_liquor_models/brand-model').brandmodel
const liquorproductModel = require('../../../_bizly_models/_menu_management_models/_liquor_models/_product_model').productModel

const addDrinkType = (req, res) => {
    drinkTypeModel.findOne({ rootDrinkType: req.body.rootDrinkType }, (err, data) => {
        if (err) {
            res.json({ sucess: false, msg: err })
        } else if (data) {
            res.json({ sucess: false, msg: "root drink type name already exists" })
        } else {
            var drinktype = new drinkTypeModel()
            drinktype.rootDrinkType = req.body.rootDrinkType
            drinktype.addedBy = req.body.userid
            drinktype.updatedBy = req.body.userid

            drinktype.save((err) => {
                if (err) {
                    throw err
                } else {
                    res.json({ sucess: true, msg: 'drinktype added' })
                }
            })
        }
    })
}


const addLiquorVarient = (req, res) => {
    drinkTypeModel.findOne({ 'liquorVarients.liquorVarientName': req.body.liquorVarientName }, (err, data) => {
        if (err) {
            res.json({ sucess: false, err: err })
        } else if (data) {
            res.json({ sucess: false, msg: "Liquor Varient Name already exists" })
        } else {
            drinkTypeModel.findByIdAndUpdate(req.params.drinktypeid,
                { $push: { liquorVarients: req.body } },
                (err, udrinktype) => {
                    if (err) {
                        throw err
                    } else if (udrinktype == null) {
                        res.json({ sucess: false, msg: "drinktype not found" })
                    } else {
                        res.json({ sucess: true, msg: "drinktype updated" })
                    }
                })
        }
    })
}



const addLiquorSubVarient = (req, res) => {
    drinkTypeModel.findOne({ 'liquorVarients.liquorSubVarients.liquorSubVarientName': req.body.liquorSubVarientName }, (err, data) => {
        if (err) {
            res.json({ sucess: false, msg: err })
        } else if (data) {
            res.json({ sucess: false, msg: "Liquor sub varient name already exists" })
        } else {
            drinkTypeModel.updateOne({ _id: req.params.drinktypeid, "liquorVarients._id": req.params.liquorvarientid },
                { $addToSet: { "liquorVarients.$.liquorSubVarients": { liquorSubVarientName: req.body.liquorSubVarientName } } },
                (err, udrinktype) => {
                    if (err) {
                        throw err
                    } else if (udrinktype == null) {
                        res.json({ sucess: false, msg: "drinktype not found" })
                    } else {
                        res.json({ sucess: true, msg: "drinktype updated" })
                    }
                })
        }
    })
}

const addLiquorSubSubVarient = (req, res) => {

    drinkTypeModel.findOne({ 'liquorVarients.liquorSubVarients.liquorSubSubVarientType.liquorSubSubVarientTypeName': req.body.liquorSubSubVarientTypeName }, (err, data) => {
        if (err) {
            res.json({ sucess: false, msg: err })
        } else if (data) {
            res.json({ sucess: false, msg: "Liquor sub sub varient name already exists" })
        } else {

            drinkTypeModel.updateOne(
                {
                    _id: req.params.drinktypeid,
                    "liquorVarients.liquorSubVarients._id": req.params.liquorsubvarientid
                },
                {
                    $push: {
                        "liquorVarients.$.liquorSubVarients.$[outer].liquorSubSubVarientType": req.body

                    }
                },
                {
                    "arrayFilters": [
                        {
                            "outer._id": req.params.liquorsubvarientid,
                        }
                    ]
                },
                (err, udrinktype) => {
                    if (err) {
                        throw err
                    } else if (udrinktype == null) {
                        res.json({ sucess: false, msg: "drinktype not found" })
                    } else {
                        res.json({ sucess: true, msg: "drinktype updated" })
                    }
                })
        }
    })
}

//optional
const getLiquorVarient = (req, res) => {
    drinkTypeModel.findById(req.params.drinktypeid, 'liquorVarients.liquorVarientName').
        exec((err, drinktype) => {
            if (err) {
                throw err
            } else if (drinktype == null) {
                res.json({ sucess: false, msg: "drinktype not found" })
            } else {
                res.json({ sucess: true, msg: "drinktype found", data: drinktype })
            }
        })
}

const getDrinkType = async (req, res) => {
    drinkTypeModel.findOne({ _id: req.params.drinktypeid }).
        exec(function (err, drinktype) {
            if (err) {
                throw err
            } else if (drinktype == null) {
                res.json({ sucess: false, msg: 'drinktype not found' })
            } else {
                res.json({ sucess: true, msg: 'drinktype found', data: drinktype })
            }

        })
}

const getAllDrinkTypes = (req, res) => {
    drinkTypeModel.find().
        exec(function (err, drinktypes) {
            if (err) {
                throw err
            } else if (drinktypes == null || drinktypes.length == 0) {
                res.json({ sucess: false, msg: "drinktypes Not Found" })
            } else {
                res.json({ sucess: true, msg: "drinktypes Found", data: drinktypes })
            }
        })
}

const deleteDrinkType = (req, res) => {
    drinkTypeModel.findByIdAndDelete(req.params.drinktypeid, (err) => {
        if (err) {
            throw err
        } else {
            res.json({ sucess: true, msg: "drinktype Deleted" })
        }
    })
}

const updateDrinkTypeName = (req, res) => {
    drinkTypeModel.findOneAndUpdate({ _id: req.params.rootdrinktypeid },
        { $set: { rootDrinkType: req.body.rootDrinkType } },
        (err, data) => {
            if (err) {
                res.json({ success: false, msg: err })
            } else if (data == null || data.length == 0) {
                res.json({ success: false, msg: "Data not found" })
            } else {
                attributesetModel.find({ attributeType: "liquor" })
                    .then((attributeData) => {
                        promiseArr = []
                        for (let i = 0; i < attributeData.length; i++) {
                            var id = attributeData[i]._id
                            if (attributeData[i].categoryType.length > 0) {
                                for (let j = 0; j < attributeData[i].categoryType.length; j++) {
                                    promiseArr.push(new Promise((resolve, reject) => {
                                        attributesetModel.findOneAndUpdate({ _id: id, "categoryType._id": req.params.rootdrinktypeid },
                                            {
                                                $set: {
                                                    "categoryType.$.rootDrinkType": req.body.rootDrinkType
                                                }
                                            }).then((udata) => {
                                                resolve(udata)
                                            }).catch((err) => {
                                                reject(err)
                                            })
                                    }))
                                }

                            } else {
                                continue;
                            }

                        }
                        Promise.all(promiseArr).then((promiseData) => {
                            liquorbrandModel.find({})
                                .then((brandData) => {
                                    promiseArr2 = []
                                    for (let k = 0; k < brandData.length; k++) {
                                        var id = brandData[k]._id
                                        if (brandData[k].categoryType.length > 0) {
                                            for (let l = 0; l < brandData[k].categoryType.length; l++) {
                                                promiseArr2.push(new Promise((resolve, reject) => {
                                                    liquorbrandModel.findOneAndUpdate({ _id: id, "categoryType._id": req.params.rootdrinktypeid },
                                                        {
                                                            $set: {
                                                                "categoryType.$.rootDrinkType": req.body.rootDrinkType
                                                            }
                                                        }).then((Budata) => {
                                                            resolve(Budata)
                                                        }).catch((err) => {
                                                            reject(err)
                                                        })
                                                }))
                                            }

                                        } else {
                                            continue
                                        }
                                    }
                                    Promise.all(promiseArr2).then((promiseArr2) => {
                                        liquorproductModel.find({ "root.id": req.params.rootdrinktypeid })
                                            .then((productData) => {
                                                promiseArr3 = []
                                                if (productData.length > 0) {
                                                    for (let m = 0; m < productData.length; m++) {
                                                        var id = productData[m]._id

                                                        promiseArr3.push(new Promise((resolve, reject) => {
                                                            if (productData[m].root.id == req.params.rootdrinktypeid && productData[m].varient.id == req.params.rootdrinktypeid) {
                                                                liquorproductModel.findOneAndUpdate({ _id: id },
                                                                    {
                                                                        $set: {
                                                                            "varient.varient": req.body.rootDrinkType,
                                                                            "root.root": req.body.rootDrinkType
                                                                        }
                                                                    }).then((Pudata) => {
                                                                        resolve(Pudata)
                                                                    }).catch((err) => {
                                                                        reject(err)
                                                                    })
                                                            } else {
                                                                liquorproductModel.findOneAndUpdate({ _id: id },
                                                                    {
                                                                        $set: {
                                                                            "root.root": req.body.rootDrinkType
                                                                        }
                                                                    }).then((Pudata) => {
                                                                        resolve(Pudata)
                                                                    }).catch((err) => {
                                                                        reject(err)
                                                                    })
                                                            }

                                                        }))
                                                    }
                                                    Promise.all(promiseArr3).then((promiseArr3) => {
                                                        res.json({ success: true, msg: "root drink type updated" })
                                                    }).catch((err) => {
                                                        res.json({ success: false, msg: "err in promise 3", error: err })
                                                    })
                                                } else {
                                                    res.json({ success: true, msg: "root drink type updated" })
                                                }
                                            }).catch((err) => {
                                                res.json({ success: false, msg: "err in product data", error: err })
                                            })
                                    }).catch((err) => {
                                        res.json({ success: false, msg: "err in promise 2", error: err })
                                    })
                                }).catch((err) => {
                                    res.json({ success: false, msg: "err in brand data", error: err })
                                })
                        }).catch((err) => {
                            res.json({ success: false, msg: "err in promsie 1 data", error: err })
                        })
                    }).catch((err) => {
                        res.json({ success: false, error: err, msg: "error in attribute set" })
                    })
            }
        })
}

const updateliquorVarientName = (req, res) => {
    drinkTypeModel.findOne({ _id: req.params.rootdrinktypeid })
        .then((result) => {
            let promiseArr = []
            for (let i = 0; i < result.liquorVarients.length; i++) {
                promiseArr.push(new Promise((resolve, reject) => {
                    drinkTypeModel.findOneAndUpdate({
                        _id: req.params.rootdrinktypeid,
                        "liquorVarients._id": req.params.liquorvarientid
                    }, {
                            $set: {
                                "liquorVarients.$.liquorVarientName": req.body.liquorVarientName
                            }
                        }).then((udata) => {
                            resolve(udata)
                        }).catch((err) => {
                            reject(err)
                        })
                }))

            }
            Promise.all(promiseArr).then((promiseArr) => {
                attributesetModel.find({ attributeType: "liquor" })
                    .then((attributeData) => {
                        promiseArr = []
                        for (let j = 0; j < attributeData.length; j++) {
                            if (attributeData[j].categoryType.length > 0) {
                                for (let k = 0; k < attributeData[j].categoryType.length; k++) {
                                    if (attributeData[j].categoryType[k].liquorVarients.length > 0) {
                                        for (let l = 0; l < attributeData[j].categoryType[k].liquorVarients.length; l++) {
                                            promiseArr.push(new Promise((resolve, reject) => {
                                                attributesetModel.findOneAndUpdate({
                                                    _id: attributeData[j]._id,
                                                    "categoryType._id": req.params.rootdrinktypeid,
                                                    "categoryType.liquorVarients._id": req.params.liquorvarientid
                                                },
                                                    {
                                                        $set: {
                                                            "categoryType.$.liquorVarients.$[outer].liquorVarientName": req.body.liquorVarientName
                                                        }
                                                    },
                                                    {
                                                        "arrayFilters": [
                                                            {
                                                                "outer._id": req.params.liquorvarientid,
                                                            }
                                                        ]
                                                    }).then((Audata) => {
                                                        resolve(Audata)
                                                    }).catch((err) => {
                                                        reject(err)
                                                    })
                                            }))
                                        }
                                    } else {
                                        continue;
                                    }
                                }
                            } else {
                                continue;
                            }
                        }
                        Promise.all(promiseArr).then((promiseArr) => {
                            liquorbrandModel.find({})
                                .then((brandData) => {
                                    promiseArr2 = []
                                    for (let m = 0; m < brandData.length; m++) {
                                        if (brandData[m].categoryType.length > 0) {
                                            for (let n = 0; n < brandData[m].categoryType.length; n++) {
                                                if (brandData[m].categoryType[n].liquorVarients.length > 0) {
                                                    for (let o = 0; o < brandData[m].categoryType[n].liquorVarients.length; o++) {
                                                        promiseArr2.push(new Promise((resolve, reject) => {
                                                            liquorbrandModel.findOneAndUpdate({
                                                                _id: brandData[m]._id,
                                                                "categoryType._id": req.params.rootdrinktypeid,
                                                                "categoryType.liquorVarients._id": req.params.liquorvarientid
                                                            },
                                                                {
                                                                    $set: {
                                                                        "categoryType.$.liquorVarients.$[outer].liquorVarientName": req.body.liquorVarientName
                                                                    }
                                                                }, {
                                                                    "arrayFilters": [
                                                                        {
                                                                            "outer._id": req.params.liquorvarientid
                                                                        }
                                                                    ]
                                                                }).then((Budata) => {
                                                                    resolve(Budata)
                                                                }).catch((err) => {
                                                                    reject(err)
                                                                })
                                                        }))
                                                    }
                                                } else {
                                                    continue
                                                }
                                            }
                                        } else {
                                            continue
                                        }
                                    }
                                    Promise.all(promiseArr2).then((promiseArr2) => {
                                        liquorproductModel.find({ "subVarient.id": req.params.liquorvarientid })
                                            .then((productData) => {
                                                promiseArr3 = []
                                                if (productData.length > 0) {
                                                    for (let p = 0; p < productData.length; p++) {
                                                        promiseArr3.push(new Promise((resolve, reject) => {
                                                            if (productData[p].subVarient.id == req.params.liquorvarientid && productData[p].varient.id == req.params.liquorvarientid) {
                                                                liquorproductModel.findOneAndUpdate({ _id: productData[p]._id },
                                                                    {
                                                                        $set: {
                                                                            "varient.varient": req.body.liquorVarientName,
                                                                            "subVarient.subVarient": req.body.liquorVarientName
                                                                        }
                                                                    }).then((Pudata) => {
                                                                        resolve(Pudata)
                                                                    }).catch((err) => {
                                                                        reject(err)
                                                                    })
                                                            } else {
                                                                liquorproductModel.findOneAndUpdate({ _id: productData[p]._id },
                                                                    {
                                                                        $set: {
                                                                            "subVarient.subVarient": req.body.liquorVarientName
                                                                        }
                                                                    }).then((Pudata) => {
                                                                        resolve(Pudata)
                                                                    }).catch((err) => {
                                                                        reject(err)
                                                                    })
                                                            }

                                                        }))
                                                    }
                                                    Promise.all(promiseArr3).then((promiseArr3) => {
                                                        res.json({ success: true, msg: "liquor Varient Name updated" })
                                                    }).catch((err) => {
                                                        res.json({ success: false, msg: "error in promise arr 3", error: err })
                                                    })
                                                } else {
                                                    res.json({ success: true, msg: "liquor Varient Name updated" })
                                                }
                                            }).catch((err) => {
                                                res.json({ success: false, msg: "error in product data", error: err })
                                            })
                                    }).catch((err) => {
                                        res.json({ success: false, msg: "error in promise arr 2", error: err })
                                    })
                                }).catch((err) => {
                                    res.json({ success: false, msg: "error in brand data", error: err })
                                })
                        }).catch((err) => {
                            res.json({ success: false, msg: "error in promise arr", error: err })
                        })
                    }).catch((err) => {
                        res.json({ success: false, msg: "error in attribute data", error: err })
                    })
            }).catch((err) => {
                res.json({ success: false, msg: "error in promise", error: err })
            })

        }).catch((err) => {
            res.json({ success: false, msg: "error in result", error: err })
        })
}

const updateliquorSubVarientsName = (req, res) => {
    drinkTypeModel.findOne({ _id: req.params.rootdrinktypeid })
        .then((result) => {
            let promiseArr = []
            for (let i = 0; i < result.liquorVarients.length; i++) {
                if (result.liquorVarients[i].liquorSubVarients.length > 0) {
                    for (let j = 0; j < result.liquorVarients[i].liquorSubVarients.length; j++) {
                        promiseArr.push(new Promise((resolve, reject) => {
                            drinkTypeModel.findOneAndUpdate({
                                _id: req.params.rootdrinktypeid,
                                "liquorVarients.liquorSubVarients._id": req.params.liquorsubvarientid
                            }, {
                                    $set: {
                                        "liquorVarients.$.liquorSubVarients.$[outer].liquorSubVarientName": req.body.liquorSubVarientName
                                    }
                                },
                                {
                                    "arrayFilters": [
                                        {
                                            "outer._id": req.params.liquorsubvarientid,
                                        }
                                    ]
                                }).then((udata) => {
                                    resolve(udata)
                                }).catch((err) => {
                                    reject(err)
                                })
                        }))
                    }
                } else {
                    continue;
                }
            }
            Promise.all(promiseArr).then((promiseArr) => {
                attributesetModel.find({ attributeType: "liquor" })
                    .then((attributeData) => {
                        promiseArr = []
                        for (let k = 0; k < attributeData.length; k++) {
                            if (attributeData[k].categoryType.length > 0) {
                                for (let l = 0; l < attributeData[k].categoryType.length; l++) {
                                    if (attributeData[k].categoryType[l].liquorVarients.length > 0) {
                                        for (let m = 0; m < attributeData[k].categoryType[l].liquorVarients.length; m++) {
                                            if (attributeData[k].categoryType[l].liquorVarients[m].liquorSubVarients.length > 0) {
                                                for (let n = 0; n < attributeData[k].categoryType[l].liquorVarients[m].liquorSubVarients.length; n++) {
                                                    promiseArr.push(new Promise((resolve, reject) => {
                                                        attributesetModel.findOneAndUpdate({
                                                            _id: attributeData[k]._id,
                                                            "categoryType._id": req.params.rootdrinktypeid,
                                                            "categoryType.liquorVarients._id": req.params.liquorvarientid,
                                                            "categoryType.liquorVarients.liquorSubVarients._id": req.params.liquorsubvarientid
                                                        },
                                                            {
                                                                $set: {
                                                                    "categoryType.$.liquorVarients.$[outer].liquorSubVarients.$[inner].liquorSubVarientName": req.body.liquorSubVarientName
                                                                }
                                                            }, {
                                                                "arrayFilters": [
                                                                    {
                                                                        "outer._id": req.params.liquorvarientid
                                                                    },
                                                                    {
                                                                        "inner._id": req.params.liquorsubvarientid
                                                                    }
                                                                ]
                                                            }).then((Audata) => {
                                                                resolve(Audata)
                                                            }).catch((err) => {
                                                                reject(err)
                                                            })
                                                    }))
                                                }
                                            } else {
                                                continue
                                            }
                                        }
                                    } else {
                                        continue
                                    }
                                }
                            } else {
                                continue
                            }
                        }
                        Promise.all(promiseArr).then((promiseArr) => {
                            liquorbrandModel.find({})
                                .then((brandData) => {
                                    promiseArr2 = []
                                    for (let o = 0; o < brandData.length; o++) {
                                        if (brandData[o].categoryType.length > 0) {
                                            for (let p = 0; p < brandData[o].categoryType.length; p++) {
                                                if (brandData[o].categoryType[p].liquorVarients.length > 0) {
                                                    for (let q = 0; q < brandData[o].categoryType[p].liquorVarients.length; q++) {
                                                        if (brandData[o].categoryType[p].liquorVarients[q].liquorSubVarients.length > 0) {
                                                            for (let r = 0; r < brandData[o].categoryType[p].liquorVarients[q].liquorSubVarients.length; r++) {
                                                                promiseArr2.push(new Promise((resolve, reject) => {
                                                                    liquorbrandModel.findOneAndUpdate({
                                                                        _id: brandData[o]._id,
                                                                        "categoryType._id": req.params.rootdrinktypeid,
                                                                        "categoryType.liquorVarients._id": req.params.liquorvarientid,
                                                                        "categoryType.liquorVarients.liquorSubVarients._id": req.params.liquorsubvarientid
                                                                    },
                                                                        {
                                                                            $set: {
                                                                                "categoryType.$.liquorVarients.$[outer].liquorSubVarients.$[inner].liquorSubVarientName": req.body.liquorSubVarientName
                                                                            }
                                                                        }, {
                                                                            "arrayFilters": [
                                                                                {
                                                                                    "outer._id": req.params.liquorvarientid
                                                                                },
                                                                                {
                                                                                    "inner._id": req.params.liquorsubvarientid
                                                                                }
                                                                            ]
                                                                        }).then((Budata) => {
                                                                            resolve(Budata)
                                                                        }).catch((err) => {
                                                                            reject(err)
                                                                        })
                                                                }))
                                                            }
                                                        } else {
                                                            continue
                                                        }
                                                    }
                                                } else {
                                                    continue
                                                }
                                            }
                                        } else {
                                            continue
                                        }
                                    }
                                    Promise.all(promiseArr2).then((promiseArr2) => {
                                        liquorproductModel.find({ "subsubVarient.id": req.params.liquorsubvarientid })
                                            .then((productData) => {
                                                promiseArr3 = []
                                                if (productData.length > 0) {
                                                    for (let s = 0; s < productData.length; s++) {
                                                        promiseArr3.push(new Promise((resolve, reject) => {
                                                            if (productData[s].subsubVarient.id == req.params.liquorsubvarientid && productData[s].varient.id == req.params.liquorsubvarientid) {
                                                                liquorproductModel.findOneAndUpdate({ _id: productData[s]._id },
                                                                    {
                                                                        $set: {
                                                                            "varient.varient": req.body.liquorSubVarientName,
                                                                            "subsubVarient.subsubVarient": req.body.liquorSubVarientName
                                                                        }
                                                                    }).then((Pudata) => {
                                                                        resolve(Pudata)
                                                                    }).catch((err) => {
                                                                        reject(err)
                                                                    })
                                                            } else {
                                                                liquorproductModel.findOneAndUpdate({ _id: productData[s]._id },
                                                                    {
                                                                        $set: {
                                                                            "subsubVarient.subsubVarient": req.body.liquorSubVarientName
                                                                        }
                                                                    }).then((Pudata) => {
                                                                        resolve(Pudata)
                                                                    }).catch((err) => {
                                                                        reject(err)
                                                                    })
                                                            }

                                                        }))
                                                    }
                                                    Promise.all(promiseArr3).then((promiseArr3) => {
                                                        res.json({ success: true, msg: "liquor Sub Varient Name updated" })
                                                    }).catch((err) => {
                                                        res.json({ success: false, msg: "error in promise arr 3", error: err })
                                                    })
                                                } else {
                                                    res.json({ success: true, msg: "liquor Sub Varient Name updated" })
                                                }
                                            }).catch((err) => {
                                                res.json({ success: false, msg: "error in product data", error: err })
                                            })
                                    }).catch((err) => {
                                        res.json({ success: false, msg: "error in promise arr 2", error: err })
                                    })
                                }).catch((err) => {
                                    res.json({ success: false, msg: "error in brand data", error: err })
                                })
                        }).catch((err) => {
                            res.json({ success: false, msg: "error in promise arr", error: err })
                        })
                    }).catch((err) => {
                        res.json({ success: false, msg: "error in attribute data", error: err })
                    })
            }).catch((err) => {
                res.json({ success: false, msg: "error in promise", error: err })
            })

        }).catch((err) => {
            res.json({ success: false, msg: "error in result", error: err })
        })
}

const updateliquorSubSubVarientTypeName = (req, res) => {
    drinkTypeModel.findOne({ _id: req.params.rootdrinktypeid })
        .then((result) => {
            let promiseArr = []
            for (let i = 0; i < result.liquorVarients.length; i++) {
                if (result.liquorVarients[i].liquorSubVarients.length > 0) {
                    for (let j = 0; j < result.liquorVarients[i].liquorSubVarients.length; j++) {
                        if (result.liquorVarients[i].liquorSubVarients[j].liquorSubSubVarientType.length > 0) {
                            for (let a = 0; a < result.liquorVarients[i].liquorSubVarients[j].liquorSubSubVarientType.length; a++) {
                                promiseArr.push(new Promise((resolve, reject) => {
                                    drinkTypeModel.findOneAndUpdate({
                                        _id: req.params.rootdrinktypeid,
                                        "liquorVarients.liquorSubVarients._id": req.params.liquorsubvarientid,
                                        "liquorVarients.liquorSubVarients.liquorSubSubVarientType._id": req.params.liquorsubsubvarienttypeid
                                    }, {
                                            $set: {
                                                "liquorVarients.$.liquorSubVarients.$[outer].liquorSubSubVarientType.$[inner].liquorSubSubVarientTypeName": req.body.liquorSubSubVarientTypeName
                                            }
                                        },
                                        {
                                            "arrayFilters": [
                                                {
                                                    "outer._id": req.params.liquorsubvarientid,
                                                },
                                                {
                                                    "inner._id": req.params.liquorsubsubvarienttypeid
                                                }
                                            ]
                                        }).then((udata) => {
                                            resolve(udata)
                                        }).catch((err) => {
                                            reject(err)
                                        })
                                }))
                            }
                        } else {
                            continue;
                        }
                    }
                } else {
                    continue;
                }
            }
            Promise.all(promiseArr).then((promiseArr) => {
                attributesetModel.find({ attributeType: "liquor" })
                    .then((attributeData) => {
                        promiseArr = []
                        for (let k = 0; k < attributeData.length; k++) {
                            if (attributeData[k].categoryType.length > 0) {
                                for (let l = 0; l < attributeData[k].categoryType.length; l++) {
                                    if (attributeData[k].categoryType[l].liquorVarients.length > 0) {
                                        for (let m = 0; m < attributeData[k].categoryType[l].liquorVarients.length; m++) {
                                            if (attributeData[k].categoryType[l].liquorVarients[m].liquorSubVarients.length > 0) {
                                                for (let n = 0; n < attributeData[k].categoryType[l].liquorVarients[m].liquorSubVarients.length; n++) {
                                                    if (attributeData[k].categoryType[l].liquorVarients[m].liquorSubVarients[n].liquorSubSubVarientType.length > 0) {
                                                        for (let o = 0; o < attributeData[k].categoryType[l].liquorVarients[m].liquorSubVarients[n].liquorSubSubVarientType.length; o++) {
                                                            promiseArr.push(new Promise((resolve, reject) => {
                                                                attributesetModel.findOneAndUpdate({
                                                                    _id: attributeData[k]._id,
                                                                    "categoryType._id": req.params.rootdrinktypeid,
                                                                    "categoryType.liquorVarients._id": req.params.liquorvarientid,
                                                                    "categoryType.liquorVarients.liquorSubVarients._id": req.params.liquorsubvarientid,
                                                                    "categoryType.liquorVarients.liquorSubVarients.liquorSubSubVarientType._id": req.params.liquorsubsubvarienttypeid
                                                                },
                                                                    {
                                                                        $set: {
                                                                            "categoryType.$.liquorVarients.$[outer].liquorSubVarients.$[inner].liquorSubSubVarientType.$[middle].liquorSubSubVarientTypeName": req.body.liquorSubSubVarientTypeName
                                                                        }
                                                                    },
                                                                    {
                                                                        "arrayFilters": [
                                                                            {
                                                                                "outer._id": req.params.liquorvarientid
                                                                            },
                                                                            {
                                                                                "inner._id": req.params.liquorsubvarientid
                                                                            },
                                                                            {
                                                                                "middle._id": req.params.liquorsubsubvarienttypeid
                                                                            }
                                                                        ]
                                                                    }).then((Audata) => {
                                                                        resolve(Audata)
                                                                    }).catch((err) => {
                                                                        reject(err)
                                                                    })
                                                            }))
                                                        }
                                                    } else {
                                                        continue
                                                    }
                                                }
                                            } else {
                                                continue
                                            }
                                        }
                                    } else {
                                        continue
                                    }
                                }
                            } else {
                                continue
                            }
                        }
                        Promise.all(promiseArr).then((promiseArr) => {
                            liquorbrandModel.find({})
                                .then((brandData) => {
                                    promiseArr2 = []
                                    for (let p = 0; p < brandData.length; p++) {
                                        if (brandData[p].categoryType.length > 0) {
                                            for (let q = 0; q < brandData[p].categoryType.length; q++) {
                                                if (brandData[p].categoryType[q].liquorVarients.length > 0) {
                                                    for (let r = 0; r < brandData[p].categoryType[q].liquorVarients.length; r++) {
                                                        if (brandData[p].categoryType[q].liquorVarients[r].liquorSubVarients.length > 0) {
                                                            for (let s = 0; s < brandData[p].categoryType[q].liquorVarients[r].liquorSubVarients.length; s++) {
                                                                if (brandData[p].categoryType[q].liquorVarients[r].liquorSubVarients[s].liquorSubSubVarientType.length > 0) {
                                                                    for (let t = 0; t < brandData[p].categoryType[q].liquorVarients[r].liquorSubVarients[s].liquorSubSubVarientType.length; t++) {
                                                                        promiseArr2.push(new Promise((resolve, reject) => {
                                                                            liquorbrandModel.findOneAndUpdate({
                                                                                _id: brandData[p]._id,
                                                                                "categoryType._id": req.params.rootdrinktypeid,
                                                                                "categoryType.liquorVarients._id": req.params.liquorvarientid,
                                                                                "categoryType.liquorVarients.liquorSubVarients._id": req.params.liquorsubvarientid,
                                                                                "categoryType.liquorVarients.liquorSubVarients.liquorSubSubVarientType._id": req.params.liquorsubsubvarienttypeid
                                                                            },
                                                                                {
                                                                                    $set: {
                                                                                        "categoryType.$.liquorVarients.$[outer].liquorSubVarients.$[inner].liquorSubSubVarientType.$[middle].liquorSubSubVarientTypeName": req.body.liquorSubSubVarientTypeName
                                                                                    }
                                                                                },
                                                                                {
                                                                                    "arrayFilters": [
                                                                                        {
                                                                                            "outer._id": req.params.liquorvarientid
                                                                                        },
                                                                                        {
                                                                                            "inner._id": req.params.liquorsubvarientid
                                                                                        },
                                                                                        {
                                                                                            "middle._id": req.params.liquorsubsubvarienttypeid
                                                                                        }
                                                                                    ]
                                                                                }).then((Audata) => {
                                                                                    resolve(Audata)
                                                                                }).catch((err) => {
                                                                                    reject(err)
                                                                                })
                                                                        }))
                                                                    }
                                                                } else {
                                                                    continue
                                                                }
                                                            }
                                                        } else {
                                                            continue
                                                        }
                                                    }
                                                } else {
                                                    continue
                                                }
                                            }
                                        } else {
                                            continue
                                        }
                                    }
                                    Promise.all(promiseArr2).then((promiseArr2) => {
                                        liquorproductModel.find({ "varientType.id": req.params.liquorsubsubvarienttypeid })
                                            .then((productData) => {
                                                promiseArr3 = []
                                                if (productData.length > 0) {
                                                    for (let u = 0; u < productData.length; u++) {
                                                        promiseArr3.push(new Promise((resolve, reject) => {
                                                            if (productData[u].varientType.id == req.params.liquorsubsubvarienttypeid && productData[u].varient.id == req.params.liquorsubsubvarienttypeid) {
                                                                liquorproductModel.findOneAndUpdate({ _id: productData[u]._id },
                                                                    {
                                                                        $set: {
                                                                            "varient.varient": req.body.liquorSubSubVarientTypeName,
                                                                            "varientType.varientType": req.body.liquorSubSubVarientTypeName
                                                                        }
                                                                    }).then((Pudata) => {
                                                                        resolve(Pudata)
                                                                    }).catch((err) => {
                                                                        reject(err)
                                                                    })
                                                            } else {
                                                                liquorproductModel.findOneAndUpdate({ _id: productData[u]._id },
                                                                    {
                                                                        $set: {
                                                                            "varientType.varientType": req.body.liquorSubSubVarientTypeName
                                                                        }
                                                                    }).then((Pudata) => {
                                                                        resolve(Pudata)
                                                                    }).catch((err) => {
                                                                        reject(err)
                                                                    })
                                                            }

                                                        }))
                                                    }
                                                    Promise.all(promiseArr3).then((promiseArr3) => {
                                                        res.json({ success: true, msg: "liquor sub sub varient type name updated" })
                                                    }).catch((err) => {
                                                        res.json({ success: false, msg: "error in promise arr 3", error: err })
                                                    })
                                                } else {
                                                    res.json({ success: true, msg: "liquor sub sub varient type name updated" })
                                                }
                                            }).catch((err) => {
                                                res.json({ success: false, msg: "error in product data", error: err })
                                            })
                                    }).catch((err) => {
                                        res.json({ success: false, msg: "error in promise arr 2", error: err })
                                    })
                                }).catch((err) => {
                                    res.json({ success: false, msg: "error in brand data", error: err })
                                })
                        }).catch((err) => {
                            res.json({ success: false, msg: "error in promise arr", error: err })
                        })
                    }).catch((err) => {
                        res.json({ success: false, msg: "error in attribute data", error: err })
                    })
            }).catch((err) => {
                res.json({ success: false, msg: "error in promise", error: err })
            })

        }).catch((err) => {
            res.json({ success: false, msg: "error in result", error: err })
        })
}

module.exports = {
    addDrinkType,
    getDrinkType,
    getAllDrinkTypes,
    deleteDrinkType,
    addLiquorVarient,
    addLiquorSubVarient,
    addLiquorSubSubVarient,
    getLiquorVarient,
    updateDrinkTypeName,
    updateliquorVarientName,
    updateliquorSubVarientsName,
    updateliquorSubSubVarientTypeName
}
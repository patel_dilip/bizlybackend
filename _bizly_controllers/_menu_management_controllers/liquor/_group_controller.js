// const groupModel = require.groupModel)
const groupModel = require('../../../_bizly_models/_menu_management_models/_liquor_models/_group_model').groupModel
const attributeModel = require('../../../_bizly_models/_menu_management_models/_liquor_models/_attribute-model').attributeModel

const addGroup = (req, res) => {
    groupModel.findOne({ groupName: req.body.groupName, 
        attribute_set_name: req.body.attribute_set_name, 
        attributeType: req.body.attributeType }, (err, data) => {
        if (err) {
            res.json({ sucess: false, msg: err })
        } else if (data) {
            res.json({ sucess: false, msg: "Group already exists" })
        } else {
            var group = new groupModel()
            group.groupName = req.body.groupName
            group.attribute_set_name = req.body.attribute_set_name
            group.assignAttributes = req.body.assignAttributes
            group.attributeType = req.body.attributeType
            group.addedBy = req.body.userid
            group.updatedBy = req.body.userid

            var assignattributesarray = req.body.assignAttributes;

            group.save((err) => {
                if (err) {
                    throw err
                } else {
                    assignattributesarray.forEach(attributeid => {
                        console.log((attributeid));
                        attributeModel.findByIdAndUpdate(attributeid, { $set: { isAssign: true } }, (err, udata) => {
                            if (err) {
                                res.json({ sucess: false, msg: err })
                            }
                        })
                    });

                    res.json({ sucess: true, msg: 'group added' })
                }
            })
        }
    })

}

const getGroup = (req, res) => {
    groupModel.findById(req.params.groupid).
        populate('assignAttributes', 'attributeName').
        populate('addedBy', 'userName').
        populate('updatedBy', 'userName').
        sort('-updatedAt').
        exec((err, group) => {
            if (err) {
                throw err
            } else if (group == null) {
                res.json({ sucess: false, msg: 'Group not found' })
            } else {
                res.json({ sucess: true, msg: ' Group found', data: group })
            }
        })
}

const getAllGroups = (req, res) => {
    groupModel.find().
        populate('assignAttributes', 'attributeName').
        populate('addedBy', 'userName').
        populate('updatedBy', 'userName').
        sort('-updatedAt').
        exec((err, data) => {
            if (err) {
                throw err
            } else if (data == null || data.length == 0) {
                res.json({ sucess: false, msg: "Group Not Found" })
            } else {
                res.json({ sucess: true, msg: "Group Found", data: data })
            }
        })
}

// check Liquor Group Already Exists
const checkGroupAlreadyExists = (req, res) => {
    groupModel.findOne({ groupName: req.params.groupName, attribute_set_name: req.params.attribute_set_name }, (err, data) => {
        if (err) {
            res.json({ success: false, msg: err })
        } else if (data) {
            res.json({ success: true, msg: "Liquor Group Name already Exists" })
        } else {
            res.json({ success: false, msg: "Liquor Group Name not available" })
        }
    })
}

//get all assignGroup Set
const getAllAssignGroupset = (req, res) => {
    groupModel.find({ attribute_set_name: req.params.attribute_set_name, attributeType: req.params.attributeType }).
        populate('assignAttributes').
        populate('addedBy', 'userName').
        populate('updatedBy', 'userName').
        exec((err, data) => {
            if (err) {
                throw err
            } else if (data == null || data.length == 0) {
                res.json({ sucess: false, msg: 'attibuteset not found' })
            } else {
                res.json({ sucess: true, msg: 'assign group attributeset found', data: data })
            }
        })
}


const updateGroup = (req, res) => {
    groupModel.findOneAndUpdate({ _id: req.params.groupid },
        {
            $set: {
                groupName: req.body.groupName,
                assignAttributes: req.body.assignAttributes,
                attributeType: req.body.attributeType,
                updatedBy: req.body.userid,
                updatedAt: Date.now()
            }
        }, (err, ugroup) => {
            if (err) {
                throw err
            } else if (ugroup == null) {
                res.json({ sucess: false, msg: "Group not found" })
            } else {
                res.json({ sucess: true, msg: "Group updated" })
            }
        })
}

const deleteGroup = (req, res) => {
    groupModel.findByIdAndDelete(req.params.groupid, (err, dGroup) => {
        if (err) {
            throw err
        } else if (dGroup == null) {
            res.json({ sucess: false, msg: "Group not found" })
        } else {
            res.json({ sucess: true, msg: "Group Deleted" })
        }
    })
}

const deleteAttributeFromGroup = (req, res) => {
    groupModel.findOneAndUpdate({ _id: req.params.groupid, assignAttributes: { $in: req.params.attributeid } },
        {
            $pull: {
                assignAttributes: req.params.attributeid
            }
        }, (err, udata) => {
            if (err) {
                res.json({ sucess: false, msg: err })
            } else if (udata == null || udata.length == 0) {
                res.json({ sucess: false, msg: "Attribute not found" })
            } else {
                res.json({ sucess: true, msg: "Attribute Deleted" })
            }
        })
}

module.exports = {
    addGroup,
    getGroup,
    getAllGroups,
    checkGroupAlreadyExists,
    getAllAssignGroupset,
    updateGroup,
    deleteGroup,
    deleteAttributeFromGroup
}
const liquorTreeModel = require("../../../_bizly_models/_menu_management_models/_liquor_models/_tree_model")
  .liquorTreeModel;
var mongoose = require("mongoose");

const addTree = (req, res) => {
  var tree = new liquorTreeModel();
  tree.liquorTree = req.body.liquorTree;
  tree.addedBy = req.body.userid;
  tree.updatedBy = req.body.userid;

  tree.save((err) => {
    if (err) {
      res.json({ sucess: false, msg: err });
    } else {
      res.json({ sucess: true });
    }
  });
};

const addLiquorTree = async (req, res) => {
  var id = mongoose.Types.ObjectId();
  for (let i = 0; i < req.body.liquorTree.length; i++) {
    req.body.liquorTree[i]["_id"] = mongoose.Types.ObjectId();
  }
  liquorTreeModel.update(
    {},
    {
      $set: {
        updatedBy: req.body.userid,
      },
      $push: {
        liquorTree: { $each: req.body.liquorTree },
      },
    },
    { upsert: true },
    (err) => {
      if (err) {
        res.json({ sucess: false, msg: err });
      } else {
        res.json({ sucess: true });
      }
    }
  );
};

const getTree = async (req, res) => {
  liquorTreeModel
    .findOne()
    .populate("addedBy", "userName")
    .populate("updatedBy", "userName")
    .sort("-updatedAt")
    .exec(function (err, tree) {
      if (err) {
        res.json({ sucess: false, msg: err });
      } else if (tree == null) {
        res.json({ sucess: false });
      } else {
        res.json({ sucess: true, data: tree });
      }
    });
};

const updateTree = (req, res) => {
  liquorTreeModel.findByIdAndUpdate(
    req.params.treeid,
    {
      $set: { liquorTree: req.body.liquorTree, updatedBy: req.body.userid },
    },
    (err) => {
      if (err) {
        res.json({ sucess: false, msg: err });
      } else {
        res.json({ sucess: true });
      }
    }
  );
};

module.exports = {
  addTree,
  getTree,
  updateTree,
  addLiquorTree,
};

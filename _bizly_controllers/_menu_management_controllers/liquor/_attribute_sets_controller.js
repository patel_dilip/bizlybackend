const attributeSetModel = require('../../../_bizly_models/_menu_management_models/_liquor_models/_attribute_set_model').attributeSetModel
const attributeGroupModel = require('../../../_bizly_models/_menu_management_models/_liquor_models/_group_model').groupModel

const addAttributeSet = (req, res) => {
    attributeSetModel.findOne({ attributeSetName: req.body.attributeSetName, attributeType: req.body.attributeType }, (err, data) => {
        if (err) {
            res.json({ sucess: false, msg: err })
        } else if (data) {
            res.json({ sucess: false, msg: "Attribute set already exists" })
        } else {
            var attributeset = new attributeSetModel()
            attributeset.attributeSetName = req.body.attributeSetName
            attributeset.assignGroups = req.body.assignGroups
            attributeset.attributeType = req.body.attributeType
            attributeset.categoryType = req.body.categoryType
            attributeset.addedBy = req.body.userid
            attributeset.updatedBy = req.body.userid

            attributeset.save((err) => {
                if (err) {
                    throw err
                } else {
                    res.json({ sucess: true, msg: 'attribute set added' })
                }
            })
        }
    })
}

const getAttributeSet = (req, res) => {
    attributeSetModel.findById(req.params.attributesetid).
        populate('assignGroups', 'groupName').
        populate('addedBy', 'userName').
        populate('updatedBy', 'userName').
        sort('-updatedAt').
        exec((err, attributeset) => {
            if (err) {
                throw err
            } else if (attributeset == null) {
                res.json({ sucess: false, msg: 'attibuteset not found' })
            } else {
                res.json({ sucess: true, msg: ' attributeset found', data: attributeset })
            }
        })
}

const getAllAttributeSets = (req, res) => {
    attributeSetModel.find().
        populate('assignGroups', 'groupName').
        populate('addedBy', 'userName').
        populate('updatedBy', 'userName').
        sort('-updatedAt').
        exec((err, data) => {
            if (err) {
                throw err
            } else if (data == null || data.length == 0) {
                res.json({ sucess: false, msg: 'attibuteset not found' })
            } else {
                res.json({ sucess: true, msg: ' attributeset found', data: data })
            }
        })
}

const getAttributeSetFromLiquorCategories = (req, res) => {
    attributeSetModel.find({ attributeType: req.body.attributeType, status: true })
        .populate('assignGroups')
        .exec((err, attributeData) => {
            if (err) {
                res.json({ success: false, msg: err })
            } else if (attributeData == null || attributeData.length == 0) {
                res.json({ success: false, msg: "Attribute data not found" })
            } else {
                var arr1 = []
                var arr2 = []
                var arr3 = []
                var arr4 = []
                var promise5 = []
                for (let i = 0; i < attributeData.length; i++) {
                    for (let j = 0; j < attributeData[i].categoryType.length; j++) {
                        if (attributeData[i].categoryType[j]._id == req.body.id &&
                            attributeData[i].categoryType[j].rootDrinkType == req.body.string &&
                            attributeData[i].categoryType[j].showChildren == true) {
                            var promise1 = new Promise((resolve, reject) => {
                                arr1.push(attributeData[i])
                                resolve(arr1)
                            })

                        } else if (attributeData[i].categoryType[j].liquorVarients.length > 0) {
                            for (let k = 0; k < attributeData[i].categoryType[j].liquorVarients.length; k++) {
                                if (attributeData[i].categoryType[j].liquorVarients[k]._id == req.body.id &&
                                    attributeData[i].categoryType[j].liquorVarients[k].liquorVarientName == req.body.string &&
                                    attributeData[i].categoryType[j].liquorVarients[k].showChildren == true) {

                                    var promise2 = new Promise((resolve, reject) => {
                                        arr2.push(attributeData[i])
                                        resolve(arr2)
                                    })
                                } else if (attributeData[i].categoryType[j].liquorVarients[k].liquorSubVarients.length > 0) {
                                    for (let l = 0; l < attributeData[i].categoryType[j].liquorVarients[k].liquorSubVarients.length; l++) {
                                        if (attributeData[i].categoryType[j].liquorVarients[k].liquorSubVarients[l]._id == req.body.id &&
                                            attributeData[i].categoryType[j].liquorVarients[k].liquorSubVarients[l].liquorSubVarientName == req.body.string &&
                                            attributeData[i].categoryType[j].liquorVarients[k].liquorSubVarients[l].showChildren == true) {

                                            var promise3 = new Promise((resolve, reject) => {
                                                arr3.push(attributeData[i])
                                                resolve(arr3)
                                            })
                                        } else if (attributeData[i].categoryType[j].liquorVarients[k].liquorSubVarients[l].liquorSubSubVarientType.length > 0) {
                                            for (let m = 0; m < attributeData[i].categoryType[j].liquorVarients[k].liquorSubVarients[l].liquorSubSubVarientType.length; m++) {
                                                if (attributeData[i].categoryType[j].liquorVarients[k].liquorSubVarients[l].liquorSubSubVarientType[m]._id == req.body.id &&
                                                    attributeData[i].categoryType[j].liquorVarients[k].liquorSubVarients[l].liquorSubSubVarientType[m].liquorSubSubVarientTypeName == req.body.string &&
                                                    attributeData[i].categoryType[j].liquorVarients[k].liquorSubVarients[l].liquorSubSubVarientType[m].showChildren == true) {

                                                    var promise4 = new Promise((resolve, reject) => {
                                                        arr4.push(attributeData[i])
                                                        resolve(arr4)
                                                    })
                                                } else {
                                                    // res.json({success:false , msg:req.body.string + "not found"})
                                                    continue;
                                                }
                                            }
                                        } else {
                                            console.log("liquor sub sub varient type length =0")
                                            continue;
                                        }
                                    }
                                } else {
                                    console.log("liquor sub varient length =0")
                                    continue;
                                }
                            }

                        } else {
                            console.log("liquor varient length = 0")
                            continue;
                        }
                    }
                }
                Promise.all([promise1, promise2, promise3, promise4]).then((finalData) => {
                    finalData.filter(data => {
                        if (data == null || data == undefined) {
                            console.log("negative")
                        } else {
                            for (let n = 0; n < data.length; n++) {
                                if (data[n].assignGroups.length > 0) {
                                    for (let o = 0; o < data[n].assignGroups.length; o++) {
                                        var groupId = data[n].assignGroups[o]._id
                                        promise5.push(new Promise((resolve, reject) => {
                                            attributeGroupModel.findOne({ _id: groupId })
                                                .populate('assignAttributes')
                                                .exec((err, attributes) => {
                                                    resolve(attributes)
                                                })
                                        }))
                                    }
                                } else {
                                    console.log("assigned group empty")
                                    continue;
                                }

                            }
                            Promise.all(promise5).then((finalAttributeData) => {
                                res.json({ success: true, attributeSets: data, groupAttribute: finalAttributeData })
                            }).catch((err) => {
                                res.json({ success: false, msg: "Error in final Attribute Data", error: err })
                            })
                        }
                    })
                }).catch((err) => {
                    res.json({ success: false, msg: "Error in final data", error: err })
                })
            }
        })
}

const getAttributeSetFromOtherCategories = (req, res) => {
    attributeSetModel.find({ attributeType: req.body.attributeType, status: true })
        .populate('assignGroups')
        .exec((err, attributeData) => {
            if (err) {
                res.json({ success: false, msg: err })
            } else if (attributeData == null || attributeData.length == 0) {
                res.json({ success: false, msg: "Attribute data not found" })
            } else {
                var arr1 = []
                var arr2 = []
                var arr3 = []
                var arr4 = []
                var promise5 = []
                for (let i = 0; i < attributeData.length; i++) {
                    for (let j = 0; j < attributeData[i].categoryType.length; j++) {
                        if (attributeData[i].categoryType[j]._id == req.body.id &&
                            attributeData[i].categoryType[j].rootCategoryName == req.body.string &&
                            attributeData[i].categoryType[j].showChildren == true) {
                            var promise1 = new Promise((resolve, reject) => {
                                arr1.push(attributeData[i])
                                resolve(arr1)
                            })

                        } else if (attributeData[i].categoryType[j].subCategories.length > 0) {
                            for (let k = 0; k < attributeData[i].categoryType[j].subCategories.length; k++) {
                                if (attributeData[i].categoryType[j].subCategories[k]._id == req.body.id &&
                                    attributeData[i].categoryType[j].subCategories[k].subCategoryName == req.body.string &&
                                    attributeData[i].categoryType[j].subCategories[k].showChildren == true) {

                                    var promise2 = new Promise((resolve, reject) => {
                                        arr2.push(attributeData[i])
                                        resolve(arr2)
                                    })
                                } else if (attributeData[i].categoryType[j].subCategories[k].subSubCategories.length > 0) {
                                    for (let l = 0; l < attributeData[i].categoryType[j].subCategories[k].subSubCategories.length; l++) {
                                        if (attributeData[i].categoryType[j].subCategories[k].subSubCategories[l]._id == req.body.id &&
                                            attributeData[i].categoryType[j].subCategories[k].subSubCategories[l].subSubCategoryName == req.body.string &&
                                            attributeData[i].categoryType[j].subCategories[k].subSubCategories[l].showChildren == true) {

                                            var promise3 = new Promise((resolve, reject) => {
                                                arr3.push(attributeData[i])
                                                resolve(arr3)
                                            })
                                        } else if (attributeData[i].categoryType[j].subCategories[k].subSubCategories[l].subSubCategoryType.length > 0) {
                                            for (let m = 0; m < attributeData[i].categoryType[j].subCategories[k].subSubCategories[l].subSubCategoryType.length; m++) {
                                                if (attributeData[i].categoryType[j].subCategories[k].subSubCategories[l].subSubCategoryType[m]._id == req.body.id &&
                                                    attributeData[i].categoryType[j].subCategories[k].subSubCategories[l].subSubCategoryType[m].subSubCategoryTypeName == req.body.string &&
                                                    attributeData[i].categoryType[j].subCategories[k].subSubCategories[l].subSubCategoryType[m].showChildren == true) {

                                                    var promise4 = new Promise((resolve, reject) => {
                                                        arr4.push(attributeData[i])
                                                        resolve(arr4)
                                                    })
                                                } else {
                                                    // res.json({success:false , msg:req.body.string + "not found"})
                                                    continue;
                                                }
                                            }
                                        } else {
                                            console.log("sub Sub Category Type length =0")
                                            continue;
                                        }
                                    }
                                } else {
                                    console.log("sub Sub Categories length =0")
                                    continue;
                                }
                            }

                        } else {
                            console.log("sub Categories length = 0")
                            continue;
                        }
                    }
                }
                Promise.all([promise1, promise2, promise3, promise4]).then((finalData) => {
                    finalData.filter(data => {
                        if (data == null || data == undefined) {
                            console.log("negative")
                        } else {
                            for (let n = 0; n < data.length; n++) {
                                if (data[n].assignGroups.length > 0) {
                                    for (let o = 0; o < data[n].assignGroups.length; o++) {
                                        var groupId = data[n].assignGroups[o]._id
                                        promise5.push(new Promise((resolve, reject) => {
                                            attributeGroupModel.findOne({ _id: groupId })
                                                .populate('assignAttributes')
                                                .exec((err, attributes) => {
                                                    resolve(attributes)
                                                })
                                        }))
                                    }
                                } else {
                                    console.log("assigned group empty")
                                    continue;
                                }

                            }
                            Promise.all(promise5).then((finalAttributeData) => {
                                res.json({ success: true, attributeSets: data, groupAttribute: finalAttributeData })
                            }).catch((err) => {
                                res.json({ success: false, msg: "Error in final Attribute Data", error: err })
                            })
                        }
                    })
                }).catch((err) => {
                    res.json({ success: false, msg: "Error in final data", error: err })
                })
            }
        })
}



// check Attribute Set Already Exists
const checkAttributeSetAlreadyExists = (req, res) => {
    attributeSetModel.findOne({ attributeSetName: req.params.attributeSetName, attributeType: req.params.attributeType }, (err, data) => {
        if (err) {
            res.json({ success: false, msg: err })
        } else if (data) {
            res.json({ success: true, msg: "Attribute Set Name already Exists" })
        } else {
            res.json({ success: false, msg: "Attribute Set Name not available" })
        }
    })
}



const updateAttributeSet = (req, res) => {
    attributeSetModel.findByIdAndUpdate(req.params.attributesetid,
        {
            $set: {
                attributeSetName: req.body.attributeSetName,
                assignGroups: req.body.assignGroups,
                attributeType: req.body.attributeType,
                categoryType: req.body.categoryType,
                updatedBy: req.body.userid,
                updatedAt: Date.now()
            }
        }, (err, uattributeset) => {
            if (err) {
                throw err
            } else if (uattributeset == null) {
                res.json({ sucess: false, msg: "attributeset not found" })
            } else {
                res.json({ sucess: true, msg: "attributeset updated" })
            }
        })
}

const deleteAttributeSet = (req, res) => {
    attributeSetModel.findByIdAndDelete(req.params.attributesetid, (err) => {
        if (err) {
            res.json({ sucess: false, msg: "attributeset not found" })
        } else {
            res.json({ sucess: true, msg: "attributeset Deleted" })
        }
    })
}

const softDeleteAttributeSet = (req, res) => {
    attributeSetModel.findByIdAndUpdate(req.params.attributesetid, { $set: { status: req.body.status } }, (err) => {
        if (err) {
            res.json({ sucess: false, msg: "attributeset not found" })
        } else {
            res.json({ sucess: true, msg: "attributeset soft Deleted" })
        }
    })
}

module.exports = {
    addAttributeSet,
    getAttributeSet,
    getAllAttributeSets,
    getAttributeSetFromLiquorCategories,
    getAttributeSetFromOtherCategories,
    checkAttributeSetAlreadyExists,
    updateAttributeSet,
    deleteAttributeSet,
    softDeleteAttributeSet

}
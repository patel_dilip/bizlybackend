const equipmentModel = require('../../../_bizly_models/_menu_management_models/_inventory_assets_models/_equipment_model').equipmentModel

const addEquipment = (req, res) => {
    equipmentModel.findOne({ equipmentName: req.body.equipmentName }, (err, data) => {
        if (err) {
            res.json({ success: false, msg: err })
        } else if (data) {
            res.json({ success: false, msg: "equipment name already exists" })
        } else {
            var equipment = new equipmentModel()
            equipment.equipmentName = req.body.equipmentName,
                equipment.category = req.body.category,
                // equipment.assignattributeSet = req.body.assignattributeSet,
                equipment.root = req.body.root,
                equipment.subcategory = req.body.subcategory,
                equipment.subsubCategory = req.body.subsubCategory,
                equipment.categoryType = req.body.categoryType,
                equipment.attribute_set = req.body.attribute_set,
                equipment.attributeSet_response = req.body.attributeSet_response,
                equipment.image = req.body.image,
                equipment.addedBy = req.body.userid,
                equipment.updatedBy = req.body.userid
            equipment.save((err) => {
                if (err) {
                    res.json({ success: false, msg: err })
                } else {
                    res.json({ success: true, msg: "Equipment Added" })
                }
            })
        }
    })
}

const getAllEquipments = (req, res) => {
    equipmentModel.find().
        populate('assignattributeSet', 'attributeSetName').
        populate('addedBy', 'userName').
        populate('updatedBy', 'userName').
        sort('-updatedAt').
        exec((err, data) => {
            if (err) {
                res.json({ success: false, msg: err })
            } else if (data == null || data.length == 0) {
                res.json({ success: false, msg: "All Equipment Found" })
            } else {
                res.json({ success: true, msg: "All Equipment Found", data: data })
            }
        })

}

const getEquipment = (req, res) => {
    equipmentModel.find(req.params.equipmentid).
        populate('assignattributeSet', 'attributeSetName').
        populate('addedBy', 'userName').
        populate('updatedBy', 'userName').
        sort('-updatedAt').
        exec((err, data) => {
            if (err) {
                res.json({ success: false, msg: err })
            } else if (data == null || data.length == 0) {
                res.json({ success: false, msg: "All Equipment Found" })
            } else {
                res.json({ success: true, msg: "All Equipment Found", data: data })
            }
        })
}

// check Equipment Already Exists
const checkEquipmentAlreadyExists = (req, res) => {
    equipmentModel.findOne({ equipmentName: req.params.equipmentName }, (err, data) => {
        if (err) {
            res.json({ success: false, msg: err })
        } else if (data) {
            res.json({ success: true, msg: "Equipment Name already Exists" })
        } else {
            res.json({ success: false, msg: "Equipment Name not available" })
        }
    })
}
 
const updateEquipment = (req, res) => {
    equipmentModel.findByIdAndUpdate(req.params.equipmentid,
        {
            $set: {
                equipmentName: req.body.equipmentName,
                category: req.body.category,
                // assignattributeSet: req.body.assignattributeSet,
                root: req.body.root,
                subcategory: req.body.subcategory,
                subsubCategory: req.body.subsubCategory,
                categoryType: req.body.categoryType,
                attribute_set: req.body.attribute_set,
                attributeSet_response: req.body.attributeSet_response,
                image: req.body.image,
                updatedBy: req.body.userid,
                updatedAt: Date.now()
            }
        }, (err, udata) => {
            if (err) {
                res.json({ success: false, msg: err })
            } else if (udata == null || udata.length == 0) {
                res.json({ success: false, msg: "Equipment Not Found" })
            } else {
                res.json({ success: true, msg: "Equipment Updated" })
            }
        })
}

const deleteEquipment = (req, res) => {
    equipmentModel.findByIdAndDelete(req.params.equipmentid, (err) => {
        if (err) {
            res.json({ success: false, msg: err })
        } else {
            res.json({ success: true, msg: "Equipment Delete" })
        }
    })
}

const softDeleteEquipment = (req, res) => {
    equipmentModel.findByIdAndUpdate(req.params.equipmentid, { $set: { status: req.body.status } }, (err) => {
        if (err) {
            res.json({ sucess: false, msg: "Equipment not found" })
        } else {
            res.json({ sucess: true, msg: "Equipment Deleted" })
        }
    })
}
module.exports = {
    addEquipment,
    getAllEquipments,
    getEquipment,
    checkEquipmentAlreadyExists,
    updateEquipment,
    deleteEquipment,
    softDeleteEquipment
}
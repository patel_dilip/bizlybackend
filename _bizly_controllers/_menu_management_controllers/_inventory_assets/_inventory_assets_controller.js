const inventoryAssetsModel = require('../../../_bizly_models/_menu_management_models/_inventory_assets_models/_inventory_assets_model').inventoryAssetsModel
const equipmentModel = require('../../../_bizly_models/_menu_management_models/_inventory_assets_models/_equipment_model').equipmentModel
const attributesetModel = require('../../../_bizly_models/_menu_management_models/_liquor_models/_attribute_set_model').attributeSetModel

const addRootInventoryAssetsCategoryName = (req, res) => {
    inventoryAssetsModel.findOne({ rootCategoryName: req.body.rootCategoryName }, (err, data) => {
        if (err) {
            res.json({ sucess: false, msg: err })
        } else if (data) {
            res.json({ sucess: false, msg: "root category name already exists" })
        } else {
            var rootcategory = new inventoryAssetsModel()
            rootcategory.rootCategoryName = req.body.rootCategoryName
            rootcategory.addedBy = req.body.userid
            rootcategory.updatedBy = req.body.userid

            rootcategory.save((err) => {
                if (err) {
                    throw err
                } else {
                    res.json({ sucess: true, msg: 'rootcategory added' })
                }
            })
        }
    })

}

const addSubcategory = (req, res) => {
    inventoryAssetsModel.findOne({ 'subCategories.subCategoryName': req.body.subCategoryName }, (err, data) => {
        if (err) {
            res.json({ sucess: false, msg: err })
        } else if (data) {
            res.json({ sucess: false, msg: "sub category name already exists" })
        } else {
            inventoryAssetsModel.findByIdAndUpdate(req.params.rootcategoryid,
                { $push: { subCategories: req.body } },
                (err, usubcategory) => {
                    if (err) {
                        throw err
                    } else if (usubcategory == null) {
                        res.json({ sucess: false, msg: "rootcategory not found" })
                    } else {
                        res.json({ sucess: true, msg: "subcategory added" })
                    }
                })
        }
    })
}

const addSubSubCategories = (req, res) => {
    inventoryAssetsModel.findOne({ 'subCategories.subSubCategories.subSubCategoryName': req.body.subSubCategoryName }, (err, data) => {
        if (err) {
            res.json({ sucess: false, msg: err })
        } else if (data) {
            res.json({ sucess: false, msg: "sub sub category name already exists" })
        } else {
            inventoryAssetsModel.updateOne({ _id: req.params.rootcategoryid, "subCategories._id": req.params.subcategoryid },
                { $addToSet: { "subCategories.$.subSubCategories": { subSubCategoryName: req.body.subSubCategoryName } } },
                (err, usubsubcategory) => {
                    if (err) {
                        throw err
                    } else if (usubsubcategory == null) {
                        res.json({ sucess: false, msg: "rootcategory not found" })
                    } else {
                        res.json({ sucess: true, msg: "subsubcategory added" })
                    }
                })
        }
    })

}

const addSubSubCategoryType = (req, res) => {
    inventoryAssetsModel.findOne({ 'subCategories.subSubCategories.subSubCategoryType.subSubCategoryTypeName': req.body.subSubCategoryTypeName }, (err, data) => {
        if (err) {
            res.json({ sucess: false, msg: err })
        } else if (data) {
            res.json({ sucess: false, msg: "sub sub category type already exists" })
        } else {
            inventoryAssetsModel.updateOne(
                {
                    _id: req.params.rootcategoryid,
                    "subCategories.subSubCategories._id": req.params.subsubcategoryid
                },
                {
                    $push: {
                        "subCategories.$.subSubCategories.$[outer].subSubCategoryType": req.body

                    }
                },
                {
                    "arrayFilters": [
                        {
                            "outer._id": req.params.subsubcategoryid,
                        }
                    ]
                },
                (err, usubsubcategorytype) => {
                    if (err) {
                        throw err
                    } else if (usubsubcategorytype == null) {
                        res.json({ sucess: false, msg: "rootcategory not found" })
                    } else {
                        res.json({ sucess: true, msg: "subsubcategorytype added" })
                    }
                })
        }
    })
}

const getRootInventoryAssetsCategories = async (req, res) => {
    inventoryAssetsModel.findOne({ _id: req.params.rootcategoryid }).
        populate('addedBy', 'userName').
        populate('updatedBy', 'userName').
        exec(function (err, rootcategory) {
            if (err) {
                throw err
            } else if (rootcategory == null) {
                res.json({ sucess: false, msg: 'rootcategory not found' })
            } else {
                res.json({ sucess: true, msg: 'rootcategory found', data: rootcategory })
            }

        })
}

const getAllRootInventoryAssetsCategories = (req, res) => {
    inventoryAssetsModel.find().
        populate('addedBy', 'userName').
        populate('updatedBy', 'userName').
        exec(function (err, rootcategories) {
            if (err) {
                throw err
            } else if (rootcategories == null || rootcategories.length == 0) {
                res.json({ sucess: false, msg: "rootcategories Not Found" })
            } else {
                res.json({ sucess: true, msg: "rootcategories Found", data: rootcategories })
            }
        })
}

const updateRootInventoryAssetsCategoryName = (req, res) => {
    inventoryAssetsModel.findOneAndUpdate({ _id: req.params.rootcategoryid },
        { $set: { rootCategoryName: req.body.rootCategoryName } },
        (err, data) => {
            if (err) {
                res.json({ success: false, msg: err })
            } else if (data == null || data.length == 0) {
                res.json({ success: false, msg: "Data not found" })
            } else {
                equipmentModel.find({ "root.id": req.params.rootcategoryid })
                    .then((equipmentData) => {
                        promiseArr = []
                        for (let i = 0; i < equipmentData.length; i++) {
                            promiseArr.push(new Promise((resolve, reject) => {
                                if (equipmentData[i].root.id == req.params.rootcategoryid && equipmentData[i].category.id == req.params.rootcategoryid) {
                                    equipmentModel.findOneAndUpdate({ _id: equipmentData[i]._id },
                                        {
                                            $set: {
                                                "category.category": req.body.rootCategoryName,
                                                "root.root": req.body.rootCategoryName
                                            }
                                        }).then((Eudata) => {
                                            resolve(Eudata)
                                        }).catch((err) => {
                                            reject(err)
                                        })
                                } else {
                                    equipmentModel.findOneAndUpdate({ _id: equipmentData[i]._id },
                                        {
                                            $set: {
                                                "root.root": req.body.rootCategoryName
                                            }
                                        }).then((Eudata) => {
                                            resolve(Eudata)
                                        }).catch((err) => {
                                            reject(err)
                                        })
                                }
                            }))
                        }
                        Promise.all(promiseArr).then((promiseArr) => {
                            attributesetModel.find({ attributeType: "equipment" })
                                .then((attributeData) => {
                                    promiseArr2 = []
                                    for (let j = 0; j < attributeData.length; j++) {
                                        if (attributeData[j].categoryType.length > 0) {
                                            for (let k = 0; k < attributeData[j].categoryType.length; k++) {
                                                promiseArr2.push(new Promise((resolve, reject) => {
                                                    attributesetModel.findOneAndUpdate({ _id: attributeData[j]._id, "categoryType._id": req.params.rootcategoryid },
                                                        {
                                                            $set: {
                                                                "categoryType.$.rootCategoryName": req.body.rootCategoryName
                                                            }
                                                        }).then((Audata) => {
                                                            resolve(Audata)
                                                        }).catch((err) => {
                                                            reject(err)
                                                        })
                                                }))
                                            }
                                        } else {
                                            continue
                                        }
                                    }
                                    Promise.all(promiseArr2).then((promiseArr2) => {
                                        res.json({ success: true, msg: "Root name Updated" })
                                    }).catch((err) => {
                                        res.json({ success: false, msg: "error in promise arr 2", error: err })
                                    })
                                }).catch((err) => {
                                    res.json({ success: false, msg: "error in attribute data", error: err })
                                })
                        }).catch((err) => {
                            res.json({ success: false, msg: "error in promise data", error: err })
                        })
                    }).catch((err) => {
                        res.json({ success: false, msg: "error in equipment data", error: err })
                    })
            }
        })
}

const updateSubcategoryName = (req, res) => {
    inventoryAssetsModel.findOne({ _id: req.params.rootcategoryid })
        .then((result) => {
            let promiseArr = []
            for (let i = 0; i < result.subCategories.length; i++) {
                promiseArr.push(new Promise((resolve, reject) => {
                    inventoryAssetsModel.findOneAndUpdate({
                        _id: req.params.rootcategoryid,
                        "subCategories._id": req.params.subcategoryid
                    }, {
                            $set: {
                                "subCategories.$.subCategoryName": req.body.subCategoryName
                            }
                        }).then((udata) => {
                            resolve(udata)
                        }).catch((err) => {
                            reject(err)
                        })
                }))

            }
            Promise.all(promiseArr).then((promiseArr) => {
                equipmentModel.find({ "subcategory.id": req.params.subcategoryid })
                    .then((equipmentData) => {
                        promiseArr1 = []
                        for (let j = 0; j < equipmentData.length; j++) {
                            promiseArr1.push(new Promise((resolve, reject) => {
                                if (equipmentData[j].subcategory.id == req.params.subcategoryid && equipmentData[j].category.id == req.params.subcategoryid) {
                                    equipmentModel.findOneAndUpdate({ _id: equipmentData[j]._id },
                                        {
                                            $set: {
                                                "category.category": req.body.subCategoryName,
                                                "subcategory.subcategory": req.body.subCategoryName
                                            }
                                        }).then((Eudata) => {
                                            resolve(Eudata)
                                        }).catch((err) => {
                                            reject(err)
                                        })
                                } else {
                                    equipmentModel.findOneAndUpdate({ _id: equipmentData[j]._id },
                                        {
                                            $set: {
                                                "subcategory.subcategory": req.body.subCategoryName
                                            }
                                        }).then((Eudata) => {
                                            resolve(Eudata)
                                        }).catch((err) => {
                                            reject(err)
                                        })
                                }
                            }))
                        }
                        Promise.all(promiseArr1).then((promiseArr1) => {
                            attributesetModel.find({ attributeType: "equipment" })
                                .then((attributeData) => {
                                    promiseArr2 = []
                                    for (let k = 0; k < attributeData.length; k++) {
                                        if (attributeData[k].categoryType.length > 0) {
                                            for (let l = 0; l < attributeData[k].categoryType.length; l++) {
                                                if (attributeData[k].categoryType[l].subCategories.length > 0) {
                                                    for (let m = 0; m < attributeData[k].categoryType[l].subCategories.length; m++) {
                                                        promiseArr2.push(new Promise((resolve, reject) => {
                                                            attributesetModel.findOneAndUpdate({
                                                                _id: attributeData[k]._id,
                                                                "categoryType._id": req.params.rootcategoryid,
                                                                "categoryType.subCategories._id": req.params.subcategoryid
                                                            }, {
                                                                    $set: {
                                                                        "categoryType.$.subCategories.$[outer].subCategoryName": req.body.subCategoryName
                                                                    }
                                                                }, {
                                                                    "arrayFilters": [
                                                                        {
                                                                            "outer._id": req.params.subcategoryid
                                                                        }
                                                                    ]
                                                                }).then((Audata) => {
                                                                    resolve(Audata)
                                                                }).catch((err) => {
                                                                    reject(err)
                                                                })
                                                        }))
                                                    }
                                                } else {
                                                    continue
                                                }
                                            }
                                        } else {
                                            continue
                                        }
                                    }
                                    Promise.all(promiseArr2).then((promiseArr2) => {
                                        res.json({ success: true, msg: "Sub category name updated" })
                                    }).catch((err) => {
                                        res.json({ success: false, msg: "error in promise arr 2", error: err })
                                    })
                                }).catch((err) => {
                                    res.json({ success: false, msg: "error in attribute data", error: err })
                                })
                        }).catch((err) => {
                            res.json({ success: false, msg: "error in promise arr 1", error: err })
                        })
                    }).catch((err) => {
                        res.json({ success: false, msg: "error in equipment data", error: err })
                    })
            }).catch((err) => {
                res.json({ success: false, msg: "error in promise", error: err })
            })

        }).catch((err) => {
            res.json({ success: false, msg: "error in result", error: err })
        })
}

const updateSubSubCategoriesName = (req, res) => {
    inventoryAssetsModel.findOne({ _id: req.params.rootcategoryid })
        .then((result) => {
            let promiseArr = []
            for (let i = 0; i < result.subCategories.length; i++) {
                if (result.subCategories[i].subSubCategories.length > 0) {
                    for (let j = 0; j < result.subCategories[i].subSubCategories.length; j++) {
                        promiseArr.push(new Promise((resolve, reject) => {
                            inventoryAssetsModel.findOneAndUpdate({
                                _id: req.params.rootcategoryid,
                                "subCategories.subSubCategories._id": req.params.subsubcategoryid
                            }, {
                                    $set: {
                                        "subCategories.$.subSubCategories.$[outer].subSubCategoryName": req.body.subSubCategoryName
                                    }
                                },
                                {
                                    "arrayFilters": [
                                        {
                                            "outer._id": req.params.subsubcategoryid,
                                        }
                                    ]
                                }).then((udata) => {
                                    resolve(udata)
                                }).catch((err) => {
                                    reject(err)
                                })
                        }))
                    }
                } else {
                    continue;
                }
            }
            Promise.all(promiseArr).then((promiseArr) => {
                equipmentModel.find({ "subsubCategory.id": req.params.subsubcategoryid })
                    .then((equipmentData) => {
                        promiseArr1 = []
                        for (let k = 0; k < equipmentData.length; k++) {
                            promiseArr1.push(new Promise((resolve, reject) => {
                                if (equipmentData[k].subsubCategory.id == req.params.subsubcategoryid && equipmentData[k].category.id == req.params.subsubcategoryid) {
                                    equipmentModel.findOneAndUpdate({ _id: equipmentData[k]._id },
                                        {
                                            $set: {
                                                "category.category": req.body.subSubCategoryName,
                                                "subsubCategory.subsubCategory": req.body.subSubCategoryName
                                            }
                                        }).then((Eudata) => {
                                            resolve(Eudata)
                                        }).catch((err) => {
                                            reject(err)
                                        })
                                } else {
                                    equipmentModel.findOneAndUpdate({ _id: equipmentData[k]._id },
                                        {
                                            $set: {
                                                "subsubCategory.subsubCategory": req.body.subSubCategoryName
                                            }
                                        }).then((Eudata) => {
                                            resolve(Eudata)
                                        }).catch((err) => {
                                            reject(err)
                                        })
                                }
                            }))
                        }
                        Promise.all(promiseArr1).then((promiseArr1) => {
                            attributesetModel.find({ attributeType: "equipment" })
                                .then((attributeData) => {
                                    promiseArr2 = []
                                    for (let l = 0; l < attributeData.length; l++) {
                                        if (attributeData[l].categoryType.length > 0) {
                                            for (let m = 0; m < attributeData[l].categoryType.length; m++) {
                                                if (attributeData[l].categoryType[m].subCategories.length > 0) {
                                                    for (let n = 0; n < attributeData[l].categoryType[m].subCategories.length; n++) {
                                                        if (attributeData[l].categoryType[m].subCategories[n].subSubCategories.length > 0) {
                                                            for (let o = 0; o < attributeData[l].categoryType[m].subCategories[n].subSubCategories.length; o++) {
                                                                promiseArr2.push(new Promise((resolve, reject) => {
                                                                    attributesetModel.findOneAndUpdate({
                                                                        _id: attributeData[l]._id,
                                                                        "categoryType._id": req.params.rootcategoryid,
                                                                        "categoryType.subCategories._id": req.params.subcategoryid,
                                                                        "categoryType.subCategories.subSubCategories._id": req.params.subsubcategoryid
                                                                    }, {
                                                                            $set: {
                                                                                "categoryType.$.subCategories.$[outer].subSubCategories.$[inner].subSubCategoryName": req.body.subSubCategoryName
                                                                            }
                                                                        }, {
                                                                            "arrayFilters": [
                                                                                {
                                                                                    "outer._id": req.params.subcategoryid
                                                                                },
                                                                                {
                                                                                    "inner._id": req.params.subsubcategoryid
                                                                                }
                                                                            ]
                                                                        }).then((Audata) => {
                                                                            resolve(Audata)
                                                                        }).catch((err) => {
                                                                            reject(err)
                                                                        })
                                                                }))
                                                            }
                                                        } else {
                                                            continue
                                                        }
                                                    }
                                                } else {
                                                    continue
                                                }
                                            }
                                        } else {
                                            continue
                                        }
                                    }
                                    Promise.all(promiseArr2).then((promiseArr2) => {
                                        res.json({ success: true, msg: "Sub sub category name updated" })
                                    }).catch((err) => {
                                        res.json({ success: false, msg: "error in promise arr 2", error: err })
                                    })
                                }).catch((err) => {
                                    res.json({ success: false, msg: "error in attribute data", error: err })
                                })
                        }).catch((err) => {
                            res.json({ success: false, msg: "error in promise arr1", error: err })
                        })
                    }).catch((err) => {
                        res.json({ success: false, msg: "error in equipment data", error: err })
                    })
            }).catch((err) => {
                res.json({ success: false, msg: "error in promise", error: err })
            })

        }).catch((err) => {
            res.json({ success: false, msg: "error in result", error: err })
        })
}

const updateSubSubCategoryTypeName = (req, res) => {
    inventoryAssetsModel.findOne({ _id: req.params.rootcategoryid })
        .then((result) => {
            let promiseArr = []
            for (let i = 0; i < result.subCategories.length; i++) {
                if (result.subCategories[i].subSubCategories.length > 0) {
                    for (let j = 0; j < result.subCategories[i].subSubCategories.length; j++) {
                        if (result.subCategories[i].subSubCategories[j].subSubCategoryType.length > 0) {
                            for (let a = 0; a < result.subCategories[i].subSubCategories[j].subSubCategoryType.length; a++) {
                                promiseArr.push(new Promise((resolve, reject) => {
                                    inventoryAssetsModel.findOneAndUpdate({
                                        _id: req.params.rootcategoryid,
                                        "subCategories.subSubCategories._id": req.params.subsubcategoryid,
                                        "subCategories.subSubCategories.subSubCategoryType._id": req.params.subsubcategorytypeid
                                    }, {
                                            $set: {
                                                "subCategories.$.subSubCategories.$[outer].subSubCategoryType.$[inner].subSubCategoryTypeName": req.body.subSubCategoryTypeName
                                            }
                                        },
                                        {
                                            "arrayFilters": [
                                                {
                                                    "outer._id": req.params.subsubcategoryid,
                                                },
                                                {
                                                    "inner._id": req.params.subsubcategorytypeid
                                                }
                                            ]
                                        }).then((udata) => {
                                            resolve(udata)
                                        }).catch((err) => {
                                            reject(err)
                                        })
                                }))
                            }
                        } else {
                            continue;
                        }
                    }
                } else {
                    continue;
                }
            }
            Promise.all(promiseArr).then((promiseArr) => {
                equipmentModel.find({ "categoryType.id": req.params.subsubcategorytypeid })
                    .then((equipmentData) => {
                        promiseArr1 = []
                        for (let k = 0; k < equipmentData.length; k++) {
                            promiseArr1.push(new Promise((resolve, reject) => {
                                if (equipmentData[k].categoryType.id == req.params.subsubcategorytypeid && equipmentData[k].category.id == req.params.subsubcategorytypeid) {
                                    equipmentModel.findOneAndUpdate({ _id: equipmentData[k]._id },
                                        {
                                            $set: {
                                                "category.category": req.body.subSubCategoryTypeName,
                                                "categoryType.categoryType": req.body.subSubCategoryTypeName
                                            }
                                        }).then((Eudata) => {
                                            resolve(Eudata)
                                        }).catch((err) => {
                                            reject(err)
                                        })
                                } else {
                                    equipmentModel.findOneAndUpdate({ _id: equipmentData[k]._id },
                                        {
                                            $set: {
                                                "categoryType.categoryType": req.body.subSubCategoryTypeName
                                            }
                                        }).then((Eudata) => {
                                            resolve(Eudata)
                                        }).catch((err) => {
                                            reject(err)
                                        })
                                }
                            }))
                        }
                        Promise.all(promiseArr1).then((promiseArr1) => {
                            attributesetModel.find({ attributeType: "equipment" })
                                .then((attributeData) => {
                                    promiseArr2 = []
                                    for (let l = 0; l < attributeData.length; l++) {
                                        if (attributeData[l].categoryType.length > 0) {
                                            for (let m = 0; m < attributeData[l].categoryType.length; m++) {
                                                if (attributeData[l].categoryType[m].subCategories.length > 0) {
                                                    for (let n = 0; n < attributeData[l].categoryType[m].subCategories.length; n++) {
                                                        if (attributeData[l].categoryType[m].subCategories[n].subSubCategories.length > 0) {
                                                            for (let o = 0; o < attributeData[l].categoryType[m].subCategories[n].subSubCategories.length; o++) {
                                                                if (attributeData[l].categoryType[m].subCategories[n].subSubCategories[o].subSubCategoryType.length > 0) {
                                                                    for (let p = 0; p < attributeData[l].categoryType[m].subCategories[n].subSubCategories[o].subSubCategoryType.length; p++) {
                                                                        promiseArr2.push(new Promise((resolve, reject) => {
                                                                            attributesetModel.findOneAndUpdate({
                                                                                _id: attributeData[l]._id,
                                                                                "categoryType._id": req.params.rootcategoryid,
                                                                                "categoryType.subCategories._id": req.params.subcategoryid,
                                                                                "categoryType.subCategories.subSubCategories._id": req.params.subsubcategoryid,
                                                                                "categoryType.subCategories.subSubCategories.subSubCategoryType._id": req.params.subsubcategorytypeid
                                                                            }, {
                                                                                    $set: {
                                                                                        "categoryType.$.subCategories.$[outer].subSubCategories.$[inner].subSubCategoryType.$[middle].subSubCategoryTypeName": req.body.subSubCategoryTypeName
                                                                                    }
                                                                                }, {
                                                                                    "arrayFilters": [
                                                                                        {
                                                                                            "outer._id": req.params.subcategoryid
                                                                                        },
                                                                                        {
                                                                                            "inner._id": req.params.subsubcategoryid
                                                                                        },
                                                                                        {
                                                                                            "middle._id": req.params.subsubcategorytypeid
                                                                                        }
                                                                                    ]
                                                                                }).then((Audata) => {
                                                                                    resolve(Audata)
                                                                                }).catch((err) => {
                                                                                    reject(err)
                                                                                })
                                                                        }))
                                                                    }
                                                                } else {
                                                                    continue
                                                                }
                                                            }
                                                        } else {
                                                            continue
                                                        }
                                                    }
                                                } else {
                                                    continue
                                                }
                                            }
                                        } else {
                                            continue
                                        }
                                    }
                                    Promise.all(promiseArr2).then((promiseArr2) => {
                                        res.json({ success: true, msg: "Sub sub category type name updated" })
                                    }).catch((err) => {
                                        res.json({ success: false, msg: "error in promise arr 2", error: err })
                                    })
                                }).catch((err) => {
                                    res.json({ success: false, msg: "error in attribute data", error: err })
                                })
                        }).catch((err) => {
                            res.json({ success: false, msg: "error in promise arr 1", error: err })
                        })
                    }).catch((err) => {
                        res.json({ success: false, msg: "error in equipment data", error: err })
                    })
            }).catch((err) => {
                res.json({ success: false, msg: "error in promise", error: err })
            })

        }).catch((err) => {
            res.json({ success: false, msg: "error in result", error: err })
        })
}

module.exports = {
    addRootInventoryAssetsCategoryName,
    addSubcategory,
    addSubSubCategories,
    addSubSubCategoryType,
    getRootInventoryAssetsCategories,
    getAllRootInventoryAssetsCategories,
    updateRootInventoryAssetsCategoryName,
    updateSubcategoryName,
    updateSubSubCategoriesName,
    updateSubSubCategoryTypeName
}
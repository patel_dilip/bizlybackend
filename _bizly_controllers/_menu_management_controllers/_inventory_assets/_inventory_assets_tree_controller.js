const assetsInventoryTreeModel = require("../../../_bizly_models/_menu_management_models/_inventory_assets_models/_inventory_assets_tree")
  .assetsInventoryTreeModel;

const addTree = (req, res) => {
  var tree = new assetsInventoryTreeModel();
  tree.inventoryAssetsTree = req.body.inventoryAssetsTree;
  tree.addedBy = req.body.userid;
  tree.updatedBy = req.body.userid;

  tree.save((err) => {
    if (err) {
      res.json({ sucess: false, msg: err });
    } else {
      res.json({ sucess: true });
    }
  });
};

const getTree = async (req, res) => {
    assetsInventoryTreeModel
    .findOne()
    .populate("addedBy", "userName")
    .populate("updatedBy", "userName")
    .sort("-updatedAt")
    .exec(function (err, tree) {
      if (err) {
        res.json({ sucess: false, msg: err });
      } else if (tree == null) {
        res.json({ sucess: false });
      } else {
        res.json({ sucess: true, data: tree });
      }
    });
};

const updateTree = (req, res) => {
    assetsInventoryTreeModel.findByIdAndUpdate(
    req.params.treeid,
    {
      $set: { inventoryAssetsTree: req.body.inventoryAssetsTree, updatedBy: req.body.userid },
    },
    (err) => {
      if (err) {
        res.json({ sucess: false, msg: err });
      } else {
        res.json({ sucess: true });
      }
    }
  );
};

module.exports = {
  addTree,
  getTree,
  updateTree,
};
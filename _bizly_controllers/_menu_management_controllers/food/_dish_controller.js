const dishModel = require('../../../_bizly_models/_menu_management_models/_food_model/dish-model').dishmodel
const adonModel = require('../../../_bizly_models/_menu_management_models/_food_model/adon-model').adonmodel

const addDish = (req, res) => {
    dishModel.findOne({ dishName: req.body.dishName }, (err, data) => {
        if (err) {
            res.json({ sucess: false, msg: err })
        } else if (data) {
            res.json({ sucess: false, msg: "Dish already exists" })
        } else {
            var dish = new dishModel()
            dish.dishName = req.body.dishName
            dish.unit = req.body.unit
            dish.veg_nonveg = req.body.veg_nonveg
            dish.cuisines = req.body.cuisines
            dish.varients = req.body.varients
            dish.images = req.body.images
            dish.addedBy = req.body.userid
            dish.updatedBy = req.body.userid

            dish.save((err) => {
                if (err) {
                    throw err
                } else {
                    res.json({ sucess: true, msg: 'dish added' })
                }
            })
        }
    })

}

const getDish = async (req, res) => {
    dishModel.findOne({ _id: req.params.dishid }).
        populate('cuisines', 'cuisineName').
        populate('varients', 'varientName').
        populate('addedBy', 'userName').
        populate('updatedBy', 'userName').
        sort('-updatedAt').
        exec(function (err, dish) {
            if (err) {
                throw err
            } else if (dish == null) {
                res.json({ sucess: false, msg: 'dish not found' })
            } else {
                res.json({ sucess: true, msg: 'dish found', data: dish })
            }

        })
}

const getAllDishes = (req, res) => {
    dishModel.find().
        populate('cuisines', 'cuisineName').
        populate('varients', 'varientName').
        populate('addedBy', 'userName').
        populate('updatedBy', 'userName').
        sort('-updatedAt').
        exec(function (err, dishes) {
            if (err) {
                throw err
            } else if (dishes == null || dishes.length == 0) {
                res.json({ sucess: false, msg: "dishes Not Found" })
            } else {
                res.json({ sucess: true, msg: "dishes Found", data: dishes })
            }
        })
}

// check Dish Already Exists
const checkDishAlreadyExists = (req, res) => {
    dishModel.findOne({ dishName: req.params.dishName }, (err, data) => {
        if (err) {
            res.json({ success: false, msg: err })
        } else if (data) {
            res.json({ success: true, msg: "Dish Name already Exists" })
        } else {
            res.json({ success: false, msg: "Dish Name not available" })
        }
    })
}

const updateDish = (req, res) => {
    dishModel.findByIdAndUpdate(req.params.dishid,
        {
            $set: {
                dishName: req.body.dishName,
                unit: req.body.unit,
                veg_nonveg: req.body.veg_nonveg,
                cuisines: req.body.cuisines,
                varients: req.body.varients,
                images: req.body.images,
                updatedBy: req.body.userid,
                updatedAt: Date.now()
            }
        }, (err, udish) => {
            if (err) {
                throw err
            } else if (udish == null) {
                res.json({ sucess: false, msg: "dish not found" })
            } else {
                res.json({ sucess: true, msg: "dish updated" })
            }
        })
}

const deleteDish = (req, res) => {
    dishModel.findByIdAndDelete(req.params.dishid, (err) => {
        if (err) {
            throw err
        } else {
            res.json({ sucess: true, msg: "dish Deleted" })
        }
    })
}

const softDeleteDish = async (req, res) => {
    try {
        dishModel.findByIdAndUpdate(req.params.dishid, { $set: { status: req.body.status } })
            .then((dishData) => {
                let dishID = dishData._id
                adonModel.find({}, { dishes: 1, _id: 1 })
                    .then((adonData) => {
                        var promiseArr = []
                        for (let i = 0; i < adonData.length; i++) {
                            for (let j = 0; j < adonData[i].dishes.length; j++) {
                                if (JSON.stringify(adonData[i].dishes[j] == JSON.stringify(dishID))) {
                                    promiseArr.push(new Promise((resolve, reject) => {
                                        adonModel.updateOne({ _id: adonData[i]._id }, { $pull: { dishes: dishID } })
                                            .then((udata) => {
                                                resolve(udata)
                                            }).catch((err) => {
                                                reject(err)
                                            })
                                    }))
                                }
                            }
                        }
                        Promise.all(promiseArr).then((promiseArr) => {
                            res.json({ success: true, msg: "Dish Deleted" })
                        }).catch((err) => {
                            res.json({ success: false, msg: "error in promise arr", error: err })
                        })
                    }).catch((err) => {
                        res.json({ success: false, msg: "error in adon data", error: err })
                    })
            }).catch((err) => {
                res.json({ success: false, msg: "error in dish Data", error: err })
            })
    } catch (ex) {
        return res.json({ success: false, error: ex.message })
    }

}

module.exports = {
    addDish,
    getDish,
    getAllDishes,
    checkDishAlreadyExists,
    updateDish,
    deleteDish,
    softDeleteDish
}
const tagModel = require('../../../_bizly_models/_menu_management_models/_food_model/tags-model').tagmodel

const addTag = (req, res) => {
    tagModel.findOne({ tagName: req.body.tagName }, (err, data) => {
        if (err) {
            res.json({ success: false, msg: err })
        } else if (data) {
            res.json({ success: false, msg: "Tag Name already exists" })
        } else {
            var tag = new tagModel()
            tag.tagName = req.body.tagName
            tag.addedBy = req.body.userid
            tag.updatedBy = req.body.userid
            tag.save((err) => {
                if (err) {
                    throw err
                } else {
                    res.json({ sucess: true, msg: 'tag added' })
                }
            })
        }
    })
}

const getTag = async (req, res) => {
    tagModel.findOne({ _id: req.params.tagid }).
        sort('-updatedAt').
        exec(function (err, tag) {
            if (err) {
                throw err
            } else if (tag == null) {
                res.json({ sucess: false, msg: 'tag not found' })
            } else {
                res.json({ sucess: true, msg: 'tag found', data: tag })
            }

        })
}

const getAllTags = (req, res) => {
    tagModel.find().
        sort('-updatedAt').
        exec(function (err, tags) {
            if (err) {
                throw err
            } else if (tags == null || tags.length == 0) {
                res.json({ sucess: false, msg: "tags Not Found" })
            } else {
                res.json({ sucess: true, msg: "tags Found", data: tags })
            }
        })
}

// check tag Already Exists
const checkTagAlreadyExists = (req, res) => {
    tagModel.findOne({ tagName: req.params.tagName }, (err, data) => {
        if (err) {
            res.json({ success: false, msg: err })
        } else if (data) {
            res.json({ success: true, msg: "Tag Name already Exists" })
        } else {
            res.json({ success: false, msg: "Tag Name not available" })
        }
    })
}

const updateTag = (req, res) => {
    tagModel.findByIdAndUpdate(req.params.tagid,
        {
            $set: {
                tagName: req.body.tagName,
                updatedBy: req.body.userid,
                updatedAt: Date.now()
            }
        }, (err, utag) => {
            if (err) {
                throw err
            } else if (utag == null) {
                res.json({ sucess: false, msg: "tag not found" })
            } else {
                res.json({ sucess: true, msg: "tag updated" })
            }
        })
}

const deleteTag = (req, res) => {
    tagModel.findByIdAndDelete(req.params.tagid, (err) => {
        if (err) {
            res.json({ sucess: false, msg: "tag not found" })
        } else {
            res.json({ sucess: true, msg: "tag Deleted" })
        }
    })
}

module.exports = {
    addTag,
    getTag,
    getAllTags,
    checkTagAlreadyExists,
    tagModel,
    updateTag,
    deleteTag
}
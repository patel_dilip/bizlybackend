const cuisinesModel = require('../../../_bizly_models/_menu_management_models/_food_model/cusines-model').cuisinemodel
const dishModel = require('../../../_bizly_models/_menu_management_models/_food_model/dish-model').dishmodel
const categoryModel = require('../../../_bizly_models/_menu_management_models/_food_model/category-model').categorymodel
var csv = require('csv-express')

const addCuisine = (req, res) => {
    cuisinesModel.findOne({ cuisineName: req.body.cuisineName }, (err, data) => {
        if (err) {
            res.json({ sucess: false, msg: err })
        } else if (data) {
            res.json({ sucess: false, msg: "Cuisine already exists" })
        } else {
            var cuisine = new cuisinesModel()
            cuisine.cuisineName = req.body.cuisineName
            cuisine.country = req.body.country
            cuisine.state = req.body.state
            cuisine.city = req.body.city
            cuisine.combinationOfCusines = req.body.combinationOfCusines
            cuisine.images = req.body.images
            cuisine.addedBy = req.body.userid
            cuisine.updatedBy = req.body.userid
            cuisine.save((err) => {
                if (err) {
                    throw err
                } else {
                    res.json({ sucess: true, msg: 'cuisine added' })
                }
            })
        }
    })
}

const getCuisine = async (req, res) => {
    cuisinesModel.findOne({ _id: req.params.cuisineid }).
        populate('combinationOfCusines', 'cuisineName').
        populate('addedBy', 'userName').
        populate('updatedBy', 'userName').
        sort('-updatedAt').
        exec(function (err, cusine) {
            if (err) {
                throw err
            } else if (cusine == null) {
                res.json({ sucess: false, msg: 'cuisine not found' })
            } else {
                res.json({ sucess: true, msg: 'cuisine found', data: cusine })
            }

        })
}

const getAllCuisines = (req, res) => {
    cuisinesModel.find().
        populate('combinationOfCusines', 'cuisineName').
        populate('addedBy', 'userName').
        populate('updatedBy', 'userName').
        sort('-updatedAt').
        exec(function (err, cuisines) {
            if (err) {
                throw err
            } else if (cuisines == null || cuisines.length == 0) {
                res.json({ sucess: false, msg: "cuisines Not Found" })
            } else {
                res.json({ sucess: true, msg: "cuisines Found", data: cuisines })
            }
        })
}

// check Cuisine Already Exists
const checkCuisineAlreadyExists = (req, res) => {
    cuisinesModel.findOne({ cuisineName: req.params.cuisineName }, (err, data) => {
        if (err) {
            res.json({ success: false, msg: err })
        } else if (data) {
            res.json({ success: true, msg: "Cuisine Name already Exists" })
        } else {
            res.json({ success: false, msg: "Cuisine Name not available" })
        }
    })
}

const updateCuisine = (req, res) => {
    cuisinesModel.findByIdAndUpdate(req.params.cuisineid,
        {
            $set: {
                cuisineName: req.body.cuisineName,
                country: req.body.country,
                state: req.body.state,
                city: req.body.city,
                combinationOfCusines: req.body.combinationOfCusines,
                images: req.body.images,
                updatedBy: req.body.userid,
                updatedAt: Date.now()
            }
        }, (err, ucuisine) => {
            if (err) {
                throw err
            } else if (ucuisine == null) {
                res.json({ sucess: false, msg: "cuisine not found" })
            } else {
                res.json({ sucess: true, msg: "cuisine updated" })
            }
        })
}

const deleteCuisine = (req, res) => {
    cuisinesModel.findByIdAndDelete(req.params.cuisineid, (err, dcuisine) => {
        if (err) {
            throw err
        } else if (dcuisine == null) {
            res.json({ sucess: false, msg: "cuisine not found" })
        } else {
            res.json({ sucess: true, msg: "cuisine Deleted" })
        }
    })
}

const softDeleteCuisine = async (req, res) => {
    try {
        cuisinesModel.findByIdAndUpdate(req.params.cuisineid, { $set: { status: req.body.status } })
            .then((cuisineData) => {
                let cuisineID = cuisineData._id

                cuisinesModel.find({}, { combinationOfCusines: 1, _id: 1 })
                    .then((combinationData) => {
                        var promiseArr = []
                        for (let i = 0; i < combinationData.length; i++) {
                            for (let j = 0; j < combinationData[i].combinationOfCusines.length; j++) {
                                if (JSON.stringify(combinationData[i].combinationOfCusines[j]) === JSON.stringify(cuisineID)) {
                                    promiseArr.push(new Promise((resolve, reject) => {
                                        cuisinesModel.updateOne({ _id: combinationData[i]._id }, { $pull: { combinationOfCusines: cuisineID } })
                                            .then((udata) => {
                                                console.log("pulled")
                                                resolve(udata)
                                            }).catch((err) => {
                                                reject(err)
                                            })
                                    })
                                    )
                                } else {
                                    console.log("enter in else")
                                }
                            }
                        }

                        Promise.all(promiseArr).then((promiseArr) => {
                            dishModel.find({}, { cuisines: 1, _id: 1 })
                                .then((dishData) => {
                                    var promiseArr2 = []
                                    for (let i = 0; i < dishData.length; i++) {
                                        for (let j = 0; j < dishData[i].cuisines.length; j++) {
                                            if (JSON.stringify(dishData[i].cuisines[j]) === JSON.stringify(cuisineID)) {
                                                promiseArr2.push(new Promise((resolve, reject) => {
                                                    dishModel.updateOne({ _id: dishData[i]._id }, { $pull: { cuisines: cuisineID } })
                                                        .then((udata) => {
                                                            resolve(udata)
                                                        }).catch((err) => {
                                                            reject(err)
                                                        })
                                                }))
                                            }
                                        }
                                    }
                                    Promise.all(promiseArr2).then((promiseArr2) => {
                                        categoryModel.find({}, { cuisines: 1, _id: 1 })
                                            .then((categoryData) => {
                                                var promiseArr3 = []
                                                for (let i = 0; i < categoryData.length; i++) {
                                                    for (let j = 0; j < categoryData[i].cuisines.length; j++) {
                                                        if (JSON.stringify(categoryData[i].cuisines[j]) === JSON.stringify(cuisineID)) {
                                                            promiseArr3.push(new Promise((resolve, reject) => {
                                                                categoryModel.updateOne({ _id: categoryData[i]._id }, { $pull: { cuisines: cuisineID } })
                                                                    .then((udata) => {
                                                                        resolve(udata)
                                                                    }).catch((err) => {
                                                                        reject(err)
                                                                    })
                                                            }))
                                                        }
                                                    }
                                                }
                                                Promise.all(promiseArr3).then((promiseArr3) => {
                                                    res.json({ success: true, msg: "Cuisine Deleted" })
                                                }).catch((err) => {
                                                    return res.json({ success: false, msg: "err in promise arr 3", error: err })
                                                })
                                            }).catch((err) => {
                                                return res.json({ success: false, msg: "err in Category Data", error: err })
                                            })
                                    }).catch((err) => {
                                        return res.json({ success: false, msg: "err in promise arr 2", error: err })
                                    })
                                }).catch((err) => {
                                    return res.json({ success: false, msg: "err in Dish Data", error: err })
                                })
                        }).catch((err) => {
                            return res.json({ success: false, msg: "err in Promise arr", error: err })
                        })
                    }).catch((err) => {
                        return res.json({ success: false, msg: "err in combination Data", error: err })
                    })
            }).catch((err) => {
                return res.json({ success: false, msg: "err in cuisine Data", error: err })
            })
    } catch (ex) {
        return res.json({ success: false, msg: ex.message })
    }

}

const addExcelCuisines = (req, res) => {
    const importArr = req.body;
    let cusineArr = new Array();
    let promiseArr = new Array();
    importArr.forEach((element, index) => {
        element.combinationOfCusines.forEach((cusine, cusineIndex) => {
            // importCusineArray(cusine, cusineArr);
            cusineArr.push(cusine);
        })
    })

    cusineArr = [...new Set(cusineArr)];
    // console.log("cusineArr", cusineArr)
    promiseArr = cusineArr.map((element) => {
        return cuisinesModel.findOne({
            cuisineName: element
        }).then(result => {
            if (result == null) {
                const cuisine = new cuisinesModel();
                cuisine.cuisineName = element;
                cuisine.country = ''
                cuisine.state = ''
                cuisine.city = ''
                cuisine.combinationOfCusines = []
                cuisine.images = []
                cuisine.addedBy = null
                cuisine.updatedBy = null
                return cuisine.save((err) => {
                    if (err) {
                        throw err
                    } else {
                        // console.log('success to insert')  
                        Promise.resolve(true)
                    }
                })
            } else {
                Promise.resolve(false);
            }
        })
    })
    Promise.all(promiseArr)
        .then((result) => {
            console.log("result", result)
        })
        .catch((err) => {
            return res.json({
                error: true
            })
        });

}


module.exports = {
    addCuisine,
    getCuisine,
    getAllCuisines,
    checkCuisineAlreadyExists,
    updateCuisine,
    deleteCuisine,
    softDeleteCuisine,
    addExcelCuisines

}
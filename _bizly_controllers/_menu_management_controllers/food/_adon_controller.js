const adonModel = require('../../../_bizly_models/_menu_management_models/_food_model/adon-model').adonmodel

const addAdon = (req, res) => {
    adonModel.findOne({ adonName: req.body.adonName }, (err, data) => {
        if (err) {
            res.json({ success: false, msg: err })
        } else if (data) {
            res.json({ success: false, msg: "Adon already exists" })
        } else {
            var adon = new adonModel()
            adon.adonName = req.body.adonName
            adon.unit = req.body.unit
            adon.veg_nonveg = req.body.veg_nonveg
            adon.adonType = req.body.adonType
            adon.dishes = req.body.dishes
            adon.images = req.body.images
            adon.addedBy = req.body.userid
            adon.updatedBy = req.body.userid

            adon.save((err) => {
                if (err) {
                    throw err
                } else {
                    res.json({ sucess: true, msg: 'adon added' })
                }
            })
        }
    })

}

const getAdon = async (req, res) => {
    adonModel.findOne({ _id: req.params.adonid }).
        populate('dishes', 'dishName').
        populate('addedBy', 'userName').
        populate('updatedBy', 'userName').
        sort('-updatedAt').
        exec(function (err, adon) {
            if (err) {
                throw err
            } else if (adon == null) {
                res.json({ sucess: false, msg: 'adon not found' })
            } else {
                res.json({ sucess: true, msg: 'adon found', data: adon })
            }

        })
}

const getAllAdons = (req, res) => {
    adonModel.find().
        populate('dishes', 'dishName').
        populate('addedBy', 'userName').
        populate('updatedBy', 'userName').
        sort('-updatedAt').
        exec(function (err, adons) {
            if (err) {
                throw err
            } else if (adons == null || adons.length == 0) {
                res.json({ sucess: false, msg: "adns Not Found" })
            } else {
                res.json({ sucess: true, msg: "adons Found", data: adons })
            }
        })
}

// check Addon Already Exists
const checkAddonAlreadyExists = (req, res) => {
    adonModel.findOne({ adonName: req.params.adonName }, (err, data) => {
        if (err) {
            res.json({ success: false, msg: err })
        } else if (data) {
            res.json({ success: true, msg: "Adon Name already Exists" })
        } else {
            res.json({ success: false, msg: "Adon Name not available" })
        }
    })
}

const updateAdon = (req, res) => {
    adonModel.findByIdAndUpdate(req.params.adonid,
        {
            $set: {
                adonName: req.body.adonName,
                unit: req.body.unit,
                veg_nonveg: req.body.veg_nonveg,
                adonType: req.body.adonType,
                dishes: req.body.dishes,
                images: req.body.images,
                updatedBy: req.body.userid,
                updatedAt: Date.now()
            }
        }, (err, uadon) => {
            if (err) {
                throw err
            } else if (uadon == null) {
                res.json({ sucess: false, msg: "adon not found" })
            } else {
                res.json({ sucess: true, msg: "adon updated" })
            }
        })
}

const deleteAdon = (req, res) => {
    adonModel.findByIdAndDelete(req.params.adonid, (err) => {
        if (err) {
            throw err
        } else {
            res.json({ sucess: true, msg: "adon Deleted" })
        }
    })
}

const softDeleteAdon = (req, res) => {
    adonModel.findByIdAndUpdate(req.params.adonid, { $set: { status: req.body.status } }, (err) => {
        if (err) {
            res.json({ sucess: false, msg: "Adon not found" })
        } else {
            res.json({ sucess: true, msg: "Adon Status Change" })
        }
    })
}

module.exports = {
    addAdon,
    getAdon,
    getAllAdons,
    checkAddonAlreadyExists,
    updateAdon,
    deleteAdon,
    softDeleteAdon
}
const categoryModel = require('../../../_bizly_models/_menu_management_models/_food_model/category-model').categorymodel

const addCategory = (req, res) => {
    categoryModel.findOne({ categoryName: req.body.categoryName }, (err, data) => {
        if (err) {
            res.json({ success: false, msg: err })
        } else if (data) {
            res.json({ sucess: false, msg: "Category already exists" })
        } else {
            var category = new categoryModel()
            category.categoryName = req.body.categoryName
            category.cuisines = req.body.cuisines
            category.tags = req.body.tags
            category.images = req.body.images,
                category.addedBy = req.body.userid,
                category.updatedBy = req.body.userid

            category.save((err) => {
                if (err) {
                    throw err
                } else {
                    res.json({ sucess: true, msg: 'category added' })
                }
            })
        }
    })

}

const getCategory = async (req, res) => {
    categoryModel.findOne({ _id: req.params.categoryid }).
        populate('cuisines', 'cuisineName').
        populate('tags', 'tagName').
        populate('addedBy', 'userName').
        populate('updatedBy', 'userName').
        sort('-updatedAt').
        exec(function (err, category) {
            if (err) {
                throw err
            } else if (category == null) {
                res.json({ sucess: false, msg: 'category not found' })
            } else {
                res.json({ sucess: true, msg: 'category found', data: category })
            }

        })
}

const getAllCategories = (req, res) => {
    categoryModel.find().
        populate({ path: 'cuisines', select: 'cuisineName' }).
        populate({ path: 'tags', select: 'tagName' }).
        populate('addedBy', 'userName').
        populate('updatedBy', 'userName').
        sort('-updatedAt').
        exec(function (err, categories) {
            if (err) {
                throw err
            } else if (categories == null || categories.length == 0) {
                res.json({ sucess: false, msg: "categories Not Found" })
            } else {
                res.json({ sucess: true, msg: "categores Found", data: categories })
            }
        })
}

// check Category Already Exists
const checkCategoryAlreadyExists = (req, res) => {
    categoryModel.findOne({ categoryName: req.params.categoryName }, (err, data) => {
        if (err) {
            res.json({ success: false, msg: err })
        } else if (data) {
            res.json({ success: true, msg: "Category Name already Exists" })
        } else {
            res.json({ success: false, msg: "Category Name not available" })
        }
    })
}

const updateCategory = (req, res) => {
    categoryModel.findByIdAndUpdate(req.params.categoryid,
        {
            $set: {
                categoryName: req.body.categoryName,
                cuisines: req.body.cuisines,
                tags: req.body.tags,
                images: req.body.images,
                updatedBy: req.body.userid,
                updatedAt: Date.now()
            }
        }, (err, ucategory) => {
            if (err) {
                throw err
            } else if (ucategory == null) {
                res.json({ sucess: false, msg: "category not found" })
            } else {
                res.json({ sucess: true, msg: "category updated" })
            }
        })
}

const deleteCategory = (req, res) => {
    categoryModel.findByIdAndDelete(req.params.categoryid, (err) => {
        if (err) {
            throw err
        } else {
            res.json({ sucess: true, msg: "category Deleted" })
        }
    })
}

const softDeleteCategory = (req, res) => {
    categoryModel.findByIdAndUpdate(req.params.categoryid, { $set: { status: req.body.status } }, (err) => {
        if (err) {
            res.json({ sucess: false, msg: "Category not found" })
        } else {
            res.json({ sucess: true, msg: "Category Status Change" })
        }
    })
}

module.exports = {
    addCategory,
    getCategory,
    getAllCategories,
    checkCategoryAlreadyExists,
    updateCategory,
    deleteCategory,
    softDeleteCategory
}
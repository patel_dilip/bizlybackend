const varientContentModel = require('../../../_bizly_models/_menu_management_models/_food_model/varient_content-model').varientcontentmodel

const addContent = (req, res) => {
    var content = new varientContentModel(req.body)
    // content.contentName = req.body.contentName
    

    content.save((err) => {
        if (err) {
            throw err
        } else {
            res.json({ sucess: true, msg: 'content added' })
        }
    })
}

const getContent = async (req, res) => {
    varientContentModel.findOne({ _id: req.params.contentid }).
        sort('-updatedAt').
        exec(function (err, content) {
            if (err) {
                throw err
            } else if (content == null) {
                res.json({ sucess: false, msg: 'content not found' })
            } else {
                res.json({ sucess: true, msg: 'content found', data: content })
            }

        })
}

const getAllContents = (req, res) => {
    varientContentModel.find().
        sort('-updatedAt').
        exec(function (err, contents) {
            if (err) {
                throw err
            } else if (contents == null || contents.length == 0) {
                res.json({ sucess: false, msg: "contents Not Found" })
            } else {
                res.json({ sucess: true, msg: "contents Found", data: contents })
            }
        })
}

// check Varient Content Already Exists
const checkVarientContentAlreadyExists = (req, res) => {
    varientContentModel.findOne({ contentName: req.params.contentName }, (err, data) => {
        if (err) {
            res.json({ success: false, msg: err })
        } else if (data) {
            res.json({ success: true, msg: "Varient Content Name already Exists" })
        } else {
            res.json({ success: false, msg: "Varient Content Name not available" })
        }
    })
}

const updateContent = (req, res) => {
    varientContentModel.findByIdAndUpdate(req.params.contentid,
        {
            $set: {
                contentName: req.body.contentName,
                updatedBy: req.body.userid,
                updatedAt: Date.now()
            }
        }, (err, ucontent) => {
            if (err) {
                throw err
            } else if (ucontent == null) {
                res.json({ sucess: false, msg: "content not found" })
            } else {
                res.json({ sucess: true, msg: "content updated" })
            }
        })
}

const deleteContent = (req, res) => {
    varientContentModel.findByIdAndDelete(req.params.contentid, (err) => {
        if (err) {
            res.json({ sucess: false, msg: "content not found" })
        } else {
            res.json({ sucess: true, msg: "content Deleted" })
        }
    })
}

module.exports = {
    addContent,
    getContent,
    getAllContents,
    checkVarientContentAlreadyExists,
    updateContent,
    deleteContent
}
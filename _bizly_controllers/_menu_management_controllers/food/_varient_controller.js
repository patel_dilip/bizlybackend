const varientModel = require('../../../_bizly_models/_menu_management_models/_food_model/varient-model').varientmodel
const dishModel = require('../../../_bizly_models/_menu_management_models/_food_model/dish-model').dishmodel

const addVarient = (req, res) => {
    varientModel.findOne({ varientName: req.body.varientName }, (err, data) => {
        if (err) {
            res.json({ sucess: false, msg: err })
        } else if (data) {
            res.json({ sucess: false, msg: "Varient already exists" })
        } else {
            var varient = new varientModel()
            varient.varientName = req.body.varientName
            varient.varientContent = req.body.varientContent
            varient.varientType = req.body.varientType
            varient.images = req.body.images
            varient.addedBy = req.body.userid
            varient.updatedBy = req.body.userid

            varient.save((err) => {
                if (err) {
                    throw err
                } else {
                    res.json({ sucess: true, msg: 'varient added' })
                }
            })
        }
    })

}

const getVarient = async (req, res) => {
    varientModel.findOne({ _id: req.params.varientid }).
        populate('varientContent', 'contentName').
        populate('addedBy', 'userName').
        populate('updatedBy', 'userName').
        sort('-updatedAt').
        exec(function (err, varient) {
            if (err) {
                throw err
            } else if (varient == null) {
                res.json({ sucess: false, msg: 'varient not found' })
            } else {
                res.json({ sucess: true, msg: 'varient found', data: varient })
            }

        })
}

const getAllVarients = (req, res) => {
    varientModel.find().
        populate('varientContent', 'contentName').
        populate('addedBy', 'userName').
        populate('updatedBy', 'userName').
        sort('-updatedAt').
        exec(function (err, varients) {
            if (err) {
                throw err
            } else if (varients == null || varients.length == 0) {
                res.json({ sucess: false, msg: "varients Not Found" })
            } else {
                res.json({ sucess: true, msg: "varients Found", data: varients })
            }
        })
}

// check Varient Already Exists
const checkVarientAlreadyExists = (req, res) => {
    varientModel.findOne({ varientName: req.params.varientName }, (err, data) => {
        if (err) {
            res.json({ success: false, msg: err })
        } else if (data) {
            res.json({ success: true, msg: "Varient Name already Exists" })
        } else {
            res.json({ success: false, msg: "Varient Name not available" })
        }
    })
}

const updateVarient = (req, res) => {
    varientModel.findByIdAndUpdate(req.params.varientid,
        {
            $set: {
                varientName: req.body.varientName,
                varientContent: req.body.varientContent,
                varientType: req.body.varientType,
                images: req.body.images,
                updatedBy: req.body.userid,
                updatedAt: Date.now()
            }
        }, (err, ivarient) => {
            if (err) {
                throw err
            } else if (ivarient == null) {
                res.json({ sucess: false, msg: "varient not found" })
            } else {
                res.json({ sucess: true, msg: "varient updated" })
            }
        })
}

const deleteVarient = (req, res) => {
    varientModel.findByIdAndDelete(req.params.varientid, (err) => {
        if (err) {
            throw err
        } else {
            res.json({ sucess: true, msg: "varient Deleted" })
        }
    })
}

const softDeleteVarient = (req, res) => {
    try {
        varientModel.findByIdAndUpdate(req.params.varientid, { $set: { status: req.body.status } })
            .then((varientData) => {
                let varientID = varientData._id
                dishModel.find({}, { varients: 1, _id: 1 })
                    .then((varientsData) => {
                        var promiseArr = []
                        for (let i = 0; i < varientsData.length; i++) {
                            for (let j = 0; j < varientsData[i].varients.length; j++) {
                                if (JSON.stringify(varientsData[i].varients[j] == JSON.stringify(varientID))) {
                                    promiseArr.push(new Promise((resolve, reject) => {
                                        dishModel.updateOne({ _id: varientsData[i]._id }, { $pull: { varients: varientID } })
                                            .then((udata) => {
                                                resolve(udata)
                                            }).catch((err) => {
                                                reject(err)
                                            })
                                    }))
                                }
                            }
                        }
                        Promise.all(promiseArr).then((promiseArr) => {
                            res.json({ success: true, msg: "varient Deleted" })
                        }).catch((err) => {
                            res.json({ success: false, msg: "error in promise arr", error: err })
                        })
                    }).catch((err) => {
                        res.json({ success: false, msg: "error in varients data", error: err })
                    })
            }).catch((err) => {
                res.json({ success: false, msg: "error in varient Data", error: err })
            })
    } catch (ex) {
        return res.json({ success: false, error: ex.message })
    }

}
module.exports = {
    addVarient,
    getVarient,
    getAllVarients,
    checkVarientAlreadyExists,
    updateVarient,
    deleteVarient,
    softDeleteVarient
}
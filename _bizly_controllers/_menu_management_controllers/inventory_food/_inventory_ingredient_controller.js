const inventoryIngredientmodel = require('../../../_bizly_models/_menu_management_models/_inventory_food_models/_inventory_ingredient_model').inventoryIngredientmodel

//Add Inventory Ingredient Data
const addIngredient = (req, res) => {
    inventoryIngredientmodel.findOne({ ingredientName: req.body.ingredientName }, (err, data) => {
        if (err) {
            res.json({ success: false, msg: err })
        } else if (data) {
            res.json({ success: false, msg: "Inventory ingredient already exists" })
        } else {
            var ingredient = new inventoryIngredientmodel()
            ingredient.ingredientName = req.body.ingredientName
            ingredient.ingredientcategory = req.body.ingredientcategory
            ingredient.unit = req.body.unit
            ingredient.root = req.body.root
            ingredient.sub = req.body.sub
            ingredient.subsub = req.body.subsub
            ingredient.type = req.body.type
            ingredient.measurement = req.body.measurement
            ingredient.addedBy = req.body.userid
            ingredient.updatedBy = req.body.userid

            ingredient.save((err) => {
                if (err) {
                    throw err
                } else {
                    res.json({ sucess: true, msg: 'Ingredient  added' })
                }
            })
        }
    })
}

// Get Inventory Ingredient Data by ingredientid
const getIngredient = (req, res) => {
    inventoryIngredientmodel.findById(req.params.ingredientid).
        populate('addedBy', 'userName').
        populate('updatedBy', 'userName').
        sort('-updatedAt').
        exec((err, ingredient) => {
            if (err) {
                throw err
            } else if (ingredient == null) {
                res.json({ sucess: false, msg: 'ingredient not found' })
            } else {
                res.json({ sucess: true, msg: ' ingredient found', data: ingredient })
            }
        })
}

//Get all Inventory Ingredient Data
const getAllIngredient = (req, res) => {
    inventoryIngredientmodel.find().
        populate('addedBy', 'userName').
        populate('updatedBy', 'userName').
        sort('-updatedAt').
        exec((err, data) => {
            if (err) {
                throw err
            } else if (data == null || data.length == 0) {
                res.json({ sucess: false, msg: 'Ingredient not found' })
            } else {
                res.json({ sucess: true, msg: ' Ingredient found', data: data })
            }
        })
}

// check Inventroy Ingredient Already Exists
const checkInevntoryIngredientAlreadyExists = (req, res) => {
    inventoryIngredientmodel.findOne({ ingredientName: req.params.ingredientName }, (err, data) => {
        if (err) {
            res.json({ success: false, msg: err })
        } else if (data) {
            res.json({ success: true, msg: "Ingredient Name already Exists" })
        } else {
            res.json({ success: false, msg: "Ingredient Name not available" })
        }
    })
}

//Update Inventroy Ingredient Data
const updateIngredient = (req, res) => {
    inventoryIngredientmodel.findByIdAndUpdate(req.params.ingredientid,
        {
            $set: {
                ingredientName: req.body.ingredientName,
                ingredientcategory: req.body.ingredientcategory,
                unit: req.body.unit,
                root: req.body.root,
                sub: req.body.sub,
                subsub: req.body.subsub,
                type: req.body.type,
                measurement: req.body.measurement,
                updatedBy: req.body.userid,
                updatedAt: Date.now()
            }
        }, (err, uingredient) => {
            if (err) {
                throw err
            } else if (uingredient == null) {
                res.json({ sucess: false, msg: "ingredient not found" })
            } else {
                res.json({ sucess: true, msg: "ingredient updated" })
            }
        })
}

//Delete Inventory Ingredient Data
const deleteIngredient = (req, res) => {
    inventoryIngredientmodel.findByIdAndDelete(req.params.ingredientid, (err) => {
        if (err) {
            res.json({ sucess: false, msg: "Ingredient not found" })
        } else {
            res.json({ sucess: true, msg: "Ingredient Deleted" })
        }
    })
}

//Soft Delete of Inventory Ingredient
const softDeleteIngredient = (req, res) => {
    inventoryIngredientmodel.findByIdAndUpdate(req.params.ingredientid, { $set: { status: req.body.status } }, (err) => {
        if (err) {
            res.json({ sucess: false, msg: "Ingredient not found" })
        } else {
            res.json({ sucess: true, msg: "Ingredient soft Deleted" })
        }
    })
}

const getIngredientUnit = (req, res) => {
    inventoryIngredientmodel.findById(req.params.ingredientid, (err, data) => {
        if (err) {
            res.json({ success: false, msg: err })
        } else if (data == null || data.length == 0) {
            res.json({ success: false})
        } else {
            res.json({ success: true, data: data })
        }
    }).select('unit')
}


module.exports = {
    addIngredient,
    getIngredient,
    getAllIngredient,
    checkInevntoryIngredientAlreadyExists,
    updateIngredient,
    deleteIngredient,
    softDeleteIngredient,
    getIngredientUnit
}
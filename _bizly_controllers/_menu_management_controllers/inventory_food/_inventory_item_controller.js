const innventoryItemModel = require('../../../_bizly_models/_menu_management_models/_inventory_food_models/_inventory_item_model').innventoryItemModel

//Add Inventory Item Data
const addInventoryItem = (req, res) => {
    innventoryItemModel.findOne({ itemName: req.body.itemName }, (err, data) => {
        if (err) {
            res.json({ success: false, msg: err })
        } else if (data) {
            res.json({ success: false, msg: "Inventroy item name already exists" })
        } else {
            var inventoryItem = new innventoryItemModel()
            inventoryItem.itemName = req.body.itemName
            inventoryItem.brand = req.body.brand
            inventoryItem.ingredient = req.body.ingredient
            inventoryItem.quantity = req.body.quantity
            inventoryItem.measurement = req.body.measurement
            inventoryItem.addedBy = req.body.userid
            inventoryItem.updatedBy = req.body.userid

            inventoryItem.save((err) => {
                if (err) {
                    throw err
                } else {
                    res.json({ sucess: true, msg: 'inventory Item added' })
                }
            })
        }
    })
}

//Get Inventory Item by inventoryitemid
const getInventoryItem = (req, res) => {
    innventoryItemModel.findById(req.params.inventoryitemid).
        populate('brand', 'BrandName').
        populate('ingredient', 'ingredientName').
        populate('addedBy', 'userName').
        populate('updatedBy', 'userName').
        sort('-updatedAt').
        exec((err, inventoryItem) => {
            if (err) {
                throw err
            } else if (inventoryItem == null) {
                res.json({ sucess: false, msg: 'inventory Item not found' })
            } else {
                res.json({ sucess: true, msg: ' inventory Item found', data: inventoryItem })
            }
        })
}

//Get All Inventory Item Data
const getAllInventoryItem = (req, res) => {
    innventoryItemModel.find().
        populate('brand', 'BrandName').
        populate('ingredient', 'ingredientName').
        populate('addedBy', 'userName').
        populate('updatedBy', 'userName').
        sort('-updatedAt').
        exec((err, data) => {
            if (err) {
                throw err
            } else if (data == null || data.length == 0) {
                res.json({ sucess: false, msg: 'innventory Item not found' })
            } else {
                res.json({ sucess: true, msg: ' innventory Item found', data: data })
            }
        })
}

// check Inventroy Item Already Exists
const checkInevntoryItemAlreadyExists = (req, res) => {
    innventoryItemModel.findOne({ itemName: req.params.itemName }, (err, data) => {
        if (err) {
            res.json({ success: false, msg: err })
        } else if (data) {
            res.json({ success: true, msg: "Item Name already Exists" })
        } else {
            res.json({ success: false, msg: "Item Name not available" })
        }
    })
}

//Update Inventory item data
const updateInventoryItem = (req, res) => {
    innventoryItemModel.findByIdAndUpdate(req.params.inventoryitemid,
        {
            $set: {
                itemName: req.body.itemName,
                brand: req.body.brand,
                ingredient: req.body.ingredient,
                quantity: req.body.quantity,
                measurement: req.body.measurement,
                updatedBy: req.body.userid,
                updatedAt: Date.now()
            }
        }, (err, uinventoryItem) => {
            if (err) {
                throw err
            } else if (uinventoryItem == null) {
                res.json({ sucess: false, msg: " inventory Item not found" })
            } else {
                res.json({ sucess: true, msg: "inventory Item updated" })
            }
        })
}

//Delete Inventory Item Data
const deleteInventoryItem = (req, res) => {
    innventoryItemModel.findByIdAndDelete(req.params.inventoryitemid, (err) => {
        if (err) {
            res.json({ sucess: false, msg: "inventory Item not found" })
        } else {
            res.json({ sucess: true, msg: "inventory Item Deleted" })
        }
    })
}

//Soft Delete Inventory Item Data
const softDeleteInventoryItem = (req, res) => {
    innventoryItemModel.findByIdAndUpdate(req.params.inventoryitemid, { $set: { status: req.body.status } }, (err) => {
        if (err) {
            res.json({ sucess: false, msg: "Inventory item not found" })
        } else {
            res.json({ sucess: true, msg: "Inventory item Deleted" })
        }
    })
}
module.exports = {
    addInventoryItem,
    getInventoryItem,
    getAllInventoryItem,
    checkInevntoryItemAlreadyExists,
    updateInventoryItem,
    deleteInventoryItem,
    softDeleteInventoryItem
}
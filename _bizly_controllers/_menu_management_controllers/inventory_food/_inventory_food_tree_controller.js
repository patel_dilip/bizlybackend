const foodInventoryTreeModel = require("../../../_bizly_models/_menu_management_models/_inventory_food_models/_inventory_food_tree")
  .foodInventoryTreeModel;

const addTree = (req, res) => {
  var tree = new foodInventoryTreeModel();
  tree.foodInventoryTree = req.body.foodInventoryTree;
  tree.addedBy = req.body.userid;
  tree.updatedBy = req.body.userid;

  tree.save((err) => {
    if (err) {
      res.json({ sucess: false, msg: err });
    } else {
      res.json({ sucess: true });
    }
  });
};

const getTree = async (req, res) => {
    foodInventoryTreeModel
    .findOne()
    .populate("addedBy", "userName")
    .populate("updatedBy", "userName")
    .sort("-updatedAt")
    .exec(function (err, tree) {
      if (err) {
        res.json({ sucess: false, msg: err });
      } else if (tree == null) {
        res.json({ sucess: false });
      } else {
        res.json({ sucess: true, data: tree });
      }
    });
};

const updateTree = (req, res) => {
    foodInventoryTreeModel.findByIdAndUpdate(
    req.params.treeid,
    {
      $set: { foodInventoryTree: req.body.foodInventoryTree, updatedBy: req.body.userid },
    },
    (err) => {
      if (err) {
        res.json({ sucess: false, msg: err });
      } else {
        res.json({ sucess: true });
      }
    }
  );
};

module.exports = {
  addTree,
  getTree,
  updateTree,
};

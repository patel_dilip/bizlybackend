const inventoryBrandModel = require('../../../_bizly_models/_menu_management_models/_inventory_food_models/_inventory_brand_model').inventoryBrandModel

// Add Inventory Brand 
const addInventoryBrand = (req, res) => {
    inventoryBrandModel.findOne({ BrandName: req.body.BrandName }, (err, data) => {
        if (err) {
            res.json({ success: false, msg: err })
        } else if (data) {
            res.json({ success: false, msg: "Inventory brand already exists" })
        } else {
            var inventoryBrand = new inventoryBrandModel()
            inventoryBrand.BrandName = req.body.BrandName
            inventoryBrand.addedBy = req.body.userid
            inventoryBrand.updatedBy = req.body.userid

            inventoryBrand.save((err) => {
                if (err) {
                    throw err
                } else {
                    res.json({ sucess: true, msg: 'inventoryBrand added' })
                }
            })
        }
    })
}

//Get Inventory Brand data By inventorybrandid
const getInventoryBrand = (req, res) => {
    inventoryBrandModel.findById(req.params.inventorybrandid).
        populate('addedBy', 'userName').
        populate('updatedBy', 'userName').
        exec((err, inventoryBrand) => {
            if (err) {
                throw err
            } else if (inventoryBrand == null) {
                res.json({ sucess: false, msg: 'inventoryBrand not found' })
            } else {
                res.json({ sucess: true, msg: ' inventoryBrand found', data: inventoryBrand })
            }
        })
}

//Get All Inventory Brand data
const getAllInventoryBrand = (req, res) => {
    inventoryBrandModel.find().
        populate('addedBy', 'userName').
        populate('updatedBy', 'userName').
        exec((err, data) => {
            if (err) {
                throw err
            } else if (data == null || data.length == 0) {
                res.json({ sucess: false, msg: 'inventoryBrand not found' })
            } else {
                res.json({ sucess: true, msg: ' inventoryBrand found', data: data })
            }
        })
}

// check Inventroy Brand Already Exists
const checkInevntoryBrandAlreadyExists = (req, res) => {
    inventoryBrandModel.findOne({ BrandName: req.params.BrandName }, (err, data) => {
        if (err) {
            res.json({ success: false, msg: err })
        } else if (data) {
            res.json({ success: true, msg: "Brand Name already Exists" })
        } else {
            res.json({ success: false, msg: "Brand Name not available" })
        }
    })
}

//update Inventory Brand Data
const updateInventoryBrand = (req, res) => {
    inventoryBrandModel.findByIdAndUpdate(req.params.inventorybrandid, req.body, (err, uinventorybrand) => {
        if (err) {
            throw err
        } else if (uinventorybrand == null) {
            res.json({ sucess: false, msg: "inventoryBrand not found" })
        } else {
            res.json({ sucess: true, msg: "inventoryBrand updated" })
        }
    })
}

// Delete Inventory Brand Data
const deleteInventoryBrand = (req, res) => {
    inventoryBrandModel.findByIdAndDelete(req.params.inventorybrandid, (err) => {
        if (err) {
            res.json({ sucess: false, msg: "inventoryBrand not found" })
        } else {
            res.json({ sucess: true, msg: "inventoryBrand Deleted" })
        }
    })
}

module.exports = {
    addInventoryBrand,
    getInventoryBrand,
    getAllInventoryBrand,
    checkInevntoryBrandAlreadyExists,
    updateInventoryBrand,
    deleteInventoryBrand
}
const inventoryFoodModel = require('../../../_bizly_models/_menu_management_models/_inventory_food_models/_inventory_food_model').inventoryfoodmodel
const ingredientModel = require('../../../_bizly_models/_menu_management_models/_inventory_food_models/_inventory_ingredient_model').inventoryIngredientmodel
//Add Root Inventory Food Category Name
const addRootInventoryFoodCategoryName = (req, res) => {
    inventoryFoodModel.findOne({ rootCategoryName: req.body.rootCategoryName }, (err, data) => {
        if (err) {
            res.json({ sucess: false, msg: err })
        } else if (data) {
            res.json({ sucess: false, msg: "root category name already exists" })
        } else {
            var rootcategory = new inventoryFoodModel()
            rootcategory.rootCategoryName = req.body.rootCategoryName
            rootcategory.addedBy = req.body.userid
            rootcategory.updatedBy = req.body.userid

            rootcategory.save((err) => {
                if (err) {
                    throw err
                } else {
                    res.json({ sucess: true, msg: 'rootcategory added' })
                }
            })
        }
    })
}

//Add Sub Category of Inventory Food
const addSubcategory = (req, res) => {
    inventoryFoodModel.findOne({ 'subCategories.subCategoryName': req.body.subCategoryName }, (err, data) => {
        if (err) {
            res.json({ sucess: false, msg: err })
        } else if (data) {
            res.json({ sucess: false, msg: "sub category name already exists" })
        } else {
            inventoryFoodModel.findByIdAndUpdate(req.params.rootcategoryid,
                { $push: { subCategories: req.body } },
                (err, usubcategory) => {
                    if (err) {
                        throw err
                    } else if (usubcategory == null) {
                        res.json({ sucess: false, msg: "rootcategory not found" })
                    } else {
                        res.json({ sucess: true, msg: "subcategory added" })
                    }
                })
        }
    })
}

//Add Sub Sub Category of Inventory Food
const addSubSubCategories = (req, res) => {
    inventoryFoodModel.findOne({ 'subCategories.subSubCategories.subSubCategoryName': req.body.subSubCategoryName }, (err, data) => {
        if (err) {
            res.json({ sucess: false, msg: err })
        } else if (data) {
            res.json({ sucess: false, msg: "sub sub category name already exists" })
        } else {
            inventoryFoodModel.updateOne({ _id: req.params.rootcategoryid, "subCategories._id": req.params.subcategoryid },
                { $addToSet: { "subCategories.$.subSubCategories": { subSubCategoryName: req.body.subSubCategoryName } } },
                (err, usubsubcategory) => {
                    if (err) {
                        throw err
                    } else if (usubsubcategory == null) {
                        res.json({ sucess: false, msg: "rootcategory not found" })
                    } else {
                        res.json({ sucess: true, msg: "subsubcategory added" })
                    }
                })
        }
    })

}

//Add Sub Sub Category Type of Inventory Food
const addSubSubCategoryType = (req, res) => {

    inventoryFoodModel.findOne({ 'subCategories.subSubCategories.subSubCategoryType.subSubCategoryTypeName': req.body.subSubCategoryTypeName }, (err, data) => {
        if (err) {
            res.json({ sucess: false, msg: err })
        } else if (data) {
            res.json({ sucess: false, msg: "sub sub category type already exists" })
        } else {
            inventoryFoodModel.updateOne(
                {
                    _id: req.params.rootcategoryid,
                    "subCategories.subSubCategories._id": req.params.subsubcategoryid
                },
                {
                    $push: {
                        "subCategories.$.subSubCategories.$[outer].subSubCategoryType": req.body

                    }
                },
                {
                    "arrayFilters": [
                        {
                            "outer._id": req.params.subsubcategoryid,
                        }
                    ]
                },
                (err, usubsubcategorytype) => {
                    if (err) {
                        throw err
                    } else if (usubsubcategorytype == null) {
                        res.json({ sucess: false, msg: "rootcategory not found" })
                    } else {
                        res.json({ sucess: true, msg: "subsubcategorytype added" })
                    }
                })
        }
    })
}

// Get Root Inventory Food Category Data by rootcategoryid
const getRootInventoryFoodCategories = async (req, res) => {
    inventoryFoodModel.findOne({ _id: req.params.rootcategoryid }).
        populate('addedBy', 'userName').
        populate('updatedBy', 'userName').
        exec(function (err, rootcategory) {
            if (err) {
                throw err
            } else if (rootcategory == null) {
                res.json({ sucess: false, msg: 'rootcategory not found' })
            } else {
                res.json({ sucess: true, msg: 'rootcategory found', data: rootcategory })
            }

        })
}

//Get All Root Inventory Food Categories Data
const getAllRootInventoryFoodCategories = (req, res) => {
    inventoryFoodModel.find().
        populate('addedBy', 'userName').
        populate('updatedBy', 'userName').
        exec(function (err, rootcategories) {
            if (err) {
                throw err
            } else if (rootcategories == null || rootcategories.length == 0) {
                res.json({ sucess: false, msg: "rootcategories Not Found" })
            } else {
                res.json({ sucess: true, msg: "rootcategories Found", data: rootcategories })
            }
        })
}

const updateRootInventoryFoodCategoryName = (req, res) => {
    inventoryFoodModel.findOneAndUpdate({ _id: req.params.rootcategoryid },
        { $set: { rootCategoryName: req.body.rootCategoryName } },
        (err, data) => {
            if (err) {
                res.json({ success: false, msg: err })
            } else if (data == null || data.length == 0) {
                res.json({ success: false, msg: "Data not found" })
            } else {
                ingredientModel.find({ "root.id": req.params.rootcategoryid })
                    .then((ingredientData) => {
                        promiseArr1 = []
                        for (let i = 0; i < ingredientData.length; i++) {
                            promiseArr1.push(new Promise((resolve, reject) => {
                                if (ingredientData[i].root.id == req.params.rootcategoryid && ingredientData[i].ingredientcategory.id == req.params.rootcategoryid) {
                                    ingredientModel.findOneAndUpdate({ _id: ingredientData[i]._id },
                                        {
                                            $set: {
                                                "ingredientcategory.category": req.body.rootCategoryName,
                                                "root.root": req.body.rootCategoryName
                                            }
                                        }).then((Iudata) => {
                                            resolve(Iudata)
                                        }).catch((err) => {
                                            reject(err)
                                        })
                                } else {
                                    ingredientModel.findOneAndUpdate({ _id: ingredientData[i]._id },
                                        {
                                            $set: {
                                                "root.root": req.body.rootCategoryName
                                            }
                                        }).then((Iudata) => {
                                            resolve(Iudata)
                                        }).catch((err) => {
                                            reject(err)
                                        })
                                }
                            }))
                        }
                        Promise.all(promiseArr1).then((promiseArr1) => {
                            res.json({ success: true, msg: "Root name Updated" })
                        }).catch((err) => {
                            res.json({ success: false, msg: "error in promise arr 1", error: err })
                        })
                    }).catch((err) => {
                        res.json({ success: false, msg: "error in ingredient data", error: err })
                    })
            }
        })
}

const updateSubcategoryName = (req, res) => {
    inventoryFoodModel.findOne({ _id: req.params.rootcategoryid })
        .then((result) => {
            let promiseArr = []
            for (let i = 0; i < result.subCategories.length; i++) {
                promiseArr.push(new Promise((resolve, reject) => {
                    inventoryFoodModel.findOneAndUpdate({
                        _id: req.params.rootcategoryid,
                        "subCategories._id": req.params.subcategoryid
                    }, {
                            $set: {
                                "subCategories.$.subCategoryName": req.body.subCategoryName
                            }
                        }).then((udata) => {
                            resolve(udata)
                        }).catch((err) => {
                            reject(err)
                        })
                }))

            }
            Promise.all(promiseArr).then((promiseArr) => {
                ingredientModel.find({ "sub.id": req.params.subcategoryid })
                    .then((ingredientData) => {
                        promiseArr1 = []
                        for (let i = 0; i < ingredientData.length; i++) {
                            promiseArr1.push(new Promise((resolve, reject) => {
                                if (ingredientData[i].sub.id == req.params.subcategoryid && ingredientData[i].ingredientcategory.id == req.params.subcategoryid) {
                                    ingredientModel.findOneAndUpdate({ _id: ingredientData[i]._id },
                                        {
                                            $set: {
                                                "ingredientcategory.category": req.body.subCategoryName,
                                                "sub.sub": req.body.subCategoryName
                                            }
                                        }).then((Iudata) => {
                                            resolve(Iudata)
                                        }).catch((err) => {
                                            reject(err)
                                        })
                                } else {
                                    ingredientModel.findOneAndUpdate({ _id: ingredientData[i]._id },
                                        {
                                            $set: {
                                                "sub.sub": req.body.subCategoryName
                                            }
                                        }).then((Iudata) => {
                                            resolve(Iudata)
                                        }).catch((err) => {
                                            reject(err)
                                        })
                                }
                            }))
                        }
                        Promise.all(promiseArr1).then((promiseArr1) => {
                            res.json({ success: true, msg: "Sub category name updated" })
                        }).catch((err) => {
                            res.json({ success: false, msg: "error in promise arr 1", error: err })
                        })
                    }).catch((err) => {
                        res.json({ success: false, msg: "error in ingredient data", error: err })
                    })
            }).catch((err) => {
                res.json({ success: false, msg: "error in promise", error: err })
            })

        }).catch((err) => {
            res.json({ success: false, msg: "error in result", error: err })
        })
}

const updateSubSubCategoriesName = (req, res) => {
    inventoryFoodModel.findOne({ _id: req.params.rootcategoryid })
        .then((result) => {
            let promiseArr = []
            for (let i = 0; i < result.subCategories.length; i++) {
                if (result.subCategories[i].subSubCategories.length > 0) {
                    for (let j = 0; j < result.subCategories[i].subSubCategories.length; j++) {
                        promiseArr.push(new Promise((resolve, reject) => {
                            inventoryFoodModel.findOneAndUpdate({
                                _id: req.params.rootcategoryid,
                                "subCategories.subSubCategories._id": req.params.subsubcategoryid
                            }, {
                                    $set: {
                                        "subCategories.$.subSubCategories.$[outer].subSubCategoryName": req.body.subSubCategoryName
                                    }
                                },
                                {
                                    "arrayFilters": [
                                        {
                                            "outer._id": req.params.subsubcategoryid,
                                        }
                                    ]
                                }).then((udata) => {
                                    resolve(udata)
                                }).catch((err) => {
                                    reject(err)
                                })
                        }))
                    }
                } else {
                    continue;
                }
            }
            Promise.all(promiseArr).then((promiseArr) => {
                ingredientModel.find({ "subsub.id": req.params.subsubcategoryid })
                    .then((ingredientData) => {
                        promiseArr1 = []
                        for (let i = 0; i < ingredientData.length; i++) {
                            promiseArr1.push(new Promise((resolve, reject) => {
                                if (ingredientData[i].subsub.id == req.params.subsubcategoryid && ingredientData[i].ingredientcategory.id == req.params.subsubcategoryid) {
                                    ingredientModel.findOneAndUpdate({ _id: ingredientData[i]._id },
                                        {
                                            $set: {
                                                "ingredientcategory.category": req.body.subSubCategoryName,
                                                "subsub.subsub": req.body.subSubCategoryName
                                            }
                                        }).then((Iudata) => {
                                            resolve(Iudata)
                                        }).catch((err) => {
                                            reject(err)
                                        })
                                } else {
                                    ingredientModel.findOneAndUpdate({ _id: ingredientData[i]._id },
                                        {
                                            $set: {
                                                "subsub.subsub": req.body.subSubCategoryName
                                            }
                                        }).then((Iudata) => {
                                            resolve(Iudata)
                                        }).catch((err) => {
                                            reject(err)
                                        })
                                }
                            }))
                        }
                        Promise.all(promiseArr1).then((promiseArr1) => {
                            res.json({ success: true, msg: "Sub sub category name updated" })
                        }).catch((err) => {
                            res.json({ success: false, msg: "error in promise arr 1", error: err })
                        })
                    }).catch((err) => {
                        res.json({ success: false, msg: "error in ingredient data", error: err })
                    })
            }).catch((err) => {
                res.json({ success: false, msg: "error in promise", error: err })
            })

        }).catch((err) => {
            res.json({ success: false, msg: "error in result", error: err })
        })
}

const updateSubSubCategoryTypeName = (req, res) => {
    inventoryFoodModel.findOne({ _id: req.params.rootcategoryid })
        .then((result) => {
            let promiseArr = []
            for (let i = 0; i < result.subCategories.length; i++) {
                if (result.subCategories[i].subSubCategories.length > 0) {
                    for (let j = 0; j < result.subCategories[i].subSubCategories.length; j++) {
                        if (result.subCategories[i].subSubCategories[j].subSubCategoryType.length > 0) {
                            for (let k = 0; k < result.subCategories[i].subSubCategories[j].subSubCategoryType.length; k++) {
                                promiseArr.push(new Promise((resolve, reject) => {
                                    inventoryFoodModel.findOneAndUpdate({
                                        _id: req.params.rootcategoryid,
                                        "subCategories.subSubCategories._id": req.params.subsubcategoryid,
                                        "subCategories.subSubCategories.subSubCategoryType._id": req.params.subsubcategorytypeid
                                    }, {
                                            $set: {
                                                "subCategories.$.subSubCategories.$[outer].subSubCategoryType.$[inner].subSubCategoryTypeName": req.body.subSubCategoryTypeName
                                            }
                                        },
                                        {
                                            "arrayFilters": [
                                                {
                                                    "outer._id": req.params.subsubcategoryid,
                                                },
                                                {
                                                    "inner._id": req.params.subsubcategorytypeid
                                                }
                                            ]
                                        }).then((udata) => {
                                            resolve(udata)
                                        }).catch((err) => {
                                            reject(err)
                                        })
                                }))
                            }
                        } else {
                            continue;
                        }
                    }
                } else {
                    continue;
                }
            }
            Promise.all(promiseArr).then((promiseArr) => {
                ingredientModel.find({ "type.id": req.params.subsubcategorytypeid })
                    .then((ingredientData) => {
                        promiseArr1 = []
                        for (let i = 0; i < ingredientData.length; i++) {
                            promiseArr1.push(new Promise((resolve, reject) => {
                                if (ingredientData[i].type.id == req.params.subsubcategorytypeid && ingredientData[i].ingredientcategory.id == req.params.subsubcategorytypeid) {
                                    ingredientModel.findOneAndUpdate({ _id: ingredientData[i]._id },
                                        {
                                            $set: {
                                                "ingredientcategory.category": req.body.subSubCategoryTypeName,
                                                "type.type": req.body.subSubCategoryTypeName
                                            }
                                        }).then((Iudata) => {
                                            resolve(Iudata)
                                        }).catch((err) => {
                                            reject(err)
                                        })
                                } else {
                                    ingredientModel.findOneAndUpdate({ _id: ingredientData[i]._id },
                                        {
                                            $set: {
                                                "type.type": req.body.subSubCategoryTypeName
                                            }
                                        }).then((Iudata) => {
                                            resolve(Iudata)
                                        }).catch((err) => {
                                            reject(err)
                                        })
                                }
                            }))
                        }
                        Promise.all(promiseArr1).then((promiseArr1) => {
                            res.json({ success: true, msg: "Sub sub category type name updated" })
                        }).catch((err) => {
                            res.json({ success: false, msg: "error in promise arr 1", error: err })
                        })
                    }).catch((err) => {
                        res.json({ success: false, msg: "error in ingredient data", error: err })
                    })
            }).catch((err) => {
                res.json({ success: false, msg: "error in promise", error: err })
            })

        }).catch((err) => {
            res.json({ success: false, msg: "error in result", error: err })
        })
}

module.exports = {
    addRootInventoryFoodCategoryName,
    addSubcategory,
    addSubSubCategories,
    addSubSubCategoryType,
    getRootInventoryFoodCategories,
    getAllRootInventoryFoodCategories,
    updateRootInventoryFoodCategoryName,
    updateSubcategoryName,
    updateSubSubCategoriesName,
    updateSubSubCategoryTypeName

}
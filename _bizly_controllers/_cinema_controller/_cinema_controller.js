const cinemaModel = require('../../_bizly_models/_cinema_models/_cinema_model').cinemaModel

const addcinemaInfo = (req, res) => {
    var info = new cinemaModel()
    info.cinema_code = req.body.cinema_code
    info.display_name = req.body.display_name
    info.legal_name = req.body.legal_name
    info.screen_series = req.body.screen_series
    info.addedBy = req.body.userid
    info.updatedBy = req.body.userid

    info.save((err, result) => {
        if (err) {
            res.json({ success: false, msg: err })
        } else {
            res.json({ success: true, msg: "Cinema Info Added", id: result._id })
        }
    })
}

// const addSeriesInfo = (req, res) => {
//     cinemaModel.findByIdAndUpdate(req.params.cinemaid,
//         {
//             screen_series: req.body
//         }, (err, udata) => {
//             if (err) {
//                 res.json({ success: false, msg: err })
//             } else {
//                 res.json({ success: true, msg: "Series info added" })
//             }
//         })
// }

const getAllCinemaData = (req, res) => {
    cinemaModel.find({})
        .populate('addedBy', 'userName')
        .populate('updatedBy', 'userName')
        .exec((err, data) => {
            if (err) {
                res.json({ success: false, msg: err })
            } else if (data == null || data.length == 0) {
                res.json({ success: false, msg: "Cinema data not found", data: [] })
            } else {
                res.json({ success: true, msg: "Cinema Data found", data: data })
            }
        })
}

const getCinemaData = (req, res) => {
    cinemaModel.find(req.params.cinemaid)
        .populate('addedBy', 'userName')
        .populate('updatedBy', 'userName')
        .exec((err, data) => {
            if (err) {
                res.json({ success: false, msg: err })
            } else if (data == null || data.length == 0) {
                res.json({ success: false, msg: "Cinema data not found" })
            } else {
                res.json({ success: true, msg: "Cinema Data found", data: data })
            }
        })
}

const updateCinemaData = (req, res) => {
    cinemaModel.findByIdAndUpdate(req.params.cinemaid, {
        $set: {
            display_name: req.body.display_name,
            legal_name: req.body.legal_name,
            screen_series: req.body.screen_series,
            updatedAt: Date.now()
        }
    }, (err, udata) => {
        if (err) {
            res.json({ success: false, msg: err })
        } else if (udata == null) {
            res.json({ success: false, msg: "Cinema data not found" })
        } else {
            res.json({ success: true, msg: "Cinema Data Updated" })
        }
    })
}

const softdeleteCinemaData = (req, res) => {
    cinemaModel.findByIdAndUpdate(req.params.cinemaid, { $set: { status: req.body.status } }, (err, data) => {
        if (err) {
            res.json({ success: false, msg: err })
        } else {
            res.json({ success: true, msg: "Cinema Deleted" })
        }
    })
}

const deleteCinemaData = (req, res) => {
    cinemaModel.findByIdAndDelete(req.params.cinemaid, (err, data) => {
        if (err) {
            res.json({ success: false, msg: err })
        } else {
            res.json({ success: true, msg: "Cinema Deleted" })
        }
    })
}

const deleteAllCinemaData = (req, res) => {
    cinemaModel.remove((err, data) => {
        if (err) {
            res.json({ success: false, msg: err })
        } else {
            res.json({ success: true, msg: "Cinema Deleted" })
        }
    })
}

module.exports = {
    addcinemaInfo,
    // addSeriesInfo,
    getAllCinemaData,
    getCinemaData,
    updateCinemaData,
    softdeleteCinemaData,
    deleteCinemaData,
    deleteAllCinemaData
}
const bizlytoPOSRoleModel = require('../../_bizly_models/_user_roles_model/_user_role_pos_model').bizlyToPOSRole
const bizlytoPOSUserModel = require('../../_bizly_models/_user_roles_model/_user_role_pos_model').bizlytoPOSUser
const POSRoleEditedHistoryModel = require('../../_bizly_models/_user_roles_model/_pos_edited_history_model').role_edited_history
const POSUserEditedHistoryModel = require('../../_bizly_models/_user_roles_model/_pos_edited_history_model').user_edited_history

//Add Role data
const addRoles = (req, res) => {
    bizlytoPOSRoleModel.findOne({ role_name: req.body.role_name }, (err, data) => {
        if (err) {
            res.json({ success: false, msg: err })
        } else if (data) {
            res.json({ success: false, msg: "Role Name Already Exists" })
        } else {
            var role = new bizlytoPOSRoleModel()
            role.role_ID = req.body.role_ID
            role.role_name = req.body.role_name
            role.description = req.body.description
            role.role_salary = req.body.role_salary
            role.user_edited = req.body.user_edited
            role.created_by_on = req.body.created_by_on
            role.addedBy = req.body.userid
            role.updatedBy = req.body.userid

            role.save((err) => {
                if (err) {
                    res.json({ success: true, msg: err })
                } else {
                    res.json({ success: true, msg: "Role added" })
                }
            })
        }
    })

}

//Get All Roles Data 
const getallRoles = (req, res) => {
    bizlytoPOSRoleModel.find().
        populate('created_by_on', 'userName').
        populate('addedBy', 'userName').
        populate('updatedBy', 'userName').
        exec((err, data) => {
            if (err) {
                res.json({ success: false, msg: err })
            } else if (data == null) {
                res.json({ success: false, msg: "Data Not Found" })
            } else {
                res.json({ success: true, msg: "Data Found", data: data })
            }
        })
}


//Get Role Data by roleid
const getRoleByID = (req, res) => {
    bizlytoPOSRoleModel.findById(req.params.roleid).
        populate('created_by_on', 'userName').
        populate('addedBy', 'userName').
        populate('updatedBy', 'userName').
        exec((err, data) => {
            if (err) {
                res.json({ success: false, msg: err })
            } else if (data == null) {
                res.json({ success: false, msg: "Data Not Found" })
            } else {
                res.json({ success: true, msg: "Data Found", data: data })
            }
        })
}

//Update Role Data
const updateRole = async (req, res) => {
    try {
        bizlytoPOSRoleModel.findById(req.params.roleid)
            .then((roleData) => {
                var OBJ = new POSRoleEditedHistoryModel()
                OBJ.objID = roleData.objID
                OBJ.role_ID = roleData.role_ID
                OBJ.role_name = roleData.role_name
                OBJ.role_salary = roleData.role_salary
                OBJ.total_users = roleData.total_users
                OBJ.access_control = roleData.access_control
                OBJ.user_edited = roleData.user_edited
                OBJ.description = roleData.description
                OBJ.status = roleData.status
                OBJ.created_by_on = roleData.created_by_on
                OBJ.addedBy = roleData.addedBy
                OBJ.updatedBy = roleData.updatedBy
                OBJ.createdAt = roleData.createdAt
                OBJ.updatedAt = roleData.updatedAt
                OBJ.save((err) => {
                    bizlytoPOSRoleModel.findByIdAndUpdate(req.params.roleid,
                        {
                            $set: {
                                role_ID: req.body.role_ID,
                                role_name: req.body.role_name,
                                description: req.body.description,
                                role_salary: req.body.role_salary,
                                total_users: req.body.total_users,
                                access_control: req.body.access_control,
                                user_edited: req.body.user_edited,
                                created_by_on: req.body.created_by_on,
                                updatedBy: req.body.userid,
                                updatedAt: Date.now()
                            }
                        }).then((udata) => {
                            res.json({ success: true, msg: "role updated" })
                        }).catch((err) => {
                            res.json({ success: false, msg: "error in udata", error: err })
                        })
                })

            }).catch((err) => {
                res.json({ success: false, msg: "error in role data", error: err })
            })
    } catch (ex) {
        return res.json({ success: false, error: ex.message })
    }

}

//Soft Delete Role Data
const deleteRole = (req, res) => {
    bizlytoPOSRoleModel.findByIdAndUpdate(req.params.roleid, { $set: { status: req.body.status } }, (err) => {
        if (err) {
            res.json({ success: false, msg: err })
        } else {
            res.json({ success: true, msg: "Data Deleted" })
        }
    })
}


//**************************                           *******************************************************
//************************** Bizly To POS Create Users *******************************************************
//**************************                           *******************************************************


//Add Users Personal Details
const addUsersPersonalDetails = (req, res) => {
    var user = new bizlytoPOSUserModel()
    user.personal_details = req.body

    user.save((err, result) => {
        if (err) {
            res.json({ success: false, msg: true })
        } else {
            res.json({ success: true, msg: "Personal Details Added", id: result._id })
        }
    })
}

//Add Users Work Details
const addUserWorkDetails = (req, res) => {
    bizlytoPOSUserModel.findByIdAndUpdate(req.params.userid,
        {
            work_details: req.body
        }, (err, udata) => {
            if (err) {
                res.json({ success: false, msg: err })
            } else {
                res.json({ success: true, msg: "Work Details Added" })
            }
        })
}

// Add Users Bank Details
const addUsersBankDetails = (req, res) => {
    bizlytoPOSUserModel.findByIdAndUpdate(req.params.userid,
        {
            bank_details: req.body
        }, (err, udata) => {
            if (err) {
                res.json({ success: false, msg: err })
            } else {
                res.json({ success: true, msg: "Bank Details Added" })
            }
        })
}

//Add User Legal Documnets Details
const addUsersLegalDocumentsDetails = (req, res) => {
    bizlytoPOSUserModel.findByIdAndUpdate(req.params.userid,
        {
            legal_documents: req.body
        }, (err, udata) => {
            if (err) {
                res.json({ success: false, msg: err })
            } else {
                res.json({ success: true, msg: "Legal Documents Details Added" })
            }
        })
}

//Add Users Corporate Detials
const addUsersCorporateDetails = (req, res) => {
    bizlytoPOSUserModel.findByIdAndUpdate(req.params.userid,
        {
            corporate_detials: req.body
        }, (err, udata) => {
            if (err) {
                res.json({ success: false, msg: err })
            } else {
                res.json({ success: true, msg: "Corporate Details Added" })
            }
        })
}

//Add Users Assets Details
const addUsersAssetsDetails = (req, res) => {
    bizlytoPOSUserModel.findByIdAndUpdate(req.params.userid,
        {
            assets: req.body
        }, (err, udata) => {
            if (err) {
                res.json({ success: false, msg: err })
            } else {
                res.json({ success: true, msg: "Assets Details Added" })
            }
        })
}

//Add Users Credential Details
const addUsersCredentialDetails = (req, res) => {
    bizlytoPOSUserModel.findByIdAndUpdate(req.params.userid,
        {
            credentials: req.body
        }, (err, udata) => {
            if (err) {
                res.json({ success: false, msg: err })
            } else {
                res.json({ success: true, msg: "Credential Details Added" })
            }
        })
}

//Get All Users Data 
const getallUsers = (req, res) => {
    bizlytoPOSUserModel.find().
        populate('personal_details.assigned_role').
        populate('personal_details.addedBy', 'userName').
        populate('personal_details.updatedBy', 'userName').
        exec((err, data) => {
            if (err) {
                res.json({ success: false, msg: err })
            } else if (data == null) {
                res.json({ success: false, msg: "Data Not Found" })
            } else {
                res.json({ success: true, msg: "Data Found", data: data })
            }
        })
}

//Get User Data by userid
const getuserByID = (req, res) => {
    bizlytoPOSUserModel.findById(req.params.userid).
        populate('personal_details.assigned_role').
        populate('personal_details.addedBy', 'userName').
        populate('personal_details.updatedBy', 'userName').
        exec((err, data) => {
            if (err) {
                res.json({ success: false, msg: err })
            } else if (data == null) {
                res.json({ success: false, msg: "Data Not Found" })
            } else {
                res.json({ success: true, msg: "Data Found", data: data })
            }
        })
}

//Update User Data
const updateUser = (req, res) => {
    try {
        bizlytoPOSUserModel.findById(req.params.userid)
            .then((userData) => {
                var user = new POSUserEditedHistoryModel()
                user.userobjID = userData.userobjID
                user.personal_details = userData.personal_details
                user.work_details = userData.work_details
                user.bank_details = userData.bank_details
                user.legal_document = userData.legal_document
                user.corporate_details = userData.corporate_details
                user.assets = userData.assets
                user.credentials = userData.credentials
                user.createdAt = userData.createdAt
                user.updatedAt = userData.updatedAt
                user.save((err) => {
                    bizlytoPOSUserModel.findByIdAndUpdate(req.params.userid,
                        {
                            $set: {
                                personal_details: req.body.personal_details,
                                work_details: req.body.work_details,
                                bank_details: req.body.bank_details,
                                legal_document: req.body.legal_document,
                                corporate_details: req.body.corporate_details,
                                assets: req.body.assets,
                                credentials: req.body.credentials,
                                updatedAt: Date.now()
                            }
                        }).then((udata) => {
                            res.json({ success: true, msg: "User Updated" })
                        }).catch((err) => {
                            res.json({ success: false, msg: "Error in user data", error: err })
                        })
                })
            }).catch((err) => {
                res.json({ success: false, msg: "Error in user data", error: err })
            })
    } catch (ex) {
        return res.json({ success: false, error: ex.message })
    }
}

//Soft Delete User  Data
const deleteUser = (req, res) => {
    bizlytoPOSUserModel.findByIdAndUpdate(req.params.userid, { $set: { "personal_details.status": req.body.status } }, (err) => {
        if (err) {
            res.json({ success: false, msg: err })
        } else {
            res.json({ success: true, msg: "Data Deleted" })
        }
    })
}
module.exports = {
    addRoles,
    getallRoles,
    getRoleByID,
    updateRole,
    deleteRole,

    //********users*********
    addUsersPersonalDetails,
    addUserWorkDetails,
    addUsersBankDetails,
    addUsersLegalDocumentsDetails,
    addUsersCorporateDetails,
    addUsersAssetsDetails,
    addUsersCredentialDetails,
    getallUsers,
    getuserByID,
    updateUser,
    deleteUser
}
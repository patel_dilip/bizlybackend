const bizlytobizlyRoleModel = require('../../_bizly_models/_user_roles_model/_user_role_bizly_model').bizlytobizlyRole
const bizlytobizlyUserModel = require('../../_bizly_models/_user_roles_model/_user_role_bizly_model').bizlytobizlyUser
const bizlyRoleEditedHistory = require('../../_bizly_models/_user_roles_model/_bizly_edited_history_model').role_edited_history
const bizlyuserEditedHistory = require('../../_bizly_models/_user_roles_model/_bizly_edited_history_model').user_edited_history

//Add Role data
const addRoles = (req, res) => {
    bizlytobizlyRoleModel.findOne({ role_name: req.body.role_name }, (err, data) => {

        if (err) {
            res.json({ success: false, msg: err })
        } else if (data) {
            res.json({ success: false, msg: "Role Name Already Exists" })
        } else {
            var role = new bizlytobizlyRoleModel()
            role.role_ID = req.body.role_ID
            role.role_name = req.body.role_name
            role.description = req.body.description
            role.role_salary = req.body.role_salary
            role.user_edited = req.body.user_edited
            role.created_by_on = req.body.created_by_on
            role.addedBy = req.body.userid
            role.updatedBy = req.body.userid

            role.save((err) => {
                if (err) {
                    res.json({ success: true, msg: err })
                } else {
                    res.json({ success: true, msg: "Role added" })
                }
            })
        }
    })

}

//Get All Roles Data 
const getallRoles = (req, res) => {
    bizlytobizlyRoleModel.find().
        populate('created_by_on', 'userName').
        populate('addedBy', 'userName').
        populate('updatedBy', 'userName').
        exec((err, data) => {
            if (err) {
                res.json({ success: false, msg: err })
            } else if (data == null) {
                res.json({ success: false, msg: "Data Not Found" })
            } else {
                res.json({ success: true, msg: "Data Found", data: data })
            }
        })
}


//Get Role Data by roleid
const getRoleByID = (req, res) => {
    bizlytobizlyRoleModel.findById(req.params.roleid).
        populate('created_by_on', 'userName').
        populate('addedBy', 'userName').
        populate('updatedBy', 'userName').
        exec((err, data) => {
            if (err) {
                res.json({ success: false, msg: err })
            } else if (data == null) {
                res.json({ success: false, msg: "Data Not Found" })
            } else {
                res.json({ success: true, msg: "Data Found", data: data })
            }
        })
}

//Update Role Data
const updateRole = (req, res) => {
    try {
        bizlytobizlyRoleModel.findById(req.params.roleid)
            .then((bizlyroleData) => {
                var OBJ = new bizlyRoleEditedHistory()
                OBJ.objID = bizlyroleData._id
                OBJ.role_ID = bizlyroleData.role_ID
                OBJ.role_name = bizlyroleData.role_name
                OBJ.description = bizlyroleData.description
                OBJ.role_salary = bizlyroleData.role_salary
                OBJ.status = bizlyroleData.status

                OBJ.user_edited = bizlyroleData.user_edited
                OBJ.created_by_on = bizlyroleData.created_by_on

                OBJ.addedBy = bizlyroleData.addedBy
                OBJ.updatedBy = bizlyroleData.updatedBy
                OBJ.createdAt = bizlyroleData.createdAt
                OBJ.updatedAt = bizlyroleData.updatedAt
                OBJ.save((err, result) => {
                    bizlytobizlyRoleModel.findByIdAndUpdate(req.params.roleid,
                        {
                            $set: {
                                role_ID: req.body.role_ID,
                                role_name: req.body.role_name,
                                description: req.body.description,
                                role_salary: req.body.role_salary,
                                user_edited: req.body.user_edited,
                                created_by_on: req.body.created_by_on,

                                updatedBy: req.body.userid,
                                updatedAt: Date.now()

                            }
                        }).then((udata) => {
                            res.json({ success: true, msg: "Role Updated" })
                        }).catch((err) => {
                            res.json({ success: false, msg: "Error in udata", error: err })
                        })
                })

            }).catch((err) => {
                res.json({ success: false, msg: "Error in bizly Role data", error: err })
            })
    } catch (ex) {
        return res.json({ success: false, error: ex.message })
    }
}

//Soft Delete Role Data
const deleteRole = (req, res) => {
    bizlytobizlyRoleModel.findByIdAndUpdate(req.params.roleid, { $set: { status: req.body.status } }, (err) => {
        if (err) {
            res.json({ success: false, msg: err })
        } else {
            res.json({ success: true, msg: "Data Deleted" })
        }
    })
}


//**************************                           *******************************************************
//**************************Bizly To Bizly Create Users*******************************************************
//**************************                           *******************************************************


//Add Users Personal Details
const addUsersPersonalDetails = (req, res) => {
    var user = new bizlytobizlyUserModel()
    user.personal_details = req.body

    user.save((err, result) => {
        if (err) {
            res.json({ success: false, msg: err })
        } else {
            res.json({ success: true, msg: "Personal Details Added", id: result._id })
        }
    })
}

//Add Users Work Details
const addUserWorkDetails = (req, res) => {
    bizlytobizlyUserModel.findByIdAndUpdate(req.params.userid,
        {
            work_details: req.body
        }, (err, udata) => {
            if (err) {
                res.json({ success: false, msg: err })
            } else {
                res.json({ success: true, msg: "Work Details Added" })
            }
        })
}

// Add Users Bank Details
const addUsersBankDetails = (req, res) => {
    bizlytobizlyUserModel.findByIdAndUpdate(req.params.userid,
        {
            bank_details: req.body
        }, (err, udata) => {
            if (err) {
                res.json({ success: false, msg: err })
            } else {
                res.json({ success: true, msg: "Bank Details Added" })
            }
        })
}

//Add User Legal Documnets Details
const addUsersLegalDocumentsDetails = (req, res) => {
    bizlytobizlyUserModel.findByIdAndUpdate(req.params.userid,
        {
            legal_documents: req.body
        }, (err, udata) => {
            if (err) {
                res.json({ success: false, msg: err })
            } else {
                res.json({ success: true, msg: "Legal Documents Details Added" })
            }
        })
}

//Add Users Corporate Detials
const addUsersCorporateDetails = (req, res) => {
    bizlytobizlyUserModel.findByIdAndUpdate(req.params.userid,
        {
            corporate_detials: req.body
        }, (err, udata) => {
            if (err) {
                res.json({ success: false, msg: err })
            } else {
                res.json({ success: true, msg: "Corporate Details Added" })
            }
        })
}

//Add Users Assets Details
const addUsersAssetsDetails = (req, res) => {
    bizlytobizlyUserModel.findByIdAndUpdate(req.params.userid,
        {
            assets: req.body
        }, (err, udata) => {
            if (err) {
                res.json({ success: false, msg: err })
            } else {
                res.json({ success: true, msg: "Assets Details Added" })
            }
        })
}

//Add Users Credential Details
const addUsersCredentialDetails = (req, res) => {
    bizlytobizlyUserModel.findByIdAndUpdate(req.params.userid,
        {
            credentials: req.body
        }, (err, udata) => {
            if (err) {
                res.json({ success: false, msg: err })
            } else {
                res.json({ success: true, msg: "Credential Details Added" })
            }
        })
}

//Get All Users Data 
const getallUsers = (req, res) => {
    bizlytobizlyUserModel.find().
        populate('personal_details.assigned_role').
        populate('personal_details.addedBy', 'username').
        populate('personal_details.updatedBy', 'username').
        exec((err, data) => {
            if (err) {
                res.json({ success: false, msg: err })
            } else if (data == null) {
                res.json({ success: false, msg: "Data Not Found" })
            } else {
                res.json({ success: true, msg: "Data Found", data: data })
            }
        })
}

//Get User Data by userid
const getuserByID = (req, res) => {
    bizlytobizlyUserModel.findById(req.params.userid).
        populate('personal_details.assigned_role').
        populate('personal_details.addedBy', 'username').
        populate('personal_details.updatedBy', 'username').
        exec((err, data) => {
            if (err) {
                res.json({ success: false, msg: err })
            } else if (data == null) {
                res.json({ success: false, msg: "Data Not Found" })
            } else {
                res.json({ success: true, msg: "Data Found", data: data })
            }
        })
}

//Update User Data
const updateUser = async (req, res) => {
    try {
        bizlytobizlyUserModel.findById(req.params.userid)
            .then((userData) => {
                // console.log("userData", userData)
                var user = new bizlyuserEditedHistory()
                user.userobjID = userData._id
                user.personal_details = userData.personal_details
                user.work_details = userData.work_details
                user.bank_details = userData.bank_details
                user.legal_documents = userData.legal_documents
                user.corporate_detials = userData.corporate_detials
                user.assets = userData.assets
                user.credentials = userData.credentials
                user.createdAt = userData.createdAt
                user.updatedAt = userData.updatedAt
                user.save((err) => {
                    bizlytobizlyUserModel.findByIdAndUpdate(req.params.userid,
                        {
                            $set: {
                                personal_details: req.body.personal_details,
                                work_details: req.body.work_details,
                                bank_details: req.body.bank_details,
                                legal_documents: req.body.legal_documents,
                                corporate_detials: req.body.corporate_detials,
                                assets: req.body.assets,
                                credentials: req.body.credentials,
                                updatedAt: Date.now()
                            }
                        }).then((udata) => {
                            res.json({ success: true, msg: "user updated" })
                        }).catch((err) => {
                            res.json({ success: false, msg: "error in udata", error: err })
                        })
                })
            }).catch((err) => {
                res.json({ success: false, msg: "error in user data", error: err })
            })
    } catch (ex) {
        return res.json({ success: false, error: ex.message })
    }

}

//Soft Delete User Data
const deleteUser = (req, res) => {
    bizlytobizlyUserModel.findByIdAndUpdate(req.params.userid, { $set: { "personal_details.status": req.body.status } }, (err) => {
        if (err) {
            res.json({ success: false, msg: err })
        } else {
            res.json({ success: true, msg: "Data Deleted" })
        }
    })
}
module.exports = {
    addRoles,
    getallRoles,
    getRoleByID,
    updateRole,
    deleteRole,

    //********users*********
    addUsersPersonalDetails,
    addUserWorkDetails,
    addUsersBankDetails,
    addUsersLegalDocumentsDetails,
    addUsersCorporateDetails,
    addUsersAssetsDetails,
    addUsersCredentialDetails,
    getallUsers,
    getuserByID,
    updateUser,
    deleteUser
}
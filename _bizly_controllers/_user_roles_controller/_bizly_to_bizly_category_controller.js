const b2bCategoryModel = require('../../_bizly_models/_user_roles_model/_bizly_to_bizly_category_model').categoryModel

//add Root parent
const addRootParent = (req, res) => {
    var obj = new b2bCategoryModel()

    obj.name = req.body.name
    obj.addedBy = req.body.userid
    obj.updatedBy = req.body.userid


    obj.save((err, result) => {
        if (err) {
            res.json({ success: false, msg: err })
        } else {
            res.json({ success: true, msg: "Root parent added", id: result._id })
        }
    })
}

// Add Childern 
const addChildern = (req, res) => {
    b2bCategoryModel.findByIdAndUpdate(req.params.rootparentid,
        {
            $push: { childern: req.body }
        }, (err, udata) => {
            if (err) {
                res.json({ success: false, msg: err })
            } else {
                res.json({ success: true, msg: "childern Added" })
            }
        })
}

// Add Childern 2
const addChildern2 = (req, res) => {
    b2bCategoryModel.updateOne({ _id: req.params.rootparentid, "childern._id": req.params.childernid },
        {
            $push: { "childern.$.childern2": req.body }
        }, (err, udata) => {
            if (err) {
                res.json({ success: false, msg: err })
            } else {
                res.json({ success: true, msg: "childern 2 Added" })
            }
        })
}

// add childern 3
const addChildern3 = (req, res) => {
    b2bCategoryModel.updateOne({
        _id: req.params.rootparentid,
        "childern.childern2._id": req.params.childern2id
    },
        {
            $push: { "childern.$.childern2.$[outer].childern3": req.body }
        }, {
            "arrayFilters": [
                {
                    "outer._id": req.params.childern2id,
                }
            ]
        }, (err, udata) => {
            if (err) {
                res.json({ success: false, msg: err })
            } else {
                res.json({ success: true, msg: "childern 3 Added" })
            }
        })
}

// add childern 4
const addChildern4 = (req, res) => {
    b2bCategoryModel.updateOne({
        _id: req.params.rootparentid,
        "childern.childern2._id": req.params.childern2id,
        "childern.childern2.childern3._id": req.params.childern3id
    },
        {
            $push: { "childern.$.childern2.$[outer].childern3.$[inner].childern4": req.body }
        }, {
            "arrayFilters": [
                {
                    "outer._id": req.params.childern2id,
                },
                {
                    "inner._id": req.params.childern3id,
                }
            ]
        }, (err, udata) => {
            if (err) {
                res.json({ success: false, msg: err })
            } else {
                res.json({ success: true, msg: "childern 4 Added" })
            }
        })
}

// add childern last
const addChildernLast = (req, res) => {
    b2bCategoryModel.updateOne({
        _id: req.params.rootparentid,
        "childern.childern2._id": req.params.childern2id,
        "childern.childern2.childern3._id": req.params.childern3id,
        "childern.childern2.childern3.childern4._id": req.params.childern4id
    },
        {
            $push: { "childern.$.childern2.$[outer].childern3.$[inner].childern4.$[ele].lastchildern": req.body }
        }, {
            "arrayFilters": [
                {
                    "outer._id": req.params.childern2id,
                },
                {
                    "inner._id": req.params.childern3id,
                },
                {
                    "ele._id": req.params.childern4id,
                }
            ]
        }, (err, udata) => {
            if (err) {
                res.json({ success: false, msg: err })
            } else {
                res.json({ success: true, msg: "childern last Added" })
            }
        })
}


//get all Category
const getALLCategories = (req, res) => {
    b2bCategoryModel.find({}, (err, data) => {
        if (err) {
            res.json({ success: false, msg: err })
        } else if (data == null || data.length == 0) {
            res.json({ success: false, msg: "not found" })
        } else {
            res.json({ success: true, msg: "Found", data: data })
        }
    })
}

const deleteAllCategory = (req, res) => {
    b2bCategoryModel.remove((err, data) => {
        if (err) {
            res.json({ success: false, msg: err })
        } else {
            res.json({ success: true, msg: "Delete All Categories" })
        }
    })
}

module.exports = {
    addRootParent,
    addChildern,
    addChildern2,
    addChildern3,
    addChildern4,
    addChildernLast,
    getALLCategories,
    deleteAllCategory
}
const roleEditedHistoryModel = require('../../_bizly_models/_user_roles_model/_bizly_edited_history_model').role_edited_history
const userEditedHistoryModel = require('../../_bizly_models/_user_roles_model/_bizly_edited_history_model').user_edited_history

const getallLastUpdatedRole = (req, res) => {
    roleEditedHistoryModel.find({ objID: req.params.id })
        .populate('addedBy', 'userName')
        .populate('updatedBy', 'userName')
        .sort({ _id: -1 })
        .limit(3)
        .exec((err, data) => {
            if (err) {
                res.json({ success: false, msg: err })
            } else {
                res.json({ success: true, msg: "data found", data: data })
            }

        })
}

const getallLastUpdatedUsers = (req, res) => {
    userEditedHistoryModel.find({ userobjID: req.params.id })
        .populate('addedBy', 'userName')
        .populate('updatedBy', 'userName')
        .sort({ _id: -1 })
        .limit(3)
        .exec((err, data) => {
            if (err) {
                res.json({ success: false, msg: err })
            } else {
                res.json({ success: true, msg: "data found", data: data })
            }

        })
}

module.exports = {
    getallLastUpdatedRole,
    getallLastUpdatedUsers
}
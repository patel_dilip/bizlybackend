const buyPOSProductModel = require("../../_bizly_models/_pos_products/_buy_pos_product_model")
  .buyPOSProductModel;

const buyposproduct = (req, res) => {
  var buyPOSProduct = new buyPOSProductModel();
  buyPOSProduct.retailBeveragesTree = req.body.retailBeveragesTree;
  buyPOSProduct.addedBy = req.body.userid;
  buyPOSProduct.updatedBy = req.body.userid;

  buyPOSProduct.save((err) => {
    if (err) {
      res.json({ sucess: false, msg: err });
    } else {
      res.json({ sucess: true });
    }
  });
};


module.exports = {
    buyposproduct
};
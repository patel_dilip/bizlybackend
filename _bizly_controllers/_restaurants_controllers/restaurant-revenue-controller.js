const restaurantRevenueModel = require('../../_bizly_models/_restaurant_models/_restaurant-revenue-model').restaurantrevenuemodel

//Add Restaurant Revenue Data
const addRestoRevenue = (req, res) => {
    var restorevenue = new restaurantRevenueModel()
    restorevenue.restoRevenue = req.body.restoRevenue
    restorevenue.save((err) => {
        if (err) {
            throw err
        } else {
            res.json({ sucess: true, msg: 'RestoRevenue added' })
        }
    })
}

//Get Restaurant Revenue Data By restorevenueid
const getRestoRevenue = (req, res) => {
    restaurantRevenueModel.findById(req.params.restorevenueid, (err, restoRevenue) => {
        if (err) {
            throw err
        } else if (restoRevenue == null) {
            res.json({ sucess: false, msg: 'RestoRevenue not found' })
        } else {
            res.json({ sucess: true, msg: 'RestoRevenue found', data: restoRevenue })
        }
    })
}

//Get All Restaurant Revenues Data
const getAllRestoRevenues = (req, res) => {
    restaurantRevenueModel.find({}, (err, data) => {
        if (err) {
            throw err
        } else if (data == null || data.length == 0) {
            res.json({ sucess: false, msg: "restoRevenues Not Found" })
        } else {
            res.json({ sucess: true, msg: "restoRevenues Found", data: data })
        }
    })
}

//Update Restaurant Revenue Data
const updateRestoRevenue = (req, res) => {
    restaurantRevenueModel.findByIdAndUpdate(req.params.restorevenueid,
        {
            $set: {
                restoRevenue: req.body.restoRevenue
            }
        }, (err, urestoRevenue) => {
            if (err) {
                throw err
            } else if (urestoRevenue == null) {
                res.json({ sucess: false, msg: "restoRevenue not found" })
            } else {
                res.json({ sucess: true, msg: "restoRevenue updated" })
            }
        })
}

//Delete Restaurant Revenue Data
const deleteRestoRevenue = (req, res) => {
    restaurantRevenueModel.findByIdAndDelete(req.params.restorevenueid, (err, drestoRevenue) => {
        if (err) {
            throw err
        } else if (drestoRevenue == null) {
            res.json({ sucess: false, msg: "restoRevenue not found" })
        } else {
            res.json({ sucess: true, msg: "restoRevenue Deleted" })
        }
    })
}

//Delete Restaurant Revenue Data
const deleteAllRestoRevenue = (req, res) => {
    restaurantRevenueModel.remove((err, drestoRevenue) => {
        if (err) {
            throw err
        } else if (drestoRevenue == null) {
            res.json({ sucess: false, msg: "restoRevenue not found" })
        } else {
            res.json({ sucess: true, msg: "restoRevenue Deleted" })
        }
    })
}

module.exports = {
    addRestoRevenue,
    getRestoRevenue,
    getAllRestoRevenues,
    updateRestoRevenue,
    deleteRestoRevenue,
    deleteAllRestoRevenue
}
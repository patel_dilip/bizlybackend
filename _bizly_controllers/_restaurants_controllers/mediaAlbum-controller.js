const mediaAlbumModel = require('../../_bizly_models/_restaurant_models/_mediaAlbum-model').mediaAlbum

// Add Media Album Data
const addmediaAlbum = (req, res) => {
    var album = new mediaAlbumModel()
    album.title = req.body.title
    album.albums = req.body.albums
    album.addedBy = req.body.userid
    album.updatedBy = req.body.userid

    album.save((err) => {
        if (err) {
            res.json({ success: false, msg: err })
        } else {
            res.json({ success: true, msg: "Media Album Added" })
        }
    })
}

// get All Media Albums Data
const getAllMediaAlbums = (req, res) => {
    mediaAlbumModel.find().
        populate('addedBy', 'userName').
        populate('updatedBy', 'userName').
        exec((err, data) => {
            if (err) {
                res.json({ success: false, msg: err })
            } else if (data == null || data.length == 0) {
                res.json({ success: false, msg: "Media Not found" })
            } else {
                res.json({ success: true, msg: "Media Found", data: data })
            }
        })
}

// get Media Album Data By mediaid
const getMediaAlbumbyID = (req, res) => {
    mediaAlbumModel.findById(req.params.mediaid).
        populate('addedBy', 'userName').
        populate('updatedBy', 'userName').
        exec((err, data) => {
            if (err) {
                res.json({ success: false, msg: err })
            } else if (data == null || data.length == 0) {
                res.json({ success: false, msg: "Media Not found" })
            } else {
                res.json({ success: true, msg: "Media Found", data: data })
            }
        })
}

// Update Media Album Data
const updateMediaAlbum = (req, res) => {
    mediaAlbumModel.findByIdAndUpdate(req.params.mediaid,
        {
            $set: {
                title: req.body.title,
                albums: req.body.albums,
                updatedBy: req.body.userid,
                updatedAt: Date.now()
            }
        }, (err, udata) => {
            if (err) {
                res.json({ success: false, msg: err })
            } else if (udata == null || udata.length == 0) {
                res.json({ success: false, msg: "media data not found" })
            } else {
                res.json({ success: true, msg: "Media data updated" })
            }
        })
}

//Delete Media Album Data
const deleteMediaAlbum = (req, res) => {
    mediaAlbumModel.findByIdAndDelete(req.params.mediaid, (err, data) => {
        if (err) {
            res.json({ success: false, msg: err })
        } else {
            res.json({ success: true, msg: "Media Data Deleted" })
        }
    })
}

module.exports = {
    addmediaAlbum,
    getAllMediaAlbums,
    getMediaAlbumbyID,
    updateMediaAlbum,
    deleteMediaAlbum
}
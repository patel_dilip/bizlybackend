const menuModel = require('../../_bizly_models/_restaurant_models/_menu-model').menuModel

// Add root category of menu
const addMenu = (req, res) => {
    var menu = new menuModel()
    menu.rootCategoryName = req.body.rootCategoryName,
        menu.isFilterable = req.body.isFilterable,
        menu.isSearchable = req.body.isSearchable,
        menu.priority = req.body.priority
    menu.addedBy = req.body.userid
    menu.updatedBy = req.body.userid

    menu.save((err) => {
        if (err) {
            res.json({ success: false, msg: err })
        } else {
            res.json({ success: true, msg: "Menu Added" })
        }
    })
}

//Add categories of menu
const addCategories = (req, res) => {
    menuModel.findByIdAndUpdate(req.params.rootCategoryId,
        { $push: { categories: req.body } },
        (err, categories) => {
            if (err) {
                throw err
            } else if (categories == null) {
                res.json({ sucess: false, msg: "rootCategory not found" })
            } else {
                res.json({ sucess: true, msg: "rootCategory updated" })
            }
        })
}

//Add Child Categories of Menu
const addChildCategories = (req, res) => {
    menuModel.updateOne({ _id: req.params.rootCategoryId, "categories._id": req.params.categoresId },
        // { $addToSet: { "categories.$.childCategories": { childCategoryName: req.body.childCategoryName } } },
        { $addToSet: { "categories.$.childCategories": req.body } },
        (err, uchildcategory) => {
            if (err) {
                throw err
            } else if (uchildcategory == null) {
                res.json({ sucess: false, msg: "rootCategory not found" })
            } else {
                res.json({ sucess: true, msg: "rootCategory updated" })
            }
        })
}

//Add Child Child Categories of Menu
const addChildChildCategories = (req, res) => {
    menuModel.updateOne(
        {
            _id: req.params.rootCategoryId,
            "categories.childCategories._id": req.params.childcategoryid
        },
        {
            $push: {
                // "categories.$.childCategories.$[outer].childChildCategories": req.body.childChildCategoryName
                "categories.$.childCategories.$[outer].childChildCategories": req.body

            }
        },
        {
            "arrayFilters": [
                {
                    "outer._id": req.params.childcategoryid,
                }
            ]
        },
        (err, uchildchildcategory) => {
            if (err) {
                throw err
            } else if (uchildchildcategory == null) {
                res.json({ sucess: false, msg: "rootCategory not found" })
            } else {
                res.json({ sucess: true, msg: "rootCategory updated" })
            }
        })
}

//Get All Menus
const getAllMenus = (req, res) => {
    menuModel.find().
        populate('addedBy', 'userName').
        populate('updatedBy', 'userName').
        exec((err, menu) => {
            if (err) {
                res.json({ sucess: false, msg: err })
            } else if (menu.length == 0 || menu == null) {
                res.json({ sucess: false, msg: 'menu not found' })
            } else {
                res.json({ sucess: true, data: menu })
            }
        })
}

//Get Menu By menuid
const getMenu = (req, res) => {
    menuModel.findById(req.params.menuid).
        populate('addedBy', 'userName').
        populate('updatedBy', 'userName').
        exec((err, menu) => {
            if (err) {
                res.json({ sucess: false, msg: err })
            } else if (menu == null) {
                res.json({ sucess: false, msg: 'menu not found' })
            } else {
                res.json({ sucess: true, data: menu })
            }
        })
}

const updateRootCategoryName = (req, res) => {
    menuModel.findOneAndUpdate({ _id: req.params.rootCategoryId },
        {
            $set: {
                rootCategoryName: req.body.rootCategoryName
            }
        }, (err, udata) => {
            if (err) {
                res.json({ success: false, msg: err })
            } else if (udata == null) {
                res.json({ success: false, msg: "Data not found" })
            } else {
                res.json({ success: true, msg: "Root name Updated" })
            }
        })
}

const updateCategoryName = (req, res) => {
    menuModel.findOne({ _id: req.params.rootCategoryId, })
        .then((data) => {
            promiseArr = []
            for (let i = 0; i < data.categories.length; i++) {
                promiseArr.push(new Promise((resolve, reject) => {
                    menuModel.findOneAndUpdate({
                        _id: req.params.rootCategoryId,
                        "categories._id": req.params.categoresId
                    }, {
                            $set: {
                                "categories.$.categoryName": req.body.categoryName
                            }
                        }).then((udata) => {
                            resolve(udata)
                        }).catch((err) => {
                            reject(err)
                        })
                }))
            }
            Promise.all(promiseArr).then((promiseArr) => {
                res.json({ success: true, msg: "Category name Updated" })
            }).catch((err) => {
                res.json({ success: false, msg: "error in Promise arr", error: err })
            })
        }).catch((err) => {
            res.json({ success: false, msg: "error in data", error: err })
        })
}

const updateChildCategoriesName = (req, res) => {
    menuModel.findOne({ _id: req.params.rootCategoryId })
        .then((data) => {
            promiseArr = []
            for (let i = 0; i < data.categories.length; i++) {
                if (data.categories[i].childCategories.length > 0) {
                    for (let j = 0; j < data.categories[i].childCategories.length; j++) {
                        promiseArr.push(new Promise((resolve, reject) => {
                            menuModel.findOneAndUpdate({
                                _id: req.params.rootCategoryId,
                                "categories.childCategories._id": req.params.childcategoryid
                            }, {
                                    $set: {
                                        "categories.$.childCategories.$[inner].childCategoryName": req.body.childCategoryName
                                    }
                                }, {
                                    "arrayFilters": [
                                        {
                                            "inner._id": req.params.childcategoryid
                                        }
                                    ]
                                }).then((udata) => {
                                    resolve(udata)
                                }).catch((err) => {
                                    reject(err)
                                })
                        }))
                    }
                } else {
                    continue
                }
            }
            Promise.all(promiseArr).then((promiseArr) => {
                res.json({ success: true, msg: "Category name Updated" })
            }).catch((err) => {
                res.json({ success: false, msg: "error in Promise arr", error: err })
            })
        }).catch((err) => {
            res.json({ success: false, msg: "error in data", error: err })
        })
}

const updateChildChildCategoryName = (req, res) => {
    menuModel.findOne({ _id: req.params.rootCategoryId })
        .then((data) => {
            promiseArr = []
            for (let i = 0; i < data.categories.length; i++) {
                if (data.categories[i].childCategories.length > 0) {
                    for (let j = 0; j < data.categories[i].childCategories.length; j++) {
                        if (data.categories[i].childCategories[j].childChildCategories.length > 0) {
                            for (let k = 0; k < data.categories[i].childCategories[j].childChildCategories.length; k++) {
                                promiseArr.push(new Promise((resolve, reject) => {
                                    menuModel.findOneAndUpdate({
                                        _id: req.params.rootCategoryId,
                                        "categories.childCategories._id": req.params.childcategoryid,
                                        "categories.childCategories.childChildCategories._id": req.params.childChildCategoryid
                                    }, {
                                            $set: {
                                                "categories.$.childCategories.$[inner].childChildCategories.$[outer].childChildCategoryName": req.body.childChildCategoryName
                                            }
                                        }, {
                                            "arrayFilters": [
                                                {
                                                    "inner._id": req.params.childcategoryid
                                                },
                                                {
                                                    "outer._id": req.params.childChildCategoryid
                                                }
                                            ]
                                        }).then((udata) => {
                                            resolve(udata)
                                        }).catch((err) => {
                                            reject(err)
                                        })
                                }))
                            }
                        }
                    }
                } else {
                    continue
                }
            }
            Promise.all(promiseArr).then((promiseArr) => {
                res.json({ success: true, msg: "child child name updated" })
            }).catch((err) => {
                res.json({ success: false, msg: "error in Promise arr", error: err })
            })
        }).catch((err) => {
            res.json({ success: false, msg: "error in data", error: err })
        })
}

module.exports = {
    addMenu,
    addCategories,
    addChildCategories,
    addChildChildCategories,
    getAllMenus,
    getMenu,
    updateRootCategoryName,
    updateCategoryName,
    updateChildCategoriesName,
    updateChildChildCategoryName

}
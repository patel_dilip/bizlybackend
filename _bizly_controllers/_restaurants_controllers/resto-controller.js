const restoModel = require('../../_bizly_models/_restaurant_models/_restaurant-model').restomodel
const fs = require('fs')

//Add Restaurant outlets Data
const addRestaurnatOutlets = (req, res) => {
    console.log("outlet>>>>>>>", req.body)
    var outlet = new restoModel()
    outlet.outletInfo = req.body.outletInfo
    outlet.addedBy = req.body.userid
    outlet.updatedBy = req.body.userid
    outlet.save((err, result) => {
        if (err) {
            res.json({ success: false, msg: err })
        } else {
            res.json({ success: true, msg: "Outlets Added", data: result._id })
        }
    })
}

// Add Restaurant Info Data
const addRestaurantInfo = (req, res) => {
    restoModel.findByIdAndUpdate(req.params.restoid,
        {
            restoInfo: req.body.restoInfo,
            updatedBy: req.body.userid,
            updatedAt: Date.now()
        }, (err, uresto) => {
            if (err) {
                res.json({ sucess: false, data: err })
            } else {
                res.json({ sucess: true, msg: "RestoInfo added" })
            }
        })
}

//Add Restaurant Establishment Data
const addRestaurantEstablishments = (req, res) => {
    restoModel.findByIdAndUpdate(req.params.restoid,
        {
            "establishmentType.estCategories": req.body

        }, (err, uresto) => {
            if (err) {
                res.json({ sucess: false, data: err })
            } else {
                res.json({ sucess: true, data: uresto })
            }
        })
}

// Add Restaurant Menu-Management menu Data
const addRestaurantmenuManagementMenu = (req, res) => {
    restoModel.findByIdAndUpdate(req.params.restoid,
        {
            "menuManagement.menu": req.body.menu,
            updatedBy: req.body.userid,
            updatedAt: Date.now()

        }, (err, uresto) => {
            if (err) {
                res.json({ sucess: false, data: err })
            } else {
                res.json({ sucess: true, data: uresto })
            }
        })
}

//Add Restaurant Menu-Management Liquor Data
const addRestaurantmenuManagementliquor = (req, res) => {
    restoModel.findByIdAndUpdate(req.params.restoid,
        {
            "menuManagement.liquor": req.body.liquor,
            updatedBy: req.body.userid,
            updatedAt: Date.now()

        }, (err, uresto) => {
            if (err) {
                res.json({ sucess: false, data: err })
            } else {
                res.json({ sucess: true, data: uresto })
            }
        })
}

//Add Restaurant Menu-Management Cuisines Data
const addRestaurantmenuManagementcuisines = (req, res) => {
    restoModel.findByIdAndUpdate(req.params.restoid, { $push: { "menuManagement.cuisines": req.body } }, (err, udata) => {
        if (err) {
            res.json({ sucess: false, msg: err })
        } else {
            res.json({ sucess: true, data: udata })
        }
    })
}

//Add Restaurant Legal Data
const addRestaurantLegalData = (req, res) => {
    restoModel.findByIdAndUpdate(req.params.restoid,
        {
            legalInfo: req.body.legalInfo,
            updatedBy: req.body.userid,
            updatedAt: Date.now()
        }, (err, uresto) => {
            if (err) {
                res.json({ sucess: false, data: err })
            } else {
                res.json({ sucess: true, data: uresto })
            }
        })
}

//Add Restaurant Media Data
const addRestaurantMedia = (req, res) => {
    restoModel.findByIdAndUpdate(req.params.restoid,
        {
            media: req.body.media,
            updatedBy: req.body.userid,
            updatedAt: Date.now()
        }, (err, umedia) => {
            if (err) {
                res.json({ sucess: false, data: err })
            } else {
                res.json({ sucess: true, data: umedia })
            }
        })
}

//Add Restaurant Layout Data
const addRestaurantLayout = (req, res) => {
    restoModel.findByIdAndUpdate(req.params.restoid,
        {
            layout: req.body.layout,
            updatedBy: req.body.userid,
            updatedAt: Date.now()
        }, (err, ulayout) => {
            if (err) {
                res.json({ sucess: false, msg: err })
            } else {
                res.json({ sucess: true, msg: ulayout })
            }
        })
}

//Add Restaurant More Info Data
const addrestaurantMoreinfo = (req, res) => {
    restoModel.findByIdAndUpdate(req.params.restoid,
        {
            $push: {
                "establishmentType.more_info": req.body
            }
        }, (err, umoreinfo) => {
            if (err) {
                res.json({ sucess: false, msg: err })
            } else {
                res.json({ sucess: true, data: umoreinfo })
            }
        })
}

//Add Restaurant Services Data
const addrestaurantServices = (req, res) => {
    restoModel.findByIdAndUpdate(req.params.restoid,
        {
            $push: {
                "establishmentType.services": req.body
            }
        }, (err, uservices) => {
            if (err) {
                res.json({ sucess: false, msg: err })
            } else {
                res.json({ sucess: true, data: uservices })
            }
        })
}

//Add Restaurnat Rules And Regulation Data
const addrestaurantRulesandregulations = (req, res) => {
    restoModel.findByIdAndUpdate(req.params.restoid,
        {
            $push: {
                "establishmentType.rulesandregulations": req.body
            }
        }, (err, urulesandregulations) => {
            if (err) {
                res.json({ sucess: false, msg: err })
            } else {
                res.json({ sucess: true, data: urulesandregulations })
            }
        })
}

//Add Restaurnat Ambiences Data
const addrestaurantAmbiences = (req, res) => {
    restoModel.findByIdAndUpdate(req.params.restoid,
        {
            $push: {
                "establishmentType.ambiences": req.body
            }
        }, (err, urulesandregulations) => {
            if (err) {
                res.json({ sucess: false, msg: err })
            } else {
                res.json({ sucess: true, data: urulesandregulations })
            }
        })
}

//Add Restaurant Organization Data
// const addrestaurantOrganization = (req, res) => {
//     restoModel.findByIdAndUpdate(req.params.restoid,
//         {
//             $push: {
//                 organization: req.body
//             }
//         }, (err, udata) => {
//             if (err) {
//                 res.json({ success: false, msg: err })
//             } else {
//                 res.json({ success: true, data: udata })
//             }
//         })
// }

// Add Restaurant Organizations outlets Data
// const addrestaurantOrganizationOutlets = (req, res) => {
//     restoModel.updateOne({ _id: req.params.restoid, "organization._id": req.params.organizationid },
//         {
//             $push: {
//                 "organization.$.outlets_arr": req.body.outlets_arr
//             }
//         }, (err, udata) => {
//             if (err) {
//                 res.json({ success: true, msg: err })
//             } else {
//                 res.json({ success: true, data: udata })
//             }
//         })
// }

//Get Restaurant Data By restoid
const getRestaurant = (req, res) => {
    restoModel.findById(req.params.restoid).
        populate('addedBy', 'userName').
        populate('updatedBy', 'userName').
        exec((err, restaurant) => {
            if (err) {
                res.json({ sucess: false, data: err })
            } else if (restaurant == null) {
                res.json({ sucess: false, data: 'No restaurant available' })
            } else {
                res.json({ sucess: true, data: restaurant })
            }
        })
}

//Get All Restaurants Data
const getAllRestaurants = (req, res) => {
    restoModel.find({}).
        populate('addedBy', 'userName').
        populate('updatedBy', 'userName').
        exec((err, restaurants) => {
            if (err) {
                res.json({ sucess: false, data: err })
            } else if (restaurants == null || restaurants.length == 0) {
                res.json({ sucess: false, data: 'No restaurants available' })
            } else {
                res.json({ sucess: true, data: restaurants })
            }
        })
}

//Update Restaurants outletInfo Data
const updateRestaurant = (req, res) => {
    restoModel.findOneAndUpdate({ _id: req.params.restoid },
        {
            outletInfo: req.body.outletInfo,
            updatedBy: req.body.userid,
            updatedAt: Date.now()

        }, (err, uresto) => {
            if (err) {
                res.json({ sucess: false, msg: err })
            } else if (uresto == null) {
                res.json({ sucess: false, msg: 'No restaurant available' })
            } else {
                res.json({ sucess: true, msg: "outlet info updated" })
            }
        })
}

// // Update Restaurant Info Data
// const updateRestaurantInfo = (req, res) => {
//     restoModel.findByIdAndUpdate(req.params.restoid,
//         {
//             restoInfo: req.body.restoInfo,
//             updatedBy: req.body.userid,
//             updatedAt: Date.now()

//         }, (err, uresto) => {
//             if (err) {
//                 res.json({ sucess: false, data: err })
//             } else {
//                 res.json({ sucess: true, msg: "RestoInfo added" })
//             }
//         })
// }

//Update Restaurant est Category
const updaterestaurantESTcategory = (req, res) => {
    restoModel.findOneAndUpdate({ _id: req.params.restoid },
        {
            "establishmentType.estCategories": req.body.estCategories,
            updatedBy: req.body.userid,
            updatedAt: Date.now()

        }, (err, umoreinfo) => {
            if (err) {
                res.json({ sucess: false, msg: err })
            } else {
                res.json({ sucess: true, msg: "est Category updated" })
            }
        })
}

//Update Restaurant More Info Data
const updaterestaurantMoreinfo = (req, res) => {
    restoModel.findOneAndUpdate({ _id: req.params.restoid },
        {
            "establishmentType.more_info": req.body.more_info,
            updatedBy: req.body.userid,
            updatedAt: Date.now()

        }, (err, umoreinfo) => {
            if (err) {
                res.json({ sucess: false, msg: err })
            } else {
                res.json({ sucess: true, msg: "more info updated" })
            }
        })
}

//Update Restaurant Services Data
const updaterestaurantServices = (req, res) => {
    restoModel.findOneAndUpdate({ _id: req.params.restoid },
        {
            "establishmentType.services": req.body.services,
            updatedBy: req.body.userid,
            updatedAt: Date.now()

        }, (err, uservices) => {
            if (err) {
                res.json({ sucess: false, msg: err })
            } else {
                res.json({ sucess: true, msg: "services updated" })
            }
        })
}

//Update Restaurnat Rules And Regulation Data
const updaterestaurantRulesandregulations = (req, res) => {
    restoModel.findOneAndUpdate({ _id: req.params.restoid },
        {
            "establishmentType.rulesandregulations": req.body.rulesandregulations,
            updatedBy: req.body.userid,
            updatedAt: Date.now()

        }, (err, urulesandregulations) => {
            if (err) {
                res.json({ sucess: false, msg: err })
            } else {
                res.json({ sucess: true, msg: "rules and regulation updated" })
            }
        })
}

//Update Restaurnat Ambiences Data
const updaterestaurantAmbiences = (req, res) => {
    restoModel.findOneAndUpdate({ _id: req.params.restoid },
        {
            "establishmentType.ambiences": req.body.ambiences,
            updatedBy: req.body.userid,
            updatedAt: Date.now()

        }, (err, urulesandregulations) => {
            if (err) {
                res.json({ sucess: false, msg: err })
            } else {
                res.json({ sucess: true, msg: "Ambience updated" })
            }
        })
}


//Update Restaurant Menu-Management Cuisines Data
const updateRestaurantmenuManagementcuisines = (req, res) => {
    restoModel.findOneAndUpdate({ _id: req.params.restoid },
        {
            "menuManagement.cuisines": req.body.cuisines,
            updatedBy: req.body.userid,
            updatedAt: Date.now()

        }, (err, udata) => {
            if (err) {
                res.json({ sucess: false, msg: err })
            } else {
                res.json({ sucess: true, data: udata })
            }
        })
}

// // Update Restaurant Menu-Management menu Data
// const updateRestaurantmenuManagementMenu = (req, res) => {
//     restoModel.findOneAndUpdate({_id:req.params.restoid},
//         {
//             "menuManagement.menu": req.body

//         }, (err, uresto) => {
//             if (err) {
//                 res.json({ sucess: false, data: err })
//             } else {
//                 res.json({ sucess: true, data: uresto })
//             }
//         })
// }

//Soft Delete for Restaurant Data 
const deleteRestaurant = (req, res) => {
    restoModel.findByIdAndUpdate(req.params.restoid, { "restoInfo.restoStatus": req.body.status }, (err, uresto) => {
        if (err) {
            res.json({ sucess: false, data: err })
        } else if (uresto == null) {
            res.json({ sucess: false, data: 'No restaurant available' })
        } else {
            res.json({ sucess: true, data: uresto })
        }
    })
}

const deleteRestaurantByid = (req, res) => {
    restoModel.findOneAndDelete({ _id: req.params.restoid }, (err, uresto) => {
        if (err) {
            res.json({ sucess: false, data: err })
        } else if (uresto == null) {
            res.json({ sucess: false, data: 'No restaurant available' })
        } else {
            res.json({ sucess: true, data: uresto })
        }
    })
}

const deleteAllRestaurant = (req, res) => {
    restoModel.remove((err, uresto) => {
        if (err) {
            res.json({ sucess: false, data: err })
        } else if (uresto == null) {
            res.json({ sucess: false, data: 'No restaurant available' })
        } else {
            res.json({ sucess: true, msg: "All Restaurant Deleted" })
        }
    })
}

//Delete Restaurant Logo Image
const deleteRestaurantLogoImage = (req, res) => {
    fs.unlink('/home/bizlypos/public_html/uploads/reataurant_logos' + '/' + req.body.file, (err) => {
        if (err) {
            res.json({ success: false, msg: err })
        } else {
            res.json({ success: true, msg: "Restaurant Logo File Deleted" })
        }
    })
}

//Delete Restaurant Banner Image
const deleteRestaurantBannerImage = (req, res) => {
    fs.unlink('/home/bizlypos/public_html/uploads/reataurant_banners' + '/' + req.body.file, (err) => {
        if (err) {
            res.json({ success: false, msg: err })
        } else {
            res.json({ success: true, msg: "Restaurant Banners File Deleted" })
        }
    })
}

//Delete Restaurant Albums Images
const deleteRestaurantAlbumsImage = (req, res) => {
    fs.unlink('/home/bizlypos/public_html/uploads/reataurant_album_images' + '/' + req.body.file, (err) => {
        if (err) {
            res.json({ success: false, msg: err })
        } else {
            res.json({ success: true, msg: "Restaurant Albums Images File Deleted" })
        }
    })
}

//Delete Restaurant Albums Videos
const deleteRestaurantAlbumsVideos = (req, res) => {
    fs.unlink('/home/bizlypos/public_html/uploads/reataurant_album_videos' + '/' + req.body.file, (err) => {
        if (err) {
            res.json({ success: false, msg: err })
        } else {
            res.json({ success: true, msg: "Restaurant Albums Video File Deleted" })
        }
    })
}

module.exports = {

    // ADD
    addRestaurnatOutlets,
    addRestaurantInfo,
    addRestaurantLegalData,
    addRestaurantEstablishments,
    addRestaurantmenuManagementMenu,
    addRestaurantmenuManagementliquor,
    addRestaurantmenuManagementcuisines,
    // addrestaurantOrganization,
    // addrestaurantOrganizationOutlets,
    addRestaurantMedia,
    addRestaurantLayout,
    addrestaurantMoreinfo,
    addrestaurantServices,
    addrestaurantRulesandregulations,
    addrestaurantAmbiences,

    //Get
    getRestaurant,
    getAllRestaurants,

    //Update
    updateRestaurant,
    // updateRestaurantInfo,
    updaterestaurantESTcategory,
    updaterestaurantMoreinfo,
    updaterestaurantServices,
    updaterestaurantRulesandregulations,
    updaterestaurantAmbiences,
    updateRestaurantmenuManagementcuisines,

    //Delete
    deleteRestaurantLogoImage,
    deleteRestaurant,
    deleteRestaurantByid,
    deleteAllRestaurant,
    deleteRestaurantBannerImage,
    deleteRestaurantAlbumsImage,
    deleteRestaurantAlbumsVideos
}
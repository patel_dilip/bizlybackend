const liquorTypeModel = require('../../_bizly_models/_restaurant_models/_liquor-type-model').liquorModel

// Add Root Drink name of Liquor Type
const addRootDrinkName = (req, res) => {
    var rootCategory = new liquorTypeModel()
    rootCategory.rootCategoryName = req.body.rootCategoryName,
        rootCategory.isFilterable = req.body.isFilterable,
        rootCategory.isSearchable = req.body.isSearchable,
        rootCategory.priority = req.body.priority
    rootCategory.addedBy = req.body.userid
    rootCategory.updatedBy = req.body.userid

    console.log(rootCategory);

    rootCategory.save((err) => {
        if (err) {
            throw err
        } else {
            res.json({ sucess: true, msg: 'rootcategory added' })
        }
    })
}

//Add Varients of Liquor type
const addVarients = (req, res) => {
    liquorTypeModel.findByIdAndUpdate(req.params.rootDrinkId,
        { $push: { categories: req.body } },
        (err, categories) => {
            if (err) {
                throw err
            } else if (categories == null) {
                res.json({ sucess: false, msg: "rootCategory not found" })
            } else {
                res.json({ sucess: true, msg: "rootCategory updated" })
            }
        })
}

//Add Sub Varients
const addSubVarients = (req, res) => {
    liquorTypeModel.updateOne({ _id: req.params.rootDrinkId, "categories._id": req.params.varientId },
        { $addToSet: { "categories.$.childCategories": req.body } },
        (err, uchildcategory) => {
            if (err) {
                throw err
            } else if (uchildcategory == null) {
                res.json({ sucess: false, msg: "rootCategory not found" })
            } else {
                res.json({ sucess: true, msg: "rootCategory updated" })
            }
        })
}

//Add Sub Sub Varients of liquor
const addSubSubVarients = (req, res) => {
    liquorTypeModel.updateOne(
        {
            _id: req.params.rootDrinkId,
            "categories.childCategories._id": req.params.subvarientid
        },
        {
            $push: {

                "categories.$.childCategories.$[outer].childChildCategories": req.body

            }
        },
        {
            "arrayFilters": [
                {
                    "outer._id": req.params.subvarientid,
                }
            ]
        },
        (err, uchildchildcategory) => {
            if (err) {
                throw err
            } else if (uchildchildcategory == null) {
                res.json({ sucess: false, msg: "rootCategory not found" })
            } else {
                res.json({ sucess: true, msg: "rootCategory updated" })
            }
        })
}

//Get All LiquorTypes Data
const getAllLiquortypes = (req, res) => {
    liquorTypeModel.find().
        populate('addedBy', 'userName').
        populate('updatedBy', 'userName').
        exec((err, liquor) => {
            if (err) {
                res.json({ sucess: false, msg: err })
            } else if (liquor.length == 0 || liquor == null) {
                res.json({ sucess: false, msg: 'liquor type not found' })
            } else {
                res.json({ sucess: true, data: liquor })
            }
        })
}

//Get Liquior type Data By liquortypeid
const getLiquortype = (req, res) => {
    liquorTypeModel.findById(req.params.liquortypeid).
        populate('addedBy', 'userName').
        populate('updatedBy', 'userName').
        exec((err, liquor) => {
            if (err) {
                res.json({ sucess: false, msg: err })
            } else if (liquor == null) {
                res.json({ sucess: false, msg: 'liquor type not found' })
            } else {
                res.json({ sucess: true, data: liquor })
            }
        })
}

const updateRootDrinkName = (req, res) => {
    liquorTypeModel.findOneAndUpdate({ _id: req.params.rootCategoryId },
        {
            $set: {
                rootCategoryName: req.body.rootCategoryName
            }
        }, (err, udata) => {
            if (err) {
                res.json({ success: false, msg: err })
            } else if (udata == null) {
                res.json({ success: false, msg: "Data not found" })
            } else {
                res.json({ success: true, msg: "Root name Updated" })
            }
        })
}

const updateCategoryName = (req, res) => {
    liquorTypeModel.findOne({ _id: req.params.rootCategoryId, })
        .then((data) => {
            promiseArr = []
            for (let i = 0; i < data.categories.length; i++) {
                promiseArr.push(new Promise((resolve, reject) => {
                    liquorTypeModel.findOneAndUpdate({
                        _id: req.params.rootCategoryId,
                        "categories._id": req.params.categoresId
                    }, {
                            $set: {
                                "categories.$.categoryName": req.body.categoryName
                            }
                        }).then((udata) => {
                            resolve(udata)
                        }).catch((err) => {
                            reject(err)
                        })
                }))
            }
            Promise.all(promiseArr).then((promiseArr) => {
                res.json({ success: true, msg: "Category name Updated" })
            }).catch((err) => {
                res.json({ success: false, msg: "error in Promise arr", error: err })
            })
        }).catch((err) => {
            res.json({ success: false, msg: "error in data", error: err })
        })
}

const updateChildCategoriesName = (req, res) => {
    liquorTypeModel.findOne({ _id: req.params.rootCategoryId })
        .then((data) => {
            promiseArr = []
            for (let i = 0; i < data.categories.length; i++) {
                if (data.categories[i].childCategories.length > 0) {
                    for (let j = 0; j < data.categories[i].childCategories.length; j++) {
                        promiseArr.push(new Promise((resolve, reject) => {
                            liquorTypeModel.findOneAndUpdate({
                                _id: req.params.rootCategoryId,
                                "categories.childCategories._id": req.params.childcategoryid
                            }, {
                                    $set: {
                                        "categories.$.childCategories.$[inner].childCategoryName": req.body.childCategoryName
                                    }
                                }, {
                                    "arrayFilters": [
                                        {
                                            "inner._id": req.params.childcategoryid
                                        }
                                    ]
                                }).then((udata) => {
                                    resolve(udata)
                                }).catch((err) => {
                                    reject(err)
                                })
                        }))
                    }
                } else {
                    continue
                }
            }
            Promise.all(promiseArr).then((promiseArr) => {
                res.json({ success: true, msg: "Category name Updated" })
            }).catch((err) => {
                res.json({ success: false, msg: "error in Promise arr", error: err })
            })
        }).catch((err) => {
            res.json({ success: false, msg: "error in data", error: err })
        })
}

const updateChildChildCategoryName = (req, res) => {
    liquorTypeModel.findOne({ _id: req.params.rootCategoryId })
        .then((data) => {
            promiseArr = []
            for (let i = 0; i < data.categories.length; i++) {
                if (data.categories[i].childCategories.length > 0) {
                    for (let j = 0; j < data.categories[i].childCategories.length; j++) {
                        if (data.categories[i].childCategories[j].childChildCategories.length > 0) {
                            for (let k = 0; k < data.categories[i].childCategories[j].childChildCategories.length; k++) {
                                promiseArr.push(new Promise((resolve, reject) => {
                                    liquorTypeModel.findOneAndUpdate({
                                        _id: req.params.rootCategoryId,
                                        "categories.childCategories._id": req.params.childcategoryid,
                                        "categories.childCategories.childChildCategories._id": req.params.childChildCategoryid
                                    }, {
                                            $set: {
                                                "categories.$.childCategories.$[inner].childChildCategories.$[outer].childChildCategoryName": req.body.childChildCategoryName
                                            }
                                        }, {
                                            "arrayFilters": [
                                                {
                                                    "inner._id": req.params.childcategoryid
                                                },
                                                {
                                                    "outer._id": req.params.childChildCategoryid
                                                }
                                            ]
                                        }).then((udata) => {
                                            resolve(udata)
                                        }).catch((err) => {
                                            reject(err)
                                        })
                                }))
                            }
                        }
                    }
                } else {
                    continue
                }
            }
            Promise.all(promiseArr).then((promiseArr) => {
                res.json({ success: true, msg: "child child name updated" })
            }).catch((err) => {
                res.json({ success: false, msg: "error in Promise arr", error: err })
            })
        }).catch((err) => {
            res.json({ success: false, msg: "error in data", error: err })
        })
}

module.exports = {
    addRootDrinkName,
    addVarients,
    addSubVarients,
    addSubSubVarients,
    getAllLiquortypes,
    getLiquortype,
    updateRootDrinkName,
    updateCategoryName,
    updateChildCategoriesName,
    updateChildChildCategoryName
}

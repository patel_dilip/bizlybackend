const ambienceModel = require('../../_bizly_models/_restaurant_models/_ambience-model').ambiencemodel

// Add Ambience
const addAmbience = (req, res) => {
    var ambience = new ambienceModel()
    ambience.title = req.body.title
    ambience.ambiences = req.body.ambiences
    ambience.addedBy = req.body.userid
    ambience.updatedBy = req.body.userid
    ambience.save((err,result) => {
        if (err) {
            throw err
        } else {
            res.json({ sucess: true, msg: 'ambience added',id: result._id })
        }
    })
}

//Get Ambience By ambienceid
const getAmbience = (req, res) => {
    ambienceModel.findById(req.params.ambienceid).
        populate('addedBy', 'userName').
        populate('updatedBy', 'userName').
        exec((err, ambience) => {
            if (err) {
                throw err
            } else if (ambience == null) {
                res.json({ sucess: false, msg: 'Ambience not found' })
            } else {
                res.json({ sucess: true, msg: 'Ambience found', data: ambience })
            }
        })
}

// Get All Ambiences
const getAllAmbiences = (req, res) => {
    ambienceModel.find({}).
        populate('addedBy', 'userName').
        populate('updatedBy', 'userName').
        exec((err, data) => {
            if (err) {
                throw err
            } else if (data == null || data.length == 0) {
                res.json({ sucess: false, msg: "Ambience Not Found" })
            } else {
                res.json({ sucess: true, msg: "Ambiences Found", data: data })
            }
        })
}

//Update Ambience Data
const updateAmbience = (req, res) => {
    ambienceModel.findByIdAndUpdate(req.params.ambienceid, req.body, (err, uambience) => {
        if (err) {
            throw err
        } else if (uambience == null) {
            res.json({ sucess: false, msg: "Ambience not found" })
        } else {
            res.json({ sucess: true, msg: "Ambience updated" })
        }
    })
}

//Delete Ambience Data by Id
const deleteAmbienceById = (req, res) => {
    ambienceModel.findByIdAndDelete(req.params.ambienceid, (err, dambience) => {
        if (err) {
            throw err
        } else if (dambience == null) {
            res.json({ sucess: false, msg: "Ambience not found" })
        } else {
            res.json({ sucess: true, msg: "Ambience Deleted" })
        }
    })
}

//Delete All Ambience Data 
const deleteALLAmbience = (req, res) => {
    ambienceModel.remove((err, dambience) => {
        if (err) {
            throw err
        } else if (dambience == null) {
            res.json({ sucess: false, msg: "Ambience not found" })
        } else {
            res.json({ sucess: true, msg: "Ambience Deleted" })
        }
    })
}
module.exports = {
    addAmbience,
    getAmbience,
    getAllAmbiences,
    updateAmbience,
    deleteAmbienceById,
    deleteALLAmbience
}
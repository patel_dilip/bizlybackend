const websiteModel = require('../../_bizly_models/_restaurant_models/_websites-model').website

//Add Website Name Data
const addWebsite = (req, res) => {
    websiteModel.findOne({ websitename: req.body.websitename }, (err, data) => {
        if (err) {
            res.json({ success: false, msg: err })
        } else if (data) {
            res.json({ success: false, msg: "This website Already Exists" })
        } else {
            var website = new websiteModel()
            website.websitename = req.body.websitename
            website.addedBy = req.body.userid
            website.updatedBy = req.body.userid

            website.save((err,result) => {
                if (err) {
                    res.json({ success: false, msg: err })
                } else {
                    res.json({ success: true, msg: "Website Added", id:result._id })
                }
            })
        }
    })

}

//Update Website Data
const updateWebsites = (req, res) => {
    websiteModel.findByIdAndUpdate(req.params.websiteid,
        {
            $set: {
                websitename: req.body.websitename,
                updatedAt: Date.now()
            }
        }, (err, udata) => {
            if (err) {
                res.json({ success: false, msg: err })
            } else if (udata == null || udata.length == 0) {
                res.json({ success: false, msg: "Data not found" })
            } else {
                res.json({ success: true, msg: "Website data updated" })
            }
        })
}

module.exports = {
    addWebsite,
    updateWebsites
}
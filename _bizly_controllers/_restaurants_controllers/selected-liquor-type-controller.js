const selected_liquor_Model = require('../../_bizly_models/_restaurant_models/_selected_liquor_type_model').selected_liquor_Model

const addRootCategory = (req, res) => {
    var rootCategory = new selected_liquor_Model()
    rootCategory.rootCategoryName = req.body.rootCategoryName,
        rootCategory.isFilterable = req.body.isFilterable,
        rootCategory.isSearchable = req.body.isSearchable,
        rootCategory.priority = req.body.priority
    rootCategory.addedBy = req.body.userid
    rootCategory.updatedBy = req.body.userid

    console.log(rootCategory);

    rootCategory.save((err,result) => {
        if (err) {
            throw err
        } else {
            res.json({ sucess: true, msg: 'rootcategory added',data:result })
        }
    })
}

const addCategories = (req, res) => {
    selected_liquor_Model.findByIdAndUpdate(req.params.rootCategoryId,
        { $push: { categories: req.body } },
        (err, categories) => {
            if (err) {
                throw err
            } else if (categories == null) {
                res.json({ sucess: false, msg: "rootCategory not found" })
            } else {
                res.json({ sucess: true, msg: "rootCategory updated" })
            }
        })
}

const addChildCategories = (req, res) => {
    selected_liquor_Model.updateOne({ _id: req.params.rootCategoryId, "categories._id": req.params.categoresId },
        // { $addToSet: { "categories.$.childCategories": { childCategoryName: req.body.childCategoryName } } },
        { $addToSet: { "categories.$.childCategories": req.body } },
        (err, uchildcategory) => {
            if (err) {
                throw err
            } else if (uchildcategory == null) {
                res.json({ sucess: false, msg: "rootCategory not found" })
            } else {
                res.json({ sucess: true, msg: "rootCategory updated" })
            }
        })
}

const addChildChildCategories = (req, res) => {
    selected_liquor_Model.updateOne(
        {
            _id: req.params.rootCategoryId,
            "categories.childCategories._id": req.params.childcategoryid
        },
        {
            $push: {
                // "categories.$.childCategories.$[outer].childChildCategories": req.body.childChildCategoryName
                "categories.$.childCategories.$[outer].childChildCategories": req.body

            }
        },
        {
            "arrayFilters": [
                {
                    "outer._id": req.params.childcategoryid,
                }
            ]
        },
        (err, uchildchildcategory) => {
            if (err) {
                throw err
            } else if (uchildchildcategory == null) {
                res.json({ sucess: false, msg: "rootCategory not found" })
            } else {
                res.json({ sucess: true, msg: "rootCategory updated" })
            }
        })
}

const getAllSelectedLiquortype = (req, res) => {
    selected_liquor_Model.find().
        populate('addedBy', 'userName').
        populate('updatedBy', 'userName').
        exec((err, data) => {
            if (err) {
                res.json({ sucess: false, msg: err })
            } else if (data.length == 0 || data == null) {
                res.json({ sucess: false, msg: 'selected liquor not found' })
            } else {
                res.json({ sucess: true, data: data })
            }
        })
}

const getSelectedLiquretype = (req, res) => {
    selected_liquor_Model.findById(req.params.liquorid).
        populate('addedBy', 'userName').
        populate('updatedBy', 'userName').
        exec((err, data) => {
            if (err) {
                res.json({ sucess: false, msg: err })
            } else if (data == null) {
                res.json({ sucess: false, msg: 'selected liquor not found' })
            } else {
                res.json({ sucess: true, data: data })
            }
        })
}

const deleteSelectedLiquortype = (req, res) => {
    selected_liquor_Model.findByIdAndDelete(req.params.liquorid, (err) => {
        if (err) {
            res.json({ success: false, msg: err })
        } else {
            res.json({ success: true, msg: "Selected Liquor Deleted" })
        }
    })
}
module.exports = {
    addRootCategory,
    addCategories,
    addChildCategories,
    addChildChildCategories,
    getAllSelectedLiquortype,
    getSelectedLiquretype,
    deleteSelectedLiquortype

}

const selected_establishmentModel = require('../../_bizly_models/_restaurant_models/_selected_establishment_model').selected_establishment_model

const addRootCategory = (req, res) => {
    var rootCategory = new selected_establishmentModel()
    rootCategory.rootCategoryName = req.body.rootCategoryName,
        rootCategory.isFilterable = req.body.isFilterable,
        rootCategory.isSearchable = req.body.isSearchable,
        rootCategory.priority = req.body.priority
    rootCategory.addedBy = req.body.userid
    rootCategory.updatedBy = req.body.userid

    console.log(rootCategory);

    rootCategory.save((err,result) => {
        if (err) {
            throw err
        } else {
            res.json({ sucess: true, msg: 'rootcategory added',data:result })
        }
    })
}

const addCategories = (req, res) => {
    selected_establishmentModel.findByIdAndUpdate(req.params.rootCategoryId,
        { $push: { categories: req.body } },
        (err, categories) => {
            if (err) {
                throw err
            } else if (categories == null) {
                res.json({ sucess: false, msg: "rootCategory not found" })
            } else {
                res.json({ sucess: true, msg: "rootCategory updated" })
            }
        })
}

const addChildCategories = (req, res) => {
    selected_establishmentModel.updateOne({ _id: req.params.rootCategoryId, "categories._id": req.params.categoresId },
        // { $addToSet: { "categories.$.childCategories": { childCategoryName: req.body.childCategoryName } } },
        { $addToSet: { "categories.$.childCategories": req.body } },
        (err, uchildcategory) => {
            if (err) {
                throw err
            } else if (uchildcategory == null) {
                res.json({ sucess: false, msg: "rootCategory not found" })
            } else {
                res.json({ sucess: true, msg: "rootCategory updated" })
            }
        })
}

const addChildChildCategories = (req, res) => {
    selected_establishmentModel.updateOne(
        {
            _id: req.params.rootCategoryId,
            "categories.childCategories._id": req.params.childcategoryid
        },
        {
            $push: {
                // "categories.$.childCategories.$[outer].childChildCategories": req.body.childChildCategoryName
                "categories.$.childCategories.$[outer].childChildCategories": req.body

            }
        },
        {
            "arrayFilters": [
                {
                    "outer._id": req.params.childcategoryid,
                }
            ]
        },
        (err, uchildchildcategory) => {
            if (err) {
                throw err
            } else if (uchildchildcategory == null) {
                res.json({ sucess: false, msg: "rootCategory not found" })
            } else {
                res.json({ sucess: true, msg: "rootCategory updated" })
            }
        })
}

const getAllEstablishments = (req, res) => {
    selected_establishmentModel.find().
        populate('addedBy', 'userName').
        populate('updatedBy', 'userName').
        exec((err, establishments) => {
            if (err) {
                res.json({ sucess: false, msg: err })
            } else if (establishments.length == 0 || establishments == null) {
                res.json({ sucess: false, msg: 'establishments not found' })
            } else {
                res.json({ sucess: true, data: establishments })
            }
        })
}

const getEstablishment = (req, res) => {
    selected_establishmentModel.findById(req.params.establishmentid).
        populate('addedBy', 'userName').
        populate('updatedBy', 'userName').
        exec((err, establishment) => {
            if (err) {
                res.json({ sucess: false, msg: err })
            } else if (establishment == null) {
                res.json({ sucess: false, msg: 'establishment not found' })
            } else {
                res.json({ sucess: true, data: establishment })
            }
        })
}

const deleteSelectedEstablishment = (req, res) => {
    selected_establishmentModel.findByIdAndDelete(req.params.establishmentid, (err) => {
        if (err) {
            res.json({ success: false, msg: err })
        } else {
            res.json({ success: true, msg: "establishment Deleted" })
        }
    })
}
module.exports = {
    addRootCategory,
    addCategories,
    addChildCategories,
    addChildChildCategories,
    getAllEstablishments,
    getEstablishment,
    deleteSelectedEstablishment

}

const rulesAndRegulationsModel = require('../../_bizly_models/_restaurant_models/_rulesandregulation-model').rulesandregulationsmodel

const addRuleAndRegulation = (req, res) => {
    var ruleandregulation = new rulesAndRegulationsModel()
    ruleandregulation.title = req.body.title
    ruleandregulation.ruleandregulations = req.body.ruleandregulations
    ruleandregulation.addedBy = req.body.userid
    ruleandregulation.updatedBy = req.body.userid
    ruleandregulation.save((err, result) => {
        if (err) {
            throw err
        } else {
            res.json({ sucess: true, msg: 'service added', id: result._id })
        }
    })
}

const getRuleAndRegulation = (req, res) => {
    rulesAndRegulationsModel.findById(req.params.ruleandregulationid).
        populate('addedBy', 'userName').
        populate('updatedBy', 'userName').
        exec((err, ruleandregulation) => {
            if (err) {
                throw err
            } else if (ruleandregulation == null) {
                res.json({ sucess: false, msg: 'Ruleandregulation not found' })
            } else {
                res.json({ sucess: true, msg: 'Ruleandregulation found', data: ruleandregulation })
            }
        })
}

const getAllRulesAndRegulations = (req, res) => {
    rulesAndRegulationsModel.find({}).
        populate('addedBy', 'userName').
        populate('updatedBy', 'userName').
        exec((err, data) => {
            if (err) {
                throw err
            } else if (data == null || data.length == 0) {
                res.json({ sucess: false, msg: "RulesAndRegulations Not Found" })
            } else {
                res.json({ sucess: true, msg: "RulesAndRegulations Found", data: data })
            }
        })
}

const updateRuleAndRegulation = (req, res) => {
    rulesAndRegulationsModel.findOneAndUpdate({ _id: req.params.ruleandregulationid },
        {
            title: req.body.title,
            ruleandregulations: req.body.ruleandregulations
        }, (err, uruleandregulation) => {
            if (err) {
                throw err
            } else if (uruleandregulation == null) {
                res.json({ sucess: false, msg: "RuleAndRegulation not found" })
            } else {
                res.json({ sucess: true, msg: "RuleAndRegulation updated" })
            }
        })
}

const deleteALLRuleAndRegulation = (req, res) => {
    rulesAndRegulationsModel.remove((err, druleandregulation) => {
        if (err) {
            throw err
        } else if (druleandregulation == null) {
            res.json({ sucess: false, msg: "RuleAndRegulation not found" })
        } else {
            res.json({ sucess: true, msg: "RuleAndRegulation Deleted" })
        }
    })
}

const deleteRuleAndRegulationById = (req, res) => {
    rulesAndRegulationsModel.findByIdAndDelete(req.params.ruleandregulationid, (err, druleandregulation) => {
        if (err) {
            throw err
        } else if (druleandregulation == null) {
            res.json({ sucess: false, msg: "RuleAndRegulation not found" })
        } else {
            res.json({ sucess: true, msg: "RuleAndRegulation Deleted" })
        }
    })
}
module.exports = {
    addRuleAndRegulation,
    getRuleAndRegulation,
    getAllRulesAndRegulations,
    updateRuleAndRegulation,
    deleteALLRuleAndRegulation,
    deleteRuleAndRegulationById
}
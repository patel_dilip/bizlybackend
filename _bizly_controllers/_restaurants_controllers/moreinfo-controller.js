const moreinfoModel = require('../../_bizly_models/_restaurant_models/_moreinfo-model').moremodel

// Add MoreInfo Data
const addMoreInfo = (req, res) => {
    var moreinfo = new moreinfoModel()
    // moreinfo.moreInfoCategory = req.body.moreInfoCategory
    moreinfo.title = req.body.title
    moreinfo.moreinfo = req.body.moreinfo
    moreinfo.addedBy = req.body.userid
    moreinfo.updatedBy = req.body.userid
    moreinfo.save((err, result) => {
        if (err) {
            throw err
        } else {
            res.json({ sucess: true, msg: 'moreifo added', id: result._id })
        }
    })
}

//Get moreinfo Data by moreinfoid
const getMoreInfo = (req, res) => {
    moreinfoModel.findById(req.params.moreinfoid).
        populate('addedBy', 'userName').
        populate('updatedBy', 'userName').
        exec((err, moreinfo) => {
            if (err) {
                throw err
            } else if (moreinfo == null) {
                res.json({ sucess: false, msg: 'Moreinfo not found' })
            } else {
                res.json({ sucess: true, msg: 'Moreinfo found', data: moreinfo })
            }
        })
}

//Get All MoreInfo Data
const getAllMoreInfos = (req, res) => {
    moreinfoModel.find({}).
        populate('addedBy', 'userName').
        populate('updatedBy', 'userName').
        exec((err, data) => {
            if (err) {
                throw err
            } else if (data == null || data.length == 0) {
                res.json({ sucess: false, msg: "Data Not Found" })
            } else {
                res.json({ sucess: true, msg: "Data Found", data: data })
            }
        })
}

//Update MoreInfo Data
const updateMoreInfo = (req, res) => {
    moreinfoModel.findOneAndUpdate({ _id: req.params.moreinfoid },
        {
            title: req.body.title,
            moreinfo: req.body.moreinfo

        }, (err, umoreinfo) => {
            if (err) {
                throw err
            } else if (umoreinfo == null) {
                res.json({ sucess: false, msg: "moreinfo not found" })
            } else {
                res.json({ sucess: true, msg: "Data updated" })
            }
        })
}

//Delete All MoreInfo Data
const deleteALLMoreInfo = (req, res) => {
    moreinfoModel.remove((err, dmoreinfo) => {
        if (err) {
            throw err
        } else if (dmoreinfo == null) {
            res.json({ sucess: false, msg: "moreinfo not found" })
        } else {
            res.json({ sucess: true, msg: "Moreinfo Deleted" })
        }
    })
}

//Delete MoreInfo Data by id
const deleteMoreInfByID = (req, res) => {
    moreinfoModel.findByIdAndDelete(req.params.moreinfoid, (err, dmoreinfo) => {
        if (err) {
            throw err
        } else if (dmoreinfo == null) {
            res.json({ sucess: false, msg: "moreinfo not found" })
        } else {
            res.json({ sucess: true, msg: "Moreinfo Deleted" })
        }
    })
}
module.exports = {
    addMoreInfo,
    getMoreInfo,
    getAllMoreInfos,
    updateMoreInfo,
    deleteALLMoreInfo,
    deleteMoreInfByID
}
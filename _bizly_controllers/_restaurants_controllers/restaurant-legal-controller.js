const restaurantLegalModel = require('../../_bizly_models/_restaurant_models/_restaurant_legal_model').restaurantLegalModel
const fs = require('fs')

//Add Restaurant Legal Data
const addRestaurantLegal = (req, res) => {
    var restaurantLegal = new restaurantLegalModel()
    restaurantLegal.accountNo = req.body.accountNo
    restaurantLegal.accountType = req.body.accountType
    restaurantLegal.bankName = req.body.bankName
    restaurantLegal.branchName = req.body.branchName
    restaurantLegal.city = req.body.city
    restaurantLegal.address = req.body.address
    restaurantLegal.IFSCCode = req.body.IFSCCode
    restaurantLegal.documents = req.body.documents
    restaurantLegal.otherDocuments = req.body.otherDocuments
    restaurantLegal.addedBy = req.body.userid
    restaurantLegal.updatedBy = req.body.userid
    restaurantLegal.save((err) => {
        if (err) {
            throw err
        } else {
            res.json({ sucess: true, msg: 'RestaurantLegal added' })
        }
    })
}

//Get Restaurant Legal Data By restaurantLegalid
const getRestaurantLegal = (req, res) => {
    restaurantLegalModel.findById(req.params.restaurantLegalid).
        populate('addedBy', 'userName').
        populate('updatedBy', 'userName').
        exec((err, restaurantLegal) => {
            if (err) {
                throw err
            } else if (restaurantLegal == null) {
                res.json({ sucess: false, msg: 'RestaurantLegal not found' })
            } else {
                res.json({ sucess: true, msg: 'RestaurantLegal found', data: restaurantLegal })
            }
        })
}

//Get ALL Restaurants Legal Data
const getAllRestaurantLegal = (req, res) => {
    restaurantLegalModel.find().
        populate('addedBy', 'userName').
        populate('updatedBy', 'userName').
        exec((err, data) => {
            if (err) {
                throw err
            } else if (data == null || data.length == 0) {
                res.json({ sucess: false, msg: "RestaurantLegal Not Found" })
            } else {
                res.json({ sucess: true, msg: "RestaurantLeaal Found", data: data })
            }
        })
}

//update Restaurant Legal Data
const updateRestaurantLegal = (req, res) => {
    restaurantLegalModel.findByIdAndUpdate(req.params.restaurantLegaleid, req.body, (err, uRestaurantLegal) => {
        if (err) {
            throw err
        } else if (uRestaurantLegal == null) {
            res.json({ sucess: false, msg: "RestaurantLegal not found" })
        } else {
            res.json({ sucess: true, msg: "RestaurantLegal updated", data: uRestaurantLegal })
        }
    })
}

//Delete Restaurant Legal Data
const deleteRestaurantLegal = (req, res) => {
    restaurantLegalModel.findByIdAndDelete(req.params.restaurantLegaleid, (err, dRestaurantLegal) => {
        if (err) {
            throw err
        } else if (dRestaurantLegal == null) {
            res.json({ sucess: false, msg: "RestaurantLegal not found" })
        } else {
            res.json({ sucess: true, msg: "RestaurantLegal Deleted", data: dRestaurantLegal })
        }
    })
}

//Delete Restaurant GST files 
const deleteRestaurantGSTfiles = (req, res) => {
    fs.unlink('/home/bizlypos/public_html/uploads/gstfiles' + '/' + req.body.file, (err) => {
        if (err) {
            res.json({ success: false, msg: err })
        } else {
            res.json({ success: true, msg: "Restaurant GST File Deleted" })
        }
    })
}

//Delete Restaurant PAN files 
const deleteRestaurantPANFiles = (req, res) => {
    fs.unlink('/home/bizlypos/public_html/uploads/panfiles' + '/' + req.body.file, (err) => {
        if (err) {
            res.json({ success: false, msg: err })
        } else {
            res.json({ success: true, msg: "Restaurant PAN File Deleted" })
        }
    })
}

//Delete Restaurant FSSAI files 
const deleteRestaurantFSSAIIFiles = (req, res) => {
    fs.unlink('/home/bizlypos/public_html/uploads/fssaifiles' + '/' + req.body.file, (err) => {
        if (err) {
            res.json({ success: false, msg: err })
        } else {
            res.json({ success: true, msg: "Restaurant FSSAI File Deleted" })
        }
    })
}

//Delete Restaurant Other Documents files 
const deleteRestaurantOtherDocument = (req, res) => {
    fs.unlink('/home/bizlypos/public_html/uploads/otherdocuments' + '/' + req.body.file, (err) => {
        if (err) {
            res.json({ success: false, msg: err })
        } else {
            res.json({ success: true, msg: "Restaurant Other Documents File Deleted" })
        }
    })
}

//Delete Restaurant NOC files 
const deleteRestaurantNOCFile = (req, res) => {
    fs.unlink('/home/bizlypos/public_html/uploads/noc' + '/' + req.body.file, (err) => {
        if (err) {
            res.json({ success: false, msg: err })
        } else {
            res.json({ success: true, msg: "Restaurant NOC File Deleted" })
        }
    })
}

//Delete Restaurant Liquor License files 
const deleteRestaurantLiquorLicenseFile = (req, res) => {
    fs.unlink('/home/bizlypos/public_html/uploads/liquorlicense' + '/' + req.body.file, (err) => {
        if (err) {
            res.json({ success: false, msg: err })
        } else {
            res.json({ success: true, msg: "Restaurant Liquor License File Deleted" })
        }
    })
}

//Delete Restaurant Eating House License files 
const deleteRestaurantEatingHouseLicenseFile = (req, res) => {
    fs.unlink('/home/bizlypos/public_html/uploads/eatinghouselicense' + '/' + req.body.file, (err) => {
        if (err) {
            res.json({ success: false, msg: err })
        } else {
            res.json({ success: true, msg: "Restaurant Eating House License File Deleted" })
        }
    })
}

//Delete Restaurant Music License files 
const deleteRestaurantMusicLicenseFile = (req, res) => {
    fs.unlink('/home/bizlypos/public_html/uploads/Musiclicense' + '/' + req.body.file, (err) => {
        if (err) {
            res.json({ success: false, msg: err })
        } else {
            res.json({ success: true, msg: "Restaurant Music License File Deleted" })
        }
    })
}

//Delete Restaurant Trade Mark For Cafe License files 
const deleteRestaurantTrademarkForCafeLicenseFile = (req, res) => {
    fs.unlink('/home/bizlypos/public_html/uploads/TrademarkforCafeLicense' + '/' + req.body.file, (err) => {
        if (err) {
            res.json({ success: false, msg: err })
        } else {
            res.json({ success: true, msg: "Restaurant Trade Mark For Cafe License File Deleted" })
        }
    })
}

//Delete Restaurant CEC License files 
const deleteRestaurantCECLicenseFile = (req, res) => {
    fs.unlink('/home/bizlypos/public_html/uploads/CEC' + '/' + req.body.file, (err) => {
        if (err) {
            res.json({ success: false, msg: err })
        } else {
            res.json({ success: true, msg: "Restaurant CEC License File Deleted" })
        }
    })
}

//Delete Restaurant Shop and Establishment Act files 
const deleteRestaurantShopandEstablishmentActFile = (req, res) => {
    fs.unlink('/home/bizlypos/public_html/uploads/ShopsandEstablishmentAct' + '/' + req.body.file, (err) => {
        if (err) {
            res.json({ success: false, msg: err })
        } else {
            res.json({ success: true, msg: "Restaurant Shop And Establishment Act File Deleted" })
        }
    })
}

//Delete Restaurant Signage files 
const deleteRestaurantSignageFile = (req, res) => {
    fs.unlink('/home/bizlypos/public_html/uploads/Signagelicense' + '/' + req.body.file, (err) => {
        if (err) {
            res.json({ success: false, msg: err })
        } else {
            res.json({ success: true, msg: "Restaurant Signage File Deleted" })
        }
    })
}

//Delete Restaurant LiftClearance files 
const deleteRestaurantLiftClearanceFile = (req, res) => {
    fs.unlink('/home/bizlypos/public_html/uploads/LiftClearance' + '/' + req.body.file, (err) => {
        if (err) {
            res.json({ success: false, msg: err })
        } else {
            res.json({ success: true, msg: "Restaurant LiftClearance File Deleted" })
        }
    })
}

//Delete Restaurant Health Trade License files 
const deleteRestaurantHealthTradeLicenseFile = (req, res) => {
    fs.unlink('/home/bizlypos/public_html/uploads/HealthTradeLicense' + '/' + req.body.file, (err) => {
        if (err) {
            res.json({ success: false, msg: err })
        } else {
            res.json({ success: true, msg: "Restaurant Health Trade License File Deleted" })
        }
    })
}
module.exports = {
    addRestaurantLegal,
    getRestaurantLegal,
    getAllRestaurantLegal,
    updateRestaurantLegal,
    deleteRestaurantLegal,
    deleteRestaurantGSTfiles,
    deleteRestaurantPANFiles,
    deleteRestaurantFSSAIIFiles,
    deleteRestaurantOtherDocument,
    deleteRestaurantNOCFile,
    deleteRestaurantLiquorLicenseFile,
    deleteRestaurantEatingHouseLicenseFile,
    deleteRestaurantMusicLicenseFile,
    deleteRestaurantTrademarkForCafeLicenseFile,
    deleteRestaurantCECLicenseFile,
    deleteRestaurantShopandEstablishmentActFile,
    deleteRestaurantSignageFile,
    deleteRestaurantLiftClearanceFile,
    deleteRestaurantHealthTradeLicenseFile
}
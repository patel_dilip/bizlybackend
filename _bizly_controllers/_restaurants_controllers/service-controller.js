const serviceModel = require('../../_bizly_models/_restaurant_models/_services-model').servicemodel

//Add Service Data
const addService = (req, res) => {
    var service = new serviceModel()
    service.title = req.body.title
    service.services = req.body.services
    service.addedBy = req.body.userid
    service.updatedBy = req.body.userid
    service.save((err, result) => {
        if (err) {
            throw err
        } else {
            res.json({ sucess: true, msg: 'service added', id: result._id })
        }
    })
}

//Get Service Data By serviceid
const getService = (req, res) => {
    serviceModel.findById(req.params.serviceId).
        populate('addedBy', 'userName').
        populate('updatedBy', 'userName').
        exec((err, service) => {
            if (err) {
                throw err
            } else if (service == null) {
                res.json({ sucess: false, msg: 'Service not found' })
            } else {
                res.json({ sucess: true, msg: 'Service found', data: service })
            }
        })
}

//Get All Services Data 
const getAllServices = (req, res) => {
    serviceModel.find({}).
        populate('addedBy', 'userName').
        populate('updatedBy', 'userName').
        exec((err, data) => {
            if (err) {
                throw err
            } else if (data == null || data.length == 0) {
                res.json({ sucess: false, msg: "Services Not Found" })
            } else {
                res.json({ sucess: true, msg: "Services Found", data: data })
            }
        })
}

//Update Service Data
const updateService = (req, res) => {
    serviceModel.findByIdAndUpdate(req.params.serviceId, req.body, (err, uservice) => {
        if (err) {
            throw err
        } else if (uservice == null) {
            res.json({ sucess: false, msg: "service not found" })
        } else {
            res.json({ sucess: true, msg: "service updated" })
        }
    })
}

//Delete Service Data By Id
const deleteServiceById = (req, res) => {
    serviceModel.findByIdAndDelete(req.params.serviceId, (err, dservice) => {
        if (err) {
            throw err
        } else if (dservice == null) {
            res.json({ sucess: false, msg: "service not found" })
        } else {
            res.json({ sucess: true, msg: "service Deleted" })
        }
    })
}

//Delete All Service Data 
const deleteALLService = (req, res) => {
    serviceModel.remove((err, dservice) => {
        if (err) {
            throw err
        } else if (dservice == null) {
            res.json({ sucess: false, msg: "service not found" })
        } else {
            res.json({ sucess: true, msg: "service Deleted" })
        }
    })
}

module.exports = {
    addService,
    getService,
    getAllServices,
    updateService,
    deleteServiceById,
    deleteALLService
}
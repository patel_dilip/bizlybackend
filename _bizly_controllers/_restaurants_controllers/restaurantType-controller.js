const restaurantTypeModel = require('../../_bizly_models/_restaurant_models/_restaurant_type_model').restaurantTypemodel

// Add Restaurant Type Data
const addRestaurantType = (req, res) => {
    var restaurantType = new restaurantTypeModel()
    restaurantType.restaurantTypeName = req.body.restaurantTypeName
    restaurantType.addedBy = req.body.userid
    restaurantType.updatedBy = req.body.userid
    restaurantType.save((err) => {
        if (err) {
            throw err
        } else {
            res.json({ sucess: true, msg: 'RestaurantType added' })
        }
    })
}

//Get Restaurant Type Data By restaurantTypeid
const getRestaurantType = (req, res) => {
    restaurantTypeModel.findById(req.params.restaurantTypeid).
        populate('addedBy', 'userName').
        populate('updatedBy', 'userName').
        exec((err, restaurantType) => {
            if (err) {
                throw err
            } else if (restaurantType == null) {
                res.json({ sucess: false, msg: 'RestaurantType not found' })
            } else {
                res.json({ sucess: true, msg: 'Restaurant found', data: restaurantType })
            }
        })
}

//Get All Restaurnat Types Data
const getAllrestaurantType = (req, res) => {
    restaurantTypeModel.find().
        populate('addedBy', 'userName').
        populate('updatedBy', 'userName').
        exec((err, data) => {
            if (err) {
                throw err
            } else if (data == null || data.length == 0) {
                res.json({ sucess: false, msg: "RestaurantType Not Found" })
            } else {
                res.json({ sucess: true, msg: "RestaurantType Found", data: data })
            }
        })   
}

//Update RestaurantType Data
const updateRestaurantType = (req, res) => {
    restaurantTypeModel.findByIdAndUpdate(req.params.restaurantTypeid, req.body, (err, uRestaurantType) => {
        if (err) {
            throw err
        } else if (uRestaurantType == null) {
            res.json({ sucess: false, msg: "RestaurantType not found" })
        } else {
            res.json({ sucess: true, msg: "RestaurantType updated", data: uRestaurantType })
        }
    })
}

//Delete RestaurnatType Data
const deleteRestaurantType = (req, res) => {
    restaurantTypeModel.findByIdAndDelete(req.params.restaurantTypeid, (err, dRestaurantType) => {
        if (err) {
            throw err
        } else if (dRestaurantType == null) {
            res.json({ sucess: false, msg: "RestaurantType not found" })
        } else {
            res.json({ sucess: true, msg: "RestaurantType Deleted", data: dRestaurantType })
        }
    })
}

module.exports = {
    addRestaurantType,
    getRestaurantType,
    getAllrestaurantType,
    updateRestaurantType,
    deleteRestaurantType
}
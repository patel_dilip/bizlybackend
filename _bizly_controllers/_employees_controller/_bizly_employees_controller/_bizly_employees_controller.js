const bizlyEmpRole = require(".././../../_bizly_models/_employees_model/_employees_roles_model")
  .bizlyEmpRole;
const roleTree = require(".././../../_bizly_models/_employees_model/_employees_roles_model")
  .roleTree;

//update tree
const addTreeData = (req, res, next) => {
  roleTree.findOneAndUpdate(
    {},
    { $push: { roleTree: req.body.roleTree } },
    { upsert: true, new: true },
    (err, data) => {
      if (err) {
        res.json({ success: false, msg: err });
      } else if (data == null) {
        res.json({ success: false, msg: "Data Not Found" });
      } else {
        res.json({ success: true, msg: "Data Found", data: data });
      }
    }
  );
};

const getRoleTree = (req, res) => {
  roleTree.find().exec((err, data) => {
    if (err) {
      res.json({ success: false, msg: err });
    } else if (data == null || data.length == 0) {
      res.json({ success: false, msg: "Data Not Found" });
    } else {
      res.json({ success: true, msg: "Data Found", data: data });
    }
  });
}
//Add Role data
const addBizlyRole = (req, res) => {
  bizlyEmpRole.findOne({ roleName: req.body.roleName }, (err, data) => {
    if (err) {
      res.json({ success: false, msg: err });
    } else if (data) {
      res.json({ success: false, msg: "Role Name Already Exists" });
    } else {
      var role = new bizlyEmpRole();
      role.roleid = req.body.roleid;
      role.roleName = req.body.roleName;
      role.role_desc = req.body.role_desc;
      role.parent_key = req.body.parent_key;
      role.assigned_users = req.body.assigned_users;
      role.access_points = req.body.access_points;
      role.created_by = req.body.created_by;
      role.version_history = req.body.version_history;
      role.updated_by = req.body.updated_by;
      role.save((err) => {
        if (err) {
          res.json({ success: false, msg: err });
        } else {
          res.json({ success: true, msg: "Role added" });
        }
      });
    }
  });
};

const getAllBizlyRoles = (req, res) => {
  bizlyEmpRole.find().exec((err, data) => {
    if (err) {
      res.json({ success: false, msg: err });
    } else if (data == null || data.length == 0) {
      res.json({ success: false, msg: "Data Not Found" });
    } else {
      res.json({ success: true, msg: "Data Found", data: data });
    }
  });
};

const getBizlyRoleByid = (req, res) => {
  bizlyEmpRole.findOne({ roleid: req.params.bizlyroleid }).exec((err, data) => {
    if (err) {
      res.json({ success: false, msg: err });
    } else if (data == null) {
      res.json({ success: false, msg: "Data Not Found" });
    } else {
      res.json({ success: true, msg: "Data Found", data: data });
    }
  });
};

const updatebizlyrole = (req, res) => {
  bizlyEmpRole.updateOne(
    { roleid: req.params.bizlyroleid },
    req.body,
    (err, data) => {
      if (err) {
        res.json({ success: false, msg: err });
      } else if (data == null) {
        res.json({ success: false, msg: "Data Not Found" });
      } else {
        res.json({ success: true, msg: "Data Found", data: data });
      }
    }
  );
};

const rolesoftdelete = (req, res) => {
  bizlyEmpRole.updateOne(
    { roleid: req.params.bizlyroleid },
    { status: false },
    (err, data) => {
      if (err) {
        res.json({ success: false, msg: err });
      } else if (data == null || data.nModified == 0) {
        res.json({ success: false, msg: "Data Not Found" });
      } else {
        res.json({ success: true, msg: "Data Found", data: data });
      }
    }
  );
};

const deleteRole = (req, res) => {
  bizlyEmpRole.deleteOne({ roleid: req.params.bizlyroleid }, (err, data) => {
    if (err) {
      res.json({ success: false, msg: err });
    } else if (data == null) {
      res.json({ success: false, msg: "Data Not Found" });
    } else {
      res.json({ success: true, msg: "Data Found", data: data });
    }
  });
};

const deleteAllRoles = (req, res) => {
  bizlyEmpRole.deleteMany((err, data) => {
    if (err) {
      res.json({ success: false, msg: err });
    } else if (data == null) {
      res.json({ success: false, msg: "Data Not Found" });
    } else {
      res.json({ success: true, msg: "Data Found", data: data });
    }
  });
};

module.exports = {
  addBizlyRole,
  getAllBizlyRoles,
  getBizlyRoleByid,
  updatebizlyrole,
  rolesoftdelete,
  deleteRole,
  deleteAllRoles,
  addTreeData,
  getRoleTree
};

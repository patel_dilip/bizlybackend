const adminAuthModel = require('../../_bizly_models/_auth_models/_admin_auth_model').adminauthmodel
const userModel = require('../../_bizly_models/_auth_models/_user_auth_model').userModel
const jwt = require('jsonwebtoken')
const config = require('../../middleware/config')

const adminSignup = (req, res) => {
    var admin = new adminAuthModel()
    admin.userName = req.body.userName
    admin.password = req.body.password

    admin.save((err) => {
        if (err) {
            throw err
        } else {
            res.json({ success: true, msg: 'username' })
        }
    })
}

const Signin = (req, res) => {
    adminAuthModel.findOne({ "userName": req.body.userName }, (err, adminData) => {
        if (err) {
            res.json({ success: false, msg: err })
        } else if (adminData != null) {
            if (adminData.password == req.body.password) {
                adminData.password = '';
                jwt.sign({ username: req.body.username, id: adminData._id }, config.secret, { expiresIn: '12h' }, (err, token) => {
                    res.json({ success: true, data: adminData, message: 'Login Successfull', token: token })
                })
            } else {
                res.json({ success: false, msg: 'incorrect password' })
            }
        } else {
            userModel.findOne({ userName: req.body.userName }, (err, user) => {
                if (err) {
                    res.json({ success: false, message: err })
                } else if (user != null) {
                    if (user.password == req.body.password) {
                        user.password = '';
                        jwt.sign({ userName: req.body.userName, id: user._id }, config.secret, { expiresIn: '12h' }, (err, token) => {
                            res.json({ success: true, data: user, message: 'Login Successfull', token: token })
                        })
                    } else {
                        res.json({ success: false, message: 'incorrect password' })
                    }
                } else {
                    res.json({ success: false, message: 'user not found' })
                }

            })
        }
    })
}

const getAllAdmins = (req, res) => {
    adminAuthModel.find({ role: "admin" }, (err, data) => {
        if (err) {
            res.json({ success: false, msg: err })
        } else if (data == null, data.length == 0) {
            res.json({ success: false, msg: "Data not found" })
        } else {
            res.json({ success: true, msg: "Admin Data Found", data: data })
        }
    })
}

module.exports = {
    adminSignup,
    Signin,
    getAllAdmins
}
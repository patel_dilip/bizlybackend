const userModel = require('../../_bizly_models/_auth_models/_user_auth_model').userModel
const jwt = require('jsonwebtoken')
const config = require('../../middleware/config')

const addUser = (req, res) => {
    var user = new userModel()
    user.role = req.body.role
    user.userName = req.body.userName
    user.firstName = req.body.firstName
    user.lastName = req.body.lastName
    user.email = req.body.email
    user.mobileno = req.body.mobileno
    user.password = req.body.password
    user.save((err, user) => {
        if (err) {
            res.json({ message: err, success: false })
        } else {
            res.json({ message: 'User  added', success: true, data: user.id })
        }
    })

}

const getSingleuser = (req, res) => {
    userModel.findOne({ _id: req.params.userid }, (err, user) => {
        if (err) {
            res.json({ message: 'User Not Found', success: false })
        } else if (user == null) {
            res.json({ message: 'User Not Found', success: false })
        } else {
            res.json({ message: 'User found', success: true, data: user })
        }
    })
}

const getAlluser = (req, res) => {
    userModel.find((err, user) => {
        if (err) {
            res.json({ message: 'User Not Found', success: false })
        } else if (user == null || user.length == 0) {
            res.json({ message: 'user Not Found', success: false })
        } else {
            res.json({ message: 'user found', success: true, data: user })
        }
    })
}

const updateUser = (req, res) => {
    userModel.findByIdAndUpdate(req.params.userid, req.body, (err, updatedata) => {
        if (err) {
            res.json({ message: 'User Not Found', success: false })
        } else {
            res.json({ message: 'User Sccuessfully updated', success: true, data: updatedata })
        }
    })
}

const deleteUser = (req, res) => {
    userModel.findByIdAndDelete(req.params.userid, (err) => {
        if (err) {
            res.json({ message: 'User Not Found', success: false })
        } else {
            res.json({ message: 'User deleted', success: true })
        }
    })
}

const changePassword = (req, res) => {
    userModel.findByIdAndUpdate(req.params.userid, { 'password': req.body.password }, (err, udata) => {
        if (err) {
            res.json({ message: 'User Not Found', success: false })
        } else if (udata == null) {
            res.json({ message: 'User Not Found', success: false })
        } else {
            res.json({ message: 'Password Changed', success: true })
        }
    })
}

// const usersignin = (req, res) => {
//     userModel.findOne({ userName: req.body.userName }, (err, user) => {
//         if (err) {
//             res.json({ success: false, message: err })
//         } else if (user != null) {
//             if (user.password == req.body.password) {
//                 user.password = '';
//                 jwt.sign({ userName: req.body.userName, id: user._id }, config.secret, { expiresIn: '12h' }, (err, token) => {
//                     res.json({ success: true, data: user, message: 'Login Successfull', token: token })
//                 })
//             } else {
//                 res.json({ success: false, message: 'incorrect password' })
//             }
//         } else {
//             res.json({ success: false, message: 'user not found' })
//         }

//     })
// }

module.exports = {
    addUser,
    getSingleuser,
    getAlluser,
    updateUser,
    deleteUser,
    changePassword,
    // usersignin

}
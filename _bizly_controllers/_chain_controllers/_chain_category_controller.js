const chainCategoryModel = require('../../_bizly_models/_chain_models/_chain_category_model').category

const addRootCategory = (req, res) => {
    var category = new chainCategoryModel()
    category.rootCategoryName = req.body.rootCategoryName
    category.addedBy = req.body.addedBy
    category.updatedBy = req.body.updatedBy
    category.save((err, result) => {
        if (err) {
            res.json({ success: false, msg: err })
        } else {
            res.json({ success: true, msg: "Root Category Added", id: result._id })
        }
    })
}

const addCategory = (req, res) => {
    chainCategoryModel.findOneAndUpdate({ _id: req.params.rootcategoryid },
        {
            $push: {
                categories: req.body
            }
        }, (err, data) => {
            if (err) {
                res.json({ success: false, msg: err })
            } else if (data == null || data.length == 0) {
                res.json({ success: false, msg: "Root Category Not Found" })
            } else {
                res.json({ success: true, msg: "Category Added" })
            }
        })
}

const getAllRootCategory = (req, res) => {
    chainCategoryModel.find().
        populate('addedBy', 'userName').
        populate('updatedBy', 'userName').
        exec((err, data) => {
            if (err) {
                res.json({ success: false, msg: err })
            } else if (data == null || data.length == 0) {
                res.json({ success: false, msg: "Data Not Found" })
            } else {
                res.json({ success: true, msg: "Data Found", data: data })
            }
        })
}

const updateRootCategoryName = (req, res) => {
    chainCategoryModel.findOneAndUpdate({ _id: req.params.rootcategoryid },
        {
            $set: {
                rootCategoryName: req.body.rootCategoryName
            }
        }, (err, udata) => {
            if (err) {
                res.json({ success: false, msg: err })
            } else if (udata == null) {
                res.json({ success: false, msg: "Data not found" })
            } else {
                res.json({ success: true, msg: "Root name Updated" })
            }
        })
}

const updateCategoryName = (req, res) => {
    chainCategoryModel.findOne({ _id: req.params.rootcategoryid, })
        .then((data) => {
            promiseArr = []
            for (let i = 0; i < data.categories.length; i++) {
                promiseArr.push(new Promise((resolve, reject) => {
                    chainCategoryModel.findOneAndUpdate({
                        _id: req.params.rootcategoryid,
                        "categories._id": req.params.categoresId
                    }, {
                            $set: {
                                "categories.$.categoryName": req.body.categoryName
                            }
                        }).then((udata) => {
                            resolve(udata)
                        }).catch((err) => {
                            reject(err)
                        })
                }))
            }
            Promise.all(promiseArr).then((promiseArr) => {
                res.json({ success: true, msg: "Category name Updated" })
            }).catch((err) => {
                res.json({ success: false, msg: "error in Promise arr", error: err })
            })
        }).catch((err) => {
            res.json({ success: false, msg: "error in data", error: err })
        })
}

module.exports = {
    addRootCategory,
    addCategory,
    getAllRootCategory,
    updateRootCategoryName,
    updateCategoryName
}
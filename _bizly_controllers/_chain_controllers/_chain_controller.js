const chainModel = require('../../_bizly_models/_chain_models/_chain_model').chainModel

//add chain info 
const addChainInfo = (req, res) => {
    var chain = new chainModel()
    chain.chain_info = req.body.chain_info
    chain.media_info = req.body.media_info
    chain.outlets = req.body.outlets
    chain.establishment_info = req.body.establishment_info
    chain.addedBy = req.body.userid
    chain.updatedBy = req.body.userid
    chain.save((err, result) => {
        if (err) {
            res.json({ success: false, msg: err })
        } else {
            res.json({ success: true, msg: "Chain Info added", id: result._id })
        }
    })
}

//get all Chain Data
const getAllChains = (req, res) => {
    chainModel.find()
        .populate('addedBy', 'userName')
        .populate('updatedBy', 'userName')
        .exec((err, data) => {
            if (err) {
                res.json({ success: false, msg: err })
            } else if (data == null || data.length == 0) {
                res.json({ success: false, msg: "Chain data not found", data: [] })
            } else {
                res.json({ success: true, msg: "Chain data found", data: data })
            }
        })
}

//get chain data by chainid
const getChainById = (req, res) => {
    chainModel.find(req.params.chainid)
        .populate('addedBy', 'userName')
        .populate('updatedBy', 'userName')
        .exec((err, data) => {
            if (err) {
                res.json({ success: false, msg: err })
            } else if (data == null || data.length == 0) {
                res.json({ success: false, msg: "Chain data not found" })
            } else {
                res.json({ success: true, msg: "Chain data found", data: data })
            }
        })
}

//Update chain media info
const updateMediaInfo = (req, res) => {
    chainModel.findOneAndUpdate({ _id: req.params.chainid },
        {
            media_info: req.body.media_info,
            updatedBy: req.body.userid,
            updatedAt: Date.now()
        }, (err, udata) => {
            if (err) {
                res.json({ success: false, msg: err })
            } else {
                res.json({ success: true, msg: "Media info updated" })
            }
        })
}


//Update chains outlets
const updateOutlets = (req, res) => {
    chainModel.findOneAndUpdate({ _id: req.params.chainid },
        {
            outlets: req.body.outlets,
            updatedBy: req.body.userid,
            updatedAt: Date.now()
        }, (err, udata) => {
            if (err) {
                res.json({ success: false, msg: err })
            } else {
                res.json({ success: true, msg: "Outlets updated" })
            }
        })
}

//Update chain Establishment more info
const updateestablishmentMoreInfo = (req, res) => {
    chainModel.findOneAndUpdate({ _id: req.params.chainid },
        {
            "establishment_info.establish_moreinfo": req.body.establish_moreinfo,
            updatedBy: req.body.userid,
            updatedAt: Date.now()
        }, (err, udata) => {
            if (err) {
                res.json({ success: false, msg: err })
            } else {
                res.json({ success: true, msg: "More Info updated" })
            }
        })
}

//Update chain Establishment Services
const updateestablishmentServices = (req, res) => {
    chainModel.findOneAndUpdate({ _id: req.params.chainid },
        {
            "establishment_info.establish_services": req.body.establish_services,
            updatedBy: req.body.userid,
            updatedAt: Date.now()
        }, (err, udata) => {
            if (err) {
                res.json({ success: false, msg: err })
            } else {
                res.json({ success: true, msg: "Services Updated" })
            }
        })
}

//Update chain Establishment Rules
const updateestablishmentRules = (req, res) => {
    chainModel.findOneAndUpdate({ _id: req.params.chainid },
        {
            "establishment_info.establish_rules": req.body.establish_rules,
            updatedBy: req.body.userid,
            updatedAt: Date.now()
        }, (err, udata) => {
            if (err) {
                res.json({ success: false, msg: err })
            } else {
                res.json({ success: true, msg: "Rules Updated" })
            }
        })
}

//Update chain Establishment Ambience
const updateestablishmentAmbience = (req, res) => {
    chainModel.findOneAndUpdate({ _id: req.params.chainid },
        {
            "establishment_info.establish_ambience": req.body.establish_ambience,
            updatedBy: req.body.userid,
            updatedAt: Date.now()
        }, (err, udata) => {
            if (err) {
                res.json({ success: false, msg: err })
            } else {
                res.json({ success: true, msg: "Ambience Updated" })
            }
        })
}

//Update chain Establishment Cuisines
const updateestablishmentCuisines = (req, res) => {
    chainModel.findOneAndUpdate({ _id: req.params.chainid },
        {
            "establishment_info.establish_cuisines": req.body.establish_cuisines,
            updatedBy: req.body.userid,
            updatedAt: Date.now()
        }, (err, udata) => {
            if (err) {
                res.json({ success: false, msg: err })
            } else {
                res.json({ success: true, msg: "Cuisines Updated" })
            }
        })
}

//update chain info
const updateChainInfo = (req, res) => {
    chainModel.findOneAndUpdate({ _id: req.params.chainid },
        {
            chain_info: req.body.chain_info,
            updatedBy: req.body.userid,
            updatedAt: Date.now()
        }, (err, udata) => {
            if (err) {
                res.json({ success: false, msg: err })
            } else if (udata == null || udata.length == 0) {
                res.json({ success: false, msg: "Chain data not found" })
            } else {
                res.json({ success: true, msg: "Chain info updated" })
            }
        })
}

const deleteChain = (req, res) => {
    chainModel.findOneAndDelete({ _id: req.params.chainid }, (err, data) => {
        if (err) {
            res.json({ success: false, msg: err })
        } else {
            res.json({ success: true, msg: "chain deleted" })
        }
    })
}

const deleteAllChain = (req, res) => {
    chainModel.remove((err, data) => {
        if (err) {
            res.json({ success: false, msg: err })
        } else {
            res.json({ success: true, msg: "chain deleted" })
        }
    })
}
module.exports = {
    addChainInfo,
    getAllChains,
    getChainById,
    updateMediaInfo,
    updateOutlets,
    updateestablishmentMoreInfo,
    updateestablishmentServices,
    updateestablishmentRules,
    updateestablishmentAmbience,
    updateestablishmentCuisines,
    updateChainInfo,
    deleteChain,
    deleteAllChain
}
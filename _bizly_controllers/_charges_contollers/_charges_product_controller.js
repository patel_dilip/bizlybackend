const charges_product_model = require("../../_bizly_models/_charges_models/_charges_product_model")
  .charges_product_model;
const charges_integration_model = require("../../_bizly_models/_charges_models/_charges_integration_model")
  .charges_integration_model;
const charges_addons_model = require("../../_bizly_models/_charges_models/_charges_addon_model")
  .charges_addon_model;

// Add Charges Product Data
const addChargesProduct = async (req, res) => {
  try {
    const chargesProduct = new charges_product_model();
    chargesProduct.productName = req.body.productName;
    chargesProduct.description = req.body.description;
    chargesProduct.images = req.body.images;
    chargesProduct.videos = req.body.videos;
    chargesProduct.completeData = req.body.completeData;
    chargesProduct.addedBy = req.body.userid;
    chargesProduct.updatedBy = req.body.userid;

    chargesProduct.save((err, result) => {
      let { completeData } = result;
      let country = completeData.map((ele) => {
        return ele.country.item_text;
      });

      let promiseArr1 = [];
      let promiseArr2 = [];
      for (let i = 0; i < result.completeData.length; i++) {
        if (
          result.completeData[i].available_addons.length != 0 ||
          result.completeData[i].available_Integration != 0
        ) {
          // if loop for addons
          if (result.completeData[i].available_addons.length != 0) {
            charges_addons_model
              .find({}, { AddonsName: 1, completeData: 1, _id: 1 })
              .then((addonData) => {
                for (let j = 0; j < addonData.length; j++) {
                  if (
                    result.completeData[i].available_addons.includes(
                      addonData[j].AddonsName
                    )
                  ) {
                    for (let k = 0; k < addonData[j].completeData.length; k++) {
                      if (
                        country.includes(
                          addonData[j].completeData[k].country.item_text
                        )
                      ) {
                        promiseArr1.push(
                          new Promise((resolve, reject) => {
                            charges_addons_model
                              .updateOne(
                                { _id: addonData[j]._id },
                                {
                                  $set: {
                                    productAssociated: req.body.productName,
                                  },
                                }
                              )
                              .then((udata) => {
                                resolve(udata);
                              })
                              .catch((err) => {
                                reject(err);
                              });
                          })
                        );
                      } else {
                        // console.log("addon country not match")
                        continue;
                      }
                    }
                  } else {
                    // console.log("addon name not match")
                    continue;
                  }
                }
              })
              .catch((err) => {
                return res.json({
                  success: false,
                  msg: "error in addon Data",
                  error: err,
                });
              });
          }

          // if loop for Integration
          if (result.completeData[i].available_Integration != 0) {
            charges_integration_model
              .find({}, { integrationName: 1, completeData: 1, _id: 1 })
              .then((integrationData) => {
                for (let l = 0; l < integrationData.length; l++) {
                  if (
                    result.completeData[i].available_Integration.includes(
                      integrationData[l].integrationName
                    )
                  ) {
                    for (
                      let m = 0;
                      m < integrationData[l].completeData.length;
                      m++
                    ) {
                      if (
                        country.includes(
                          integrationData[l].completeData[m].country.item_text
                        )
                      ) {
                        promiseArr2.push(
                          new Promise((resolve, reject) => {
                            charges_integration_model
                              .updateOne(
                                { _id: integrationData[l]._id },
                                {
                                  $set: {
                                    productAssociated: req.body.productName,
                                  },
                                }
                              )
                              .then((updated) => {
                                resolve(updated);
                              })
                              .catch((err) => {
                                reject(err);
                              });
                          })
                        );
                      } else {
                        // console.log("integration country not match")
                        continue;
                      }
                    }
                  } else {
                    // console.log('integration name not match')
                    continue;
                  }
                }
              })
              .catch((err) => {
                return res.json({
                  success: false,
                  msg: "error in Integration Data",
                  error: err,
                });
              });
          }
        }
      }
      Promise.all([promiseArr1, promiseArr2])
        .then((finalPrmoise) => {
          res.json({ success: true, msg: "Product added" });
        })
        .catch((err) => {
          return res.json({
            success: false,
            msg: "error in final promise",
            error: err,
          });
        });
    });
  } catch (ex) {
    return res.json({ success: false, error: ex.message });
  }
};

//Get All Charges Products Data
const getAllChargesProducts = (req, res) => {
  charges_product_model
    .find({})
    .populate("addedBy", "userName")
    .populate("updatedBy", "userName")
    .sort("-updatedAt")
    .exec((err, data) => {
      if (err) {
        res.json({ success: false, msg: err });
      } else if (data == null || data.length == 0) {
        res.json({ success: false, msg: "Charges product not found" });
      } else {
        res.json({ success: true, msg: "Charges product Found", data: data });
      }
    });
};

//Get Charges Product Data By productid
const getChargesProduct = (req, res) => {
  charges_product_model
    .findById(req.params.productid)
    .populate("addedBy", "userName")
    .populate("updatedBy", "userName")
    .sort("-updatedAt")
    .exec((err, data) => {
      if (err) {
        res.json({ success: false, msg: err });
      } else if (data == null || data.length == 0) {
        res.json({ success: false, msg: "Charges product not found" });
      } else {
        res.json({ success: true, msg: "Charges product Found", data: data });
      }
    });
};

// get Complete Data by product name
const getCompleteDataByName = (req, res) => {
  charges_product_model.find(
    { productName: req.params.productname },
    { completeData: 1, _id: 1 },
    (err, data) => {
      if (err) {
        res.json({ success: false, msg: err });
      } else if (data == null || data.length == 0) {
        res.json({ success: false, msg: "This Product Name not available" });
      } else {
        res.json({ success: true, msg: "Product Name Found", data: data });
      }
    }
  );
};

//Update Charges Product Data
const updateChargeProduct = async (req, res) => {
  try {
    charges_product_model.findByIdAndUpdate(
      req.params.productid,
      {
        $set: {
          productName: req.body.productName,
          description: req.body.description,
          images: req.body.images,
          videos: req.body.videos,
          completeData: req.body.completeData,
          updatedBy: req.body.userid,
          updatedAt: Date.now(),
        },
      },
      (err, udata) => {
        let promiseArr1 = [];
        let promiseArr2 = [];
        for (let i = 0; i < req.body.completeData.length; i++) {
          if (
            req.body.completeData[i].available_addons.length != 0 ||
            req.body.completeData[i].available_Integration != 0
          ) {
            // if loop for addon
            if (req.body.completeData[i].available_addons.length != 0) {
              charges_addons_model
                .find({}, { AddonsName: 1, completeData: 1, _id: 1 })
                .then((addonData) => {
                  for (let j = 0; j < addonData.length; j++) {
                    if (
                      req.body.completeData[i].available_addons.includes(
                        addonData[j].AddonsName
                      )
                    ) {
                      for (
                        let k = 0;
                        k < addonData[j].completeData.length;
                        k++
                      ) {
                        if (
                          addonData[j].completeData[k].country.item_text ==
                          req.body.completeData[i].country.item_text
                        ) {
                          promiseArr1.push(
                            new Promise((resolve, reject) => {
                              charges_addons_model
                                .updateOne(
                                  { _id: addonData[j]._id },
                                  {
                                    $set: {
                                      productAssociated: req.body.productName,
                                    },
                                  }
                                )
                                .then((updatedAddon) => {
                                  resolve(updatedAddon);
                                })
                                .catch((err) => {
                                  reject(err);
                                });
                            })
                          );
                        } else {
                          // console.log("country not match")
                          continue;
                        }
                      }
                    } else {
                      // console.log("addon name not match")
                      continue;
                    }
                  }
                })
                .catch((err) => {
                  return res.json({
                    success: false,
                    msg: "Error in addon Data",
                    error: err,
                  });
                });
            }
            // if loop for integration
            if (req.body.completeData[i].available_Integration != 0) {
              charges_integration_model
                .find({}, { integrationName: 1, completeData: 1, _id: 1 })
                .then((integrationData) => {
                  for (let l = 0; l < integrationData.length; l++) {
                    if (
                      req.body.completeData[i].available_Integration.includes(
                        integrationData[l].integrationName
                      )
                    ) {
                      for (
                        let m = 0;
                        m < integrationData[l].completeData.length;
                        m++
                      ) {
                        if (
                          integrationData[l].completeData[m].country
                            .item_text ==
                          req.body.completeData[i].country.item_text
                        ) {
                          promiseArr2.push(
                            new Promise((resolve, reject) => {
                              charges_integration_model
                                .updateOne(
                                  { _id: integrationData[l]._id },
                                  {
                                    $set: {
                                      productAssociated: req.body.productName,
                                    },
                                  }
                                )
                                .then((updatedintegration) => {
                                  resolve(updatedintegration);
                                })
                                .catch((err) => {
                                  reject(err);
                                });
                            })
                          );
                        } else {
                          // console.log("country not match")
                          continue;
                        }
                      }
                    } else {
                      // console.log("integration name not match")
                      continue;
                    }
                  }
                })
                .catch((err) => {
                  return res.json({
                    success: false,
                    msg: "Error in integration Data",
                    error: err,
                  });
                });
            }
          }
        }
        Promise.all([promiseArr1, promiseArr2])
          .then((finalPromise) => {
            res.json({ success: true, msg: "Product Updated" });
          })
          .catch((err) => {
            return res.json({
              success: false,
              msg: "Error in final promise",
              error: err,
            });
          });
      }
    );
  } catch (ex) {
    return res.json({ success: false, error: ex.message });
  }
};

//Delete Charges Product Data
const deleteChargesProduct = async (req, res) => {
  try {
    charges_product_model
      .findByIdAndUpdate(req.params.productid, {
        $set: { status: req.body.status },
      })
      .then((updateProductData) => {
        let productName = updateProductData.productName;
        charges_integration_model
          .find({ productAssociated: productName })
          .then((integrationData) => {
            let promiseArr1 = integrationData.map((element) => {
              return new Promise((resolve, reject) => {
                charges_integration_model
                  .updateOne(
                    { productAssociated: element.productAssociated },
                    { $set: { productAssociated: "" } }
                  )
                  .then((udata) => {
                    resolve(true);
                  })
                  .catch((err) => {
                    reject(false);
                  });
              });
            });
            Promise.all(promiseArr1)
              .then((promiseArr1) => {
                charges_addons_model
                  .find({ productAssociated: productName })
                  .then((addonData) => {
                    let promiseArr2 = addonData.map((element) => {
                      charges_addons_model
                        .updateOne(
                          { productAssociated: element.productAssociated },
                          { $set: { productAssociated: "" } }
                        )
                        .then((udata) => {
                          resolve(true);
                        })
                        .catch((err) => {
                          reject(false);
                        });
                    });
                    Promise.all(promiseArr2)
                      .then((promiseArr2) => {
                        res.json({ success: true, msg: "Product Deleted" });
                      })
                      .catch(() => {
                        return res.json({
                          success: false,
                          msg: "error in promise 2 all",
                          error: err,
                        });
                      });
                  })
                  .catch((err) => {
                    return res.json({
                      success: false,
                      msg: "error in addon Data",
                      error: err,
                    });
                  });
              })
              .catch((err) => {
                return res.json({
                  success: false,
                  msg: "error in promise 1 all",
                  error: err,
                });
              });
          })
          .catch((err) => {
            return res.json({
              success: false,
              msg: "error in integration data",
              error: err,
            });
          });
      })
      .catch((err) => {
        return res.json({
          success: false,
          msg: "error in updateproduct",
          error: err,
        });
      });
  } catch (ex) {
    res.json({ error: ex.message });
  }
};

// Import Charges Product List
const importChargesProduct = (req, res) => {
  charges_product_model.collection.insertMany(req.body, function (err, docs) {
    res.json({ sucess: true, msg: "List Imported   " });
  });
};

const updateProductStatus = (req, res) => {
  charges_product_model.findOneAndUpdate(
    {
      _id: req.params.id,
      "completeData.country.item_text": req.params.countryname,
    },
    // {$set: {"completeData.$[].country.status": !"completeData.$[].country.status"}},
    { $set: { "completeData.$.country.status": req.body.status } },
    (err, resp) => {
      if (err) {
        res.json({ success: false, msg: err });
      } else if (resp == null) {
        res.json({ success: false });
      } else {
        res.json({ success: true });
      }
    }
  );
  // charges_product_model.findOneAndUpdate({ _id: req.params.id, countryname: "completeData.country.item_text"})
};

module.exports = {
  addChargesProduct,
  getAllChargesProducts,
  getChargesProduct,
  getCompleteDataByName,
  updateChargeProduct,
  deleteChargesProduct,
  importChargesProduct,
  updateProductStatus,
};

const charges_addon_model = require("../../_bizly_models/_charges_models/_charges_addon_model")
  .charges_addon_model;
const charges_integration_model = require("../../_bizly_models/_charges_models/_charges_integration_model")
  .charges_integration_model;
const charges_product_model = require("../../_bizly_models/_charges_models/_charges_product_model")
  .charges_product_model;

// Add Charges Addon Data
const addChargesAddon = (req, res) => {
  const chargesAddon = new charges_addon_model();
  chargesAddon.AddonsName = req.body.AddonsName;
  chargesAddon.productAssociated = req.body.productAssociated;
  chargesAddon.description = req.body.description;
  chargesAddon.images = req.body.images;
  chargesAddon.videos = req.body.videos;
  chargesAddon.completeData = req.body.completeData;
  chargesAddon.addedBy = req.body.userid;
  chargesAddon.updatedBy = req.body.userid;

  chargesAddon.save((err, result) => {
    let { completeData } = result;
    let country = completeData.map((ele) => {
      return ele.country.item_text;
    });

    charges_product_model
      .find(
        { productName: result.productAssociated },
        { completeData: 1, _id: 1, productName: 1 }
      )
      .then((productNameData) => {
        let promiseArr = [];
        for (let i = 0; i < productNameData.length; i++) {
          for (let j = 0; j < productNameData[i].completeData.length; j++) {
            if (
              country.includes(
                productNameData[i].completeData[j].country.item_text
              )
            ) {
              console.log("enter in matching contry");
              promiseArr.push(
                new Promise((resolve, reject) => {
                  charges_product_model
                    .updateOne(
                      {
                        _id: productNameData[i]._id,
                        "completeData.country.item_text":
                          productNameData[i].completeData[j].country.item_text,
                      },
                      {
                        $push: {
                          "completeData.$.available_addons":
                            req.body.AddonsName,
                        },
                      }
                    )
                    .then((udata) => {
                      console.log("addon pushed to product");
                      resolve(udata);
                    })
                    .catch((err) => {
                      reject(err);
                    });
                })
              );
            } else {
              continue;
            }
          }
        }
        Promise.all(promiseArr)
          .then((promiseArr) => {
            res.json({ success: true, msg: "Charges Addon added" });
          })
          .catch((err) => {
            res.json({
              success: false,
              msg: "error in promise all ",
              error: err,
            });
          });
      })
      .catch((err) => {
        res.json({
          success: false,
          msg: "error in product name data",
          error: err,
        });
      });
  });
};

//Get All Charges Addons Data
const getAllChargesAddons = (req, res) => {
  charges_addon_model
    .find({})
    .populate("addedBy", "userName")
    .populate("updatedBy", "userName")
    .sort("-updatedAt")
    .exec((err, data) => {
      if (err) {
        res.json({ success: false, msg: err });
      } else if (data == null || data.length == 0) {
        res.json({ success: false, msg: "Charges addon not found" });
      } else {
        res.json({ success: true, msg: "Charges Addon Found", data: data });
      }
    });
};

//Get Charges Addon Data By addonid
const getChargesAddon = (req, res) => {
  charges_addon_model
    .findById(req.params.addonid)
    .populate("addedBy", "userName")
    .populate("updatedBy", "userName")
    .sort("-updatedAt")
    .exec((err, data) => {
      if (err) {
        res.json({ success: false, msg: err });
      } else if (data == null || data.length == 0) {
        res.json({ success: false, msg: "Charges addon not found" });
      } else {
        res.json({ success: true, msg: "Charges Addon Found", data: data });
      }
    });
};

// get Complete Data by addon name
const getCompleteDataByName = (req, res) => {
  charges_addon_model.find(
    { AddonsName: req.params.addonname },
    { completeData: 1, _id: 1 },
    (err, data) => {
      if (err) {
        res.json({ success: false, msg: err });
      } else if (data == null || data.length == 0) {
        res.json({ success: false, msg: "This Addon Name not available" });
      } else {
        res.json({ success: true, msg: "addon Name Found", data: data });
      }
    }
  );
};

//Update Charges Addon Data
const updateChargeAddon = async (req, res) => {
  try {
    charges_addon_model
      .findByIdAndUpdate(req.params.addonid, {
        $set: {
          AddonsName: req.body.AddonsName,
          productAssociated: req.body.productAssociated,
          description: req.body.description,
          images: req.body.images,
          videos: req.body.videos,
          completeData: req.body.completeData,
          updatedBy: req.body.userid,
          updatedAt: Date.now(),
        },
      })
      .then((udata) => {
        promiseArr = [];
        for (let i = 0; i < req.body.completeData.length; i++) {
          charges_product_model
            .find(
              { productName: req.body.productAssociated },
              { completeData: 1, _id: 1 }
            )
            .then((productData) => {
              for (let j = 0; j < productData.length; j++) {
                for (let k = 0; j < productData[j].completeData.length; j++) {
                  if (
                    req.body.completeData[i].country.item_text ==
                    productData[j].completeData[k].country.item_text
                  ) {
                    if (
                      productData[j].completeData[k].available_addons.includes(
                        udata.AddonsName
                      )
                    ) {
                      console.log("addon name match");
                      continue;
                    } else {
                      promiseArr.push(
                        new Promise((resolve, reject) => {
                          charges_product_model
                            .updateOne(
                              {
                                _id: productData[j]._id,
                                "completeData.country.item_text":
                                  productData[j].completeData[k].country
                                    .item_text,
                              },
                              {
                                $push: {
                                  "completeData.$.available_addons":
                                    req.body.AddonsName,
                                },
                              }
                            )
                            .then((pushed) => {
                              resolve(pushed);
                            })
                            .catch((err) => {
                              reject(err);
                            });
                        })
                      );
                    }
                  } else {
                    console.log("Country not match");
                  }
                }
              }
            })
            .catch((err) => {
              return res.json({
                success: false,
                msg: "Error in product Data",
                error: err,
              });
            });
        }
        Promise.all(promiseArr)
          .then((promiseArr) => {
            res.json({ success: true, msg: "Charges addon updated" });
          })
          .catch((err) => {
            return res.json({
              success: false,
              msg: "Error in promise arr",
              error: err,
            });
          });
      })
      .catch((err) => {
        return res.json({ success: false, msg: "Error in udata", error: err });
      });
  } catch (ex) {
    return res.json({ success: false, msg: ex.message });
  }
};

//Delete Charges Addon Data
const deleteChargesAddon = async (req, res) => {
  try {
    charges_addon_model
      .findByIdAndUpdate(req.params.addonid, {
        $set: { status: req.body.status },
      })
      .then((addondata) => {
        let addonName = addondata.AddonsName;
        // console.log(addonName)
        charges_integration_model
          .find({ addOnsAssociated: addondata.AddonsName })
          .then((integrationData) => {
            let promiseArr = integrationData.map((element) => {
              return new Promise((resolve, reject) => {
                charges_integration_model
                  .updateOne(
                    { addOnsAssociated: element.addOnsAssociated },
                    { $set: { addOnsAssociated: "" } }
                  )
                  .then((udata) => {
                    resolve(true);
                  })
                  .catch((err) => {
                    reject(false);
                    console.log("reject 1:", false, err);
                  });
              });
            });
            Promise.all(promiseArr)
              .then((promiseArr) => {
                charges_product_model
                  .find({}, { completeData: 1, _id: 1 })
                  .then((productData) => {
                    var promiseArr = [];
                    for (let i = 0; i < productData.length; i++) {
                      for (
                        let j = 0;
                        j < productData[i].completeData.length;
                        j++
                      ) {
                        if (
                          productData[i].completeData[j].available_addons
                            .length != 0
                        ) {
                          for (
                            let k = 0;
                            k <
                            productData[i].completeData[j].available_addons
                              .length;
                            k++
                          ) {
                            var adonIndex = productData[i].completeData[
                              j
                            ].available_addons.indexOf(addonName);
                            if (adonIndex != -1) {
                              promiseArr.push(
                                new Promise((resolve, reject) => {
                                  charges_product_model
                                    .updateOne(
                                      {
                                        _id: productData[i]._id,
                                        "completeData.available_addons": addonName,
                                      },
                                      {
                                        $pull: {
                                          "completeData.$.available_addons": addonName,
                                        },
                                      }
                                    )
                                    .then((udata) => {
                                      console.log("deleted");
                                      resolve(udata);
                                    })
                                    .catch((err) => {
                                      console.log(err);
                                      reject(err);
                                    });
                                })
                              );
                            } else {
                              console.log("enter in else");
                            }
                          }
                        } else {
                          console.log("Addon Not available");
                        }
                      }
                    }
                    Promise.all(promiseArr)
                      .then((promiseArr) => {
                        res.json({ success: true, msg: "Addon Deleted" });
                      })
                      .catch((err) => {
                        res.json({
                          success: false,
                          msg: "error in promise all",
                          error: err,
                        });
                      });
                  })
                  .catch((err) => {
                    return res.json({ success: false, msg: err });
                  });
              })
              .catch((err) => {
                return res.json({ success: false, msg: err });
              });
          })
          .catch((err) => {
            return res.json({ success: false, msg: err });
          });
      })
      .catch((err) => {
        return res.json({ success: false, msg: err });
      });
  } catch (ex) {
    return res.json({ error: ex.message });
  }
};

// Import Charges Addon List
const importChargesAddon = (req, res) => {
  charges_addon_model.collection.insertMany(req.body, function (err, docs) {
    res.json({ sucess: true, msg: "List Imported   " });
  });
};

const updateChargeAddonStatus = (req, res) => {
  charges_addon_model.findOneAndUpdate(
    {
      _id: req.params.id,
      "completeData.country.item_text": req.params.countryname,
    },
    { $set: { "completeData.$.country.status": req.body.status } },
    (err, resp) => {
      // console.log("addonupdate", resp);
      // console.log(
      //   "----------------------------------------------------------------"
      // );
      if (err) {
        res.json({ success: false, msg: err });
      } else if (resp == null) {
        res.json({ success: false, msg: resp });
      } else {
        let addonName = resp.AddonsName;
        charges_product_model.findOneAndUpdate(
          {
            "completeData.available_addons.item_text": addonName,
            "completeData.available_addons.country.country":
              req.params.countryname,
          },
          {
            $set: {
              "completeData.0.available_addons.$.country.status":
                req.body.status,
            },
          },
          // { arrayFilters: [{ "outer.item_text": addonName }, { "inner.country": req.params.countryname }] },
          (err, data) => {
            if (err) {
              res.json({ success: false, message: err });
            } else {
              charges_integration_model.findOneAndUpdate(
                {
                  "addOnsAssociated.addon": addonName,
                  "addOnsAssociated.country": req.params.countryname
                },
                { $set: { "addOnsAssociated.status": req.body.status } },
                (err) => {
                  if (err) {
                    res.json({ success: false, msg: err });
                  } else {
                    res.json({ success: true });
                  }
                }
              );
              //   res.json({ success: true, msg: data });
            }
          }
          // {
          //   "completeData.country.item_text": req.params.countryname,
          // },
          // { $set: { "completeData.$.country.status": req.body.status } },
          // (err, resp) => {
          //   if (err) {
          //     res.json({ success: false, msg: err });
          //   } else if (resp == null) {
          //     res.json({ success: false });
          //   } else {
          //     res.json({ success: true });
          //   }
          // }
        );
        // res.json({ success: true });
      }
    }
  );
};

const getChargesAddonsBtCountry = (req, res) => {
  charges_addon_model.find(
    {
      "completeData.country.item_text": req.params.countryname,
    },
    (err, data) => {
      if (err) {
        res.json({ sucess: false, msg: err });
      } else if (data == null || data.length == 0) {
        res.json({ sucess: false });
      } else {
        res.json({ sucess: true, data: data });
      }
    }
  );
};

module.exports = {
  addChargesAddon,
  getAllChargesAddons,
  getChargesAddon,
  getCompleteDataByName,
  updateChargeAddon,
  deleteChargesAddon,
  importChargesAddon,
  updateChargeAddonStatus,
  getChargesAddonsBtCountry,
};

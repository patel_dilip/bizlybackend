const charges_integration_model = require("../../_bizly_models/_charges_models/_charges_integration_model")
  .charges_integration_model;
const charges_product_model = require("../../_bizly_models/_charges_models/_charges_product_model")
  .charges_product_model;

// Add Charges Integration Data
const addChargesIntegration = (req, res) => {
  const chargesIntegration = new charges_integration_model();
  chargesIntegration.integrationName = req.body.integrationName;
  chargesIntegration.productAssociated = req.body.productAssociated;
  chargesIntegration.addOnsAssociated = req.body.addOnsAssociated;
  chargesIntegration.TypeOfIntegration = req.body.TypeOfIntegration;
  chargesIntegration.description = req.body.description;
  chargesIntegration.images = req.body.images;
  chargesIntegration.videos = req.body.videos;
  chargesIntegration.completeData = req.body.completeData;
  chargesIntegration.addedBy = req.body.userid;
  chargesIntegration.updatedBy = req.body.userid;

  chargesIntegration.save((err, result) => {
    let { completeData } = result;
    let country = completeData.map((ele) => {
      return ele.country.item_text;
    });
    charges_product_model
      .find(
        { productName: result.productAssociated },
        { completeData: 1, _id: 1, productName: 1 }
      )
      .then((productNameData) => {
        let promiseArr = [];
        for (let i = 0; i < productNameData.length; i++) {
          for (let j = 0; j < productNameData[i].completeData.length; j++) {
            if (
              country.includes(
                productNameData[i].completeData[j].country.item_text
              )
            ) {
              promiseArr.push(
                new Promise((resolve, reject) => {
                  charges_product_model
                    .updateOne(
                      {
                        _id: productNameData[i]._id,
                        "completeData.country.item_text":
                          productNameData[i].completeData[j].country.item_text,
                      },
                      {
                        $push: {
                          "completeData.$.available_Integration":
                            req.body.integrationName,
                        },
                      }
                    )
                    .then((udata) => {
                      resolve(udata);
                    })
                    .catch((err) => {
                      reject(err);
                    });
                })
              );
            } else {
              continue;
            }
          }
        }
        Promise.all(promiseArr)
          .then((promiseArr) => {
            res.json({ success: true, msg: "Charges integration added" });
          })
          .catch((err) => {
            res.json({
              success: false,
              msg: "error in promise all ",
              error: err,
            });
          });
      })
      .catch((err) => {
        res.json({
          success: false,
          msg: "error in product name data",
          error: err,
        });
      });
  });
};

//Get All Charges Integrations Data
const getAllChargesIntegrations = (req, res) => {
  charges_integration_model
    .find({})
    
    .populate("addedBy", "userName")
    .populate("updatedBy", "userName")
    .sort("-updatedAt")
    .exec((err, data) => {
      if (err) {
        res.json({ success: false, msg: err });
      } else if (data == null || data.length == 0) {
        res.json({ success: false, msg: "Charges integration not found" });
      } else {
        res.json({
          success: true,
          msg: "Charges integration Found",
          data: data,
        });
      }
    });
};

//Get Charges Integration Data By integrationid
const getChargesIntegration = (req, res) => {
  charges_integration_model
    .findById(req.params.integrationid)
    .populate("addedBy", "userName")
    .populate("updatedBy", "userName")
    .sort("-updatedAt")
    .exec((err, data) => {
      if (err) {
        res.json({ success: false, msg: err });
      } else if (data == null || data.length == 0) {
        res.json({ success: false, msg: "Charges integration not found" });
      } else {
        res.json({
          success: true,
          msg: "Charges integration Found",
          data: data,
        });
      }
    });
};

// get Complete Data by integration name
const getCompleteDataByName = (req, res) => {
  charges_integration_model.find(
    { integrationName: req.params.integrationname },
    { completeData: 1, _id: 1 },
    (err, data) => {
      if (err) {
        res.json({ success: false, msg: err });
      } else if (data == null || data.length == 0) {
        res.json({
          success: false,
          msg: "This Integration Name not available",
        });
      } else {
        res.json({ success: true, msg: "Integration Name Found", data: data });
      }
    }
  );
};

//Update Charges Integration Data
const updateChargeIntegration = async (req, res) => {
  try {
    charges_integration_model
      .findByIdAndUpdate(req.params.integrationid, {
        $set: {
          integrationName: req.body.integrationName,
          productAssociated: req.body.productAssociated,
          addOnsAssociated: req.body.addOnsAssociated,
          TypeOfIntegration: req.body.TypeOfIntegration,
          description: req.body.description,
          images: req.body.images,
          videos: req.body.videos,
          completeData: req.body.completeData,
          updatedBy: req.body.userid,
          updatedAt: Date.now(),
        },
      })
      .then((udata) => {
        promiseArr = [];
        for (let i = 0; i < req.body.completeData.length; i++) {
          charges_product_model
            .find(
              { productName: req.body.productAssociated },
              { completeData: 1, _id: 1 }
            )
            .then((productData) => {
              for (let j = 0; j < productData.length; j++) {
                for (let k = 0; k < productData[j].completeData.length; k++) {
                  if (
                    req.body.completeData[i].country.item_text ==
                    productData[j].completeData[k].country.item_text
                  ) {
                    if (
                      productData[j].completeData[
                        k
                      ].available_Integration.includes(udata.integrationName)
                    ) {
                      console.log("integration name match");
                      continue;
                    } else {
                      promiseArr.push(
                        new Promise((resolve, reject) => {
                          charges_product_model
                            .updateOne(
                              {
                                _id: productData[j]._id,
                                "completeData.country.item_text":
                                  productData[j].completeData[k].country
                                    .item_text,
                              },
                              {
                                $push: {
                                  "completeData.$.available_Integration":
                                    req.body.integrationName,
                                },
                              }
                            )
                            .then((pushed) => {
                              resolve(pushed);
                            })
                            .catch((err) => {
                              reject(err);
                            });
                        })
                      );
                    }
                  } else {
                    console.log("Country not match");
                  }
                }
              }
            })
            .catch((err) => {
              return res.json({
                success: false,
                msg: "Error in integration data",
                error: err,
              });
            });
        }
        Promise.all(promiseArr)
          .then((promiseArr) => {
            res.json({ success: true, msg: "Integration Updated" });
          })
          .catch((err) => {
            return res.json({
              success: false,
              msg: "Error in Promise arr",
              error: err,
            });
          });
      })
      .catch((err) => {
        return res.json({ success: false, msg: "Error in udata", error: err });
      });
  } catch (ex) {
    return res.json({ success: false, error: ex.error });
  }
};

//Delete Charges Integration Data
const deleteChargesIntegration = async (req, res) => {
  try {
    charges_integration_model
      .findByIdAndUpdate(req.params.integrationid, {
        $set: { status: req.body.status },
      })
      .then((integrationData) => {
        let integrationname = integrationData.integrationName;
        charges_product_model
          .find({}, { completeData: 1, _id: 1 })
          .then((productCompleteData) => {
            var promiseArr = [];
            for (let i = 0; i < productCompleteData.length; i++) {
              for (
                let j = 0;
                j < productCompleteData[i].completeData.length;
                j++
              ) {
                if (
                  productCompleteData[i].completeData[j].available_Integration
                    .length != 0
                ) {
                  for (
                    let k = 0;
                    k <
                    productCompleteData[i].completeData[j].available_Integration
                      .length;
                    k++
                  ) {
                    var integrationIndex = productCompleteData[i].completeData[
                      j
                    ].available_Integration.indexOf(integrationname);
                    if (integrationIndex != -1) {
                      promiseArr.push(
                        new Promise((resolve, reject) => {
                          charges_product_model
                            .updateOne(
                              {
                                _id: productCompleteData[i]._id,
                                "completeData.available_Integration": integrationname,
                              },
                              {
                                $pull: {
                                  "completeData.$.available_Integration": integrationname,
                                },
                              }
                            )
                            .then((udata) => {
                              resolve(udata);
                            })
                            .catch((err) => {
                              reject(err);
                            });
                        })
                      );
                    } else {
                      console.log("enter in else");
                    }
                  }
                } else {
                  console.log("Integration Not Available");
                }
              }
            }
            Promise.all(promiseArr)
              .then((promiseArr) => {
                res.json({ success: true, msg: "Integration Deleted" });
              })
              .catch((err) => {
                res.json({
                  success: false,
                  msg: "error in promise all",
                  error: err,
                });
              });
          })
          .catch((err) => {
            return res.json({
              success: false,
              error: err,
              msg: "error in int complete data",
            });
          });
      })
      .catch((err) => {
        return res.json({
          success: false,
          error: err,
          msg: "error in integration data",
        });
      });
  } catch (ex) {
    return res.json({ error: ex.message });
  }
};

// Import Charges Integration List
const importChargesIntegration = (req, res) => {
  charges_integration_model.collection.insertMany(req.body, function (
    err,
    docs
  ) {
    res.json({ sucess: true, msg: "List Imported   " });
  });
};

const updateIntegrationStatus = (req, res) => {
  charges_integration_model.findOneAndUpdate(
    {
      _id: req.params.id,
      "completeData.country.item_text": req.params.countryname,
    },
    { $set: { "completeData.$.country.status": req.body.status } },
    (err, resp) => {
      if (err) {
        res.json({ success: false, msg: err });
      } else if (resp == null) {
        res.json({ success: false, msg: resp });
      } else {
        let integrationName = resp.integrationName;
        // console.log(req.params.countryname, integrationName);
        charges_product_model.findOneAndUpdate(
          {
            "completeData.available_Integration.item_text": integrationName,
            "completeData.available_Integration.country.country":
              req.params.countryname,
          },
          {
            $set: {
              "completeData.0.available_Integration.$.country.status":
                req.body.status,
            },
          },
          // { arrayFilters: [{ "outer.item_text": integrationName }, { "inner.country": req.params.countryname }] },
          (err, data) => {
            if (err) {
              res.json({ success: false, message: err });
            } else {
              res.json({ success: true });
            }
          }
        );
      }
    }
  );
};

const getChargesIntegrationByCountryName = (req, res) => {
  charges_integration_model.find(
    {
      "completeData.country.item_text":
        req.params.countryname,
    },
    (err, data) => {
      if (err) {
        res.json({ sucess: false, msg: err });
      } else if (data == null || data.length == 0) {
        res.json({ sucess: false });
      } else {
        res.json({ sucess: true, data: data });
      }
    }
  );
};
module.exports = {
  addChargesIntegration,
  getAllChargesIntegrations,
  getChargesIntegration,
  getCompleteDataByName,
  updateChargeIntegration,
  deleteChargesIntegration,
  importChargesIntegration,
  updateIntegrationStatus,
  getChargesIntegrationByCountryName,
};

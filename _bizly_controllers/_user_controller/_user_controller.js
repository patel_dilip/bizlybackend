const userModel = require("../../_bizly_models/_user_model/_user_model")
  .userModel;

const addUser = (req, res) => {
  var user = new userModel(req.body);
  user.created_by = req.body.userid;
  user.updatedBy = req.body.userid;
  user.save((err) => {
    if (err) {
      res.json({ success: false, msg: err });
    } else {
      res.json({ success: true });
    }
  });
};

const getUser = (req, res) => {
  userModel
    .findOne({ _id: req.params.userid })
    .populate("personal_details.role")
    .populate("created_by", "userName")
    .populate("updatedBy", "userName")
    .exec((err, user) => {
      if (err) {
        res.json({ success: false, msg: err });
      } else if (user == null) {
        res.json({ success: false });
      } else {
        res.json({ success: true, data: user });
      }
    });
};

const getAllUsers = (req, res) => {
  userModel
    .find()
    .populate("personal_details.role", "role_name")
    .populate("created_by", "userName")
    .populate("updatedBy", "userName")
    .exec((err, users) => {
      if (err) {
        res.json({ success: false, msg: err });
      } else if (users == null) {
        res.json({ success: false });
      } else {
        res.json({ success: true, data: users });
      }
    });
};

const updateUser = (req, res) => {
  userModel.findOneAndUpdate(
    { _id: req.params.userid },
    req.body,
    (err, user) => {
      if (err) {
        res.json({ success: false, msg: err });
      } else {
        res.json({ success: true });
      }
    }
  );
};

const softDeleteUser = (req, res) => {
  userModel.findOneAndUpdate(
    { _id: req.params.userid },
    { $set: { status: req.body.status } },
    (err, data) => {
      if (err) {
        res.json({ success: false, msg: err });
      } else {
        res.json({ success: true });
      }
    }
  );
};

const deleteUser = (req, res) => {
  userModel.deleteOne({ _id: req.params.userid }, (err, data) => {
    if (err) {
      res.json({ success: false, msg: err });
    } else if (data == null) {
      res.json({ success: false });
    } else {
      res.json({ success: true });
    }
  });
};

const deleteAllUsers = (req, res) => {
  userModel.deleteMany((err, data) => {
    if (err) {
      res.json({ success: false, msg: err });
    } else if (data == null) {
      res.json({ success: false });
    } else {
      res.json({ success: true });
    }
  });
};

module.exports = {
  addUser,
  getUser,
  getAllUsers,
  updateUser,
  softDeleteUser,
  deleteUser,
  deleteAllUsers,
};

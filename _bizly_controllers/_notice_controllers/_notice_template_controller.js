const noticetempalateModel = require('../../_bizly_models/_notice_models/_notice_template_model').noticetemplate

//Add Notice Template Data
const addNoticeTemplate = (req, res) => {
    var template = new noticetempalateModel()
    template.template_id = req.body.template_id
    template.template_name = req.body.template_name
    template.file = req.body.file
    template.select_folder = req.body.select_folder
    template.subject = req.body.subject
    template.template_description = req.body.template_description
    template.message = req.body.message
    template.available_for_template = req.body.available_for_template
    template.addedBy = req.body.userid
    template.updatedBy = req.body.userid

    template.save((err) => {
        if (err) {
            res.json({ success: false, msg: err })
        } else {
            res.json({ success: true, msg: "Notice Template Added" })
        }
    })
}

//Get All Notice Templates Data
const getAllTemplates = (req, res) => {
    noticetempalateModel.find().
        populate('addedBy', 'userName').
        populate('updatedBy', 'userName').
        exec((err, result) => {
            if (err) {
                res.json({ success: false, msg: err })
            } else if (result == null || result.length == 0) {
                res.json({ success: false, msg: "Notice Template Not Found" })
            } else {
                res.json({ success: true, msg: "Notice Template Found", data: result })
            }
        })
}

//Get Notice Template By noticetemplateid
const getTemplateByID = (req, res) => {
    noticetempalateModel.findById(req.params.noticetemplateid).
        populate('addedBy', 'userName').
        populate('updatedBy', 'userName').
        exec((err, result) => {
            if (err) {
                res.json({ success: false, msg: err })
            } else if (result == null || result.length == 0) {
                res.json({ success: false, msg: "Notice Template Not Found" })
            } else {
                res.json({ success: true, msg: "Notice Template Found", data: result })
            }
        })
}

//update Notice Template Data
const updateNoticeTemplate = (req, res) => {
    noticetempalateModel.findByIdAndUpdate(req.params.noticetemplateid,
        {
            $set: {
                template_id: req.body.template_id,
                template_name: req.body.template_name,
                file: req.body.file,
                select_folder: req.body.select_folder,
                subject: req.body.subject,
                template_description: req.body.template_description,
                message: req.body.message,
                available_for_template: req.body.available_for_template,
                updatedBy: req.body.userid,
                updatedAt: Date.now()
            }
        }, (err, udata) => {
            if (err) {
                res.json({ success: false, msg: err })
            } else if (udata == null || udata.length == 0) {
                res.json({ success: false, msg: "Notice Template Not Found" })
            } else {
                res.json({ success: true, msg: "Notice Template Update" })
            }
        })
}

//Soft Delete Notice Template data
const deleteNoticeTemplate = (req, res) => {
    noticetempalateModel.findByIdAndUpdate(req.params.noticetemplateid,
        {
            $set: {
                status: req.body.status
            }
        }, (err, udata) => {
            if (err) {
                res.json({ success: false, msg: err })
            } else {
                res.json({ success: true, msg: "Notice Template Deleted" })
            }
        })
}

module.exports = {
    addNoticeTemplate,
    getAllTemplates,
    getTemplateByID,
    updateNoticeTemplate,
    deleteNoticeTemplate
}
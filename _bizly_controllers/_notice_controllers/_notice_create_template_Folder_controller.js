const notice_createTemplateFolder_model = require('../../_bizly_models/_notice_models/_notice_create_Template_Folders_model').folder
const fs = require('fs')

// Create notice Template Folder
const noticeCreateTemplateFolder = (req, res) => {
    console.log("req.body", req.body)
    var dir = "/home/bizlypos/public_html/uploads/notice_template_folders" + "/" + req.body.foldername;
    if (!fs.existsSync(dir)) {
        fs.mkdirSync(dir, 0o776)
        notice_createTemplateFolder_model.findOne({ userid: req.body.userid }, (err, data) => {
            if (err) {
                res.json({ success: false, msg: err })
            } else if (data) {
                console.log("data", data)
                notice_createTemplateFolder_model.updateOne({ userid: req.body.userid }, { $push: { createdfolders: req.body.foldername } }, (err, udata) => {
                    if (err) {
                        res.json({ success: false, msg: err })
                    } else if (udata == null || udata.length == 0) {
                        res.json({ success: false, msg: "Notice Template folder not update" })
                    } else {
                        res.json({ success: true, msg: "Template Folder name pushed" })
                    }
                })
            } else {
                var createfolder = new notice_createTemplateFolder_model()
                createfolder.userid = req.body.userid
                createfolder.createdfolders = req.body.foldername
                createfolder.addedBy = req.body.userid
                createfolder.updatedBy = req.body.userid

                createfolder.save((err) => {
                    if (err) {
                        res.json({ success: false, msg: err })
                    } else {
                        res.json({ success: true, msg: req.body.foldername + " Folder Created" })
                    }
                })
            }
        })
    } else {
        res.json({ success: false, msg: req.body.foldername + " this folder already exists" })
    }
}

//get Notice Created Template folders
const getNoticeCreateTemplateFolder = (req, res) => {
    notice_createTemplateFolder_model.findOne({ userid: req.params.userid }).
        populate('addedby', 'userName').
        populate('updatedBy', 'userName').
        exec((err, data) => {
            if (err) {
                res.json({ success: false, msg: err })
            } else if (data == null || data.length == 0) {
                res.json({ success: false, msg: "Template Folder not found" })
            } else {
                res.json({ success: true, msg: "Created Template folders found", data: data })
            }
        })
}

module.exports = {
    noticeCreateTemplateFolder,
    getNoticeCreateTemplateFolder
}
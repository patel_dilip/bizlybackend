const noticegroupModel = require('../../_bizly_models/_notice_models/_notice_group_model').notice_group

//Add Notice Group Data
const addNoticeGroup = (req, res) => {
    var group = new noticegroupModel()
    group.group_For = req.body.group_For
    group.group_Name = req.body.group_Name
    group.group_member_list = req.body.group_member_list
    group.addedBy = req.body.userid
    group.updatedBy = req.body.userid

    group.save((err) => {
        if (err) {
            res.json({ success: false, msg: err })
        } else {
            res.json({ success: true, msg: "Notice Group Added" })
        }
    })
}

//Get All Notice Group Data
const getAllNoticeGroups = (req, res) => {
    noticegroupModel.find().
        populate('addedBy', 'userName').
        populate('updatedBy', 'userName').
        exec((err, result) => {
            if (err) {
                res.json({ success: false, msg: err })
            } else if (result == null || result.length == 0) {
                res.json({ success: false, msg: "Notice Group Data Not Found" })
            } else {
                res.json({ success: true, msg: "Notice Group Data Found", data: result })
            }
        })
}

//Get Notice Group Data By noticegroupid
const getNoticeGroupByID = (req, res) => {
    noticegroupModel.findById(req.params.noticegroupid).
        populate('addedBy', 'userName').
        populate('updatedBy', 'userName').
        exec((err, result) => {
            if (err) {
                res.json({ success: false, msg: err })
            } else if (result == null || result.length == 0) {
                res.json({ success: false, msg: "Notice Group Data Not Found" })
            } else {
                res.json({ success: true, msg: "Notice Group Data Found", data: result })
            }
        })
}

//Update Notice Group Data
const updateNoticeGroup = (req, res) => {
    noticegroupModel.findByIdAndUpdate(req.params.noticegroupid,
        {
            $set: {
                group_For: req.body.group_For,
                group_Name: req.body.group_Name,
                group_member_list: req.body.group_member_list,
                updatedBy: req.body.userid,
                updatedAt: Date.now()
            }
        }, (err, udata) => {
            if (err) {
                res.json({ success: false, msg: err })
            } else if (udata == null || udata.length == 0) {
                res.json({ success: false, msg: "notice group data not found" })
            } else {
                res.json({ success: true, msg: "Notice Group updated" })
            }
        })
}

//Soft Delete Notice Group Data
const deleteNoticeGroup = (req, res) => {
    noticegroupModel.findByIdAndUpdate(req.params.noticegroupid,
        {
            $set: {
                status: req.body.status,
            }
        }, (err, data) => {
            if (err) {
                res.json({ success: false, msg: err })
            } else {
                res.json({ success: true, msg: "Notice Group Deleted" })
            }
        })
}

module.exports = {
    addNoticeGroup,
    getAllNoticeGroups,
    getNoticeGroupByID,
    updateNoticeGroup,
    deleteNoticeGroup
}
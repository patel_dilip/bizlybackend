const noticeModel = require('../../_bizly_models/_notice_models/_notice_model').notice
const fs = require('fs')

//Add Notice Data
const addNotice = (req, res) => {
    var notice = new noticeModel()
    notice.template_id = req.body.template_id
    notice.template_name = req.body.template_name
    notice.file = req.body.file
    notice.select_folder = req.body.select_folder
    notice.subject = req.body.subject
    notice.template_description = req.body.template_description
    notice.message = req.body.message
    notice.available_for_template = req.body.available_for_template
    notice.mode = req.body.mode
    notice.addedBy = req.body.userid
    notice.updatedBy = req.body.userid

    notice.save((err) => {
        if (err) {
            res.json({ success: false, msg: err })
        } else {
            res.json({ success: true, msg: "Notice Added" })
        }
    })
}

//Get All Notice Data
const getAllNotices = (req, res) => {
    noticeModel.find().
        populate('addedBy', 'userName').
        populate('updatedBy', 'userName').
        exec((err, result) => {
            if (err) {
                res.json({ success: false, msg: err })
            } else if (result == null || result.length == 0) {
                res.json({ success: false, msg: "Notice Not Found" })
            } else {
                res.json({ success: true, msg: "Notice Found", data: result })
            }
        })
}

//get Notice Data By noticeid
const getNoticeByID = (req, res) => {
    noticeModel.findById(req.params.noticeid).
        populate('addedBy', 'userName').
        populate('updatedBy', 'userName').
        exec((err, result) => {
            if (err) {
                res.json({ success: false, msg: err })
            } else if (result == null || result.length == 0) {
                res.json({ success: false, msg: "Notice Not Found" })
            } else {
                res.json({ success: true, msg: "Notice Found", data: result })
            }
        })
}

//Update Notice Data
const updateNotice = (req, res) => {
    noticeModel.findByIdAndUpdate(req.params.noticeid,
        {
            $set: {
                template_id: req.body.template_id,
                template_name: req.body.template_name,
                file: req.body.file,
                select_folder: req.body.select_folder,
                subject: req.body.subject,
                template_description: req.body.template_description,
                message: req.body.message,
                available_for_template: req.body.available_for_template,
                mode: req.body.mode,
                updatedBy: req.body.userid,
                updatedAt: Date.now()
            }
        }, (err, udata) => {
            if (err) {
                res.json({ success: false, msg: err })
            } else if (udata == null || udata.length == 0) {
                res.json({ success: false, msg: "notice not found" })
            } else {
                res.json({ success: true, msg: "Notice Updated" })
            }
        })
}

//Soft delete Notice Data
const deleteNotice = (req, res) => {
    noticeModel.findByIdAndUpdate(req.params.noticeid, { $set: { status: req.body.status } }, (err, udata) => {
        if (err) {
            res.json({ success: false, msg: err })
        } else if (udata == null || udata.length == 0) {
            res.json({ success: false, msg: 'not updated' })
        } else {
            res.json({ success: true, msg: "notice deleted" })
        }
    })
}

const createFolder = (req, res) => {
    var dir = "/home/bizlypos/public_html/uploads/notice_folders" + "/" + req.body.foldername;
    if (!fs.existsSync(dir)) {
        fs.mkdirSync(dir, 0o776)
        res.json({ success: true, msg: req.body.foldername + " Folder Created" })

    } else {
        res.json({ success: false, msg: req.body.foldername + " this folder already exists" })
    }

}

module.exports = {
    addNotice,
    getAllNotices,
    getNoticeByID,
    updateNotice,
    deleteNotice,
    createFolder
}
const notice_createCircularFolder_model = require('../../_bizly_models/_notice_models/_notice_create_circular_folder_model').folder
const fs = require('fs')

// Create notice Circular Folder
const noticeCreateCircularFolder = (req, res) => {
    console.log("req.body", req.body)
    var dir = "/home/bizlypos/public_html/uploads/notice_circular_folders" + "/" + req.body.foldername;
    if (!fs.existsSync(dir)) {
        fs.mkdirSync(dir, 0o776)
        notice_createCircularFolder_model.findOne({ userid: req.body.userid }, (err, data) => {
            if (err) {
                res.json({ success: false, msg: err })
            } else if (data) {
                console.log("data", data)
                notice_createCircularFolder_model.updateOne({ userid: req.body.userid }, { $push: { createdfolders: req.body.foldername } }, (err, udata) => {
                    if (err) {
                        res.json({ success: false, msg: err })
                    } else if (udata == null || udata.length == 0) {
                        res.json({ success: false, msg: "Notice Circular folder not update" })
                    } else {
                        res.json({ success: true, msg: "Circular Folder name pushed" })
                    }
                })
            } else {
                var createfolder = new notice_createCircularFolder_model()
                createfolder.userid = req.body.userid
                createfolder.createdfolders = req.body.foldername
                createfolder.addedBy = req.body.userid
                createfolder.updatedBy = req.body.userid

                createfolder.save((err) => {
                    if (err) {
                        res.json({ success: false, msg: err })
                    } else {
                        res.json({ success: true, msg: req.body.foldername + " Folder Created" })
                    }
                })
            }
        })
    } else {
        res.json({ success: false, msg: req.body.foldername + " this folder already exists" })
    }
}

//get Notice Created Circular folders
const getNoticeCreateCircularFolder = (req, res) => {
    notice_createCircularFolder_model.findOne({ userid: req.params.userid }).
        populate('addedby', 'userName').
        populate('updatedBy', 'userName').
        exec((err, data) => {
            if (err) {
                res.json({ success: false, msg: err })
            } else if (data == null || data.length == 0) {
                res.json({ success: false, msg: "Circular Folder not found" })
            } else {
                res.json({ success: true, msg: "Created Circular folders found", data: data })
            }
        })
}

module.exports = {
    noticeCreateCircularFolder,
    getNoticeCreateCircularFolder
}
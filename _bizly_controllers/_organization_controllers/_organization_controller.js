const organizationModel = require('../../_bizly_models/_organization_model/_organization_model').organizartion
const restaurantModel = require('../../_bizly_models/_restaurant_models/_restaurant-model').restomodel

//Add Organization
const addOrganization = (req, res) => {
    var organization = new organizationModel()
    organization.organizationCode = req.body.organizationCode
    organization.organization_name = req.body.organization_name
    organization.representative_name = req.body.representative_name
    organization.org_email = req.body.org_email
    organization.org_contact = req.body.org_contact
    organization.outlets_arr = req.body.outlets_arr
    organization.addedBy = req.body.userid
    organization.updatedBy = req.body.userid

    organization.save((err, result) => {
        if (err) {
            res.json({ success: false, msg: err })
        } else {
            res.json({ success: true, msg: "Organization added", data: result._id })
        }
    })
}

//Add Organization Outles
const addOrganizationOutlets = (req, res) => {
    organizationModel.findByIdAndUpdate(req.params.organizationid,
        {
            $push: {
                outlets_arr: req.body.outletsid
            }
        }, (err, udata) => {
            if (err) {
                res.json({ success: false, msg: err })
            } else if (udata == null) {
                res.json({ success: false, msg: "Organization Not found" })
            } else {
                res.json({ success: true, msg: "Outlets added" })
            }
        })
}

//get All Organizations
const getAllOrganizations = (req, res) => {
    organizationModel.find({}).
        populate('outlets_arr').
        populate('addedBy', 'userName').
        populate('updatedBy', 'userName').
        exec((err, data) => {
            if (err) {
                res.json({ success: false, msg: err })
            } else if (data == null || data.length == 0) {
                res.json({ success: false, msg: "Organization Not Found", data: [] })
            } else {
                res.json({ success: true, msg: "Organization found", data: data })
            }
        })
}

//get Organization By organizationid
const getAllOrganizationById = (req, res) => {
    organizationModel.findOne({ _id: req.params.organizationid }).
        populate('outlets_arr').
        populate('addedBy', 'userName').
        populate('updatedBy', 'userName').
        exec((err, data) => {
            if (err) {
                res.json({ success: false, msg: err })
            } else if (data == null || data.length == 0) {
                res.json({ success: false, msg: "Organization Not Found", data: [] })
            } else {
                res.json({ success: true, msg: "Organization found", data: data })
            }
        })
}

//update Organization Data
const updateOrganization = (req, res) => {
    organizationModel.findByIdAndUpdate(req.params.organizationid, {
        $set: {
            organization_name: req.body.organization_name,
            representative_name: req.body.representative_name,
            org_email: req.body.org_email,
            org_contact: req.body.org_contact,
            outlets_arr: req.body.outlets_arr,
            updatedBy: req.body.userid,
            updatedAt: Date.now()
        }
    }, (err, udata) => {
        if (err) {
            res.json({ success: false, msg: err })
        } else if (udata == null || udata.length == 0) {
            res.json({ success: false, msg: "Not updated" })
        } else {
            res.json({ success: true, msg: "Organization Updated" })
        }
    })
}

//soft delete Organization Data
const softDeleteOrganization = (req, res) => {
    organizationModel.findByIdAndUpdate(req.params.organizationid, {
        $set: {
            status: req.body.status
        }
    }, (err, udata) => {
        if (err) {
            res.json({ success: false, msg: err })
        } else if (udata == null || udata.length == 0) {
            res.json({ success: false, msg: "Not updated" })
        } else {
            res.json({ success: true, msg: "Organization soft Deleted" })
        }
    })
}

//delete Organization
const deleteOrganization = (req, res) => {
    organizationModel.findOne({ _id: req.params.organizationid })
        .then((organizationData) => {
            promiseArr = []
            for (let i = 0; i < organizationData.outlets_arr.length; i++) {
                promiseArr.push(new Promise((resolve, reject) => {
                    restaurantModel.findOneAndDelete({ _id: organizationData.outlets_arr[i]._id })
                        .then((Rudata) => {
                            resolve(Rudata)
                        }).catch((err) => {
                            reject(err)
                        })
                }))
            }
            Promise.all(promiseArr).then((promiseArr) => {
                organizationModel.findByIdAndDelete({ _id: req.params.organizationid })
                    .then((data) => {
                        res.json({ success: true, msg: "Organization Deleted" })
                    }).catch((err) => {
                        res.json({ success: false, msg: "error in data", error: err })
                    })
            }).catch((err) => {
                res.json({ success: false, msg: "error in promise arr", error: err })
            })
        }).catch((err) => {
            res.json({ success: false, msg: "error in organization data", error: err })
        })
}

//delete Organization
const deleteAllOrganization = (req, res) => {
    organizationModel.find()
        .then((organizationData) => {
            promiseArr = []
            for (let i = 0; i < organizationData.length; i++) {
                if (organizationData[i].outlets_arr.length > 0) {
                    for (let j = 0; j < organizationData[i].outlets_arr.length; j++) {
                        promiseArr.push(new Promise((resolve, reject) => {
                            restaurantModel.remove({ _id: organizationData[i].outlets_arr[j]._id })
                                .then((Rudata) => {
                                    resolve(Rudata)
                                }).catch((err) => {
                                    reject(err)
                                })
                        }))
                    }
                } else {
                    continue
                }
            }
            Promise.all(promiseArr).then((promiseArr) => {
                organizationModel.remove({})
                    .then((data) => {
                        res.json({ success: true, msg: "Organization Deleted" })
                    }).catch((err) => {
                        res.json({ success: false, msg: "error in data", error: err })
                    })
            }).catch((err) => {
                res.json({ success: false, msg: "error in promise arr", error: err })
            })
        }).catch((err) => {
            res.json({ success: false, msg: "error in organization data", error: err })
        })
}
module.exports = {
    addOrganization,
    addOrganizationOutlets,
    getAllOrganizations,
    getAllOrganizationById,
    updateOrganization,
    softDeleteOrganization,
    deleteOrganization,
    deleteAllOrganization
}